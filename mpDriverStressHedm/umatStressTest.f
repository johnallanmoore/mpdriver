C
C     UMAT
C      
      SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD,
     1     RPL,DDSDDT,DRPLDE,DRPLDT,
     2     STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED, CMNAME,
     3     NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT,
     4     CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,KSTEP,KINC)
C     
C       IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I, J, NDI,NSHR,NTENS,NSTATV,NPROPS, K
      INTEGER NOEL,NPT,LAYER,KSPT,KSTEP,KINC
      INTEGER NSTATEV_TRANS, ISYS, ITER, ITERMAX, IACTIVE
      INTEGER NTRANS, NSLIP, NPROPS_TRANS, ICP, NACTIVE, ITRANS
      INTEGER ISYS_A, ISYS_B

      REAL*8 STRESS,STATEV,DDSDDE,SSE,SPD,SCD,
     1     RPL,DDSDDT,DRPLDE,DRPLDT,
     2     STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,
     3     PROPS,COORDS,DROT,PNEWDT,
     4     CELENT,DFGRD0,DFGRD1

      CHARACTER*80 CMNAME
      DIMENSION STRESS(NTENS),STATEV(NSTATV),
     1     DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS),
     2     STRAN(NTENS),DSTRAN(NTENS),TIME(2),PREDEF(1),DPRED(1),
     3     PROPS(NPROPS),COORDS(3),DROT(3,3),
     4     DFGRD0(3,3),DFGRD1(3,3)

      REAL*8 SIJ(6,6), FE_TAU_0(3,3), FE_INV_TAU_0(3,3), DET_FE_TAU_0
      REAL*8 FET_TAU_0(3,3), DUMMY, FET_INV_TAU_0(3,3), DFGRD0T(3,3)
      REAL*8 CTOTAL(3,3), ETOTAL2D(3,3), ETOTAL1D(6)

      REAL*8 TRANSRESIS, ALPHAM, ALPHAA, C44M, HEAT_LATENT
      REAL*8 TEHTA_REF_LOW, THETA_TRANS, GAMMA0, THETA_REF
      REAL*8 THETA_REF_HIGH, THETA_REF_LOW, CM, CB, TR, PTCON
      REAL*8 GM, GB, AMATA, DDM_GG_2D, DDA_GG_2D, DDM_GG, AM
      REAL*8 GDOT0, HINT, AMATM, QL, AHARD, SS, H0, GBSLIP
      REAL*8 CMSLIP, PROPST, TOT_ZETA_TAU, S0ALPHASLIP, GMSLIP
      REAL*8 CBSLIP, TIME_HT, DELTAGAMMA_TRIAL, EQPLASTICOLD
      REAL*8 NSTATEVOFFSET, ITIMETEMP1, ITIMETEMP2, TIME_RT
      REAL*8 GAMMANORM, GAMMATOL, ZETATOL, FTRANSOLD, FP_LP
      REAL*8 SLIPLP, ZETANORM, DDEQ_T, FTRANSNEW, CAUCHY2D
      REAL*8 DGAMMAPT_DTSTAR, C_ALPHA_1D, DELTA_GAMMAPT, ZETA_TAU
      REAL*8 FTR_LTR, TRLP, FDRIVE_TRANS, DELTAGAMMA
      REAL*8 FTRANSFORMATIONNEW, FPOLD, TPK1D_TAU, IERRORCP
      REAL*8 DGAMMA_DTSTAR, C_ALPHA_1DSLIP, SHARD_TAU
      REAL*8 ERRORDGAMMA, FPLASTICNEW, FP_LP_POST, FPEFFNEW
      REAL*8 ERRORZETA, ZETA_TRIAL, TAU, FET_TAU, FE_TAU
      REAL*8 DETFPEFFNEW, FPEFFNEWINV, DELTAIJ, CPLASTIC
      REAL*8 FPLASTICNEWT, CE, TPK2D_TAU, ESTRAIN1D, EPLASTIC2D
      REAL*8 ESTRAIN2D, DDEQ_T4D, DET_FE_TAU, FE_INV_TAU, TOTALGAMMA0
      REAL*8 DELTA_GAMMACP, TPK2D_FET_TAU, SHARD_TRIAL, FORCEINT
      REAL*8 DDA_GG, DDM_4D, TOTALGAMMA, DDA_4D, DDA_2D, DDM_2D
      REAL*8 S0ALPHA, C12M, C11M, C11A, C12A, C44A, SHARD_T
      REAL*8 S0, DELTAGAMMA_T, TOT_ZETA_T, ZETA_T
      REAL*8 FPLASTICOLD, FTRANSFORMATIONOLD, FPEFFOLD, EULER
      REAL*8 CINV, TOT_ZETA_T_0, GARG, QLAT
      
      REAL*8 TAUOVERG, SHARD_0,SHARDDOT, HBETA, HAB

      PARAMETER(NSLIP  = 12)
      PARAMETER(NTRANS = 24)

      REAL*8 RES(NSLIP), RESMAG, DRDG(NSLIP),DGDOTDGAMMA(NSLIP)
      REAL*8 DRES(NSLIP), GETSIGN(NSLIP)

      DIMENSION CB(3,NTRANS),CM(3,NTRANS),S0ALPHA(3,3,NTRANS),
     &     EULER(3),TR(3,3),GB(3,NTRANS),GM(3,NTRANS),
     &     DDA_2D(6,6),DDM_2D(6,6),DDA_4D(3,3,3,3),DDM_4D(3,3,3,3),
     &     DDA_GG(3,3,3,3),DDM_GG(3,3,3,3),
     &     DDA_GG_2D(6,6),DDM_GG_2D(6,6),
     &     AMATA(6),AMATM(6),
     &     FPEFFOLD(3,3),FPEFFNEW(3,3),
     &     ZETA_T(NTRANS),ZETA_TAU(NTRANS),
     &     CAUCHY2D(3,3),FTRANSNEW(3,3),DDEQ_T(6,6),
     &     DELTA_GAMMAPT(NTRANS),C_ALPHA_1D(6,NTRANS),
     &     DGAMMAPT_DTSTAR(3,3,NTRANS),TPK1D_TAU(6),
     &     DDEQ_T4D(3,3,3,3),FDRIVE_TRANS(NTRANS),
     &     CINV(6,6)

C PLASTICITY
      DIMENSION    CBSLIP(3,NSLIP),CMSLIP(3,NSLIP),
     &     GBSLIP(3,NSLIP),GMSLIP(3,NSLIP),
     &     S0ALPHASLIP(3,3,NSLIP),
     &     DELTAGAMMA_T(NSLIP),DELTAGAMMA_TRIAL(NSLIP),
     &     DELTAGAMMA(NSLIP),
     &     SHARD_T(NSLIP),SHARD_TRIAL(NSLIP),SHARD_TAU(NSLIP),
     &     SLIPLP(3,3),FP_LP(3,3),FTRANSOLD(3,3),TRLP(3,3),
     &     FPOLD(3,3),FTR_LTR(3,3),
     &     ERRORDGAMMA(NSLIP),ZETA_TRIAL(NTRANS),ERRORZETA(NTRANS),
     &     FPEFFNEWINV(3,3),FE_TAU(3,3),FET_TAU(3,3),CE(3,3),
     &     ESTRAIN2D(3,3),ESTRAIN1D(6),
     &     TPK2D_TAU(3,3),TPK2D_FET_TAU(3,3),
     &     FE_INV_TAU(3,3),
     &     C_ALPHA_1DSLIP(6,NSLIP),
     &     DGAMMA_DTSTAR(3,3,NSLIP),DELTA_GAMMACP(NSLIP),TAU(NSLIP),
     &     SHARD_0(NSLIP), SHARDDOT(NSLIP), HAB(NSLIP,NSLIP),
     &     HBETA(NSLIP),QLAT(NSLIP,NSLIP)

C     INTERACTION
      DIMENSION HINT(NTRANS,NTRANS),FORCEINT(NTRANS)

      DIMENSION PROPST(10)

      DIMENSION PTCON(7)

      DIMENSION     FPLASTICOLD(3,3),FPLASTICNEW(3,3),
     &     FTRANSFORMATIONOLD(3,3),
     &     FTRANSFORMATIONNEW(3,3),
     &     FPLASTICNEWT(3,3),CPLASTIC(3,3),EPLASTIC2D(3,3), 
     &     FP_LP_POST(3,3)


      NPROPS_TRANS=13

C     unpack needed state/history variables

      FE_TAU_0(1,1) = STATEV(1)
      FE_TAU_0(1,2) = STATEV(2)
      FE_TAU_0(1,3) = STATEV(3)
      FE_TAU_0(2,1) = STATEV(4)
      FE_TAU_0(2,2) = STATEV(5)
      FE_TAU_0(2,3) = STATEV(6)
      FE_TAU_0(3,1) = STATEV(7)
      FE_TAU_0(3,2) = STATEV(8)
      FE_TAU_0(3,3) = STATEV(9)

      TOT_ZETA_T_0 = STATEV(10)      



C    inital values 
      IF (KSTEP.EQ.1) THEN
         IF (KINC.EQ.1) THEN
            DO I=1,3
               DO J=1,3
                  IF (I.EQ.J) THEN
                     FPEFFOLD(I,J)= 1.D0
                     FPLASTICOLD(I,J)= 1.D0
                     FTRANSFORMATIONOLD(I,J) = 1.D0
                     FE_TAU_0(I,J) = 1.D0
                  ELSE
                     FPEFFOLD(I,J)= 0.D0
                     FPLASTICOLD(I,J)= 0.D0
                     FTRANSFORMATIONOLD(I,J) = 0.D0
                     FE_TAU_0(I,J) = 0.D0
                  ENDIF
               ENDDO
            ENDDO
         ELSE
            FPEFFOLD(1,1) = STATEV(101)
            FPEFFOLD(1,2) = STATEV(102)
            FPEFFOLD(1,3) = STATEV(103)
            FPEFFOLD(2,1) = STATEV(104)
            FPEFFOLD(2,2) = STATEV(105)
            FPEFFOLD(2,3) = STATEV(106)
            FPEFFOLD(3,1) = STATEV(107)
            FPEFFOLD(3,2) = STATEV(108)
            FPEFFOLD(3,3) = STATEV(109)

            FPLASTICOLD(1,1) = STATEV(109+2*NSLIP+1)
            FPLASTICOLD(1,2) = STATEV(109+2*NSLIP+2)
            FPLASTICOLD(1,3) = STATEV(109+2*NSLIP+3)
            FPLASTICOLD(2,1) = STATEV(109+2*NSLIP+4)
            FPLASTICOLD(2,2) = STATEV(109+2*NSLIP+5)
            FPLASTICOLD(2,3) = STATEV(109+2*NSLIP+6)
            FPLASTICOLD(3,1) = STATEV(109+2*NSLIP+7)
            FPLASTICOLD(3,2) = STATEV(109+2*NSLIP+8)
            FPLASTICOLD(3,3) = STATEV(109+2*NSLIP+9)

         ENDIF
      ENDIF


C     Extract Properties
      C11A=PROPS(1)
      C12A=PROPS(2)
      C44A=PROPS(3)
      C11M=PROPS(4)
      C12M=PROPS(5)
      C44M=PROPS(6)
      ALPHAA=PROPS(7)
      ALPHAM=PROPS(8)
      TRANSRESIS=PROPS(9)
      GAMMA0=PROPS(10)
      THETA_TRANS=PROPS(11)
      THETA_REF_LOW=PROPS(12)
      HEAT_LATENT=PROPS(13)    
      GDOT0 = PROPS(17)
      AM    = PROPS(18)     
      S0    = PROPS(19)  
      H0    = PROPS(20)  
      SS    = PROPS(21)  
      AHARD = PROPS(22)  
      QL    = PROPS(23)
      THETA_REF_HIGH=PROPS(24)


      ICP = 1
      IF(ICP.EQ.1)THEN
         DO ISYS=1,NSLIP
            IF (KSTEP.EQ.1) THEN

C              IF (abs(STATEV(109+NSLIP+ISYS)).LT.1.D-12) THEN
               IF (KINC.EQ.1) THEN
                  SHARD_0(ISYS) = S0
               ELSE
                  SHARD_0(ISYS) = STATEV(109+NSLIP+ISYS)
               ENDIF
            ELSE
               SHARD_0(ISYS) = STATEV(109+NSLIP+ISYS)
            ENDIF
         ENDDO
      ENDIF


C     read euler angles from prop definition
      EULER(1) = PROPS(25)
      EULER(2) = PROPS(26)
      EULER(3) = PROPS(27) 

C     Determine the PK2 stress from previous Fe
C     S = J * F^-1 * sigma * F^-T
      CALL MATINV3(FE_TAU_0,FE_INV_TAU_0,DET_FE_TAU_0)

      CALL TRANS(FE_TAU_0,FET_TAU_0)
      CALL TR1TO2(STRESS,CAUCHY2D)
      CALL MATINV3(FET_TAU_0,FET_INV_TAU_0,DUMMY)

      TPK2D_TAU = matmul(CAUCHY2D,FET_INV_TAU_0)
      TPK2D_TAU = matmul(FE_INV_TAU_0,TPK2D_TAU)
      DO I=1,3
         DO J=1,3
            TPK2D_TAU(I,J) = TPK2D_TAU(I,J)*DET_FE_TAU_0
         END DO
      END DO

      CALL TR2TO1(TPK2D_TAU,TPK1D_TAU)

      CALL INICRYSSLIP(CBSLIP,CMSLIP)
      
      CALL EULER_SLIP(EULER,TR)

      CALL ROTATE_CRYS_VECTOR(CBSLIP,TR,GBSLIP,NSLIP)
      CALL ROTATE_CRYS_VECTOR(CMSLIP,TR,GMSLIP,NSLIP)

C     Schmid tensor for slip
      DO ISYS=1,NSLIP
         DO I=1,3
            DO J=1,3
               S0ALPHASLIP(I,J,ISYS)=GBSLIP(I,ISYS)*
     &              GMSLIP(J,ISYS)
            ENDDO          
         ENDDO                                 
      ENDDO   

      CALL MAKE_ELASTIC_STIFFNESS(DDA_2D,C11A,C12A,C44A)
      CALL MAKE_ELASTIC_STIFFNESS(DDM_2D,C11M,C12M,C44M)
      CALL TR2TO4(DDA_2D,DDA_4D)
      CALL TR2TO4(DDM_2D,DDM_4D)
      CALL ROTATE4D(DDA_4D,TR,DDA_GG)
      CALL ROTATE4D(DDM_4D,TR,DDM_GG)
      CALL TR4TO2(DDA_GG,DDA_GG_2D)
      CALL TR4TO2(DDM_GG,DDM_GG_2D)


C     Effective Modulus
      DO I=1,6
         DO J=1,6
            DDEQ_T(I,J)=((1-TOT_ZETA_T_0)*DDA_GG_2D(I,J))+
     &           ((TOT_ZETA_T_0)*DDM_GG_2D(I,J))
         ENDDO
      ENDDO

C     Inverse of consititive tensor
      DO I=1,6
         DO J=1,6
            SIJ(I,J)=DDEQ_T(I,J)
         ENDDO
      ENDDO

      CALL MATINV(SIJ,6,6)

C     Calculate elastic GreenLagrange strain from PK2 stress
      ESTRAIN1D = matmul(SIJ,TPK1D_TAU)

C     Convert elastic GreenLagrange strain to Deformation gradients
C     check that convention in subroutines matches MIDAS

      call strain2F(ESTRAIN1D,FE_TAU)

C     Calculate resolved shear stress

         DO ISYS=1,NSLIP
            TAU(ISYS)=0
            DO I=1,3
               DO J=1,3
                  TAU(ISYS)=TAU(ISYS)+
     &                 (TPK2D_TAU(I,J)*S0ALPHASLIP(I,J,ISYS))
               ENDDO
            ENDDO

C     Calculate slip rate

            TAUOVERG=DBLE(TAU(ISYS)/SHARD_0(ISYS)) 
            IF(TAUOVERG.GE.0)THEN
               DELTAGAMMA(ISYS)=
     &              GDOT0*DTIME*(ABS(TAUOVERG)**(1/AM))

            ELSE
               DELTAGAMMA(ISYS)=-
     &              GDOT0*DTIME*(ABS(TAUOVERG)**(1/AM))

            ENDIF 


         ENDDO
      statev(300) = SHARD_0(1)


      DO ISYS=1,NSLIP
         SHARD_TAU(ISYS)=SHARD_0(ISYS)
      ENDDO


      DO ISYS_A=1,NSLIP
         DO ISYS_B=1,NSLIP
            IF(ISYS_A.EQ.ISYS_B)THEN
               QLAT(ISYS_A,ISYS_B)=QL
            ELSE
               QLAT(ISYS_A,ISYS_B)=1.0
            ENDIF
         ENDDO
      ENDDO       


CCCCCCCCCCCCCC Start hardening Newton Raphson Loop
      DO K = 1,1000
C     update critical stress

C         Hardening law updated here
          DO ISYS=1,NSLIP

             GARG=DBLE(1.0-(SHARD_TAU(ISYS)/SS))
             HBETA(ISYS)=H0*(ABS(GARG)**AHARD)

          ENDDO  

          DO ISYS_A = 1,NSLIP
             DO ISYS_B= 1,NSLIP
                HAB(ISYS_A,ISYS_B)=QLAT(ISYS_A,ISYS_B)*
     &               HBETA(ISYS_B)
             ENDDO
          ENDDO


          DO ISYS_A=1,NSLIP
             SHARDDOT(ISYS_A)=0
             DO ISYS_B=1,NSLIP
                SHARDDOT(ISYS_A)=SHARDDOT(ISYS_A)+
     &               (HAB(ISYS_A,ISYS_B)*
     &               ABS(DELTAGAMMA(ISYS_B)))
             ENDDO            
             SHARD_TAU(ISYS_A)=SHARD_0(ISYS_A)+SHARDDOT(ISYS_A)
          ENDDO

          RESMAG = 0.D0
          DO ISYS=1,NSLIP
             TAUOVERG=DBLE(TAU(ISYS)/SHARD_TAU(ISYS)) 
             if (TAUOVERG.lt. 1e-12) then
                TAUOVERG = 1.D0
             end if
             IF(TAUOVERG.GE.0)THEN
               RES(ISYS)=
     &            GDOT0*DTIME*(ABS(TAUOVERG)**(1/AM)) - DELTAGAMMA(ISYS) 
               GETSIGN(ISYS) = 1.D0
            ELSE
               RES(ISYS)= -
     &           GDOT0*DTIME*(ABS(TAUOVERG)**(1/AM)) - DELTAGAMMA(ISYS)
               GETSIGN(ISYS) = -1.D0
            ENDIF
            RESMAG = RESMAG + RES(ISYS)**2.0
          END DO


          RESMAG = RESMAG**(0.5D0)
C          print *,RESMAG 
          if (abs(RESMAG) < 1e-5) then
               goto 42
          endif

         DO ISYS=1,NSLIP   
            DRDG(ISYS) = GETSIGN(ISYS)* (GDOT0*DTIME/AM)
     &                 * abs(TAUOVERG)**((1.D0/AM) - 1.D0)
     &                 * -1.D0* TAU(ISYS)*SHARD_TAU(ISYS)**(-2.D0)

            GARG=DBLE(1.0-(SHARD_TAU(ISYS)/SS))
            HBETA(ISYS)=H0*(ABS(GARG)**AHARD)

            DO ISYS_A = 1,NSLIP
             DO ISYS_B= 1,NSLIP
                HAB(ISYS_A,ISYS_B)=QLAT(ISYS_A,ISYS_B)*
     &               HBETA(ISYS_B)
             ENDDO
            ENDDO

C           APPROXIMATION 
            DGDOTDGAMMA(ISYS) = HAB(ISYS,ISYS)
            DRES(ISYS) = DRDG(ISYS)*DGDOTDGAMMA(ISYS)

            IF (abs(DRES(ISYS)).ge.1e-6) then
               DELTAGAMMA(ISYS)= DELTAGAMMA(ISYS)- RES(ISYS)/DRES(ISYS) 
            endif 

         ENDDO ! end islip loop

c$$$           if (K.lt.500) then
c$$$              print *, TAU(1)
c$$$           else
c$$$                 STOP
c$$$           end if 

      ENDDO ! newton raphson loop
      
42    IF (K.GE.1000) THEN
         print *, "Hardening Loop Did Not Converge"
      ENDIF
      STOP

C     Calculate Plastic Velocity Gradient in itermieiat config
          DO I=1,3
             DO J=1,3
                SLIPLP(I,J)=0.0
                DO ISYS=1,NSLIP      
                   SLIPLP(I,J)=SLIPLP(I,J)+((1-TOT_ZETA_TAU)*
     &                  DELTAGAMMA(ISYS)*DTIME*
     &                  S0ALPHASLIP(I,J,ISYS))
                ENDDO
             ENDDO
          ENDDO


          CALL PROD(SLIPLP,FPEFFOLD,FP_LP) 
          CALL PROD(SLIPLP,FPLASTICOLD,FP_LP_POST) 
          DO I=1,3
             DO J=1,3
                FPLASTICNEW(I,J)=
     &               FPLASTICOLD(I,J)+FP_LP_POST(I,J)
             ENDDO
          ENDDO            

      CALL TRANS(FPLASTICNEW,FPLASTICNEWT)
      CALL PROD(FPLASTICNEWT,FPLASTICNEW,CPLASTIC)

      DO I=1,3
         DO J=1,3
            FPEFFNEW(I,J)=FPEFFOLD(I,J)+FP_LP(I,J)+FTR_LTR(I,J)
         ENDDO
      ENDDO


      DO I=1,3
         DO J=1,3
            IF(I.EQ.J)THEN
               DELTAIJ=1.0
            ELSE
               DELTAIJ=0.0
            ENDIF
            EPLASTIC2D(I,J)=0.50*(CPLASTIC(I,J)-DELTAIJ)
        ENDDO
      ENDDO

C     Calcultate total F
      DFGRD0 = matmul(FE_TAU,FPEFFNEW)
      CALL TRANS(DFGRD0,DFGRD0T)
      CALL PROD(DFGRD0T,DFGRD0,CTOTAL)

      DO I=1,3
         DO J=1,3
            ETOTAL2D(I,J)=0.50*(CTOTAL(I,J)-DELTAIJ)
        ENDDO
      ENDDO

      CALL TR2TO1_KINE(ETOTAL2D,ETOTAL1D)


      DO I=1,6
         STRAN(I) =ETOTAL1D(I)
      ENDDO


C     1- 9 old Fe
      STATEV(1) =  FE_TAU(1,1)
      STATEV(2) =  FE_TAU(1,2)
      STATEV(3) =  FE_TAU(1,3) 
      STATEV(4) =  FE_TAU(2,1)
      STATEV(5) =  FE_TAU(2,2)
      STATEV(6) =  FE_TAU(2,3) 
      STATEV(7) =  FE_TAU(3,1) 
      STATEV(8) =  FE_TAU(3,2) 
      STATEV(9) =  FE_TAU(3,3) 

      STATEV(10) = TOT_ZETA_T      

C     101 - 109 effective plastic deformation gradient

      STATEV(101) = FPEFFNEW(1,1)
      STATEV(102) = FPEFFNEW(1,2)
      STATEV(103) = FPEFFNEW(1,3)
      STATEV(104) = FPEFFNEW(2,1)
      STATEV(105) = FPEFFNEW(2,2)
      STATEV(106) = FPEFFNEW(2,3)
      STATEV(107) = FPEFFNEW(3,1)
      STATEV(108) = FPEFFNEW(3,2)
      STATEV(109) = FPEFFNEW(3,3)

C     110 - 121 (assuming nslip = 12) JAM
      DO ISYS=1,NSLIP
         STATEV(109+ISYS) = DELTAGAMMA(ISYS)/DTIME
      ENDDO
C     122 - 133 (assuming nslip = 12) JAM
      DO ISYS=1,NSLIP
         STATEV(109+NSLIP+ISYS) = SHARD_TAU(ISYS)      
      ENDDO

C     202-210 Plastic strain

      STATEV(202)=EPLASTIC2D(1,1)
      STATEV(203)=EPLASTIC2D(1,2)
      STATEV(204)=EPLASTIC2D(1,3)
      STATEV(205)=EPLASTIC2D(2,1)
      STATEV(206)=EPLASTIC2D(2,2)
      STATEV(207)=EPLASTIC2D(2,3)
      STATEV(208)=EPLASTIC2D(3,1)
      STATEV(209)=EPLASTIC2D(3,2)
      STATEV(210)=EPLASTIC2D(3,3)



C     END UMAT JAM
      END
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC 

C     MATRIX MULTIPLICATION SUBROUTINE 

      SUBROUTINE MATMUL(CC,AA,BB,N,M,L)
      
C     INCLUDE 'aba_param.inc'
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)

C     THIS SUBROUTINE RETURNS MARTIX [CC] = MATRIX [AA] * MATRIX [BB]
C     N=NO. OF ROWS OF [AA]      =NO. OF ROWS OF [CC]
C     M=NO. OF COLUMNS OF [AA]   =NO. OF ROWS OF [BB]
C     L=NO. OF COLUMNS OF [BB]   =NO. OF COLUMNS OF [CC]

      DIMENSION AA(N,M),BB(M,L),CC(N,L)

      DO 10 I=1,N
         DO 10 J=1,L
            CC(I,J)=0.0
            DO 10 K=1,M
 10                        CC(I,J)=CC(I,J)+AA(I,K)*BB(K,J)
               RETURN 
               END  



C     CALCULATES THE INVERSE OF A 3*3 MATRIX
      SUBROUTINE MATINV3(A,AI,DET)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

C     INCLUDE 'aba_param.inc'

      DIMENSION A(3,3), AI(3,3)
C     
      DET=(A(1,1)*A(2,2)*A(3,3)-A(1,1)*A(2,3)*A(3,2)-A(2,1)*A(1,2)
     &     *A(3,3)+A(2,1)*A(1,3)*A(3,2)+A(3,1)*A(1,2)*A(2,3)-A(3,1)
     &     *A(1,3)*A(2,2))
      AI(1,1) =  ( A(2,2)*A(3,3)-A(2,3)*A(3,2))/DET
      AI(1,2) = -( A(1,2)*A(3,3)-A(1,3)*A(3,2))/DET
      AI(1,3) = -(-A(1,2)*A(2,3)+A(1,3)*A(2,2))/DET
      AI(2,1) = -( A(2,1)*A(3,3)-A(2,3)*A(3,1))/DET
      AI(2,2) =  ( A(1,1)*A(3,3)-A(1,3)*A(3,1))/DET
      AI(2,3) = -( A(1,1)*A(2,3)-A(1,3)*A(2,1))/DET
      AI(3,1) =  ( A(2,1)*A(3,2)-A(2,2)*A(3,1))/DET
      AI(3,2) = -( A(1,1)*A(3,2)-A(1,2)*A(3,1))/DET
      AI(3,3) =  ( A(1,1)*A(2,2)-A(1,2)*A(2,1))/DET
      RETURN
      END


       !-----------------------------------------------------------------
       !----Approx. Deformation tensor F from Green-Lagrange Strain -----
       !----I think its just an approximatation, but it might be exaxt --
       !-----------------------------------------------------------------
       subroutine strain2F(strain,dfgrd)

       implicit none

       integer j,k
       real*8 strainMat(3,3), Cmat(3,3), Ceig(3,3), CVeig(3,3)
       real*8 sqrtCeig(3,3)
       real*8 strain(6), dfgrd(3,3)

       ! convert stain in Voigt notation to tensor
          call voigt2tensor(strain,strainMat)
          
       ! calculate right Cauchy Green Tensor
          do j = 1,3
             do k = 1,3
                if (j.eq.k) then
                   Cmat(j,k) = 2.D0*strainMat(j,k) + 1.D0
                   else
                   Cmat(j,k) = 2.D0*strainMat(j,k)
                endif
             end do
          end do

          ! Cacluate the eigenvalues Ceig and Vector CVeig of C tensor
          call eig(Cmat,1.D-9,Ceig,CVeig)

          ! take sqrt of diagonal eigenvalues, this is the F approximation
          do j = 1,3
             do k = 1,3
                sqrtCeig(j,k) = sqrt(Ceig(j,k))
             end do
          end do

          ! use eigen vectors to get back to full F tensor
          dfgrd = matmul(matmul(CVeig, sqrtCeig),transpose(CVeig) )

       end subroutine strain2F


       !-----------------------------------------------------------------
       !--------------- turn voigt vector to tensor matrix  ------------
       !--- only works for kinematric tensors (e.g.,, strain, strainrate)
       !-----------------------------------------------------------------
      subroutine voigt2Tensor(Avec,Amat)

        implicit none

        real*8 Avec(6), Amat(3,3)

        ! assuming kinematic tensor
        Amat(1,1) = Avec(1)
        Amat(2,2) = Avec(2)
        Amat(3,3) = Avec(3)
        Amat(1,2) = 0.5D0*Avec(4)
        Amat(2,1) = Amat(1,2)
        Amat(1,3) = 0.5D0*Avec(5)
        Amat(3,1) = Amat(1,3)
        Amat(2,3) = 0.5D0*Avec(6)
        Amat(3,2) = Amat(2,3)

      end subroutine voigt2Tensor


       !-----------------------------------------------------------------
       !--------------- turn tensor matrixx to voigt vector ------------
       !--- only works for kinematric tensors (e.g.,, strain, strainrate)
       !-----------------------------------------------------------------
      subroutine tensor2Voigt(Amat,Avec)

        implicit none

        real*8 Avec(6), Amat(3,3)

        ! assuming kinematic tensor
        Avec(1) = Amat(1,1)
        Avec(2) = Amat(2,2)
        Avec(3) = Amat(3,3)
        Avec(4) = 2.D0*Amat(1,2)
        Avec(5) = 2.D0*Amat(1,3)
        Avec(6) = 2.D0*Amat(2,3)


      end subroutine tensor2Voigt


! Determine eigenvalues, Aeig,  and eigenvectors, Geig, of the matrix A
! to within a tolerance, tol using Jacobi iteration method
!
! to reproduce the orginal matrix:
!
!                A =  matmul(matmul(Geig,Aeig),transpose(Geig))
!
!      Programmed by Jacob Smith, Apple Austin TX (as of 2021), circa 2013
      subroutine eig(A,tol,Aeig,Geig)


        implicit none

        integer i

        real*8 conv,A(3,3),Aeig(3,3),G(3,3),Geig(3,3),tol,maxv,tau,t,c,s
        real*8 Aeig1,Aeig2,Aeig3,Geig1(3),Geig2(3),Geig3(3),Gcount1
        real*8 Gcount2,maxvo
        integer counter

        Gcount1 = 0.
        Gcount2 = 0.

        conv = sqrt(A(1,2)**2. + A(1,3)**2. + A(2,3)**2.)
        Aeig = A
        Geig = 0.
        do i=1,3
           Geig(i,i) = 1.
        enddo
 
        maxvo = 1e25
        counter = 0
        do while(conv.GT.tol)
           counter = counter + 1
           if (counter.GE.50) then
              print *,'eigenvalue minimization failed',conv
              goto 100
           endif
           G = 0.
           maxv = max(abs(Aeig(1,2)),abs(Aeig(1,3)),abs(Aeig(2,3)))
           if (maxv.EQ.abs(Aeig(1,2))) then
              tau = (Aeig(1,1) - Aeig(2,2))/(2.*Aeig(1,2))
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)
              G(1,1) = c
              G(2,2) = c
              G(1,2) = -s
              G(2,1) = s
              G(3,3) = 1.
           elseif (maxv.EQ.abs(Aeig(1,3))) then
              tau = (Aeig(1,1) - Aeig(3,3))/(2.*Aeig(1,3))       
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)        
              G(1,1) = c
              G(3,3) = c
              G(1,3) = -s
              G(3,1) = s
              G(2,2) = 1.        
           else
              tau = (Aeig(2,2) - Aeig(3,3))/(2.*Aeig(2,3))
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)        
              G(2,2) = c
              G(3,3) = c
              G(2,3) = -s
              G(3,2) = s
              G(1,1) = 1.        
           endif
           maxvo = maxv
           Aeig = matmul(matmul(G,Aeig),transpose(G))
           Geig = matmul(Geig,transpose(G))
           conv = sqrt(Aeig(1,2)**2. + Aeig(1,3)**2. + Aeig(2,3)**2.)
        enddo

! reorganize the principal values into max, min, mid

        Aeig1 = max(Aeig(1,1),Aeig(2,2),Aeig(3,3))
        Aeig2 = min(Aeig(1,1),Aeig(2,2),Aeig(3,3))

        do i=1,3
           if ((Aeig1.EQ.Aeig(i,i)).AND.(Gcount1.EQ.0.)) then
              Geig1 = Geig(:,i)
              Gcount1 = 1.
           elseif ((Aeig2.EQ.Aeig(i,i)).AND.(Gcount2.EQ.0.)) then
              Geig2 = Geig(:,i)
              Gcount2 = 1.
           else
              Aeig3 = Aeig(i,i)
              Geig3 = Geig(:,i)
           endif
        enddo

        Aeig = 0.
        Aeig(1,1) = Aeig1
        Aeig(2,2) = Aeig2
        Aeig(3,3) = Aeig3
        Geig(:,1) = Geig1
        Geig(:,2) = Geig2
        Geig(:,3) = Geig3

100     return

      end subroutine eig


      SUBROUTINE MATINV (A,M,ISIZE)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C----------------------------------------------------------
C     INVERSE AND DETERMINANT OF A BY THE GAUSS-JORDAN METHOD.
C     M IS THE ORDER OF THE SQUARE MATRIX, A.
C     A-INVERSE REPLACES A.
C     DETERMINANT OF A IS PLACED IN DET.
C     COOLEY AND LOHNES (1971:63)
C-----------------------------------------------------------
      PARAMETER (IDIM=50)
      DIMENSION A(ISIZE,ISIZE),IPVT(IDIM+2),PVT(IDIM+2),IND(IDIM+2,2)
      DET=.1D1
      DO 10 J=1,M
 10            IPVT(J)=0
         DO 100 I=1,M
C     SEARCH FOR THE PIVOT ELEMENT
            AMAX=.0D0
            DO 40 J=1,M
               IF(IPVT(J).EQ.1)GO TO 40
               DO 30 K=1,M
                  IF(IPVT(K)-1)20,30,130
 20                              IF(DABS(AMAX).GE.DABS(A(J,K)))GO TO 30
                  IROW=J
                  ICOL=K
                  AMAX=A(J,K)
 30                           CONTINUE
 40                                    CONTINUE
            IPVT(ICOL)=IPVT(ICOL)+1
C     INTERCHANGE THE ROWS TO PUT THE PIVOT ELEMENT ON THE DIAGONAL
            IF(IROW.EQ.ICOL)GO TO 60
            DET=-DET
            DO 50 L=1,M
               SWAP=A(IROW,L)
               A(IROW,L)=A(ICOL,L)
 50                        A(ICOL,L)=SWAP
 60                                    IND(I,1)=IROW
               IND(I,2)=ICOL
               PVT(I)=A(ICOL,ICOL)
C     DIVIDE THE PIVOT ROW BY THE PIVOT ELEMENT
               A(ICOL,ICOL)=.1D1
               DO 70 L=1,M
                  A(ICOL,L)=A(ICOL,L)/PVT(I)
 70                           CONTINUE
C     REDUCE THE NON-PIVOT ROWS
               DO 100 L1=1,M
                  IF(L1.EQ.ICOL)GO TO 90
                  SWAP=A(L1,ICOL)
                  A(L1,ICOL)=.0D0
                  IF(SWAP.LT..1D-30.AND.SWAP.GT.-.1D-30)SWAP=.0D0
                  DO 80 L=1,M
                     A(L1,L)=A(L1,L)-A(ICOL,L)*SWAP
 80                                 CONTINUE
 90                                                CONTINUE
 100                                                          CONTINUE
C     INTERCHANGE THE COLUMNS
               DO 120 I=1,M
                  L=M+1-I
                  IF(IND(L,1).EQ.IND(L,2))GO TO 120
                  IROW=IND(L,1)
                  ICOL=IND(L,2)
                  DO 110 K=1,M
                     SWAP=A(K,IROW)
                     A(K,IROW)=A(K,ICOL)
                     A(K,ICOL)=SWAP
 110                               CONTINUE
 120                                          CONTINUE
 130                                                     CONTINUE
               RETURN
               END


      SUBROUTINE INICRYSSLIP(CBSLIP,CMSLIP)   
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER NSLIP, I, ISYS
      REAL*8 CBSLIP, CMSLIP, CBMAG, CMMAG
c      INCLUDE 'globals2.inc'
      PARAMETER(NSLIP = 12)
      DIMENSION CBSLIP(3,12),CMSLIP(3,12)

C       WRITE(*,*)'IN INICRYS'
C       WRITE(*,*)'IN INICRYS'
C      OPEN(35,FILE=PWD//SLIPIN)
C      Superslipsys.inc file information here
C      DO I=1,NSLIP
C         READ(35,*)(CMSLIP(J,I),J=1,3),(CBSLIP(J,I),J=1,3)
C      ENDDO
C
C     CMSLIP Data
      CMSLIP(1,1)  =  0.D0
      CMSLIP(2,1)  =  1.D0
      CMSLIP(3,1)  =  0.D0
      CMSLIP(1,2)  =  0.D0
      CMSLIP(2,2)  =  0.D0
      CMSLIP(3,2)  =  1.D0
      CMSLIP(1,3)  =  1.D0
      CMSLIP(2,3)  =  0.D0
      CMSLIP(3,3)  =  0.D0
      CMSLIP(1,4)  =  0.D0
      CMSLIP(2,4)  =  0.D0
      CMSLIP(3,4)  =  1.D0
      CMSLIP(1,5)  =  1.D0
      CMSLIP(2,5)  =  0.D0
      CMSLIP(3,5)  =  0.D0
      CMSLIP(1,6)  =  0.D0
      CMSLIP(2,6)  =  1.D0
      CMSLIP(3,6)  =  0.D0
      CMSLIP(1,7)  =  0.D0
      CMSLIP(2,7)  =  1.D0
      CMSLIP(3,7)  =  1.D0
      CMSLIP(1,8)  =  0.D0
      CMSLIP(2,8)  =  1.D0
      CMSLIP(3,8)  = -1.D0
      CMSLIP(1,9)  =  1.D0
      CMSLIP(2,9)  =  0.D0
      CMSLIP(3,9)  =  1.D0
      CMSLIP(1,10) =  1.D0
      CMSLIP(2,10) =  0.D0
      CMSLIP(3,10) = -1.D0
      CMSLIP(1,11) =  1.D0
      CMSLIP(2,11) =  1.D0
      CMSLIP(3,11) =  0.D0
      CMSLIP(1,12) = -1.D0
      CMSLIP(2,12) =  1.D0
      CMSLIP(3,12) =  0.D0
C     CMSLIP Data
      CBSLIP(1,1)  =  1.D0
      CBSLIP(2,1)  =  0.D0
      CBSLIP(3,1)  =  0.D0
      CBSLIP(1,2)  =  1.D0
      CBSLIP(2,2)  =  0.D0
      CBSLIP(3,2)  =  0.D0
      CBSLIP(1,3)  =  0.D0
      CBSLIP(2,3)  =  1.D0
      CBSLIP(3,3)  =  0.D0
      CBSLIP(1,4)  =  0.D0
      CBSLIP(2,4)  =  1.D0
      CBSLIP(3,4)  =  0.D0
      CBSLIP(1,5)  =  0.D0
      CBSLIP(2,5)  =  0.D0
      CBSLIP(3,5)  =  1.D0
      CBSLIP(1,6)  =  0.D0
      CBSLIP(2,6)  =  0.D0
      CBSLIP(3,6)  =  1.D0
      CBSLIP(1,7)  =  1.D0
      CBSLIP(2,7)  =  0.D0
      CBSLIP(3,7)  =  0.D0
      CBSLIP(1,8)  =  1.D0
      CBSLIP(2,8)  =  0.D0
      CBSLIP(3,8)  =  0.D0
      CBSLIP(1,9)  =  0.D0
      CBSLIP(2,9)  =  1.D0
      CBSLIP(3,9)  =  0.D0
      CBSLIP(1,10) =  0.D0
      CBSLIP(2,10) =  1.D0
      CBSLIP(3,10) =  0.D0
      CBSLIP(1,11) =  0.D0
      CBSLIP(2,11) =  0.D0
      CBSLIP(3,11) =  1.D0
      CBSLIP(1,12) =  0.D0
      CBSLIP(2,12) =  0.D0
      CBSLIP(3,12) =  1.D0

C      WRITE(*,*)CMSLIP
C      WRITE(*,*)CBSLIP
      DO ISYS=1,NSLIP
         CMMAG=SQRT(CMSLIP(1,ISYS)**2+CMSLIP(2,ISYS)**2+
     &        CMSLIP(3,ISYS)**2)
         CBMAG=SQRT(CBSLIP(1,ISYS)**2+CBSLIP(2,ISYS)**2+
     &        CBSLIP(3,ISYS)**2) 
         DO I=1,3
            CMSLIP(I,ISYS)=CMSLIP(I,ISYS)/CMMAG
            CBSLIP(I,ISYS)=CBSLIP(I,ISYS)/CBMAG
         ENDDO
      ENDDO
      
C       CLOSE(35)
   
C       WRITE(*,*)'OUT INICRYS'
      END


C-----------------------------------------------------

C     THIS SUBROUTINE CALCULATES THE TRANSFORMATION MATRIX
C----------------------------------------------------------
C     PHI   - EULER(1)
C     THETA - EULER(2)
C     OMEGA - EULER(3)
C---------------------------------------------------


      SUBROUTINE EULER_SLIP(EULER,TLG)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)  

C     INCLUDE 'aba_param.inc'

      DIMENSION EULER(3),TLG(3,3),TLGT(3,3)

      PI=4.0*ATAN(1.0)

      PHI=EULER(1)*PI/180.0 
      THETA =EULER(2)*PI/180.0
      OMEGA  =EULER(3)*PI/180.0 

      SP=DSIN(PHI)                      
      CP=DCOS(PHI)                     
      ST=DSIN(THETA)                     
      CT=DCOS(THETA)                    
      SO=DSIN(OMEGA)                    
      CO=DCOS(OMEGA)   
      TLG(1,1)=CO*CP-SO*SP*CT
      TLG(1,2)=CO*SP+SO*CT*CP   
      TLG(1,3)=SO*ST   
      TLG(2,1)=-SO*CP-SP*CO*CT 
      TLG(2,2)=-SO*SP+CT*CO*CP
      TLG(2,3)=CO*ST
      TLG(3,1)=SP*ST       
      TLG(3,2)=-ST*CP       
      TLG(3,3)=CT
C$$$C$$$C     TLG IS GLOBAL TO LOCAL IF IT IS BUNGE DEF. OF EULER ANGLE
C$$$C$$$      CALL TRANS(TLG,TLGT)

      RETURN
      END   

C-----------------------------------------------------
      SUBROUTINE ROTATE_CRYS_VECTOR(A,Q,A_PRIME,NDIM)
C      IMPLICIT REAL*8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER NDIM, ISYS, I, J
      REAL*8 A, Q, A_PRIME

c      INCLUDE 'globals2.inc'
      DIMENSION A(3,NDIM),A_PRIME(3,NDIM),Q(3,3)
      DO ISYS=1,NDIM
         DO I=1,3
            A_PRIME(I,ISYS)=0.0
            DO J=1,3
               A_PRIME(I,ISYS)=A_PRIME(I,ISYS)+Q(I,J)*A(J,ISYS)
            ENDDO
         ENDDO
      ENDDO
      RETURN
      END


      SUBROUTINE MAKE_ELASTIC_STIFFNESS(DD_2D,C11,C12,C44)
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      REAL*8 DD_2D, C11, C12, C44
      DIMENSION DD_2D(6,6)
      CALL ARRAYINITIALIZE2(DD_2D,6,6)
      DD_2D(1,1)=C11
      DD_2D(2,2)=C11
      DD_2D(3,3)=C11
      DD_2D(4,4)=C44
      DD_2D(5,5)=C44
      DD_2D(6,6)=C44
      DD_2D(1,2)=C12
      DD_2D(2,1)=C12
      DD_2D(1,3)=C12
      DD_2D(3,1)=C12
      DD_2D(2,3)=C12
      DD_2D(3,2)=C12
      END


      SUBROUTINE TR2TO1(A_2D,A_1D)
C     Does this subroutine work for kinetic and kinematic tensors???? JAM
C     This may need fixed
C      IMPLICIT REAL*8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      REAL*8 A_2D, A_1D

      DIMENSION A_2D(3,3),A_1D(6)
      A_1D(1)=A_2D(1,1)
      A_1D(2)=A_2D(2,2)
      A_1D(3)=A_2D(3,3)
      A_1D(4)=0.5*(A_2D(1,2)+A_2D(2,1))
      A_1D(5)=0.5*(A_2D(1,3)+A_2D(3,1))
      A_1D(6)=0.5*(A_2D(2,3)+A_2D(3,2))
      RETURN
      END


      SUBROUTINE TR2TO1_KINE(A_2D,A_1D)
C     kinematic tensors JAM
C     This may need fixed
C      IMPLICIT REAL*8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      REAL*8 A_2D, A_1D

      DIMENSION A_2D(3,3),A_1D(6)
      A_1D(1)=A_2D(1,1)
      A_1D(2)=A_2D(2,2)
      A_1D(3)=A_2D(3,3)
      A_1D(4)=(A_2D(1,2)+A_2D(2,1))
      A_1D(5)=(A_2D(1,3)+A_2D(3,1))
      A_1D(6)=(A_2D(2,3)+A_2D(3,2))
      RETURN
      END


C------------------------------------------------------------
C     TRANSFORMS THE FOURTH ORDER ELASTICITY TENSOR FROM 
C     CRYSTAL SYSTEM TO GLOBAL 
C     COURTESY L.ANAND
      SUBROUTINE ROTATE4D(ELMATC,Q,ELMAT)
      IMPLICIT REAL*8(A-H,O-Z)
C     TRANSFORMS A FOURTH ORDER TENSOR 
      DIMENSION ELMATC(3,3,3,3),ELMAT(3,3,3,3),Q(3,3)
      DIMENSION AUXMAT1(3,3,3,3),AUXMAT2(3,3,3,3)

      DO 20 I=   1,3
         DO 20 N =  1,3
            DO 20 IO = 1,3
               DO 20 IP = 1,3
                  AUXMAT1(I,N,IO,IP) = 0.0
                  DO 15  M = 1,3
                     AUXMAT1(I,N,IO,IP) = AUXMAT1(I,N,IO,IP) +
     &                    ELMATC(M,N,IO,IP)*Q(I,M)
 15                                 CONTINUE
 20                                             CONTINUE

               DO 30 I=   1,3
                  DO 30 J =  1,3
                     DO 30 IO = 1,3
                        DO 30 IP = 1,3
                           AUXMAT2(I,J,IO,IP) = 0.0
                           DO 25  N = 1,3
                              AUXMAT2(I,J,IO,IP) = AUXMAT2(I,J,IO,IP) +
     &                             AUXMAT1(I,N,IO,IP)*Q(J,N)
 25                                                   CONTINUE
 30                                                   CONTINUE

                        DO 40 I  = 1,3
                           DO 40 J  = 1,3
                              DO 40 K  = 1,3
                                 DO 40 IP = 1,3
                                    AUXMAT1(I,J,K,IP) = 0.0
                                    DO 35  IO = 1,3
                                       AUXMAT1(I,J,K,IP) = 
     &                                      AUXMAT1(I,J,K,IP) +
     &                                      AUXMAT2(I,J,IO,IP)*Q(K,IO)
 35                                                             CONTINUE
 40                                                             CONTINUE

                                 DO 50 I  = 1,3
                                    DO 50 J  = 1,3
                                       DO 50 K  = 1,3
                                          DO 50 L  = 1,3
                                             ELMAT(I,J,K,L) = 0.0
                                             DO 45  IP = 1,3
                                                ELMAT(I,J,K,L) = 
     &                                               ELMAT(I,J,K,L) +
     &                                               AUXMAT1(I,J,K,IP)
     &                                               *Q(L,IP)
 45                                                             CONTINUE
 50                                                             CONTINUE

                                          
                                          RETURN
                                          END


C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     CONVERTS FOURTH ORDER TENSOR TO EQUIVALENT SECOND ORDER TENSOR

      SUBROUTINE TR4TO2(ELMAT,ELMATR)
      IMPLICIT REAL*8(A-H,O-Z)
C     COURTESY L.ANAND
      DIMENSION ELMAT(3,3,3,3), ELMATR(6,6)
      
      DO 10 I = 1,6
         DO 10 J = 1,6
                ELMATR(I,J) = 0.0
 10                   CONTINUE
         
         
         ELMATR(1,1)  = ELMAT(1,1,1,1)
         ELMATR(1,2)  = ELMAT(1,1,2,2)
         ELMATR(1,3)  = ELMAT(1,1,3,3) 
         ELMATR(1,4)  = 0.5 * ( ELMAT(1,1,1,2) + ELMAT(1,1,2,1) )
         ELMATR(1,5)  = 0.5 * ( ELMAT(1,1,1,3) + ELMAT(1,1,3,1) )
         ELMATR(1,6)  = 0.5 * ( ELMAT(1,1,2,3) + ELMAT(1,1,3,2) )


         ELMATR(2,1)  = ELMAT(2,2,1,1)
         ELMATR(2,2)  = ELMAT(2,2,2,2)
         ELMATR(2,3)  = ELMAT(2,2,3,3) 
         ELMATR(2,4)  = 0.5 * ( ELMAT(2,2,1,2) + ELMAT(2,2,2,1) )
         ELMATR(2,5)  = 0.5 * ( ELMAT(2,2,1,3) + ELMAT(2,2,3,1) )
         ELMATR(2,6)  = 0.5 * ( ELMAT(2,2,2,3) + ELMAT(2,2,3,2) )
         
         ELMATR(3,1)  = ELMAT(3,3,1,1)
         ELMATR(3,2)  = ELMAT(3,3,2,2)
         ELMATR(3,3)  = ELMAT(3,3,3,3) 
         ELMATR(3,4)  = 0.5 * ( ELMAT(3,3,1,2) + ELMAT(3,3,2,1) )
         ELMATR(3,5)  = 0.5 * ( ELMAT(3,3,1,3) + ELMAT(3,3,3,1) )
         ELMATR(3,6)  = 0.5 * ( ELMAT(3,3,2,3) + ELMAT(3,3,3,2) )   
         
         ELMATR(4,1)  = ELMAT(1,2,1,1)
         ELMATR(4,2)  = ELMAT(1,2,2,2)
         ELMATR(4,3)  = ELMAT(1,2,3,3) 
         ELMATR(4,4)  = 0.5 * ( ELMAT(1,2,1,2) + ELMAT(1,2,2,1) )
         ELMATR(4,5)  = 0.5 * ( ELMAT(1,2,1,3) + ELMAT(1,2,3,1) )
         ELMATR(4,6)  = 0.5 * ( ELMAT(1,2,2,3) + ELMAT(1,2,3,2) ) 
         
         ELMATR(5,1)  = ELMAT(1,3,1,1)
         ELMATR(5,2)  = ELMAT(1,3,2,2)
         ELMATR(5,3)  = ELMAT(1,3,3,3) 
         ELMATR(5,4)  = 0.5 * ( ELMAT(1,3,1,2) + ELMAT(1,3,2,1) )
         ELMATR(5,5)  = 0.5 * ( ELMAT(1,3,1,3) + ELMAT(1,3,3,1) )
         ELMATR(5,6)  = 0.5 * ( ELMAT(1,3,2,3) + ELMAT(1,3,3,2) ) 

         ELMATR(6,1)  = ELMAT(2,3,1,1)
         ELMATR(6,2)  = ELMAT(2,3,2,2)
         ELMATR(6,3)  = ELMAT(2,3,3,3) 
         ELMATR(6,4)  = 0.5 * ( ELMAT(2,3,1,2) + ELMAT(2,3,2,1) )
         ELMATR(6,5)  = 0.5 * ( ELMAT(2,3,1,3) + ELMAT(2,3,3,1) )
         ELMATR(6,6)  = 0.5 * ( ELMAT(2,3,2,3) + ELMAT(2,3,3,2) ) 
         
         
         RETURN

         END

C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     CALCULATES THE TRANSPOSE OF A MATRIX

      SUBROUTINE TRANS(X,X_T)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C     INCLUDE 'aba_param.inc'
      DIMENSION X(3,3),X_T(3,3) 
C     
      DO I=1,3
         DO J=1,3
            X_T(J,I)=X(I,J)
         ENDDO
      ENDDO
      RETURN
      END

C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     CALCULATES THE TRANSPOSE OF A MATRIX

      SUBROUTINE TRANSMN(X,X_T,M,N)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C     INCLUDE 'aba_param.inc'
      DIMENSION X(M,N),X_T(N,M) 
C     
      DO I=1,M
         DO J=1,N
            X_T(J,I)=X(I,J)
         ENDDO
      ENDDO
      RETURN
      END
C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
      SUBROUTINE TR1TO2(X,XX)
C      IMPLICIT REAL*8 (A-H,O-Z)  
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      REAL*8 X,XX    
      DIMENSION X(6),XX(3,3)
      XX(1,1)=X(1)                                                    
      XX(2,2)=X(2)                                                   
      XX(3,3)=X(3)                                                  
      XX(2,3)=X(6)                                                 
      XX(3,2)=X(6)                                                
      XX(3,1)=X(5)                                               
      XX(1,3)=X(5)                                               
      XX(1,2)=X(4)                                              
      XX(2,1)=X(4)
      END


C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     CONVERTS A SECOND ORDER TENSOR TO A EQUIVALENT FOURTH ORDER TENSOR

      SUBROUTINE TR2TO4(ELMATR,ELMAT)
C      IMPLICIT REAL*8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I,J,K,L
      REAL*8 ELMATR, ELMAT

C     THIS SUBROUTINE RECONSTRUCTS A STIFFNESS MATRIX ELMATREC
C     AS A 3 X 3 X 3 X 3 MATRIX FROM ITS 
C     REDUCED 6 X 6 FORM ELMATR
C     
C     IMPLICIT REAL*8(A-H,O-Z) 
      DIMENSION ELMATR(6,6),ELMAT(3,3,3,3)
      
      DO 10 I = 1,3
         DO 10 J = 1,3
            DO 10 K = 1,3
               DO 10 L = 1,3
                  ELMAT(I,J,K,L) = 0.0
 10                           CONTINUE
               

C     
C     RECONSTRUCT THE MATRIX ELMAT FROM ITS REDUCED FORM ELMATR
C     
               
               ELMAT(1,1,1,1)  = ELMATR(1,1)
               ELMAT(1,1,2,2)  = ELMATR(1,2)
               ELMAT(1,1,3,3)  = ELMATR(1,3) 
               ELMAT(1,1,1,2)  = ELMATR(1,4)
               ELMAT(1,1,2,1)  = ELMATR(1,4)
               ELMAT(1,1,1,3)  = ELMATR(1,5)
               ELMAT(1,1,3,1)  = ELMATR(1,5)
               ELMAT(1,1,2,3)  = ELMATR(1,6)
               ELMAT(1,1,3,2)  = ELMATR(1,6)
               

               ELMAT(2,2,1,1)  = ELMATR(2,1)
               ELMAT(2,2,2,2)  = ELMATR(2,2)
               ELMAT(2,2,3,3)  = ELMATR(2,3) 
               ELMAT(2,2,1,2)  = ELMATR(2,4)
               ELMAT(2,2,2,1)  = ELMATR(2,4)
               ELMAT(2,2,1,3)  = ELMATR(2,5)
               ELMAT(2,2,3,1)  = ELMATR(2,5)
               ELMAT(2,2,2,3)  = ELMATR(2,6)
               ELMAT(2,2,3,2)  = ELMATR(2,6)
               
               ELMAT(3,3,1,1)  = ELMATR(3,1)
               ELMAT(3,3,2,2)  = ELMATR(3,2)
               ELMAT(3,3,3,3)  = ELMATR(3,3) 
               ELMAT(3,3,1,2)  = ELMATR(3,4)
               ELMAT(3,3,2,1)  = ELMATR(3,4)
               ELMAT(3,3,1,3)  = ELMATR(3,5)
               ELMAT(3,3,3,1)  = ELMATR(3,5)
               ELMAT(3,3,2,3)  = ELMATR(3,6)
               ELMAT(3,3,3,2)  = ELMATR(3,6)
               
               ELMAT(1,2,1,1)  =  ELMATR(4,1)
               ELMAT(2,1,1,1)  =  ELMATR(4,1)
               ELMAT(1,2,2,2)  =  ELMATR(4,2)
               ELMAT(2,1,2,2)  =  ELMATR(4,2)
               ELMAT(1,2,3,3)  =  ELMATR(4,3)
               ELMAT(2,1,3,3)  =  ELMATR(4,3)
               ELMAT(1,2,1,2)  =  ELMATR(4,4) 
               ELMAT(2,1,1,2)  =  ELMATR(4,4) 
               ELMAT(1,2,2,1)  =  ELMATR(4,4)
               ELMAT(2,1,2,1)  =  ELMATR(4,4)
               ELMAT(1,2,1,3)  =  ELMATR(4,5) 
               ELMAT(2,1,1,3)  =  ELMATR(4,5) 
               ELMAT(1,2,3,1)  =  ELMATR(4,5)
               ELMAT(2,1,3,1)  =  ELMATR(4,5)
               ELMAT(1,2,2,3)  =  ELMATR(4,6) 
               ELMAT(2,1,2,3)  =  ELMATR(4,6) 
               ELMAT(1,2,3,2)  =  ELMATR(4,6)
               ELMAT(2,1,3,2)  =  ELMATR(4,6)
               
               
               ELMAT(1,3,1,1)  =  ELMATR(5,1)
               ELMAT(3,1,1,1)  =  ELMATR(5,1)
               ELMAT(1,3,2,2)  =  ELMATR(5,2)
               ELMAT(3,1,2,2)  =  ELMATR(5,2)
               ELMAT(1,3,3,3)  =  ELMATR(5,3)
               ELMAT(3,1,3,3)  =  ELMATR(5,3)
               ELMAT(1,3,1,2)  =  ELMATR(5,4) 
               ELMAT(3,1,1,2)  =  ELMATR(5,4) 
               ELMAT(1,3,2,1)  =  ELMATR(5,4)
               ELMAT(3,1,2,1)  =  ELMATR(5,4)
               ELMAT(1,3,1,3)  =  ELMATR(5,5) 
               ELMAT(3,1,1,3)  =  ELMATR(5,5) 
               ELMAT(1,3,3,1)  =  ELMATR(5,5)
               ELMAT(3,1,3,1)  =  ELMATR(5,5)
               ELMAT(1,3,2,3)  =  ELMATR(5,6) 
               ELMAT(3,1,2,3)  =  ELMATR(5,6) 
               ELMAT(1,3,3,2)  =  ELMATR(5,6)
               ELMAT(3,1,3,2)  =  ELMATR(5,6)
               
               ELMAT(2,3,1,1)  =  ELMATR(6,1)
               ELMAT(3,2,1,1)  =  ELMATR(6,1)
               ELMAT(2,3,2,2)  =  ELMATR(6,2)
               ELMAT(3,2,2,2)  =  ELMATR(6,2)
               ELMAT(2,3,3,3)  =  ELMATR(6,3)
               ELMAT(3,2,3,3)  =  ELMATR(6,3)
               ELMAT(2,3,1,2)  =  ELMATR(6,4) 
               ELMAT(3,2,1,2)  =  ELMATR(6,4) 
               ELMAT(2,3,2,1)  =  ELMATR(6,4)
               ELMAT(3,2,2,1)  =  ELMATR(6,4)
               ELMAT(2,3,1,3)  =  ELMATR(6,5) 
               ELMAT(3,2,1,3)  =  ELMATR(6,5) 
               ELMAT(2,3,3,1)  =  ELMATR(6,5)
               ELMAT(3,2,3,1)  =  ELMATR(6,5)
               ELMAT(2,3,2,3)  =  ELMATR(6,6) 
               ELMAT(3,2,2,3)  =  ELMATR(6,6) 
               ELMAT(2,3,3,2)  =  ELMATR(6,6)
               ELMAT(3,2,3,2)  =  ELMATR(6,6)

               RETURN 
               END


C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
      SUBROUTINE PROD(A,B,C)                                          
C     
C     COMPUTES THE MATRIX PRODUCT C=A*B  ALL MATRICIES ARE 3X3
C     
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                             
C     INCLUDE 'aba_param.inc'
      DIMENSION A(3,3),B(3,3),C(3,3)                                  
C     
      DO 200 J=1,3                                                    
         DO 200 I=1,3                                                    
            S=0.0                                                           
            DO 100 K=1,3                                                    
               S=S+A(I,K)*B(K,J)                                             
 100                   CONTINUE
            C(I,J)=S                                                        
 200             CONTINUE
         RETURN
         END   



      SUBROUTINE ARRAYINITIALIZE2(A,NDIMX,NDIMY)
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I,J, NDIMX, NDIMY
      REAL*8 A
      DIMENSION A(NDIMX,NDIMY)
      DO I=1,NDIMX
         DO J=1,NDIMY
            A(I,J)=0.0
         ENDDO
      ENDDO
      END 

      SUBROUTINE ARRAYINITIALIZE1(A,NDIMX)
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I
      REAL*8 A, NDIMX
      DIMENSION A(NDIMX)
      DO I=1,NDIMX
         A(I)=0.0
      ENDDO
      END 
