C                                                  _______
C                 \    /  |    |   |\  /|    /\       |
C                  \  /   |    |   | \/ |   /__\      |
C                   \/    |____|   |    |  /    \     |
C
C  Updates stresses, internal variables, and energies for 3D solid elements ONLY
C
C  
C     J2-PLASTICITY MATERIAL SUBROUTINE - RADIAL RETURN METHOD
C              VIA MATRIX INVERSION INSTEAD OF THE CLOSED FORM 
C              SOLUTION FOR THE PLASTIC STRAIN-RATE MULTIPLIER
C              INCREMENT WRITTEN FOR THE ABAQUS\EXPLICIT MODULE
C  
C  ============================================================================
      subroutine vumat(
C Read only -
     1     nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,
     2     stepTime, totalTime, dt, cmname, coordMp, charLength,
     3     props, density, strainInc, relSpinInc,
     4     tempOld, stretchOld, defgradOld, fieldOld,
     5     stressOld, stateOld, enerInternOld, enerInelasOld,
     6     tempNew, stretchNew, defgradNew, fieldNew,
C Write only -
     7     stressNew, stateNew, enerInternNew, enerInelasNew)
C
C      include 'vaba_param.inc'
C

       implicit none
 
       character*80 cmname
        
       integer  nprops, nblock, ndir, nshr
       integer  nstatev, nfieldv
      
       real*8 dt,lanneal,stepTime,totalTime

       real*8 density, coordMp,props, 
     1  charLength, strainInc,relSpinInc, tempOld,
     2  stretchOld,defgradOld,fieldOld, stressOld,
     3  stateOld, enerInternOld,enerInelasOld, tempNew,
     4  stretchNew,defgradNew,fieldNew,stressNew, stateNew,
     5  enerInternNew, enerInelasNew


       dimension
     1  density(nblock), coordMp(nblock,*),props(nprops), 
     1  charLength(nblock), strainInc(nblock,ndir+nshr),
     2  relSpinInc(nblock,nshr), tempOld(nblock),
     3  stretchOld(nblock,ndir+nshr),
     4  defgradOld(nblock,ndir+nshr+nshr),
     5  fieldOld(nblock,nfieldv), stressOld(nblock,ndir+nshr),
     6  stateOld(nblock,nstatev), enerInternOld(nblock),
     7  enerInelasOld(nblock), tempNew(nblock),
     8  stretchNew(nblock,ndir+nshr),
     8  defgradNew(nblock,ndir+nshr+nshr),
     9  fieldNew(nblock,nfieldv),
     1  stressNew(nblock,ndir+nshr), stateNew(nblock,nstatev),
     2  enerInternNew(nblock), enerInelasNew(nblock)

        integer i, j, k, counter
C-----------------------------------------------------------------------------
C                       Start new variable definitions here
C-----------------------------------------------------------------------------
      logical YIELD
      logical DEBUG
      real*8 e,xnu,sy0,e0,n
      real*8 tol,lame1,G
      real*8 C(6,6)
      real*8 I6(6,6),Ivect(6),II(6,6),I2(2,2),Ih(6,6),IkronI(6,6)
      real*8 Cinv(6,6),tr,tsig(6),epb0,sy,H, dsig(6)
      real*8 rvec(6),plmul,depb,epb
      real*8 drdsig(6,6)
      real*8 dqdlam,Amat(7,7),Ainv(7,7),r(6),rl(7),rr(7),fn,den
      real*8 plinc,trr,fncheck,rmat(3,3), one3
      real*8 sig(6), AinvE(7,7), plincE, coef, P4(6,6)
      real*8 fyield, ret, sigret, tsigh, sigh, norm(6), sigeE
      real*8 drdq1(6), tsige, sige, three2, tdev(6), nxn(6,6)
      real*8 sijsij, dhdsig(6),dhdq, dfdq, dev(6), sqrt23
C     for debug
      real*8 rvec0(6), dfy, A11, sigm(6), sigp(6), delta
      real*8 rvecp(6), rvecm(6), trrm, trrp, dsigp(6), dsigm(6)

C      H =750.D0
      DEBUG = .TRUE.

C-----------------------------------------------------------------------------
C	           Initialize material data from input file
C-----------------------------------------------------------------------------
      e    = props(1) ! Young's modulus
      xnu  = props(2) ! Poisson's ratio
      sy0  = props(3) ! initial yield strength
      e0   = props(4) ! initial yield strain
      n    = props(5) ! matrix stress exponent
      tol  = props(6)
C
      lame1 = e*xnu/((1.D0+xnu)*(1.D0-2.D0*xnu)) ! lame1
      G     = e/(2.D0*(1.D0+xnu)) ! lame2
C
      three2 = 3.D0/2.D0
      sqrt23 = sqrt(2.D0/3.D0)
      one3 = 1.D0/3.D0
C
      C = 0.
      do i=1,3
         C(i,i) = lame1+2.*G
         C(i+3,i+3) = 2.*G
      enddo
      C(1,2) = lame1
      C(2,3) = lame1
      C(1,3) = lame1
      C(2,1) = lame1
      C(3,1) = lame1
      C(3,2) = lame1
C

      I6 = 0.
      do i=1,6
         I6(i,i) = 1.D0
      enddo

      Ivect = 0.D0
      do i=1,3
         Ivect(i) = 1.D0
      enddo

      call kron(Ivect,Ivect,6,IkronI)

      P4 = I6 - one3*IkronI

C
      call invert(C,6,Cinv)

      
C-----------------------------------------------------------------------------           
C	               Start element loop
C-----------------------------------------------------------------------------
      do k = 1,nblock
C-----------------------------------------------------------------------------
C                         Get trial stress
C-----------------------------------------------------------------------------
         tr    = strainInc(k,1) + strainInc(k,2) + strainInc(k,3)
         tsig(1) = stressOld(k,1) + lame1*tr + 2.D0*G*strainInc(k,1) 
         tsig(2) = stressOld(k,2) + lame1*tr + 2.D0*G*strainInc(k,2)
         tsig(3) = stressOld(k,3) + lame1*tr + 2.D0*G*strainInc(k,3)
         tsig(4) = stressOld(k,4) + 2.D0*G*strainInc(k,4)  
         tsig(5) = stressOld(k,5) + 2.D0*G*strainInc(k,5) 
         tsig(6) = stressOld(k,6) + 2.D0*G*strainInc(k,6)
C-----------------------------------------------------------------------------
C              Bring in the state variables and initialize
C-----------------------------------------------------------------------------
         epb0 = stateOld(k,1)
C         sy = sy0 + H*epb0
         sy = sy0*(1.D0+epb0/e0)**n
         H  = (n/e0)*sy0*(1.D0+epb0/e0)**(n-1.D0)


C-----------------------------------------------------------------------------
C              Get the trial pressure 
C-----------------------------------------------------------------------------
         tsigh = one3*(tsig(1)+tsig(2)+tsig(3))

C-----------------------------------------------------------------------------
C        Deviatoric Stress 
C-----------------------------------------------------------------------------
         tdev(1) = tsig(1) - tsigh
         tdev(2) = tsig(2) - tsigh
         tdev(3) = tsig(3) - tsigh
         tdev(4) = tsig(4)
         tdev(5) = tsig(5)
         tdev(6) = tsig(6)

         sijsij = tdev(1)**2.D0 + tdev(2)**2.D0 + tdev(3)**2.D0 + 
     &     2.D0*tdev(4)**2.D0 + 2.D0*tdev(5)**2.D0 + 2.D0*tdev(6)**2.D0 
         
         tsige = sqrt(three2*sijsij)


C-----------------------------------------------------------------------------
C        yield condition
C-----------------------------------------------------------------------------
	 fyield = tsige - sy
C-----------------------------------------------------------------------------
C        Get return direction for stress update and the unit normal
C-----------------------------------------------------------------------------

         rvec(1) = three2*tdev(1)/tsige
         rvec(2) = three2*tdev(2)/tsige
         rvec(3) = three2*tdev(3)/tsige 
         rvec(4) = three2*tdev(4)/tsige
         rvec(5) = three2*tdev(5)/tsige
         rvec(6) = three2*tdev(6)/tsige 
         if (DEBUG) then 
            rvec0(1) = stateOld(k,2)
            rvec0(2) = stateOld(k,3)
            rvec0(3) = stateOld(k,4)
            rvec0(4) = stateOld(k,5)
            rvec0(5) = stateOld(k,6)
            rvec0(6) = stateOld(k,7)
         endif
C-----------------------------------------------------------------------------
C        Get return direction  unit normal
C-----------------------------------------------------------------------------
         norm(1) = sqrt23*rvec(1)
         norm(2) = sqrt23*rvec(2)
         norm(3) = sqrt23*rvec(3)
         norm(4) = sqrt23*rvec(4)
         norm(5) = sqrt23*rvec(5)
         norm(6) = sqrt23*rvec(6)

C
C 	   AMMENDED RETURN DIRECTIONS lets keep this to see what it does
C
c$$$         trr = rvec(1)+rvec(2)+rvec(3)
c$$$         rvec(1) = rvec(1) - trr/3.D0 
c$$$         rvec(2) = rvec(2) - trr/3.D0 
c$$$         rvec(3) = rvec(3) - trr/3.D0 
C
C-----------------------------------------------------------------------------
C        I use the magnitude of the strain increment to help convergence
C-----------------------------------------------------------------------------
C         call dblcontr(strainInc(k,:),strainInc(k,:),effstr)
         plmul = sqrt(strainInc(k,1)**2.D0 + strainInc(k,2)**2.D0
     &           + strainInc(k,3)**2.D0 + 2.D0*(strainInc(k,4)**2.D0
     &           + strainInc(k,5)**2.D0 + strainInc(k,6)**2.D0))
         depb  = plmul
C-----------------------------------------------------------------------------
C           Check for yielding
C-----------------------------------------------------------------------------
         YIELD    = .FALSE.
C
            if ( fyield > 0.D0) then
C
                YIELD = .TRUE.
C            THIS WAS THE PROBLEM sige needed defined
                sig = tsig
                sige = tsige
                epb = epb0
                sigh = tsigh
                dev = tdev
C
            else
C
                YIELD = .FALSE.
C
                sig  = tsig
                sige = tsige
                epb   = epb0
C
            end if 
C-----------------------------------------------------------------------------            
C        Begin Minimization loop
C-----------------------------------------------------------------------------
         counter = 0
         do while (YIELD)
C           
            counter = counter + 1
C-----------------------------------------------------------------------------
C           Put a stopper in case minimzation fails (usually around 50 iterations)
C-----------------------------------------------------------------------------
            if (counter .GE. 50) then
                print *,'Did not minimize'
                go to 100
            end if
C-----------------------------------------------------------------------------
C           Get the plastic increment
C-----------------------------------------------------------------------------
C          Determine the inverse of the matrix A

           call kron(norm,norm,6,nxn)
    
C-----------------------------------------------------------------------------
C           dr/dsig
C-----------------------------------------------------------------------------						
            drdsig(1:6,1:6) =(three2/sige) * (P4(1:6,1:6) 
     &                     - nxn(1:6,1:6))
C-----------------------------------------------------------------------------
C           dr/dq1 
C-----------------------------------------------------------------------------
            drdq1(1:6) = 0.D0

C-----------------------------------------------------------------------------
C           dh/dsigma
C-----------------------------------------------------------------------------
            dhdsig(1:6) = 0.D0

C-----------------------------------------------------------------------------
C           dhdq
C-----------------------------------------------------------------------------
            dhdq = 0.D0

C-----------------------------------------------------------------------------
C           dfdq
C-----------------------------------------------------------------------------
            dfdq = -1.D0*H

C-----------------------------------------------------------------------------
C           h
C-----------------------------------------------------------------------------
            dqdlam = 1.D0

C-----------------------------------------------------------------------------
C            A matrix
C-----------------------------------------------------------------------------
           		
            Amat(1:6,1:6) = Cinv(1:6,1:6) + plmul*drdsig(1:6,1:6)
            Amat(1:6,7) = plmul*drdq1(1:6)       
C           dh/dsigma h = dq/dlambda
            Amat(7,1:6) = plmul*dhdsig(1:6) 
C           dh/dq - 1
            Amat(7,7) = plmul*dhdq - 1.D0

            call invert(Amat,7,Ainv)

C          Change this to the transpose based
C          on what i beleive is correct from anisoCA.f
            Ainv = transpose(Ainv)

            coef = 6.D0*G**2.D0*plmul/(sy + 3.D0 * G*plmul)
            AinvE(1:7,1:7) = 0.D0
            AinvE(1:6,1:6) = C - coef*(P4 - nxn)
            AinvE(7,7) = -1.D0

C ------------------------------------------------------------
C          Get the left and right multiplied direction vectors
C ------------------------------------------------------------
            rl(1:6) = rvec(1:6)
            rl(7) = dfdq
C
            rr(1:6) = rvec(1:6)
C           h (lower case) dq/dlambda
            rr(7)   = dqdlam

C           yield criteria
            fn    = fyield
C           matrix multiplier to get Jacobian denominator
            call sclrval(rl,rr,Ainv,7,den)
C
            plinc = fn/den

            plincE = fn/(3.D0*G + H)

            if (DEBUG) then
               A11 = (rvec(1) - rvec0(1))/(sig(1) - stressOld(k,1))
               print *, "========="
               print *, "drdsig_an", drdsig(1,1)
               print *, "drdsig_fd", A11
C               if (counter.eq.2) stop
            endif


C
C-----------------------------------------------------------------------------
C        Update state variables and plastic multiplier
C-----------------------------------------------------------------------------
            plmul = plmul + plinc
C
C           is this right?
C            depb  = plmul*sigret/sy
            depb = plmul*dqdlam
C
            epb = epb0 + depb

            if (DEBUG) then
               A11 = (rvec(1) - rvec0(1))/(depb)
               print *, "========="
               print *, "drdq1_an", drdq1(1)
               print *, "drdq1_fd", A11
C               if (counter.eq.2) stop
               A11 = (rvec(1) - rvec0(1))/(depb)
               print *, "========="
               print *, "dhdsig_an", dhdsig(1)
               print *, "dhdsig_fd", A11
            endif





C
C            sy = sy0 + H*epb
            sy = sy0*(1.D0+epb/e0)**n
            H  = (n/e0)*sy0*(1.D0+epb/e0)**(n-1.D0)
C-----------------------------------------------------------------------------
C           Update the stress measures
C-----------------------------------------------------------------------------
            trr = rvec(1) + rvec(2) + rvec(3)
            dsig(1) = plmul*lame1*trr + 2.D0*plmul*G*rvec(1)
            dsig(2) = plmul*lame1*trr + 2.D0*plmul*G*rvec(2)
            dsig(3) = plmul*lame1*trr + 2.D0*plmul*G*rvec(3)
            dsig(4) = 2.D0*plmul*G*rvec(4)
            dsig(5) = 2.D0*plmul*G*rvec(5)
            dsig(6) = 2.D0*plmul*G*rvec(6)

            sig(1:6) = tsig(1:6) - dsig(1:6)

C-----------------------------------------------------------------------------
C              Get the hydrostatic stress 
C-----------------------------------------------------------------------------
            sigh = one3*(sig(1)+sig(2)+sig(3))

C-----------------------------------------------------------------------------
C        Deviatoric Stress 
C-----------------------------------------------------------------------------
            dev(1) = sig(1) - sigh
            dev(2) = sig(2) - sigh
            dev(3) = sig(3) - sigh
            dev(4) = sig(4)
            dev(5) = sig(5)
            dev(6) = sig(6)

            sijsij = dev(1)**2.D0 + dev(2)**2.D0 + dev(3)**2.D0 + 
     &      2.D0*dev(4)**2.D0 + 2.D0*dev(5)**2.D0 + 2.D0*dev(6)**2.D0 
         
            sige = sqrt(three2*sijsij)
C           exact sige
            sigeE = tsige - 3.D0*G*plmul

C	    Yield Criteria
	    fyield = sige - sy
	
C-----------------------------------------------------------------------------
C        Get return direction for stress update and the unit normal
C-----------------------------------------------------------------------------

            rvec(1) = three2*dev(1)/sige
            rvec(2) = three2*dev(2)/sige
            rvec(3) = three2*dev(3)/sige 
            rvec(4) = three2*dev(4)/sige
            rvec(5) = three2*dev(5)/sige
            rvec(6) = three2*dev(6)/sige 

C-----------------------------------------------------------------------------
C        Get return direction  unit normal
C-----------------------------------------------------------------------------
            norm(1) = sqrt23*rvec(1)
            norm(2) = sqrt23*rvec(2)
            norm(3) = sqrt23*rvec(3)
            norm(4) = sqrt23*rvec(4)
            norm(5) = sqrt23*rvec(5)
            norm(6) = sqrt23*rvec(6)

C
C 	   AMMENDED RETURN DIRECTIONS lets keep this to see what it does
C
c$$$            trr = rvec(1)+rvec(2)+rvec(3)
c$$$            rvec(1) = rvec(1) - trr/3.D0 
c$$$            rvec(2) = rvec(2) - trr/3.D0 
c$$$            rvec(3) = rvec(3) - trr/3.D0 

C
C-----------------------------------------------------------------------------
C         Check the yield condition again
C         This was wrong, could it be the issue
C         change fn/sy to fncheck
C-----------------------------------------------------------------------------
            fncheck = abs(fyield/sy)
C
            if (fncheck > 1.D-9) then
                YIELD = .TRUE.
            else
                YIELD = .FALSE.
            end if
C-----------------------------------------------------------------------------
C        End minimization loop
C-----------------------------------------------------------------------------
         enddo
C-----------------------------------------------------------------------------
C               Exit procedure
C-----------------------------------------------------------------------------

C-----------------------------------------------------------------------------
C              Update new stresses 
C-----------------------------------------------------------------------------
100         stressNew(k,1) = sig(1)
            stressNew(k,2) = sig(2)
            stressNew(k,3) = sig(3)
            stressNew(k,4) = sig(4)
            stressNew(k,5) = sig(5)
            stressNew(k,6) = sig(6)
C-----------------------------------------------------------------------------
C              Update the internal variables
C-----------------------------------------------------------------------------

            stateNew(k,1)  = epb
            if (DEBUG) then
               stateNew(k,2)  = rvec(1)
               stateNew(k,3)  = rvec(2)
               stateNew(k,4)  = rvec(3)
               stateNew(k,5)  = rvec(4)
               stateNew(k,6)  = rvec(5)
               stateNew(k,7)  = rvec(6)
            endif
C-----------------------------------------------------------------------------
C              Verify the increment in plastic work
C-----------------------------------------------------------------------------
C            dep(1) = plmul*r(1)
C            dep(2) = plmul*r(2)
C            dep(3) = plmul*r(3)
C            dep(4) = plmul*r(4)
C            dep(5) = plmul*r(5) 
C            dep(6) = plmul*r(6)
C
C            dwp1 = sig(1)*dep(1)+sig(2)*dep(2)+sig(3)*dep(3)
C     &             +2.*(sig(4)*dep(4)
C     &             +sig(5)*dep(5)+sig(6)*dep(6))
C            dwp2 = (1.-f)*sy*depb
C            if (epb.GT.0.) then
C               stateNew(k,4) = dwp1-dwp2
C            endif
C-----------------------------------------------------------------------------
C     End of subroutine
C-----------------------------------------------------------------------------
      enddo
      return
      end
C #============================================================================
C #============================================================================
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine invert(A,n,Ainv)
C A is an nxn matrix, n is the matrix dimension, Ainv is A**(-1)
C this subroutin uses Gaussian elimination to determine the inverse
C of an nxn square matrix
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C      include 'vaba_param.inc'
      implicit none

      INTEGER :: i,n,k,counter,j,g
      REAL*8 :: A(n,n),Ainv(n,n),Atemp(n,n),Id(n,n),den,mul

         Id = 0.
         do i=1,n
            Id(i,i) = 1.
         enddo

         Ainv = Id

         Atemp = A

         if (sum(A) .EQ. 0.) then
            Ainv = A;
         else
C zero out lower left diagonal
            do i=1,n
C set the ith column in the ith row equal to 1
               if (Atemp(i,i) .NE. 1.) then
                  if (Atemp(i,i) .NE. 0.) then          
                     den = Atemp(i,i)
                     Atemp(i,:) = Atemp(i,:)/den
                     Ainv(i,:) = Ainv(i,:)/den
                  else
                     if (i .EQ. n) then
                        stop 'matrix is singular'
                     endif
                     do k=i+1,n
                        if (Atemp(k,i) .NE. 0.) then
                           counter = k
                           exit
                        endif
                        if (k .EQ. n) then
                           stop 'matrix is singular'
                        endif
                     enddo
                     den = Atemp(counter,i)
                     Atemp(i,:) = Atemp(counter,:)/den + Atemp(i,:)
                     Ainv(i,:) = Ainv(counter,:)/den + Ainv(i,:)
                  endif
               endif
C set the ith column in all rows > i equal to 0
               do k=i+1,n
                  if (Atemp(k,i) .NE. 0.) then
                     mul = Atemp(k,i)
                     Atemp(k,:) = -mul*Atemp(i,:) + Atemp(k,:)
                     Ainv(k,:) = -mul*Ainv(i,:) + Ainv(k,:)
                  endif
               enddo
           enddo
C zero out upper right diagonal
           do i=2,n
              j = n - i + 2
              do k=1,j-1
                 g = j - k
                 if (Atemp(g,j) .NE. 0.) then
                    mul = Atemp(g,j)
                    Atemp(g,:) = -mul*Atemp(j,:) + Atemp(g,:)
                    Ainv(g,:) = -mul*Ainv(j,:) + Ainv(g,:)
                 endif
              enddo
           enddo
        endif

      return

      end subroutine invert

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine kron(a,b,n,C)

C this subroutine takes the kronecker product
C of two vectors a and b and gives back an nxn
C matrix C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C      include 'vaba_param.inc'

      implicit none

      INTEGER :: n,i,j
      REAL*8 :: a(n),b(n),C(n,n)

         do i=1,n
            do j=1,n
               C(i,j) = a(i)*b(j)
            enddo
         enddo
   
      return
 
      end subroutine kron


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine sclrval(av,bv,Amat,n,sclr)

C     This subroutine takes two vectors av and bv and
C a matrix Amat and determines the scalar product av*Amat*bv
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C      include 'vaba_param.inc'
      implicit none

      INTEGER :: n, i , j
      REAL*8 :: sclr,av(n),bv(n),Amat(n,n),temp(n),elsum

         do i=1,n
            elsum = 0.
            do j=1,n
               elsum = elsum + av(j)*Amat(j,i)
            enddo
            temp(i) = elsum
         enddo

         sclr = 0.
         do i=1,n
            sclr = sclr + temp(i)*bv(i)
         enddo

      return

      end subroutine sclrval
C
C
C
      subroutine dblcontr(A,B,sclr)
 
C      include 'vaba_param.inc'
      implicit none

      integer i
      REAL*8 :: A(6),B(6),sclr

         sclr = 0.
         do i=1,6
            if (i.LE.3) then
               sclr = sclr + A(i)*B(i)
            else
               sclr = sclr + 2.*A(i)*B(i)
            endif
         enddo

      return

      end subroutine

      subroutine dblcontr3x6(A,B,sclr)
 
C      include 'vaba_param.inc'
      implicit none

      integer i
      REAL*8 :: A(3),B(6),sclr

         sclr = 0.
         do i=1,3
            sclr = sclr + A(i)*B(i)
         enddo

      return

      end subroutine


C Determine eigenvalues, Aeig,  and eigenvectors, Geig, of the matrix A
C to within a tolerance, tol using Jacobi iteration method
C
C to reproduce the orginal matrix:
C
C                A =  matmul(matmul(Geig,Aeig),transpose(Geig))
C
      subroutine eig(A,tol,Aeig,Geig)

C        include 'vaba_param.inc'
        implicit none

        integer i

        real*8 conv,A(3,3),Aeig(3,3),G(3,3),Geig(3,3),tol,maxv,tau,t,c,s
        real*8 Aeig1,Aeig2,Aeig3,Geig1(3),Geig2(3),Geig3(3),Gcount1
        real*8 Gcount2,maxvo
        integer counter

        Gcount1 = 0.
        Gcount2 = 0.

        conv = sqrt(A(1,2)**2. + A(1,3)**2. + A(2,3)**2.)
        Aeig = A
        Geig = 0.
        do i=1,3
           Geig(i,i) = 1.
        enddo
 
        maxvo = 1e25
        counter = 0
        do while(conv.GT.tol)
           counter = counter + 1
           if (counter.GE.50) then
              print *,'eigenvalue minimization failed',conv
              goto 100
           endif
           G = 0.
           maxv = max(abs(Aeig(1,2)),abs(Aeig(1,3)),abs(Aeig(2,3)))
           if (maxv.EQ.abs(Aeig(1,2))) then
              tau = (Aeig(1,1) - Aeig(2,2))/(2.*Aeig(1,2))
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)
              G(1,1) = c
              G(2,2) = c
              G(1,2) = -s
              G(2,1) = s
              G(3,3) = 1.
           elseif (maxv.EQ.abs(Aeig(1,3))) then
              tau = (Aeig(1,1) - Aeig(3,3))/(2.*Aeig(1,3))       
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)        
              G(1,1) = c
              G(3,3) = c
              G(1,3) = -s
              G(3,1) = s
              G(2,2) = 1.        
           else
              tau = (Aeig(2,2) - Aeig(3,3))/(2.*Aeig(2,3))
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)        
              G(2,2) = c
              G(3,3) = c
              G(2,3) = -s
              G(3,2) = s
              G(1,1) = 1.        
           endif
           maxvo = maxv
           Aeig = matmul(matmul(G,Aeig),transpose(G))
           Geig = matmul(Geig,transpose(G))
           conv = sqrt(Aeig(1,2)**2. + Aeig(1,3)**2. + Aeig(2,3)**2.)
        enddo

C reorganize the principal values into max, min, mid

        Aeig1 = max(Aeig(1,1),Aeig(2,2),Aeig(3,3))
        Aeig2 = min(Aeig(1,1),Aeig(2,2),Aeig(3,3))

        do i=1,3
           if ((Aeig1.EQ.Aeig(i,i)).AND.(Gcount1.EQ.0.)) then
              Geig1 = Geig(:,i)
              Gcount1 = 1.
           elseif ((Aeig2.EQ.Aeig(i,i)).AND.(Gcount2.EQ.0.)) then
              Geig2 = Geig(:,i)
              Gcount2 = 1.
           else
              Aeig3 = Aeig(i,i)
              Geig3 = Geig(:,i)
           endif
        enddo

        Aeig = 0.
        Aeig(1,1) = Aeig1
        Aeig(2,2) = Aeig2
        Aeig(3,3) = Aeig3
        Geig(:,1) = Geig1
        Geig(:,2) = Geig2
        Geig(:,3) = Geig3

100     return

      end subroutine eig
