#!/usr/local/bin/python3

import numpy as np
import matplotlib.pyplot as plt

def forwardDiff(x,y):
    dydx = np.zeros(len(y))
    for i in range(len(y)-1):
        if (x[i+1] - x[i]) > 1.e-12:
            dydx[i] = (y[i+1] - y[i]) / (x[i+1] - x[i])
        else:
            dydx[i] = 0.0
    return dydx

def centralDiff(x,y):
    dydx = np.zeros(len(y))
    for i in range(1,len(y)-1):
        if (x[i+1] - x[i-1]) > 1.e-12:
            dydx[i] = (y[i+1] - y[i-1]) / (x[i+1] - x[i-1])
        else:
            dydx[i] = 0.0
    return dydx


resultsFiles = ['tanDebug.dat']
lineStyles = ['-','--']
counter = 0
for resultsFile in resultsFiles:

    data = np.loadtxt(resultsFile)

    time = data[:,0]

    Dd = data[:,1]
    sig11 = data[:,2]
    dDddsig11 = data[:,3]
    Jd = data[:,4]
    dDddJd = data[:,5]

    dDddJdPython = np.zeros(len(Jd))
    for i in range(1,len(Jd)):
        dt = time[i]- time[i-1]
        dDddJdPython[i] = (1./3.)*Jd[i-1]/(dt*Jd[i]*Jd[i])
    
    dDddsigFD = centralDiff(sig11,Dd)
    dDddJdFD = centralDiff(Jd,Dd)
 
    plt.rcParams.update({'font.size': 16})
    plt.figure(1)
    plt.plot(sig11,Dd,lineStyles[counter])

    plt.figure(2)

    plt.plot(sig11,dDddsig11,'-k' )
    plt.plot(sig11,dDddsigFD,'--r')

    plt.figure(3)
    plt.plot(Jd,Dd,'k-')

    plt.figure(4)
    plt.plot(Jd,dDddJd,'k-')
    plt.plot(Jd,dDddJdFD,'--r')
    #plt.plot(Jd,dDddJdPython,'--m')
    counter = counter + 1

plt.rcParams.update({'font.size': 16})
plt.figure(1)
plt.ylabel(r'$D^d$')
plt.xlabel(r'$\sigma_{11}$')
plt.gcf().subplots_adjust(left=0.20)
plt.gcf().subplots_adjust(bottom=0.15)


plt.rcParams.update({'font.size': 16})
plt.figure(2)
plt.xlabel(r'$\sigma_{11}$')
plt.ylabel(r'$\partial D^d / \partial \sigma$')
plt.xlim([0, 3000])
plt.ylim([-0.0000001, 0.0000001])
plt.gcf().subplots_adjust(left=0.30)
plt.gcf().subplots_adjust(bottom=0.15)
plt.legend(['an','fd'])

plt.rcParams.update({'font.size': 16})
plt.figure(3)
plt.xlabel(r'$J_d$')
plt.ylabel(r'$D^d$')
plt.gcf().subplots_adjust(left=0.20)
plt.gcf().subplots_adjust(bottom=0.15)

plt.figure(4)
plt.xlabel(r'$J_d$')
plt.ylabel(r'$\partial D^d / \partial J_d$')
plt.gcf().subplots_adjust(left=0.20)
plt.gcf().subplots_adjust(bottom=0.15)
#plt.ylim([0.,10.])
plt.legend(['an','fd'])

plt.figure(5)
plt.plot(Jd,dDddJd/dDddJdFD)

plt.show()
