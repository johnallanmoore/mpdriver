c===================================================================
c
c  This UMAT subroutine implements the following model in ABAQUS.
c
c-------------------------------------------------------------------
C******************************************************************
C This code is being modified to incorporate the effects of BCT crystal
C plasticity and for Fe-C materials. Modification start date 10/2006
C incorporate the slip effects of BCC and FCC together

      subroutine umat (stress,  statev,  ddsdde,  sse,     spd,
     &			scd,     rpl,     ddsddt,  drplde,  drpldt,
     &			strain,  dstrain, time,    dtime,   temp,
     &			dtemp,   predef,  dpred,   cmname,  ndi,
     &			nshr,    ntens,   nstatv,  props,   nprops,
     &			coords,  drot,    pnewdt,  celent,  dfgrd0,
     &			dfgrd1,  noel,    npt,     layer,   kspt,
     &			kstep,   kinc )

C      implicit double precision (a-h,o-z)
      implicit none

      real*8 stress,  statev,  ddsdde,  sse,     spd
      real*8 scd,     rpl,     ddsddt,  drplde,  drpldt
      real*8 strain,  dstrain, time,    dtime,   temp
      real*8 dtemp,   predef,  dpred,  props
      real*8 coords,  drot,    pnewdt,  celent,  dfgrd0
      real*8 dfgrd1

      integer ndi, nshr, ntens, nstatv, nprops, npt, layer
      integer kspt, kstep, kinc, noel, iguess

      integer i,j,k,m ,n,l, N_incr, ia, ib, N_ctr 
      integer max_grains, max_loops, num_slip_sys
      integer num_slip_sys1
      real*8 Pi, Tolerance, C11, C12, C13, C33, C44
      real*8 gamma_dot_zero, flow_exp, g_zero
      real*8 Hdir, igrn, xLatent, Adir, iopt
      real*8 mat_data, theta_ang
      real*8 y,z, a_zero, psi_ang, phi_ang, del
      real*8 sum, psi, g0,  E_p, E_p_eff
      real*8 s1, c1,  a0, F_p_inv_0, E_eff
      real*8 E_p_eff_cum, s2, c2,s3,c3
      real*8 dir_cos,C0,  C, xs0,xm0
      real*8 g, a, F0, F1, F_el, F_el_inv
      real*8 xs, xm, array1, E_el, Spk2
      real*8 array2, sig,  tau, gamma_dot
      real*8 dTaudGd, F_p_inv, g_sat, func
      real*8 xL_p, sse_ref, t1, dgadgb, daadga
      real*8 array3, grad,d_gamma_dot, sse_old
      real*8 gamma_try,  ddpdsig,dDddsig, dDidsig, E_tot
      real*8 trace, sig_eff, array6, Array4, Array5
      real*8 delta_gamma, delta_E_p, sum1, ddsdde_4th
      real*8 sum2, array7, dlpdsd, F_dot, F_inv, sig0, xL
      real*8 g1,g2,g3,g4, caRatio, a1(1,3), a2(1,3), a3(1,3)
      real*8 Ma(3,3), detM, b1(1,3), b2(1,3), b3(1,3)
      real*8 b1T(3,1), b2T(3,1), b3T(3,1), gSlip(3,1)
      real*8 a1T(3,1), a2T(3,1), a3T(3,1), dSlip(3,1)
      real*8 da(3,1), fdot0, dfdotdf, Dd
C     Functions
      real*8 determinant, power, sgn
C     IS Fp, g_sat, xL_p, Array7, dlpdsd, 
C     F_dot, F_inv, sig0, xL never defined
      parameter(max_grains	= 1,
     &		max_loops	= 10,
     &		Tolerance	= 0.00001,
     &        num_slip_sys1=24)

      character*8 cmname, ans
      logical Converged, Improved

c-------------------------------------------------------------------
c  Dimension arrays passed into the UMAT sub
c-------------------------------------------------------------------

      dimension
     &	coords(3),	! Coordinates of Gauss pt. being evaluated
     &	ddsdde(ntens,ntens), ! Tangent Stiffness Matrix
     &	ddsddt(ntens),	! Change in stress per change in temperature
     &	dfgrd0(3,3),	! Deformation gradient at beginning of step
     &	dfgrd1(3,3),	! Deformation gradient at end of step
     &	dpred(1),	! Change in predefined state variables
     &	drplde(ntens),	! Change in heat generation per change in strain
     &	drot(3,3),	! Rotation matrix
     &	dstrain(ntens),	! Strain increment tensor stored in vector form
     &	predef(1),	! Predefined state vars dependent on field variables
     &	props(nprops),	! Material properties passed in
     &	statev(nstatv),	! State Variables
     &	strain(ntens),	! Strain tensor stored in vector form
     &	stress(ntens),	! Cauchy stress tensor stored in vector form
     &	time(2)		! Step Time and Total Time
                                                                                
c-------------------------------------------------------------------            
c  Dimension other arrays used in this UMAT
c-------------------------------------------------------------------            

      dimension
     &	array1	(3,3),		! Dummy array
     &	array2	(3,3),		! Dummy array
     &	array3	(num_slip_sys1,num_slip_sys1), ! A_matrix of Newton Raphson
     &	array4	(6,6),		! Dummy array used in Voigt notation
     &	array5	(6,6),		! Inverse of array4().
     &	array6	(3,3,3,3),	! 4th rank dummy array
     &	array7	(3,3,3,3),	! Another 4th rank dummy array
     &	a0	(num_slip_sys1), ! Kinematic stress at beginning of step
     &	a	(num_slip_sys1), ! Kinematic stress at end of step
     &	C0	(3,3,3,3),	! Local  4th rank elastic stiffness tensor
     &	C	(3,3,3,3),	! Global 4th rank elastic stiffness tensor
     &	del	(3,3),		! Kronecker delta tensor
     &	ddpdsig	(3,3,3,3),	! deriv of D_p wrt sig * dt
     &	ddsdde_4th(3,3,3,3),	! 4th rank tangent stiffness tensor.
     &	daadga	(num_slip_sys1),	! deriv of a_alpha wrt del_gamma_alpha
     &	dgadgb	(num_slip_sys1,num_slip_sys1),! deriv of g_alpha wrt del_gamma_beta
     &	d_gamma_dot(num_slip_sys1),! Gamma_dot step from Newt-Raphson
     &	dlpdsd	(3,3,3,3),	! deriv of xL_p wrt sigma_dot
     &	dir_cos	(3,3),! Direction cosines 
     &	dTaudGd	(num_slip_sys1,num_slip_sys1), ! Deriv of Tau wrt Gamma_dot
     &	E_el	(3,3),		! Elastic Green strain tensor
     &	E_tot	(3,3),		! Green strain tensor E_el + E_p
     &	F0	(3,3),		! F at beginning of sub increment
     &	F1	(3,3),		! F at end of sub increment
     &	F_el	(3,3),		! Elastic part of F
     &	F_el_inv(3,3),		! Inverse of elastic part of F
     &	F_dot	(3,3),		! F-dot
     &	F_inv	(3,3),		! Inverse of deformation gradient
     &	F_p_inv_0(3,3),! Inverse of plastic part of F at beginning
     &	F_p_inv(3,3),! Inverse of plastic part of F at end of step

     &	func	(num_slip_sys1),	! Function that is solved to get gamma_dot(k)
     &	g0	(num_slip_sys1), ! Ref stress at beginning of step
     &	g	(num_slip_sys1), ! Ref stress at end of step
     &	gamma_dot(num_slip_sys1),! shear strain rate on system
     &	gamma_try(num_slip_sys1),! Candidate gamma_dots for line search
     &	grad(num_slip_sys1),	! gradient of sum-sqares-error
     &  psi	(3),            ! Euler angles
     &	sig0	(3,3),		! Stress tensor at beginning of step.
     &	sig	(3,3),		! Stress tensor
     &	Spk2	(3,3),		! 2nd Piola Kirkhoff stress
     &	tau	(num_slip_sys1),! Resolved shear stress for slip dir.
     &	xL	(3,3),		! Eulerian velocity gradient
     &	xL_p	(3,3),		! Plastic vel grad in current configuration
     &	xs0	(3,num_slip_sys1),! Inter config slip directions in global coords
     &	xs	(3,num_slip_sys1),! Current config slip directions in global coords
     &	xm0	(3,num_slip_sys1),! Inter config plane normals in global coords
     &	xm	(3,num_slip_sys1),! Current config plane normals in global coords
     &	y	(3,num_slip_sys1),! Miller indices of slip plane normals 
     &	z	(3,num_slip_sys1), ! Miller indices of slip directions
     &  delta_E_p(3,3),         !increment of plastic strain tensor
     &  delta_gamma(num_slip_sys1), !increment of shear strain on each slip sys
     &	E_p(3,3),
     &	dDddsig	(3,3,3,3),	! deriv of D_d wrt sig * dt
     &	dDidsig	(3,3,3,3)	! deriv of D_inelastic wrt sig * dt
      REAL*8 omega, dt_incr, Hdyn,Adyn
      INTEGER N_incr_total, counter, iporo

      real*8 F_el_invT(3,3), EdotFinv(3,3)
      real*8 Dp(3,3),Dp_eff, sigDp 

      real*8 pc1,pc2, fini, pf0, pf, pfn, Jd, kirch(3,3)
      real*8 one3, pkirch, hh1, hh3, two3, mExp
      real*8 am, bm, OmegaS, nhalf, skirch(3,3)
      real*8 sinhbmt, coshbmt, dtheta, T
      real*8 wmm(num_slip_sys1)

      real*8 trDd, sumSlip, sumG, fdotca, pfGuess
      real*8 JdGuess, func_pf, Jd0
      real*8 dfdtrDd, ddthetadf, dfdotcadf
      real*8 drfdtrDd, dfdotcadgdot
      real*8 dsumGdg(num_slip_sys1), dsumGdgdot(num_slip_sys1)
      real*8 dfdotcadp
      real*8 d_trDd, drgdtrDd(num_slip_sys1)
      real*8 drfdgdot(num_slip_sys1)
      real*8 dfdotcadsumG, trDd_try, Je, Jddot, Jddot0
      real*8 FdInV(3,3),FFdInv(3,3),FdTerm, Aterm
      real*8 pftol, rf, df, drfdf, flowStress
      real*8 deleteFlag, wmmAve, wmmMin, Jtot
      real*8 bulk,pEOS, Bterm, dAdf, dx0(24),dx1(12)
      real*8 dddTerm,arraydx1(12,12), arraydx0(24,24)
      real*8 Jd0pEOS,dddTermDt, dDddJd, Dd0, sig110

C     Adyn is never defined
C     Hdyn is never defined
C     why is is looping over grains
C     why is it storing 4th order C tensor
C     why is it calculating a 4th order c tensor
C     begin loop over grains needs removed
C     what is N_incr_total
C     How is F0 and F1 calculated
      one3 = 1.D0/3.D0
      two3 = 2.D0/3.D0
      Pi = 4.D0 * atan(1.D0)
      nhalf = -0.5D0
      wmmMin = 0.01D0
C     according to MMPDS elongation for Ti6Al4V is 5-10%

c-------------------------------------------------------------------
c   Determine number of grains.  Divide the total number of ISVs
c   by the number of ISVs per grain.
c   3 - Euler angles
c   9 - F_p(i,j)
c  12 - g(i) for 12 slip systems
c  12 - a(i) for 12 slip systems
c ----
c  36
c-------------------------------------------------------------------

c      num_grains = nstatv / 40

      sig110 = stress(1)
      
c-------------------------------------------------------------------
c  Assign props() array to logical variable names
C... Props(16) is the parameter to identify whether BCC or FCC
C.... Props(8) parameter for multiple elements per grain
C     Props(8)=0 single element in one grain
C     Props(8)=1 and above for multple element for one grain
C... Props(12) parameter to identify for polycrystal and single cryst anal
c    Props(12)=0 polycrystal with orientations for subroutine RANGRN
C....Props(12)=1 Polycrystal with multiple elements per grain
C....Props(12)=2 polycrystal with orientations from .inp file
C... Props(12)=3 single crystal analysis
c-------------------------------------------------------------------

      C11	= props(1)
      C12	= props(2)
      C13       = props(3)
      C33       = props(4)  
      C44	= props(5)
      gamma_dot_zero= props(6)
      flow_exp	= props(7)
      g_zero	= props(8)
      Hdir	= props(9)
      Hdyn      = props(10)
      igrn	= props(11)
      xLatent	= props(12)
      a_zero    = props(13)
      Adir      = props(14)
      Adyn      = props(15)
      iopt= props(16) !!! options for different grain analysis
      mat_data=props(17)   ! 0 = hcp, 1 = bcc
      psi_ang   = props(18)
      theta_ang = props(19)
      phi_ang   = props(20)
      g1        = props(21)! basal system strength weight
      g2        = props(22)! prism system weight
      g3        = props(23)! pyramid <a> system weight
      g4        = props(24)! pyramid <a+c> system weight
      caRatio   = props(25)! ratio of c length to a length
      pc1       = props(26)
      pc2       = props(27)
      fini      = props(28)
	
c... assign the number of slip systems
c      if(mat_data.eq.1)  num_slip_sys1	= 12 !!! FCC
c	if(mat_data.eq.2) num_slip_sys1	= 48 !!! BCC
c.... added for polycrystal analysis
 
c-------------------------------------------------------------------
c  Initialize Kronnecker delta tensor
c-------------------------------------------------------------------

      do i = 1,3
         do j = 1,3
            del(i,j) = 0.0D0
         end do
         del(i,i) = 1.0D0
      end do

      if (mat_data.eq.1) then 
C        Bcc material
         num_slip_sys	= 12
C        y = plane, z = direction
C........(110)[1-11]
         y(1,1)= 1.0D0
         y(2,1)= 1.0D0
         y(3,1)= 0.0D0
         z(1,1)= 1.0D0
         z(2,1)=-1.0D0
         z(3,1)= 1.0D0
C........(110) [-111]
         y(1,2)= 1.0D0
         y(2,2)= 1.0D0
         y(3,2)= 0.0D0
         z(1,2)=-1.0D0
         z(2,2)= 1.0D0
         z(3,2)= 1.0D0
C.........(101)[-111]
         y(1,3)= 1.0D0
         y(2,3)= 0.0D0
         y(3,3)= 1.0D0
         z(1,3)=-1.0D0
         z(2,3)= 1.0D0
         z(3,3)= 1.0D0
C.........(101)[11-1]
         y(1,4)= 1.0D0
         y(2,4)= 0.0D0
         y(3,4)= 1.0D0
         z(1,4)= 1.0D0
         z(2,4)= 1.0D0
         z(3,4)=-1.0D0
C..........(1-10)[111]
         y(1,5)= 1.0D0
         y(2,5)=-1.0D0
         y(3,5)= 0.0D0
         z(1,5)= 1.0D0
         z(2,5)= 1.0D0
         z(3,5)= 1.0D0
C..........(1-10)[11-1]
         y(1,6)= 1.0D0
         y(2,6)=-1.0D0
         y(3,6)= 0.0D0
         z(1,6)= 1.0D0
         z(2,6)= 1.0D0
         z(3,6)= -1.0D0
C.........(01-1)[111]
         y(1,7)= 0.0D0
         y(2,7)= 1.0D0
         y(3,7)=-1.0D0
         z(1,7)= 1.0D0
         z(2,7)= 1.0D0
         z(3,7)= 1.0D0
C.........(01-1)[-111]
         y(1,8)= 0.0D0
         y(2,8)= 1.0D0
         y(3,8)=-1.0D0
         z(1,8)=-1.0D0
         z(2,8)= 1.0D0
         z(3,8)= 1.0D0
C..........(-101)[111]
         y(1,9)=-1.0D0
         y(2,9)= 0.0D0
         y(3,9)= 1.0D0
         z(1,9)= 1.0D0
         z(2,9)= 1.0D0
         z(3,9)= 1.0D0
C..........(-101)[1-11]
         y(1,10)=-1.0D0
         y(2,10)= 0.0D0
         y(3,10)= 1.0D0
         z(1,10)= 1.0D0
         z(2,10)=-1.0D0
         z(3,10)= 1.0D0
C.......(011)[1-11]
         y(1,11)= 0.0D0
         y(2,11)= 1.0D0
         y(3,11)= 1.0D0
         z(1,11)= 1.0D0
         z(2,11)=-1.0D0
         z(3,11)= 1.0D0
C........(011)[11-1]
         y(1,12)= 0.0D0
         y(2,12)= 1.0D0
         y(3,12)= 1.0D0
         z(1,12)= 1.0D0
         z(2,12)= 1.0D0
         z(3,12)=-1.0D0
      else
C        HCP material
         num_slip_sys	= 24
c-------------------------------------------------------------------
c  Assign slip system normals and slip directions for an HCP.
c-------------------------------------------------------------------
c    for Ti6Al4V the slip systems are
C    Basal <11-20>(0002)
C ... Prism <11-20>{10-10}
C ... Pyramid <a> <11-20>{10-11}
C ... Pyramid <a+c> <11-23>{10-11}
C     y = slip normal miller indices
C     z = slip direction m
C.... Basal Slip System
C......................
	 y(1,1)=0.0D0
	 y(2,1)=0.0D0
         y(3,1)=2.0D0
         z(1,1)=3.0D0
         z(2,1)=3.0D0
         z(3,1)=0.0D0
C......................
         y(1,2)=0.0D0
         y(2,2)=0.0D0
         y(3,2)=2.0D0
         z(1,2)=-3.0D0
         z(2,2)=0.0D0
         z(3,2)=0.0D0
C......................
         y(1,3)=0.0D0
         y(2,3)=0.0D0
         y(3,3)=2.0D0
         z(1,3)=0.0D0
         z(2,3)=-3.0D0
         z(3,3)=0.0D0
C.... Prism Slip System
C......................
         y(1,4)=1.0D0
         y(2,4)=-1.0D0
         y(3,4)=0.0D0
         z(1,4)=3.0D0
         z(2,4)=3.0D0
         z(3,4)=0.0D0
C......................
         y(1,5)=0.0D0
         y(2,5)=1.0D0
         y(3,5)=0.0D0
         z(1,5)=-3.0D0
         z(2,5)=0.0D0
         z(3,5)=0.0D0
C......................
         y(1,6)=1.0D0
         y(2,6)=0.0D0
         y(3,6)=0.0D0
         z(1,6)=0.0D0
         z(2,6)=-3.0D0
         z(3,6)=0.0D0
C.... Pyramid <a> Slip System
C......................
         y(1,7)=1.0D0
         y(2,7)=-1.0D0
         y(3,7)=1.0D0
         z(1,7)=3.0D0
         z(2,7)=3.0D0
         z(3,7)=0.0D0
C......................
         y(1,8)=-1.0D0
         y(2,8)=1.0D0
         y(3,8)=1.0D0
         z(1,8)=3.0D0
         z(2,8)=3.0D0
         z(3,8)=0.0D0
C......................
         y(1,9)=0.0D0
         y(2,9)=-1.0D0
         y(3,9)=1.0D0
         z(1,9)=-3.0D0
         z(2,9)=0.0D0
         z(3,9)=0.0D0
C......................
         y(1,10)=0.0D0
         y(2,10)=1.0D0
         y(3,10)=1.0D0
         z(1,10)=-3.0D0
         z(2,10)=0.0D0
         z(3,10)=0.0D0
C......................
         y(1,11)=-1.0D0
         y(2,11)=0.0D0
         y(3,11)=1.0D0
         z(1,11)=0.0D0
         z(2,11)=-3.0D0
         z(3,11)=0.0D0
C......................
         y(1,12)=1.0D0
         y(2,12)=0.0D0
         y(3,12)=1.0D0
         z(1,12)=0.0D0
         z(2,12)=-3.0D0
         z(3,12)=0.0D0
C.... Pyramid <a+c> Slip System
C......................
         y(1,13)=1.0D0
         y(2,13)=-1.0D0
         y(3,13)=1.0D0
         z(1,13)=0.0D0
         z(2,13)=-3.0D0
         z(3,13)=-3.0D0
C......................
         y(1,14)=1.0D0
         y(2,14)=-1.0D0
         y(3,14)=1.0D0
         z(1,14)=-3.0D0
         z(2,14)=0.0D0
         z(3,14)=3.0D0
C......................
         y(1,15)=-1.0D0
         y(2,15)=1.0D0
         y(3,15)=1.0D0
         z(1,15)=0.0D0
         z(2,15)=-3.0D0
         z(3,15)=3.0D0
C......................
         y(1,16)=-1.0D0
         y(2,16)=1.0D0
         y(3,16)=1.0D0
         z(1,16)=-3.0D0
         z(2,16)=0.0D0
         z(3,16)=-3.0D0
C......................
         y(1,17)=0.0D0
         y(2,17)=-1.0D0
         y(3,17)=1.0D0
         z(1,17)=0.0D0
         z(2,17)=-3.0D0
         z(3,17)=-3.0D0
C......................
         y(1,18)=0.0D0
         y(2,18)=-1.0D0
         y(3,18)=1.0D0
         z(1,18)=3.0D0
         z(2,18)=3.0D0
         z(3,18)=3.0D0
C......................
         y(1,19)=0.0D0
         y(2,19)=1.0D0
         y(3,19)=1.0D0
         z(1,19)=0.0D0
         z(2,19)=-3.0D0
         z(3,19)=3.0D0
C......................
         y(1,20)=0.0D0
         y(2,20)=1.0D0
         y(3,20)=1.0D0
         z(1,20)=3.0D0
         z(2,20)=3.0D0
         z(3,20)=-3.0D0
C......................
         y(1,21)=-1.0D0
         y(2,21)=0.0D0
         y(3,21)=1.0D0
         z(1,21)=3.0D0
         z(2,21)=3.0D0
         z(3,21)=3.0D0
C......................
         y(1,22)=-1.0D0
         y(2,22)=0.0D0
         y(3,22)=1.0D0
         z(1,22)=-3.0D0
         z(2,22)=0.0D0
         z(3,22)=-3.0D0
C......................
         y(1,23)=1.0D0
         y(2,23)=0.0D0
         y(3,23)=1.0D0
         z(1,23)=-3.0D0
         z(2,23)=0.0D0
         z(3,23)=3.0D0
C......................
         y(1,24)=1.0D0
         y(2,24)=0.0D0
         y(3,24)=1.0D0
         z(1,24)=3.0D0
         z(2,24)=3.0D0
         z(3,24)=-3.0D0
c-------------------------------------------------------------------
c  Convert miller indices to direction and normal plane vectors
C  which are not the same for hcp
c-------------------------------------------------------------------
C     hcp lattice vectors
         a1(1,1:3) = (/1.D0, 0.D0, 0.D0/)
         a2(1,1:3) = (/-0.5D0, 8.66025404D-1, 0.0D0/)
         a3(1,1:3) = (/0.D0, 0.D0, caRatio/)

C     matrix of lattice vectors
         Ma(1,1) = a1(1,1)
         Ma(2,1) = a1(1,2)
         Ma(3,1) = a1(1,3)
         Ma(1,2) = a2(1,1)
         Ma(2,2) = a2(1,2)
         Ma(3,2) = a2(1,3)
         Ma(1,3) = a3(1,1)
         Ma(2,3) = a3(1,2)
         Ma(3,3) = a3(1,3)

         detM = determinant(Ma)

         call cross_product(a2(1,1),a2(1,2),a2(1,3)
     1     ,a3(1,1),a3(1,2),a3(1,3)
     2     ,b1(1,1),b1(1,2),b1(1,3))

         call cross_product(a3(1,1),a3(1,2),a3(1,3)
     1     ,a1(1,1),a1(1,2),a1(1,3)
     2     ,b2(1,1),b2(1,2),b2(1,3))

         call cross_product(a1(1,1),a1(1,2),a1(1,3)
     1     ,a2(1,1),a2(1,2),a2(1,3)
     2     ,b3(1,1),b3(1,2),b3(1,3))

         b1 = b1/detM
         b2 = b2/detM
         b3 = b3/detM

         b1T = transpose(b1)
         b2T = transpose(b2)
         b3T = transpose(b3)

         a1T = transpose(a1)
         a2T = transpose(a2)
         a3T = transpose(a3)

c-------------------------------------------------------------------
C  convert miller indices to cartesean vectors and
c  Normalise the Miller indices to length one.
c-------------------------------------------------------------------
         do i = 1,num_slip_sys
C        convert slip direction z to cubic x,y,z
            dSlip = z(1,i)*a1T + z(2,i)*a2T + z(3,i)*a3T
            z(1,i) = dSlip(1,1)
            z(2,i) = dSlip(2,1)
            z(3,i) = dSlip(3,1)

C        convert slip normal y to cubic x,y,z
            gSlip = y(1,i)*b1T + y(2,i)*b2T + y(3,i)*b3T
            y(1,i) = gSlip(1,1)
            y(2,i) = gSlip(2,1)
            y(3,i) = gSlip(3,1)
         end do

      end if

      do i = 1,num_slip_sys
C        normialize the slip plane and dirction vecotrs
         call normalize_vector( y(1,i), y(2,i), y(3,i) )
         call normalize_vector( z(1,i), z(2,i), z(3,i) )
      end do
c-------------------------------------------------------------------
c  Initialize internal variables for initial time step
c-------------------------------------------------------------------

      if (time(2) .eq. 0.0) then

c-------------------------------------------------------------------
c  Check for normality of Miller indices.
c-------------------------------------------------------------------

      do k = 1,num_slip_sys
        sum = 0.0D0
        do i = 1,3
          sum = sum + y(i,k) * z(i,k)
        end do
        if (abs(sum) .gt. tolerance) then
          write(7,*) 'The Miller indices are WRONG!!!'
          write(7,*) 'on slip system # ',k
          STOP
        end if
      end do

c-------------------------------------------------------------------
c  Generate Euler angles  1-3
c-------------------------------------------------------------------
c..... modification for polycrystal analysis 
c.... single element is one grain from subroutine RANGRN

        
        psi(1) = psi_ang*Pi/180.0D0
        psi(2) = theta_ang*Pi/180.0D0
        psi(3) = phi_ang*Pi/180.0D0
        
c
c-------------------------------------------------------------------
c  Initialize reference shear stress for each slip system of
c  each grain.  

        do n = 1,num_slip_sys
           if (n <= 3) then
              g0(n) = g1*g_zero
           else if (n <= 6) then
              g0(n) = g2*g_zero
           else if (n <= 9) then
              g0(n) = g3*g_zero
           else
              g0(n) = g4*g_zero
           end if
        end do
            

c-------------------------------------------------------------------
c  Initialize kinematic stress for each slip system of
c  each grain.   
c-------------------------------------------------------------------

        do n = 1,num_slip_sys
          a0(n) = a_zero
        end do
            
c-------------------------------------------------------------------
c  Initialize F_p_inv_0   
c-------------------------------------------------------------------

        do i = 1,3
           do j = 1,3
              F_p_inv_0(i,j) = 0.0D0
           end do
           F_p_inv_0(i,i) = 1.0D0
        end do
      
c-------------------------------------------------------------------
c  Initialize E_p  
c-------------------------------------------------------------------

        do i = 1,3
	  do j = 1,3
           E_p(i,j) = 0.D0
          end do
	end do  
c-------------------------------------------------------------------
c  Initialize E_eff  
c-------------------------------------------------------------------

        E_eff = 0.D0

c-------------------------------------------------------------------
c  Initialize E_p_eff    47
c-------------------------------------------------------------------

        E_p_eff = 0.D0

c-------------------------------------------------------------------
c  Initialize E_p_eff_cum   48
c-------------------------------------------------------------------

        E_p_eff_cum = 0.D0 


c-------------------------------------------------------------------
c  Read beginning of time step porosity
c-------------------------------------------------------------------

        pf0 = fini

c-------------------------------------------------------------------
c  Read beginning of elastic Jacobian
c-------------------------------------------------------------------

        Je = 1.D0

c-------------------------------------------------------------------
c  Begining damage Jacobian rate 
c-------------------------------------------------------------------

        Jddot = 0.D0
        Jddot0 = Jddot
c-------------------------------------------------------------------
c  Deletion Flag
c-------------------------------------------------------------------
        deleteFlag = 1.0D0

c-------------------------------------------------------------------
c  End of initializations.  Read in internal variables.
c-------------------------------------------------------------------

      else  ! time ~= 0

         n = 0
            
c-------------------------------------------------------------------
c  Read in Euler Angles 1-3
c-------------------------------------------------------------------

         do i = 1,3
           n = n + 1
           psi(i) = statev(n)
         end do
         
            
c-------------------------------------------------------------------
c  Read inverse of the plastic part of F     4-12
c-------------------------------------------------------------------

         do j = 1,3
           do i = 1,3
             n = n + 1
             F_p_inv_0(i,j) = statev(n)
           end do
         end do
         
c-------------------------------------------------------------------
c   Deletion Flag     unlucky 13
c-------------------------------------------------------------------
         n = n + 1 
         deleteFlag = statev(n)

c-------------------------------------------------------------------
c  Read E_p        14 - 22
c-------------------------------------------------------------------

        do i = 1,3
         do j = 1,3
            n = n + 1
            E_p(i,j) = statev(n)
         end do
        end do

c-------------------------------------------------------------------
c  Read E_eff      23
c-------------------------------------------------------------------
        n = n + 1
	E_eff = statev(n)

c-------------------------------------------------------------------
c  Read E_p_eff     24
c-------------------------------------------------------------------
        n = n + 1
	E_p_eff = statev(n)

c-------------------------------------------------------------------
c  Read E_p_eff_cum  25
c-------------------------------------------------------------------
        n = n + 1
        E_p_eff_cum = statev(n)

c-------------------------------------------------------------------
c     Flow stress        26
c-------------------------------------------------------------------
        n = n + 1
        flowStress = statev(n)

c-------------------------------------------------------------------
c     Effective Stress                     27
c------------------------------------------------------------------
        n = n + 1
        sig_eff = statev(n)

c-------------------------------------------------------------------
c  Read beginning of time step porosity 28
c-------------------------------------------------------------------
        n = n + 1
        pf0 = statev(n)

c-------------------------------------------------------------------
c  Read beginning of damage Jacomian rate 29
c-------------------------------------------------------------------
        n = n+1
        Jddot = statev(n)
        Jddot0 = Jddot

c-------------------------------------------------------------------
c     5 blanks (maybe extract stuff here later)   30-35
c------------------------------------------------------------------
        n = n+1
        n = n+1
        n = n+1
        n = n+1
        n = n+1

c-------------------------------------------------------------------
c  Read reference shear values      
c  24 slip systems  SDV 35-58
C  12 slip systems  SDV 35-46
c-------------------------------------------------------------------

        
        do i = 1,num_slip_sys
          n = n + 1
          g0(i) = statev(n)
        end do
        

c-------------------------------------------------------------------
c  Read kinematic stress values
c  24 slip systems  SDV 59-82
C  12 slip systems  SDV 47-59
c-------------------------------------------------------------------

         do i = 1,num_slip_sys
           n = n + 1
           a0(i) = statev(n)
         end do


c-------------------------------------------------------------------
c  End of initializations
c-------------------------------------------------------------------

      end if ! (time = 0)

c-------------------------------------------------------------------
c  Calculate direction cosines based on Euler angles
c-------------------------------------------------------------------
c.... using XZX rotation for the direction cosines to alinng 111 direction parallel to 010 (tube axis direction)

      s1 = sin(psi(1))
      c1 = cos(psi(1))
      s2 = sin(psi(2))
      c2 = cos(psi(2))
      s3 = sin(psi(3))
      c3 = cos(psi(3))
            
      dir_cos(1,1) = c2
      dir_cos(2,1) = c1*s2
      dir_cos(3,1) = s1*s2
      dir_cos(1,2) = -1*c3*s2
      dir_cos(2,2) = c1*c2*c3-s1*s3
      dir_cos(3,2) = c1*s3+c3*c2*s1
      dir_cos(1,3) = s3*s2
      dir_cos(2,3) = -1*c2*c1*s3-c3*s1
      dir_cos(3,3) = c1*c3-c2*s1*s3

      

c-------------------------------------------------------------------
c  Initialize ANISOTROPIC 4th rank elastic stiffness tensor
c-------------------------------------------------------------------

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          C0(i,j,k,l) = C12 * del(i,j) * del(k,l) +
     &		  C44 * (del(i,k)*del(j,l)+del(i,l)*del(k,j))
         end do
        end do
       end do
      end do
      C0(1,1,3,3) = C13
      C0(3,3,1,1) = C13
      C0(1,1,1,1) = C11
      C0(2,2,2,2) = C11
      C0(3,3,3,3) = C33

c--------------------------------------------------------------------
c  Rotate local anisotropic elasticity tensor to
c  global coordinates.
c--------------------------------------------------------------------

      call rotate_4th(dir_cos,C0,C)

c--------------------------------------------------------------------
c  Convert Miller Indices to global coordinates
c--------------------------------------------------------------------

      do n = 1,num_slip_sys
        do i = 1,3
          xs0(i,n) = 0.0D0
          xm0(i,n) = 0.0D0
          do j = 1,3
            xs0(i,n) = xs0(i,n) + dir_cos(i,j) * z(j,n)
            xm0(i,n) = xm0(i,n) + dir_cos(i,j) * y(j,n)
          end do
        end do
      end do

c--------------------------------------------------------------------
c  Initialize number of subincrements.  Note that the
c  subincrement initially equals the total increment.  This
c  remains the case unless the process starts to diverge.
c--------------------------------------------------------------------

      N_incr       = 1
      N_incr_total = 1

c====================================================================
c  Top of Subincrement Time Step Integration Loop.
c  Calculate subincrement time step size.
c====================================================================

  100 dt_incr = dtime / N_incr_total
C      print*,N_incr_total

c-------------------------------------------------------------------
c  Initialize ref shear stress
c-------------------------------------------------------------------

      do n = 1,num_slip_sys
        g(n) = g0(n)
      end do

c-------------------------------------------------------------------
c  Initialize back stress
c-------------------------------------------------------------------

      do n = 1,num_slip_sys
        a(n) = a0(n)
      end do

c--------------------------------------------------------------------
c  Initialize deformation gradients for beginning and
c  end of subincrement.
c--------------------------------------------------------------------

      do i = 1,3
        do j = 1,3
          F0(i,j) = dfgrd0(i,j) + (dfgrd1(i,j) - dfgrd0(i,j)) *
     &					(N_incr - 1) / N_incr_total
          F1(i,j) = dfgrd0(i,j) + (dfgrd1(i,j) - dfgrd0(i,j)) *
     &						N_incr / N_incr_total
        end do
      end do

c-------------------------------------------------------------------
c    set porosity (is this in the right spot????
c-------------------------------------------------------------------
      pf = pf0

c-------------------------------------------------------------------
c  Jacobian A.20 Moore et al j dyn behavior materials 2019
c-------------------------------------------------------------------
      pfn = 0.D0 ! for now
      Jd = (1 - fini)/(1-(pf - pfn)) 

c--------------------------------------------------------------------
c  calculate inverse damage deformation rate
c--------------------------------------------------------------------
      do i = 1,3
         do j = 1,3
            FdInv(i,j) = 0.D0 
         end do
      end do
      FdTerm = Jd**(-1.D0/3.D0)
      FdInv(1,1) = FdTerm
      FdInv(2,2) = FdTerm
      FdInv(3,3) = FdTerm

c--------------------------------------------------------------------
c  Multiply F() by F_d_inv() 
c--------------------------------------------------------------------
      call aa_dot_bb(3,F0,FdInV,FFdInv)

c--------------------------------------------------------------------
c  Multiply F()*F_d_inv by F_p_inv() to get F_el()
c--------------------------------------------------------------------

      call aa_dot_bb(3,FFDInv,F_p_inv_0,F_el)
      call inverse_3x3(F_el,F_el_inv)

c--------------------------------------------------------------------
c  Rotate xs0 and xm0 to current coordinates, called xs and xm.
c--------------------------------------------------------------------

      do n = 1,num_slip_sys
        do i = 1,3
          xs(i,n) = 0.0D0
          xm(i,n) = 0.0D0
          do j = 1,3
            xs(i,n) = xs(i,n) + F_el(i,j) * xs0(j,n)
            xm(i,n) = xm(i,n) + xm0(j,n)  * F_el_inv(j,i)
          end do
        end do
      end do

c--------------------------------------------------------------------
c  Calculate elastic Green Strain
c--------------------------------------------------------------------

      call transposeMat(3,F_el,array1)
      call aa_dot_bb(3,array1,F_el,E_el)
      do i = 1,3
        E_el(i,i) = E_el(i,i) - 1
        do j = 1,3
          E_el(i,j) = E_el(i,j) / 2
        end do
      end do 
c--------------------------------------------------------------------
c  Multiply the anisotropic stiffness tensor by the Green strain 
c  to get the 2nd Piola Kirkhhoff stress
c--------------------------------------------------------------------

      call aaaa_dot_dot_bb(3,C,E_el,Spk2)
 

c--------------------------------------------------------------------
c  Convert from PK2 stress to Cauchy stress
c--------------------------------------------------------------------

      Je = determinant(F_el)
      call transposeMat(3,F_el,array2)
      call aa_dot_bb(3,F_el,Spk2,array1)
      call aa_dot_bb(3,array1,array2,kirch)

c-------------------------------------------------------------------
c     hydrostatic Kirchhoff stress 
c-------------------------------------------------------------------

      pkirch = one3*(kirch(1,1) + kirch(2,2) + kirch(3,3))

c-------------------------------------------------------------------
c     Deviatoric Kirchhoff stress 
c-------------------------------------------------------------------
      do i = 1,3
       do j = 1,3
          if (i.eq.j) then
             skirch(i,j) = kirch(i,j) - pkirch
          else
             skirch(i,j) = kirch(i,j)
          end if
       end do
      end do      

C      do i = 1,3
C       do j = 1,3
C          sig(i,j) = kirch(i,j) / (Je*Jd)
C       end do
C      end do

c--------------------------------------------------------------------
c  Calculate resolved shear stress for each slip system.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        tau(k) = 0.0D0
        do j = 1,3
          do i = 1,3
            tau(k) = tau(k) + xs(i,k) * xm(j,k) * skirch(i,j)
          end do
        end do
      end do



c-------------------------------------------------------------------
c  Calculate beginning of time step degradation function
C  Moore, international journal of fracture
C  A degradation function consistent with Cocks-Ashby porosity kinetics
c-------------------------------------------------------------------
C     modifed Martin damage function parameters that are functions
C     of porosity (called h1 etc in text) and stress
      mExp = 1.D0/flow_exp
      bm    = pc2*(2.D0 - mExp)/(2.D0 + mExp)
      hh1 = 1.D0 + two3*pf
      hh3 = (1.D0 - pf)**(1.D0/(mExp+1.D0))
      am = (pc1/pc2)*hh1*sqrt(6.D0)*sqrt(two3)*(2.D0+mExp)/(2.D0-mExp)
      dtheta = (1.D0/(1.D0-pf)**(1.D0/mExp) - (1.D0 -pf))/(1.D0-pf)

C     Degrdation Function
      call get_sumG(sumG,g,gamma_dot,sumSlip,num_slip_sys, dsumGdg,
     &                dsumGdgdot)
      call get_wmmAve(wmmAve, pf, mExp, bm, pkirch, sumG, pc1, pc2, hh3)

      do n = 1,num_slip_sys
         T = pkirch/g0(n)
         sinhbmt = sinh(bm*T)
         coshbmt = cosh(bm*T)
         OmegaS = am*coshbmt*dtheta
C         wmm(n) = hh3*(hh1 + OmegaS)**(nhalf)
C        debug fix
         wmm(n) = wmmAve
      end do
       
c--------------------------------------------------------------------
c  Calculate 1st estimate of gamma_dot for each slip system.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
C      degrade CRSS and back stress by degradation fun. wmm
       gamma_dot(k)=gamma_dot_zero*power((tau(k)-wmm(k)*a(k))/
     &				(wmm(k)*g(k)),flow_exp)
      end do
   
c--------------------------------------------------------------------
c  Calculate d(Tau)/d(Gamma_dot)
c--------------------------------------------------------------------

      do ia = 1,num_slip_sys
       do ib = 1,num_slip_sys
        dTaudGd(ia,ib) = 0.0D0

        do i = 1,3
         do j = 1,3
          array1(i,j) = xs0(i,ib) * xm0(j,ib)
         end do
        end do
        call aaaa_dot_dot_bb(3,C0,array1,array2)

        do i = 1,3
         do j = 1,3
          array1(i,j) = xs0(i,ia) * xm0(j,ia)
         end do
        end do
        call aa_dot_dot_bb(3,array1,array2,dTaudGd(ia,ib))

        dTaudGd(ia,ib) = dTaudGd(ia,ib) * dt_incr
       end do !ib
      end do !ia
      
c--------------------------------------------------------------------
c    sum of slip from each system for use in porosity kinetics
c--------------------------------------------------------------------
C     g	(num_slip_sys,max_grains)
      call get_sumSlip(sumSlip,num_slip_sys,gamma_dot)
      
      call get_sumG(sumG,g,gamma_dot,sumSlip,num_slip_sys, dsumGdg,
     &                dsumGdgdot)
      call get_fdotca(fdotca, pc1,bm, pkirch, dtheta,sumSlip,sumG )

c--------------------------------------------------------------------
c  Initial guess current time step porosity
c--------------------------------------------------------------------

      pfGuess = pf0 + fdotca*dtime
      pf = pfGuess

c--------------------------------------------------------------------
c  beginninge of time step Jd values = Jd0
c--------------------------------------------------------------------

      call get_Jd(Jd0,fini,pf0,pfn )
      Dd0 = one3*Jddot0/Jd0
c--------------------------------------------------------------------
c  Initial guess at trace of Dd
c--------------------------------------------------------------------
      call get_Jd(JdGuess,fini,pfGuess,pfn )
      trDd = (JdGuess - Jd)/(dtime*JdGuess)
      
c====================================================================
c  Begin Newton-Raphson iterative loops.
c====================================================================

      converged = .false.

      counter = 0

      do while (.not.converged)

      converged = .true. 
  
c--------------------------------------------------------------------
c  Calculate g, F_p_inv, F_el, sig, tau, func, sse.
c  sse = sum of squared error
c--------------------------------------------------------------------

C     func in the residual values

C     g(1,m) is wrong!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
C     g should have dimensions of # of slip systems

      call eval_func(xs0,	dt_incr,	gamma_dot,	
     &		     xm0,	F1,		num_slip_sys,
     &		     C,		F_p_inv,	F_p_inv_0,
     &		     g0,	Hdir,		Hdyn,
     &		     a0,	Adir,		Adyn,
     &		     g_sat,	F_el,		flow_exp,
     &		     kirch,	tau,	        gamma_dot_zero,
     &		     g,	        func,		xL_p,
     &		     xLatent,	sse,		a, 
     &               wmm ,      trDd,           Jd0, 
     &               fini,      pf0,            pfn, 
     &               pc1,       bm,             mExp,
     &               Jd,        Jddot,          skirch )

      sse_ref = sse

c--------------------------------------------------------------------
c     calculate pressure from kirch stress
c--------------------------------------------------------------------
      pkirch = one3*(kirch(1,1) + kirch(2,2) + kirch(3,3))

c--------------------------------------------------------------------
c     calculate Cauchy stress from kirchoff stress
c--------------------------------------------------------------------
C      do i = 1,3
C          do j = 1,3
C             sig(i,j) = kirch(i,j)/(Je*Jd)           
C          end do
C      end do

c--------------------------------------------------------------------
c     get porosity for updated value of Jd
c--------------------------------------------------------------------
      call get_f_fromJd(Jd,fini,pf,pfn )

c--------------------------------------------------------------------
c    sum of slip from each system for use in porosity kinetics
c--------------------------------------------------------------------
C     g	(num_slip_sys,max_grains)
      call get_sumSlip(sumSlip,num_slip_sys,gamma_dot)
      
      call get_sumG(sumG,g,gamma_dot,sumSlip,num_slip_sys,
     &              dsumGdg, dsumGdgdot)
      call get_fdotca(fdotca, pc1,bm, pkirch, dtheta,sumSlip,sumG )

c--------------------------------------------------------------------
c     porosity term from cocks ashby porosity evolution equation
c     (cocks ashby call porosity theta)
c--------------------------------------------------------------------
      dtheta = (1.D0/(1.D0-pf)**(1.D0/mExp) - (1.D0 -pf))/(1.D0-pf)

c--------------------------------------------------------------------
c  Begin calculation of the partial derivatives needed for 
c  the Newton-Raphson step!!!
c  Calculate derivative of the hardening variable, g-alpha,
c  w.r.t. gamma-dot-beta.
c--------------------------------------------------------------------

      sum = 0
      do ia = 1,num_slip_sys
        sum = sum + abs(gamma_dot(ia))
      end do
      do ia = 1,num_slip_sys
       do ib = 1,num_slip_sys
         t1 = xLatent
         if (ia .eq. ib) t1 = 1.0D0
         dgadgb(ia,ib) = (Hdir * t1 - Hdyn*g(ia)) * dt_incr / 
     &			(1 + Hdyn * sum * dt_incr)
         if(gamma_dot(ib).lt.0.0D0)dgadgb(ia,ib)=-dgadgb(ia,ib)
       end do
      end do

c--------------------------------------------------------------------
c  Calculate derivative of kinematic stress, a-alpha
c  w.r.t. gamma-dot-beta.
c--------------------------------------------------------------------

      do ia = 1,num_slip_sys
         daadga(ia) = (Adir - Adyn * A(ia) * sgn(gamma_dot(ia)))
     &       * dt_incr / (1 + Adyn * dt_incr * abs(gamma_dot(ia)))
      end do

c--------------------------------------------------------------------
c  Form "A-matrix" of derivatives wrt d_gamma_beta
c--------------------------------------------------------------------

      do ia = 1,num_slip_sys
        do ib = 1,num_slip_sys
          array3(ia,ib) = dgadgb(ia,ib) * (tau(ia)-
     &                                 wmm(ia)*a(ia))/
     &                                 (wmm(ia)*g(ia))
        end do
        array3(ia,ia) = array3(ia,ia) + 
     &		wmm(ia)*g(ia) / (flow_exp * gamma_dot_zero) *
     &		abs(power((tau(ia)-wmm(ia)*a(ia))/
     &          (wmm(ia)*g(ia)),(1.D0-flow_exp)))
      end do

c--------------------------------------------------------------------
c  Add d(Tau)/d(Gamma_dot) to the A-matrix for the Newton-
c  Raphson iteration.
c--------------------------------------------------------------------

      do ia = 1,num_slip_sys
        do ib = 1,num_slip_sys
          array3(ia,ib) = array3(ia,ib) + dTaudGd(ia,ib)
        end do
      end do

c--------------------------------------------------------------------
c  Add d(a-alpha)/d(Gamma_dot) to the A-matrix for the Newton-
c  Raphson iteration.
c--------------------------------------------------------------------

      do ia = 1,num_slip_sys
        array3(ia,ia) = array3(ia,ia) + daadga(ia)
      end do
Ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c--------------------------------------------------------------------
c  Calculation of Jacobian component for porosity residual
c--------------------------------------------------------------------
Cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

c--------------------------------------------------------------------
c  derivative porosity (f) wrt trDd
c--------------------------------------------------------------------
C      dfdtrDd = ( 1.D0 - fini)*Jd*dtime*exp(trDd)

c--------------------------------------------------------------------
c  derivative porosity term (dtheta) in cocks ashby expresion wrt 
c  to porosity (f)
c--------------------------------------------------------------------
C      ddthetadf = flow_exp*power(1.D0 - pf,-flow_exp-1.D0) + 1.D0

c--------------------------------------------------------------------
c  derivative of cocks ashby porosity evolution exprextion (fdotca)
c  wrt porosity f
c--------------------------------------------------------------------
C     Need to make sure T pkirch and sumG are defined
      T = pkirch/sumG
      sinhbmt = sinh(bm*T)
      coshbmt = cosh(bm*T)
C      dfdotcadf = pc1*sumSlip*ddthetadf
C      dfdotcadf = dfdotcadf*sinhbmt

c--------------------------------------------------------------------
c derivative of porosity evoluation wrt sum of slip rates
c--------------------------------------------------------------------
C      dfdotcadgdot = pc1*sinhbmt*dtheta

c--------------------------------------------------------------------
c derivative of porosity residual wrt trD
c--------------------------------------------------------------------
C      drfdtrDd = dfdtrDd/dtime - dfdotcadf*dfdtrDd

c--------------------------------------------------------------------
c derivative of porosity evoluation wrt hydrstatic
c kirchhoff stress
c--------------------------------------------------------------------
C      dfdotcadp = pc1*dtheta*sumSlip*bm/sumG

C     need to incoroprate  !!!!!!!!!!!!!!!!!!!
c     dgadgb and daadgb in to derivative !!!!!!!!!!!!
C     also need to see if I am double calculating 
C     dTaudGd

c--------------------------------------------------------------------
c derivative of porosity evoluation wrt hydrstatic
c the weighted sum of the slip system strengths
c--------------------------------------------------------------------
C      dfdotcadsumG = -1.D0*pc1*dtheta*sumSlip*coshbmt
C     &             * bm*pkirch*sumG**-2.D0
c--------------------------------------------------------------------
c derivative of shear stress residual wrt 
c trace of damage deforamtion rate
c its all zero so if that checks out maybe deleate this
c--------------------------------------------------------------------
C      do ia = 1,num_slip_sys
C           drgdtrDd(ia) = 0.D0
C      end do

c--------------------------------------------------------------------
c derivative of porosity residual wrt 
c slip rate
c its all zero so if that checks out maybe deleate this
c--------------------------------------------------------------------
C      do ia = 1,num_slip_sys
C           drfdgdot(ia) = -1.d0*dfdotcadgdot
C     &                    - dfdotcadsumG * (dsumGdgdot(ia)
C     &                    + dsumGdg(ia)*dgadgb(ia,ia))
C      end do

c--------------------------------------------------------------------
c derivative of slip equation wrt 
c trace of damage deforamtion rate
c--------------------------------------------------------------------
C      do ia = 1,num_slip_sys
C          array3(ia,num_slip_sys+1) = drgdtrDd(ia)
C      end do

c--------------------------------------------------------------------
c derivative of porosity residual wrt 
c slip rates
c--------------------------------------------------------------------
C      do ia = 1,num_slip_sys
C          array3(num_slip_sys+1,ia) = drfdgdot(ia)
C      end do

c--------------------------------------------------------------------
c derivative of porosity residual wrt 
c  trace of damage deforamtion rate tr(Dd)
c--------------------------------------------------------------------
      
C      array3(num_slip_sys+1,num_slip_sys+1) = drfdtrDd
      
c--------------------------------------------------------------------
c  Calculate the gradient of sse wrt gamma_dot().  Will be used
c  later to ensure that line search is in the correct direction.
c--------------------------------------------------------------------

      do j = 1,num_slip_sys
        grad(j) = 0.0D0
        do i = 1,num_slip_sys
          grad(j) = grad(j) + func(i) * array3(i,j)
        end do
        grad(j) = 2 * grad(j)
      end do

c--------------------------------------------------------------------
c  Grad needs to include dsse / dtr(Dd) stuff
c--------------------------------------------------------------------





c--------------------------------------------------------------------
c  Solve for increment of gamma_dot.  Solution is returned 
c  in the func() array.
c--------------------------------------------------------------------

      if (mat_data.eq.1) then
         do i = 1,num_slip_sys
            dx1(i) = func(i)
         end do

         do i = 1,num_slip_sys
            do j = 1,num_slip_sys
               arraydx1(i,j) = array3(i,j)
            end do
         end do

         call simeq(num_slip_sys,arraydx1,dx1)
         do i = 1,num_slip_sys
            func(i) = dx1(i)
         end do         
      else
         do i = 1,num_slip_sys
            dx0(i) = func(i)
         end do

         do i = 1,num_slip_sys
            do j = 1,num_slip_sys
               arraydx0(i,j) = array3(i,j)
            end do
         end do

         call simeq(num_slip_sys,arraydx0,dx0)
         do i = 1,num_slip_sys
            func(i) = dx0(i)
         end do
      end if
            
            
c--------------------------------------------------------------------
c  Store offset in d_gamma_dot(k) 
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
         d_gamma_dot(k) = func(k)
      end do

c--------------------------------------------------------------------
c  Store offset in d_gamma_dot(k) 
c--------------------------------------------------------------------

    
C      d_trDd = func(num_slip_sys + 1)

c--------------------------------------------------------------------
c  Check to make sure that N-R step leads 'down hill' the 
c  sse surface.
c--------------------------------------------------------------------

      sum = 0.0D0
      do i = 1,num_slip_sys
        sum = sum - grad(i) * d_gamma_dot(i)
      end do

      if (sum .gt. 0.0D0) then
        do i = 1,num_slip_sys
          d_gamma_dot(i) = -d_gamma_dot(i)
        end do
      end if

c--------------------------------------------------------------------
c  Multiply step size by two 'cause next loop will divide it by 2.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        d_gamma_dot(k) = d_gamma_dot(k) * 2.D0
      end do
      
c====================================================================
c  Begin line search.
c====================================================================

      improved = .false.

      do N_ctr = 1,max_loops

      sse_old = sse

c--------------------------------------------------------------------
c  Divide step size by two.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        d_gamma_dot(k) = d_gamma_dot(k) / 2
        gamma_try(k)   = gamma_dot(k) + d_gamma_dot(k)
      end do

c--------------------------------------------------------------------
c  Divide porosity DOF by 2 as well
c  and calculate trial increment to check improvment
c--------------------------------------------------------------------

C      d_trDd = d_trDd / 2.0D0

C      trDd_try = trDd + d_trDd
       trDd_try = trDd
      
c--------------------------------------------------------------------
c  Calculate g, F_p_inv, F_el, sig, tau, func, and sse based
c  on gamma_try(k)
c--------------------------------------------------------------------

      call eval_func(xs0,	dt_incr,	gamma_try,	
     &		     xm0,	F1,		num_slip_sys,
     &		     C,		F_p_inv,	F_p_inv_0,
     &		     g0,	Hdir,		Hdyn,
     &		     a0,	Adir,		Adyn,
     &		     g_sat,	F_el,		flow_exp,
     &		     kirch,	tau,	        gamma_dot_zero,
     &		     g,	        func,		xL_p,
     &		     xLatent,	sse,		a, 
     &               wmm ,      trDd_try,       Jd0, 
     &               fini,      pf0,            pfn, 
     &               pc1,       bm,             mExp, 
     &               Jd,        Jddot,          skirch)

C    need an evaluate function for porosity or work into this one
c--------------------------------------------------------------------
c    calculate Cauchy stress from kirchoff stress
c--------------------------------------------------------------------
C      do i = 1,3
C       do j = 1,3
C          sig(i,j) = kirch(i,j) / (Je*Jd)
C       end do
C      end do
c--------------------------------------------------------------------
c  Check for 'convergence' of the line search.  Note that the line
c  search doesn't waste time converging closely.  This is 'cause
c  the N-R step does so much better.
c--------------------------------------------------------------------

      if ((sse_old.le.sse_ref).and.(sse.ge.sse_old).and.
     &					(N_ctr.gt.1)) improved=.true.

      if (improved) go to 200

      end do ! Linear Search

c--------------------------------------------------------------------
c  Add "d_gamma_dot" to gamma_dot to get new values for
c  this iteration. 
c--------------------------------------------------------------------

  200 do k = 1,num_slip_sys
        gamma_dot(k) = gamma_dot(k) + d_gamma_dot(k) * 2.0D0
      end do

c--------------------------------------------------------------------
c  Add "d_trDd" to trDd to get new values for
c  this iteration (still not sure about the 2 ???) 
c--------------------------------------------------------------------
C      trDd = trDd + d_trDd * 2.D0


c--------------------------------------------------------------------
c   Update tr(Dd) using increment from line search
c   d_tr(Dd), 
c   I don't know about 2 here or in the gamma_dot update?????
c--------------------------------------------------------------------
C      trDd = trDd + d_trDd * 2.0

c--------------------------------------------------------------------
c  If (sse_old > tolerance) then this step has not converged.
c--------------------------------------------------------------------
      counter = counter + 1

      if (sse_old .gt. tolerance) converged = .false.

c--------------------------------------------------------------------
c  If (sse_old > sse_ref/2) then convergence is too slow and
c  increment is divided into two subincrements.
c--------------------------------------------------------------------
  
      if ((sse_old.gt.sse_ref/2.0D0).and.(.not.converged)) then
        N_incr = 2 * N_incr - 1
        N_incr_total = 2 * N_incr_total
        go to 100
      end if

c--------------------------------------------------------------------
c  End iterative loop.
c--------------------------------------------------------------------

      end do ! 'Newton Raphson Iterative Loop'

c--------------------------------------------------------------------
c  If another subincrement remains to be done, then reinitialize
c  F_p_inv_0 and g0.  F0 and F1 gets reinitialized back at the
c  top of this loop.
c--------------------------------------------------------------------

      if (N_incr .lt. N_incr_total) then
        if (N_incr .eq. (N_incr/2)*2) then	! N_incr is 'even'
          N_incr = N_incr / 2 + 1
          N_incr_total = N_incr_total / 2
        else					! N_incr is 'odd'
          N_incr = N_incr + 1
        end if
        do i = 1,3
          do j = 1,3
            F_p_inv_0(i,j) = F_p_inv(i,j)
          end do
        end do
        do i = 1,num_slip_sys
          g0(i) = g(i)
          a0(i) = a(i)
        end do
        go to 100
      end if
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     this is where I start the fsolver starts
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      Jd0pEOS = Jd
C     g	(num_slip_sys,max_grains)
      call get_sumSlip(sumSlip,num_slip_sys,gamma_dot)
      
      call get_sumG(sumG,g,gamma_dot,sumSlip,num_slip_sys,
     &              dsumGdg, dsumGdgdot)

c$$$      T = pkirch/sumG
c$$$      sinhbmt = sinh(bm*T)
c$$$      coshbmt = cosh(bm*T)
c$$$
c$$$      Aterm = pc1*sinhbmt*sumSlip
c$$$      pftol = 1.D-6
c$$$      pf0 = pf
c$$$      pf = pfGuess
c$$$ 
c$$$      do iporo = 1,100
c$$$         rf = Aterm*(1.D0/(1.D0-pf)**flow_exp 
c$$$     &      - (1.D0-pf)) - (pf - pf0)/dtime;
c$$$         if (rf.lt.pftol) then
c$$$            goto 1000
c$$$         end if
c$$$         drfdf = Aterm*(flow_exp*(1.D0 - pf)**(-flow_exp-1.D0)) 
c$$$     &         - 1.D0/dtime
c$$$         df = -rf/drfdf
c$$$         pf = pf + df
c$$$      end do

C     tolorence for porosity solver and guesses
      pftol = 1.D-6
      pf0 = pf
      pf = pfGuess
C     total Jacobian
      Jtot = Je*Jd
C     avarage isotropic bulk modulus
      bulk = C12 + two3*C44
C     Newton Solver
      do iporo = 1,100
         call get_Jd(Jd,fini,pf,pfn )
C        get Je
         do i = 1,3
            do j = 1,3
               FdInv(i,j) = 0.D0 
            end do
         end do         
         FdTerm = Jd**(-1.D0/3.D0)
         FdInv(1,1) = FdTerm
         FdInv(2,2) = FdTerm
         FdInv(3,3) = FdTerm
         call aa_dot_bb(3,F1,FdInV,FFdInv)
         call aa_dot_bb(3,FFDInv,F_p_inv,F_el)
         Je = determinant(F_el)
C        calculate pressure for Je
         pEOS = - bulk*(1.D0/Je - 1.D0)
         T = pEOS/sumG
         sinhbmt = sinh(bm*T)
         coshbmt = cosh(bm*T)
         Aterm = pc1*sinhbmt*sumSlip
         rf = Aterm*(1.D0/(1.D0-pf)**flow_exp 
     &      - (1.D0-pf)) - (pf - pf0)/dtime
         if (rf.lt.pftol) then
            goto 1000
         end if
         drfdf = Aterm*(flow_exp*(1.D0 - pf)**(-flow_exp-1.D0)) 
     &         + Aterm - 1.D0/dtime
         dAdf = pc1**sumSlip*coshbmT*(bm/sumG)*K*Je**(-2.D0)*(-JTot)
         Bterm = 1.D0/((1-pf)**flow_exp) - (1.D0 -pf)
         drfdf = drfdf + dAdf*Bterm
         df = -rf/drfdf
         pf = pf + df
      end do

C     Check bounds of porosity

1000  if (iporo >= 100) then
C         print *, "could not find porosity"
         call get_Jd(Jd,fini,pf0,pfn )
         do i = 1,3
            do j = 1,3
               FdInv(i,j) = 0.D0 
            end do
         end do 
         FdTerm = Jd**(-1.D0/3.D0)
         FdInv(1,1) = FdTerm
         FdInv(2,2) = FdTerm
         FdInv(3,3) = FdTerm
         call aa_dot_bb(3,F1,FdInV,FFdInv)
C        should this be F_p_inv_1 ????
         call aa_dot_bb(3,FFDInv,F_p_inv,F_el)
         Je = determinant(F_el)
C        calculate pressure for Je
         pEOS = - bulk*(1.D0/Je - 1.D0)
         T = pEOS/sumG
         sinhbmt = sinh(bm*T)
         coshbmt = cosh(bm*T)
         Aterm = pc1*sinhbmt*sumSlip
         Bterm = 1.D0/((1-pf0)**flow_exp) - (1.D0 -pf0)
         fdot0 = Aterm*Bterm
         dfdotdf = Aterm*(flow_exp*(1.D0 - pf)**(-flow_exp-1.D0)) 
     &         + Aterm
         dAdf = pc1**sumSlip*coshbmT*(bm/sumG)*K*Je**(-2.D0)*(-JTot)
         dfdotdf = dfdotdf + dAdf*Bterm
         pf = -fdot0/(dfdotdf - 1.D0/dtime) + pf0
      end if

      if (pf < 0.D0) then
         pf = 0.D0
      else if (pf >= 1.D0) then
         deleteFlag = 0.D0
         print *, "element deleted"
      end if


C      do i = 1,3
C       do j = 1,3
C          sig(i,j) = sig(i,j)*Jd0pEOS
C       end do
C      end do

      call get_Jd(Jd,fini,pf,pfn )

C     total Jacobian
      Jtot = Je*Jd 

C     Cauchy deviatoric stress
      do i = 1,3
       do j = 1,3
          sig(i,j) = skirch(i,j)/Jtot
       end do
      end do
C     Cauchy pressure
      pEOS = pEOS/Jtot

C     Cauchy Stress
      sig(1,1) = skirch(1,1) + pEOS
      sig(2,2) = skirch(2,2) + pEOS
      sig(3,3) = skirch(3,3) + pEOS


c--------------------------------------------------------------------
c     Check degradation function for materials failure
c---------------------------------------------------------------------
      call get_wmmAve(wmmAve, pf, mExp, bm, pkirch, sumG, pc1, pc2, hh3)

C      print *, wmmAve
      if (wmmAve <= wmmMin) then
         deleteFlag = 0.D0
         print *, "element deleted"
      end if

c--------------------------------------------------------------------
c  Write out Euler Angles in Kocks Convention.
c--------------------------------------------------------------------

      call aa_dot_bb(3,F_el,dir_cos,array1)
      call kocks_angles(npt,m,time(2),array1)


c--------------------------------------------------------------------
c  Rotate xs0 and xm0 to current coordinates, called xs and xm.
c--------------------------------------------------------------------

      call inverse_3x3(F_el,F_el_inv)
      do n = 1,num_slip_sys
        do i = 1,3
          xs(i,n) = 0.0D0
          xm(i,n) = 0.0D0
          do j = 1,3
            xs(i,n) = xs(i,n) + F_el(i,j) * xs0(j,n)
            xm(i,n) = xm(i,n) + xm0(j,n)  * F_el_inv(j,i)
          end do
        end do
      end do

c--------------------------------------------------------------------
c  Calculate the derivative of the plastic part of the rate of
c  deformation tensor in the current configuration wrt sigma.
c--------------------------------------------------------------------

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          ddpdsig(i,j,k,l) = 0.0D0
          do n = 1,num_slip_sys
           ddpdsig(i,j,k,l) = ddpdsig(i,j,k,l) + (xs(i,n)*xm(j,n) +
     &       xm(i,n) * xs(j,n)) * (xs(k,n) * xm(l,n) + xm(k,n) *
     &       xs(l,n)) * abs(power((tau(n)-wmm(n)*a(n))/
     &               (wmm(n)*g(n)),
     &		flow_exp-1.0D0)) / (wmm(n)*g(n))
          end do ! num_slip_sys
         end do ! l
        end do ! k
       end do ! j
      end do ! i


C      do i = 1,3
C         dDddsig(i,i,i,i) = dDddsig(i,i,i,i) +  dddTerm
C      end do ! i


c      write(7,'(12f7.3)')(gamma_dot(i,m)/0.0004,i=1,12)

c--------------------------------------------------------------------
c  Calculate Green Strain
c--------------------------------------------------------------------
      call transposeMat(3,F1,array1)
	call aa_dot_bb(3,array1,F1,E_tot)

      do i = 1,3
        E_tot(i,i) = E_tot(i,i) - 1
        do j = 1,3
          E_tot(i,j) = E_tot(i,j) / 2
        end do 
      end do 
c====================================================================
c  Begin calculation of the Jacobian (the tangent stiffness matrix).
c====================================================================

c--------------------------------------------------------------------
c  Output stress, strain, etc. for post processing.
c--------------------------------------------------------------------

      sum = 0.0
      do i = 1,3
       do j = 1,3
        sum = sum + sig(i,j) * sig(i,j)
       end do
      end do
      trace = sig(1,1) + sig(2,2) + sig(3,3)
      sig_eff=sqrt(1.5 * sum - 0.5 * trace**2)

c      write(7,'(a3,i5,5f14.6)')'BM1',ninc,time(2),
c     &	abs(log(dfgrd1(3,3)))+dfgrd1(1,3)/dfgrd1(3,3)/1.7320508,
c     &	sig_eff,sig(3,3),sig(1,3)

c      write(7,'(a3,i5,5f14.6)')'BM1',ninc,time(2),sig(1,2),sig(2,3)
c--------------------------------------------------------------------
c  Calculate the inverse of F_el
c--------------------------------------------------------------------

      call inverse_3x3(F_el,F_el_inv)

c--------------------------------------------------------------------
c  Scale by appropriate constants and divide by num_grains to
c  get the average value.  ALSO multiply by 'dtime' which is
c  d(sig)/d(sig_dot).
c--------------------------------------------------------------------

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          ddpdsig(i,j,k,l) = ddpdsig(i,j,k,l) * dtime * flow_exp *
     &		gamma_dot_zero
         end do ! l
        end do ! k
       end do ! j
      end do ! i

c--------------------------------------------------------------------
c   Calcualte time rate of Jd = dot(Jd)_n+1
c--------------------------------------------------------------------
      Jddot = (Jd - Jd0)/dtime
      Dd = one3*Jddot/Jd

c--------------------------------------------------------------------
c  Calculate the derivative of the damage part of the rate of
c  deformation tensor in the current configuration wrt sigma.
C  the whole thing is multiplied my dt, that why no dt in dddTerm
c--------------------------------------------------------------------
C      dddTerm = (-1.D0/9.D0)*(Jd0/(Jd**3.D0*Je*bulk))
C      dDddJd = (1.D0/3.D0)*(Jd0/(dtime*Jd*Jd))
C      if (abs(Jd - Jd0).gt.1.D-9) then
C         dDddJd = (Dd - Dd0)/(Jd - Jd0)
C      else 
C         dDddJd = 0.D0
C      end if 
C      dddTerm = (-1.D0/3.D0)*(1.D0/(Jd*Je*bulk))*dDddJd
      if (abs(sig(1,1) - sig110).gt.1.D-9) then
         dddTerm = (Dd - Dd0)/(sig(1,1) - sig110)
      else 
         dddTerm = 0.D0
      end if
      print*, "dddTerm1 ", dddTerm

      dddTerm = (1.D0 / (1.D0 - pf))*pc1*sumSlip
     &        * cosh(bm*pEOS/sumG)*bm/sumG

      print*,  "dddTerm2 ", dddTerm
C      dddterm = 0.D0
C      dDddJd = (1.D0/3.D0)*(1.D0/dtime)*(1.D0/Jd - (Jd- Jd0)/(Jd*Jd))
      dddTermDt = dddTerm
      open(unit=3,file="tanDebug.dat")
      write(3,"(E,$)") time(1), Dd, sig(1,1), dddTermDt, Jd, dDddJd
      write(3,"(E)"  )

c--------------------------------------------------------------------
c     get the derivative on inelastic deformation rate
C     ie Dp + Dd wrt to sig
c--------------------------------------------------------------------
      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
            dDidsig(i,j,k,l) = ddpdsig(i,j,k,l)
         end do ! l
        end do ! k
       end do ! j
      end do ! i

      do i = 1,3
          dDidsig(i,i,i,i) = dDidsig(i,i,i,i) +  dddTerm
      end do ! i

c--------------------------------------------------------------------
c  Multiply the 4th rank elastic stiffness tensor by the derivative
c  of the plastic part of the rate of deformation tensor wrt sig_dot.
c--------------------------------------------------------------------

      call aaaa_dot_dot_bbbb(3,C,dDidsig,array6)

c--------------------------------------------------------------------
c  Add 4th rank identity tensor to array6()
c--------------------------------------------------------------------

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          array6(i,j,k,l) = array6(i,j,k,l) + 0.5D0 * 
     &		(del(i,k) * del(j,l) + del(i,l) * del(j,k))
         end do
        end do
       end do
      end do

c--------------------------------------------------------------------
c  Need to take the inverse of Array4.  Since it relates two 2nd
c  rank tensors that are both symmetric, Array4 can be xformed to 
c  Voigt notation style in order to do the inverse, then xformed back.
c--------------------------------------------------------------------

      call forth_to_Voigt (Array6,Array4)
      call inverse        (6,Array4,Array5)
      call Voigt_to_forth (Array5,Array6)

c--------------------------------------------------------------------
c  Multiply Array6 by C, the elastic stiffness matrix to
c  finally get the Jocobian, but as a 4th rank tensor.
c--------------------------------------------------------------------

      call aaaa_dot_dot_bbbb (3,Array6,C,ddsdde_4th)

c--------------------------------------------------------------------
c  Store the stress tensor in the ABAQUS stress 'vector'
c--------------------------------------------------------------------

      do i = 1,ndi
         stress(i) = sig(i,i)
      end do
      if (nshr .eq. 1) stress(ndi+1) = sig(1,2)
      if (nshr .eq. 3) then
         stress(4) = sig(1,2)
         stress(5) = sig(1,3)
         stress(6) = sig(2,3)
      end if

c--------------------------------------------------------------------
c  Store the Jocbian in Voigt notation form.
c--------------------------------------------------------------------

      do i = 1,3
       do j = i,3   ! not 1 !!!
        ia = i
        if (i.ne.j) ia=i+j+1
        do k = 1,3
         do l = k,3 ! not 1 !!!
          ib = k
          if (k.ne.l) ib=k+l+1
          array4(ia,ib) = ddsdde_4th(i,j,k,l)
          IF(IB.GE.4) ARRAY4(IA,IB) = 2 * ARRAY4(IA,IB)
         end do
        end do
       end do
      end do

c      call print_array(6,array4)

      do i =1,6
        do j = 1,6
          ddsdde(i,j) = 0.0D0
        end do
      end do

      if (ndi .eq. 1) then			! 1-D
         ddsdde(1,1) = array4(1,1)
      else if (ndi .eq. 2) then			! 2-D plane stress & axi
         do i = 1,2
            do j = 1,2
               ddsdde(i,j) = array4(i,j)
            end do
         end do
         ddsdde(1,3) = array4(1,4)
         ddsdde(2,3) = array4(2,4)
         ddsdde(3,1) = array4(4,1)
         ddsdde(3,2) = array4(4,2)
         ddsdde(3,3) = array4(4,4)
      else if (ndi .eq. 3 .and. nshr .eq. 1) then ! plane strain
         do i = 1,4
            do j = 1,4
               ddsdde(i,j) = array4(i,j)
            end do
         end do
      else					! Full 3-D
         do i = 1,6
            do j = 1,6
               ddsdde(i,j) = array4(i,j)
            end do
         end do
      end if

c-------------------------------------------------------------------
c  Store the internal variables in the statev() array
c-------------------------------------------------------------------

         n = 0

            
c-------------------------------------------------------------------
c  Store the Euler Angles  1-3
c-------------------------------------------------------------------

         do i = 1,3
           n = n + 1
           statev(n) = psi(i)
         end do
            
c-------------------------------------------------------------------
c  Store the plastic part of F   4-12
c-------------------------------------------------------------------


          do j = 1,3
             do i = 1,3
                n = n + 1
                statev(n) = F_p_inv(i,j)
             end do
          end do

c-------------------------------------------------------------------
c  Element Deletion Fag     unlucky 13
c-------------------------------------------------------------------
         n = n + 1
         statev(n) = deleteFlag 
c-------------------------------------------------------------------
c  Plastic Strain Tensor and  Calculations 14-22
c-------------------------------------------------------------------
c  Increment of plastic shear strain accumulated on each slip system
c  over this time step.
      do i = 1,num_slip_sys
	  delta_gamma(i) = gamma_dot(i)*dtime
	end do

	do j = 1,3
	  do k = 1,3
             delta_E_p(j,k) =  0.0D0
	  end do
	end do

c  Increment of the plastic strain tensor
	do j = 1,3
	  do k = 1,3
	    do l = 1,num_slip_sys
	      delta_E_p(j,k) = delta_E_p(j,k) + 0.5D0*delta_gamma(l)*
     &     (xs0(j,l)*xm0(k,l)+xs0(k,l)*xm0(j,l))
          end do
	  end do
	end do 

c  Plastic strain tensor
      do i = 1,3
	  do j = 1,3
	    E_p(i,j) = E_p(i,j)+delta_E_p(i,j)
	  end do
	end do 
    
	do i = 1,3
	 do j = 1,3
	   n = n+1
	   statev(n) = E_p(i,j)
	 end do
      end do
c-------------------------------------------------------------------	      
c     Effective total strain              23
c-------------------------------------------------------------------
      call aa_dot_dot_bb(3,E_tot,E_tot,sum)
      E_eff = sqrt((2.D0/3.D0)*sum)
      n = n+1
      statev(n)= E_eff

c-------------------------------------------------------------------
c     Effective Plastic Strain           24
c-------------------------------------------------------------------
      call aa_dot_dot_bb(3,E_p,E_p,sum1)
      E_p_eff = sqrt((2.D0/3.D0)*sum1)

      if (E_p_eff.lt.0.D0) E_p_eff = 0.D0
      n = n+1
      statev(n)= E_p_eff
c-------------------------------------------------------------------
c     Effective plastic strain increment 25
c-------------------------------------------------------------------
      sum2 = 0.D0
      call aa_dot_dot_bb(3,delta_E_p,delta_E_p,sum2)
      sum2=sqrt((2.D0/3.D0)*sum2)
      E_p_eff_cum = sum2 + E_p_eff_cum

      n = n+1       
      statev(n) = E_p_eff_cum

c-------------------------------------------------------------------
c     flow stress calculation               26
c------------------------------------------------------------------
C     from BLME book eq 3.3.22 convert dot(Ep) to Dp
      call transposeMat(3,F_el_inv,F_el_invT)
      call aa_dot_bb(3,delta_E_p,F_el_inv,EdotFinv)
      call aa_dot_bb(3,F_el_invT,EdotFinv,Dp)

C     double dot product of Cauchy Stress and plastic
C     Deformation rate
      call aa_dot_dot_bb(3,sig,Dp,sigDp)

C     Effective plastic deformation rate
      call aa_dot_dot_bb(3,Dp,Dp,Dp_eff)
      Dp_eff=sqrt((2.D0/3.D0)*Dp_eff)

C     "flow stress" Sig:Dp = flowStress*Dp_effective
C     flowStress = Sig:Dp/Dp_effective
C     if degradation function is ~1.0
      flowStress = sigDp/Dp_eff

      n = n + 1 
      statev(n) = flowStress
c-------------------------------------------------------------------
c     Effective Stress                     27
c------------------------------------------------------------------
      n = n + 1 
      statev(n) = sig_eff

c-------------------------------------------------------------------
c     Porosity                             28
c------------------------------------------------------------------
      n = n + 1
      statev(n) = pf

c-------------------------------------------------------------------
c     Rate of damage Jacobian              29
c------------------------------------------------------------------
      n = n + 1
      statev(n) = Jddot

c-------------------------------------------------------------------
c     blanks              30-35
c     state variables after these blanks depend on the number
c     of slip systems
c     so these blanks allow space for more variables with 
C     constant index
c------------------------------------------------------------------
      n = n + 1
      statev(n) = wmmAve
      n = n + 1
      statev(n) = pEOS
      n = n + 1
      statev(n) = 0.D0
      n = n + 1
      statev(n) = 0.D0
      n = n + 1
      statev(n) = 0.D0
c-------------------------------------------------------------------
c  Store the reference shear stresses.     
c  24 slip systems  SDV 35-58
C  12 slip systems  SDV 35-46
c-------------------------------------------------------------------


      do i = 1,num_slip_sys
         n = n + 1
         statev(n) = g(i)
      end do


c-------------------------------------------------------------------
c  Store the back stresses.     
c  24 slip systems  SDV 59-82
c  12 slip systems  SDV 46-58
c-------------------------------------------------------------------


       do i = 1,num_slip_sys
          n = n + 1
          statev(n) = a(i)
       end do


      return
      end
c====================================================================
c====================================================================
c====================== S U B R O U T I N E S =======================
c====================================================================
c====================================================================
c
c  Calculate a vector cross product.
c
c  c = a cross b
c
c--------------------------------------------------------------------

      subroutine cross_product(a1,a2,a3,b1,b2,b3,c1,c2,c3)
      
      implicit none
      real*8 a1, a2, a3, b1,b2,b3,c1,c2,c3
      
      c1 = a2 * b3 - a3 * b2
      c2 = a3 * b1 - a1 * b3
      c3 = a1 * b2 - a2 * b1

      return
      end

c====================================================================
c====================================================================
c
c  Normalize the length of a vector to one.
c
c--------------------------------------------------------------------

      subroutine normalize_vector(x,y,z)

C      implicit double precision (a-h,o-z)
      real*8 x,y,z
      REAL*8 xlength

      xlength = sqrt(x*x+y*y+z*z)
      x = x / xlength
      y = y / xlength
      z = z / xlength

      return
      end

c====================================================================
c====================================================================
c
c  Transpose an ( n x n ) tensor.
c
c--------------------------------------------------------------------

      subroutine transposeMat(n,a,b)

      implicit none
      
      integer i,j,n
      real*8 a(n,n), b(n,n)

      do i = 1,n
         do j = 1,n
            b(i,j) = a(j,i)
         end do
      end do

      return
      end


c====================================================================
c====================================================================
c
c  Calculate the dot product of two 2nd rank tensors.
c  Result is stored in cc(i,j)
c
c--------------------------------------------------------------------

      subroutine aa_dot_bb(n,a,b,c)

      implicit none
      integer i,j,k,n

      real*8 a(n,n), b(n,n), c(n,n)

      do i = 1,n
         do j = 1,n
            c(i,j) = 0
            do k = 1,n
               c(i,j) = c(i,j) + a(i,k) * b(k,j)
            end do
         end do
      end do

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the double dot product of two 2nd rank tensors.
c
c--------------------------------------------------------------------

      subroutine aa_dot_dot_bb(n,a,b,sum)

      implicit none
      integer i,j,n

      real*8 a(n,n), b(n,n), sum

      sum = 0.0D0
      do i = 1,n
         do j = 1,n
            sum = sum + a(i,j) * b(i,j)
         end do
      end do

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the double dot product of two 4th rank tensors.
c  Result is stored in c(i,j,k,l)
c
c--------------------------------------------------------------------

      subroutine aaaa_dot_dot_bbbb(n,a,b,c)

      implicit none

      integer i,j,k,l,n, m1, m2 
      real*8 a(n,n,n,n), b(n,n,n,n), c(n,n,n,n)

      do i = 1,n
       do j = 1,n
        do k = 1,n
         do l = 1,n
          c(i,j,k,l) = 0
          do m1 = 1,n
           do m2 = 1,n
            c(i,j,k,l) = c(i,j,k,l) + a(i,j,m1,m2) * b(m1,m2,k,l)
           end do !m2
          end do !m1
         end do !l
        end do !k
       end do !j
      end do !i

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the double dot product of a 4th rank tensor and
c  a 2nd rank tensor.  Result is stored in c(i,j).
c
c--------------------------------------------------------------------

      subroutine aaaa_dot_dot_bb(n,a,b,c)

      implicit none
      integer i,j,k,l,n

      real*8 a(n,n,n,n), b(n,n), c(n,n)

      do i = 1,n
       do j = 1,n
        c(i,j) = 0
        do k = 1,n
         do l = 1,n
          c(i,j) = c(i,j) + a(i,j,k,l) * b(k,l)
         end do !l
        end do !k
       end do !j
      end do !i

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the double dot product of a 2nd rank tensor and
c  a 4th rank tensor.  Result is stored in c(i,j).
c
c--------------------------------------------------------------------

      subroutine aa_dot_dot_bbbb(n,a,b,c)

      implicit none
      
      integer i,j,k,l,n
      real*8 a(n,n), b(n,n,n,n), c(n,n)

      do i = 1,n
       do j = 1,n
        c(i,j) = 0
        do k = 1,n
         do l = 1,n
          c(i,j) = c(i,j) + a(k,l) * b(k,l,i,j)
         end do !l
        end do !k
       end do !j
      end do !i

      return
      end

c====================================================================
c====================================================================
c
c  Rotates any 3x3x3x3 tensor by a rotation matrix.
c
c  c(i,j,k,l) = a(i,m) * a(j,n) * a(k,p) * a(l,q) * b(m,n,p,q)
c
c--------------------------------------------------------------------

      subroutine rotate_4th(a,b,c)

      implicit none

      integer i,j,m,n,k,l
      real*8 a(3,3), b(3,3,3,3), c(3,3,3,3), d(3,3,3,3)

      do m = 1,3
       do n = 1,3
        do k = 1,3
         do l = 1,3
          d(m,n,k,l) = a(k,1) * (a(l,1) * b(m,n,1,1) + 
     &		a(l,2) * b(m,n,1,2) + a(l,3) * b(m,n,1,3)) +
     &		a(k,2) * (a(l,1) * b(m,n,2,1) + 
     &		a(l,2) * b(m,n,2,2) + a(l,3) * b(m,n,2,3)) +
     &		a(k,3) * (a(l,1) * b(m,n,3,1) + 
     &		a(l,2) * b(m,n,3,2) + a(l,3) * b(m,n,3,3))
         end do
        end do
       end do
      end do

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          c(i,j,k,l) = a(i,1) * (a(j,1) * d(1,1,k,l) + 
     &		a(j,2) * d(1,2,k,l) + a(j,3) * d(1,3,k,l)) +
     &		a(i,2) * (a(j,1) * d(2,1,k,l) + 
     &		a(j,2) * d(2,2,k,l) + a(j,3) * d(2,3,k,l)) +
     &		a(i,3) * (a(j,1) * d(3,1,k,l) + 
     &		a(j,2) * d(3,2,k,l) + a(j,3) * d(3,3,k,l))
         end do
        end do
       end do
      end do

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the inverse of a 3 x 3 matrix.
c
c--------------------------------------------------------------------

      subroutine inverse_3x3(a,b)

      implicit none

      integer i,j
      real*8 a(3,3), b(3,3), det

      b(1,1) = a(2,2) * a(3,3) - a(3,2) * a(2,3)
      b(1,2) = a(3,2) * a(1,3) - a(1,2) * a(3,3)
      b(1,3) = a(1,2) * a(2,3) - a(2,2) * a(1,3)
      b(2,1) = a(3,1) * a(2,3) - a(2,1) * a(3,3)
      b(2,2) = a(1,1) * a(3,3) - a(3,1) * a(1,3)
      b(2,3) = a(2,1) * a(1,3) - a(1,1) * a(2,3)
      b(3,1) = a(2,1) * a(3,2) - a(3,1) * a(2,2)
      b(3,2) = a(3,1) * a(1,2) - a(1,1) * a(3,2)
      b(3,3) = a(1,1) * a(2,2) - a(2,1) * a(1,2)

      det = a(1,1) * b(1,1) + a(1,2) * b(2,1) + a(1,3) * b(3,1)

      do i = 1,3
         do j = 1,3
            b(i,j) = b(i,j) / det
         end do
      end do

      return
      end


c====================================================================
c====================================================================
c
c  Solve simultaneous equations using LU decomposition (Crout's method)
c  Result is stored in b(i)
c
c  Reference: "Numerical Recipes" Section 2.3  p. 31
c
c--------------------------------------------------------------------

      subroutine simeq(n,a,b)

      implicit none

      integer n
      real*8 a(n,n), b(n), index(n)

      call LU_Decomp(n,a,index)
      call LU_BackSub(n,a,index,b)

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the inverse of a matrix using 
c  LU decomposition (Crout's method)
c
c  Reference: "Numerical Recipes" Section 2.3  p. 31
c
c--------------------------------------------------------------------

      subroutine inverse(n,a,b)

      implicit none

      integer i,j,n
      real*8 a(n,n), b(n,n), c(n,n), index(n)

      do i = 1,n
         do j = 1,n
            c(i,j) = a(i,j)
         end do
      end do

      do i = 1,n
         do j = 1,n
            b(i,j) = 0.0D0
         end do
         b(i,i) = 1.0D0
      end do

      call LU_Decomp(n,c,index)
      do j = 1,n
         call LU_BackSub(n,c,index,b(1,j))
      end do

      return
      end

c====================================================================
c====================================================================
c
c  This sub performs an LU Decomposition (Crout's method) on the 
c  matrix "a". It uses partial pivoting for stability. The index()
c  vector is used for the partial pivoting.  The v() vector is 
c  a dummy work area.
c
c  Reference: "Numerical Recipes" Section 2.3  p. 31
c
c--------------------------------------------------------------------

      subroutine LU_Decomp(n,a,index)

      implicit none

      integer i,j,k,n, imax
      real*8 a(n,n), index(n), v(n), tiny, dummy
      real*8 a_max, sum

      tiny = 1.0D-20

c--------------------------------------------------------------------
c  Loop over the rows to get the implicit scaling info.
c--------------------------------------------------------------------

      do i = 1,n
         a_max = 0.0D0
         do j = 1,n
            a_max = max(a_max,abs(a(i,j)))
         end do !j
         v(i) = 1.0D0 / a_max
      end do !i

c--------------------------------------------------------------------
c  Begin big loop over all the columns.
c--------------------------------------------------------------------

      do j = 1,n

         do i = 1,j-1
            sum = a(i,j)
            do k = 1,i-1
               sum = sum - a(i,k) * a(k,j)
            end do
            a(i,j) = sum
         end do

         a_max = 0.0D0
         do i = j,n
            sum = a(i,j)
            do k = 1,j-1
               sum = sum - a(i,k) * a(k,j)
            end do
            a(i,j) = sum
            dummy = v(i) * abs(sum)
            if ( dummy .gt. a_max ) then
               imax = i
               a_max = dummy
            end if
         end do

c--------------------------------------------------------------------
c  Pivot rows if necessary.
c--------------------------------------------------------------------

         if ( j .ne. imax ) then
            do k = 1,n
               dummy = a(imax,k)
               a(imax,k) = a(j,k)
               a(j,k) = dummy
            end do
            v(imax) = v(j)
         end if
         index(j) = imax

c--------------------------------------------------------------------
c  Divide by the pivot element.
c--------------------------------------------------------------------

         if ( a(j,j) .eq. 0.0D0 ) a(j,j) = tiny
         if ( j .ne. n ) then
            dummy = 1.0D0 / a(j,j)
            do i = j+1,n
               a(i,j) = a(i,j) * dummy
            end do
         end if

      end do !j

      return
      end

c====================================================================
c====================================================================
c
c  Solves a set of simultaneous equations by doing back substitution.
c  The answer in returned in the b() vector.  The a(,) matrix
c  must have already been "LU Decomposed" by the above subroutine.
c
c  Reference: "Numerical Recipes" Section 2.3  p. 31
c
c--------------------------------------------------------------------

      subroutine LU_BackSub(n,a,index,b)

      implicit none

      integer i,j,m,n, ii
      real*8 a(n,n), index(n), b(n)
      real*8 sum

      ii = 0

c--------------------------------------------------------------------
c  Do the forward substitution.
c--------------------------------------------------------------------

      do i = 1,n
         m = index(i)
         sum = b(m)
         b(m) = b(i)
         if ( ii .ne. 0 ) then
            do j = ii,i-1
               sum = sum - a(i,j) * b(j)
            end do
         else if ( sum .ne. 0.0D0 ) then
            ii = i
         end if
         b(i) = sum
      end do

c--------------------------------------------------------------------
c  Do the back substitution.
c--------------------------------------------------------------------

      do i = n,1,-1
         sum = b(i)
         if ( i .lt. n ) then
            do j = i+1,n
               sum = sum - a(i,j) * b(j)
            end do
         end if
         b(i) = sum / a(i,i)
      end do

      return
      end
      
c====================================================================
c====================================================================
c
c  Restore a symmetric 4th rank tensor stored in Voigt notation 
c  back to its 4th rank form.
c
c--------------------------------------------------------------------

      subroutine Voigt_to_forth(b,a)

      implicit none

      integer i,j,k,l, ia , ib
      real*8 a(3,3,3,3), b(6,6)

      do i = 1,3
       do j = 1,3
        ia = i
        if (i.ne.j) ia=9-i-j
        do k = 1,3
         do l = 1,3
          ib = k
          if (k.ne.l) ib=9-k-l
          a(i,j,k,l) = b(ia,ib)
          if (ia.gt.3) a(i,j,k,l) = a(i,j,k,l) / 2.D0
          if (ib.gt.3) a(i,j,k,l) = a(i,j,k,l) / 2.D0
         end do
        end do
       end do
      end do

      return
      end


c====================================================================
c====================================================================
c
c  Store a SYMMETRIC 4th rank tensor in Voigt notation.
c
c--------------------------------------------------------------------

      subroutine forth_to_Voigt(a,b)

      implicit none

      integer i,j,k,l, ia, ib
      real*8 a(3,3,3,3), b(6,6)

      do i = 1,3
       do j = i,3   ! not 1 !!!
        ia = i
        if (i.ne.j) ia=9-i-j
        do k = 1,3
         do l = k,3 ! not 1 !!!
          ib = k
          if (k.ne.l) ib=9-k-l
          b(ia,ib) = a(i,j,k,l)
         end do
        end do
       end do
      end do

      return
      end



c====================================================================
c====================================================================
c
c  Perform x**y but while retaining the sign of x.
c
c--------------------------------------------------------------------

      function power(x,y)

C      implicit double precision (a-h,o-z)
      implicit none
      real*8 x,y, power

      if (x.eq.0.0) then
        if (y.gt.0.0) then
          power = 0.0
        else if (y .lt. 0.0) then
          power = 1.0d+300
        else
          power = 1.0
        end if
      else
         power = y * log10(abs(x))
         if (power .gt. 300.) then
           power = 1.d+300
         else
           power = 10.d0 ** power
         end if
         if (x .lt. 0.0) power = -power
      end if

      return
      end

c====================================================================
c====================================================================
c
c  Return the sign of a number.
c
c--------------------------------------------------------------------

      function sgn(a)

C      implicit double precision (a-h,o-z)
      implicit none
      real*8 a, sgn
      sgn = 1.0
      if (a .lt. 0.0) sgn = -1.0D0

      return
      end 

c====================================================================
c====================================================================
c
c  Calculate the determinant of a 3 x 3 matrix.
c
c--------------------------------------------------------------------

      function determinant(a)

C      implicit double precision (a-h,o-z)
      implicit none
      real*8 b1,b2,b3, determinant

      real*8 a(3,3)

      b1 = a(2,2) * a(3,3) - a(3,2) * a(2,3)
      b2 = a(3,1) * a(2,3) - a(2,1) * a(3,3)
      b3 = a(2,1) * a(3,2) - a(3,1) * a(2,2)

      determinant = a(1,1) * b1 + a(1,2) * b2 + a(1,3) * b3

      return
      end

c===================================================================
c===================================================================
c
c  Print out an array.
c
c-------------------------------------------------------------------

      subroutine print_array(n,a)

      implicit none

      integer n,i,j
      real*8 a(n,n)

      do i = 1,n
         write(6,'(10f12.3)')(a(i,j),j=1,n)
      end do
      print*,' '

      return
      end

c===================================================================
c===================================================================
c
c  Print out Euler angles in Kocks notation.
c
c-------------------------------------------------------------------

      subroutine kocks_angles(npt,m,time,array1)

      implicit none

      integer npt,m
      real*8 array1(3,3), time, pi
      real*8 psi, theta, phi

      pi = 4.D0 * atan(1.0D0)

      if (abs(array1(3,3)) .gt. 0.99999D0) then
        psi   = atan2(array1(2,1),array1(1,1))
        theta = 0.0D0
        phi   = 0.0D0
      else
        psi   = atan2(array1(2,3),array1(1,3))
        theta = acos(array1(3,3))
        phi   = atan2(array1(3,2),-array1(3,1))
      end if

      psi   = 180 * psi   / pi
      theta = 180 * theta / pi
      phi   = 180 * phi   / pi


c      write(7,'(a3,2i5,4f10.3)')'BM2',ninc,m,time,psi,theta,phi

      return
      end

c=======================================================================
c=======================================================================
c
c  Evaluate function to be minimized to zero.  gamma_dot()'s are
c  input and several things are output.
c
c-----------------------------------------------------------------------


      subroutine eval_func( xs0,	dtime,		gamma_dot,	
     &			    xm0,	F1,		num_slip_sys,
     &			    C,		F_p_inv,	F_p_inv_0,
     &			    g0,		Hdir,		Hdyn,
     &			    a0,		Adir,		Adyn,
     &			    g_sat,	F_el,		flow_exp,
     &			    kirch,	tau,		gamma_dot_zero,
     &			    g,		func,		xL_p,
     &			    xLatent,	sse,		a, 
     &                      wmm,        trDd,           Jd0, 
     &			    fini,       pf0,            pfn, 
     &                      c1,         bm,             mExp,
     &                      Jd,         Jddot,          skirch)

      implicit none
      
      integer ii, ia, ib, k,n, num_slip_sys 
      real*8 dtime, sum, Je, determinant, sum00
      real*8 xLatent, Hdir, HDyn, Adir, power
      real*8 Adyn, sse, gamma_dot_zero, flow_exp
      real*8 gamma_dot_max, g_sat
      real*8 dsumGdg(num_slip_sys), dsumGdgdot(num_slip_sys)
      

      real*8
     &	xs0(3,num_slip_sys),	xm0(3,num_slip_sys),	F_p_inv_0(3,3),
     &	F1(3,3),		C(3,3,3,3),		F_p_inv(3,3),
     &	g0(num_slip_sys),	xL_p_inter(3,3),	F_el(3,3),
     &	E_el(3,3),		Spk2(3,3),		
     &	tau(num_slip_sys),	g(num_slip_sys),	array1(3,3),
     &	func(num_slip_sys),	gamma_dot(num_slip_sys),array2(3,3),
     &	F_el_inv(3,3),		xL_p(3,3),
     &	xs(3,num_slip_sys),	xm(3,num_slip_sys),
     &	a0(num_slip_sys),	a(num_slip_sys)
	REAL*8 omega
	INTEGER i,j

      real*8 wmm(num_slip_sys), trDd, Jd0, Jd, Jddot
      real*8 FdInv(3,3), FdTerm, FFdInv(3,3), kirch(3,3)
      real*8 pkirch, pf0, pf, fini, pfn, f, fdotca, skirch(3,3)
      real*8 c1, bm, mExp, sumSlip, sumG, dtheta, func_pf

c*** Note that xs0 and xm0 are in INTERMEDIATE configuration!!!

c--------------------------------------------------------------------
c  Calculate the plastic part of the
c  velocity gradient in the intermediate configuration.
c--------------------------------------------------------------------

      do i = 1,3
        do j = 1,3
          xL_p_inter(i,j) = 0.0D0
          do k = 1,num_slip_sys
            xL_p_inter(i,j) = xL_p_inter(i,j) + 
     &			xs0(i,k) * xm0(j,k) * gamma_dot(k)
          end do
        end do
      end do

c--------------------------------------------------------------------
c  Begin calculation process of F_p_n+1 = exp(xL_p_inter*dt).F_p_n
c--------------------------------------------------------------------

      do i = 1,3
        do j = 1,3
          array1(i,j) = xL_p_inter(i,j) * dtime
        end do
      end do

c--------------------------------------------------------------------
c  Calculate omega.
c--------------------------------------------------------------------

      sum = 0
      do i = 1,3
        do j = 1,3
          sum = sum + array1(i,j) * array1(i,j)
        end do
      end do
      omega = sqrt(0.5D0 * sum)

c--------------------------------------------------------------------
c  Continue calculating intermediate stuff needed for F_p_n+1
c--------------------------------------------------------------------
      call aa_dot_bb(3,array1,array1,array2)
      do i = 1,3
        if (abs(omega).ge.0.00000001D0) then   ! if omega=0 then no need
          do j = 1,3
            array1(i,j) = array1(i,j) * sin(omega) / omega +
     &			array2(i,j) * (1-cos(omega)) / omega**2
	    	if (j .eq. 4) then
			print *,omega
		end if 
          end do
        end if
        array1(i,i) = 1 + array1(i,i)
      end do
c--------------------------------------------------------------------
c   Finally multiply arrays to get F_p_inv at end of time step.
c--------------------------------------------------------------------

      call inverse_3x3(array1,array2)
      call aa_dot_bb(3,F_p_inv_0,array2,F_p_inv)

c--------------------------------------------------------------------
c   Calcualte det of F_d (damage) = Jd_n+1
c--------------------------------------------------------------------
C      Jd = exp(trDd*dtime)*Jd0
      call get_Jd(Jd,fini,pf0,pfn)

c--------------------------------------------------------------------
c   Calcualte time rate of Jd = dot(Jd)_n+1
c--------------------------------------------------------------------
C      Jddot = (Jd - Jd0)/dtime

c--------------------------------------------------------------------
c   Calcualte Inverse F_d_n+1
c--------------------------------------------------------------------
      do i = 1,3
         do j = 1,3
            FdInv(i,j) = 0.D0 
         end do
      end do 
      FdTerm = Jd**(-1.D0/3.D0)
      FdInv(1,1) = FdTerm
      FdInv(2,2) = FdTerm
      FdInv(3,3) = FdTerm

c--------------------------------------------------------------------
c  Multiply F() by F_d_inv() 
c--------------------------------------------------------------------
      call aa_dot_bb(3,F1,FdInV,FFdInv)

c--------------------------------------------------------------------
c  Multiply F()*F_d_inv by F_p_inv() to get F_el()
c--------------------------------------------------------------------
      call aa_dot_bb(3,FFDInv,F_p_inv,F_el)
C      call aa_dot_bb(3,F1,F_p_inv,F_el)
      call inverse_3x3(F_el,F_el_inv)

c--------------------------------------------------------------------
c  Rotate director vectors from intermediate configuration to
c  the current configuration.
c--------------------------------------------------------------------
      do n = 1,num_slip_sys
        do i = 1,3
          xs(i,n) = 0.0D0
          xm(i,n) = 0.0D0
          do j = 1,3
            xs(i,n) = xs(i,n) + F_el(i,j) * xs0(j,n)
            xm(i,n) = xm(i,n) + xm0(j,n)  * F_el_inv(j,i)
          end do
        end do
      end do
c--------------------------------------------------------------------
c  Calculate elastic Green Strain
c--------------------------------------------------------------------

      call transposeMat(3,F_el,array1)
      call aa_dot_bb(3,array1,F_el,E_el)
      do i = 1,3
        E_el(i,i) = E_el(i,i) - 1
        do j = 1,3
          E_el(i,j) = E_el(i,j) / 2
        end do
      end do 

c--------------------------------------------------------------------
c  Multiply the stiffness tensor by the Green strain to get
c  the 2nd Piola Kirkhhoff stress
c--------------------------------------------------------------------

      call aaaa_dot_dot_bb(3,C,E_el,Spk2)


c--------------------------------------------------------------------
c  Convert from PK2 stress to Kirchhoff stress
c--------------------------------------------------------------------

      Je = determinant(F_el)
      call transposeMat(3,F_el,array2)
      call aa_dot_bb(3,F_el,Spk2,array1)
      call aa_dot_bb(3,array1,array2,kirch)


c--------------------------------------------------------------------
c  Convert from Kirchhoff stress to Cauchy stress
c--------------------------------------------------------------------
C      do i = 1,3
C        do j = 1,3
C          sig(i,j) = kirch(i,j) / (Je*Jd)
C        end do
C      end do

C     Need to Cauchy stress?


c--------------------------------------------------------------------
c  Calculate hydrostatic kirchhoff stress
c--------------------------------------------------------------------
      pkirch = (1.D0/3.D0)*(kirch(1,1) + kirch(2,2) + kirch(3,3))

c-------------------------------------------------------------------
c     Deviatoric Kirchhoff stress 
c-------------------------------------------------------------------
      do i = 1,3
       do j = 1,3
          if (i.eq.j) then
             skirch(i,j) = kirch(i,j) - pkirch
          else
             skirch(i,j) = kirch(i,j)
          end if
       end do
      end do

c--------------------------------------------------------------------
c  Calculate resolved shear stress for each slip system.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        tau(k) = 0.0D0
        do j = 1,3
          do i = 1,3
            tau(k) = tau(k) + xs(i,k) * xm(j,k) * skirch(i,j)
          end do 
        end do
      end do
      
c--------------------------------------------------------------------
c  Calculate hardening law for ref shear stress
c--------------------------------------------------------------------

      sum = 0
      do i = 1,num_slip_sys
        sum = sum + abs(gamma_dot(i))
      end do
      do k = 1,num_slip_sys
        sum00 = xLatent * sum - (xLatent - 1) * abs(gamma_dot(k))
        g(k) = (g0(k) + Hdir*sum00*dtime) / (1 + Hdyn*sum*dtime)
      end do

c--------------------------------------------------------------------
c  Calculate hardening law for back stress
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        a(k) = (a0(k) + Adir * dtime * gamma_dot(k))
     &			 / (1.D0 + Adyn * dtime * abs(gamma_dot(k)))
      end do


c--------------------------------------------------------------------
c  Calculate curent time step porosity
c--------------------------------------------------------------------
C      call get_f_fromJd(Jd,fini,pf,pfn )
C      dtheta = (1.D0/(1.D0-pf)**(1.D0/mExp) - (1.D0 -pf))/(1.D0-pf)

c--------------------------------------------------------------------
c    sum of slip from each system for use in porosity kinetics
c--------------------------------------------------------------------
C     g	(num_slip_sys,max_grains)
C      sumSlip = 0.D0
C      do k = 1,num_slip_sys
C       sumSlip = sumSlip + abs(gamma_dot(k))
C      end do
      
C      call get_sumG(sumG,g,gamma_dot,sumSlip,num_slip_sys,
C     &               dsumGdg, dsumGdgdot)

c--------------------------------------------------------------------
c  Calculate cocks-ashby porosity evlotuion rate 
C  Note that here the degradation function w is 
C  assumed to be from beginning of time step
c  maybe this can be updated in the future
c--------------------------------------------------------------------
C     Need to pass this out of function
C      call get_fdotca(fdotca, c1,bm, pkirch, dtheta,sumSlip,sumG )

c--------------------------------------------------------------------
c  Calculate function values.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        func(k) = tau(k)- wmm(k)*a(k) 
     &                  - wmm(k)*g(k) * power( (gamma_dot(k)/
     &			gamma_dot_zero), (1.D0/flow_exp) )
      end do
c--------------------------------------------------------------------
c  Calculate porosity residual values
c--------------------------------------------------------------------
C      call eval_func_pf(func_pf,pf,pf0,dtime,fdotca)
C      func(num_slip_sys + 1) = func_pf
c--------------------------------------------------------------------
c  Calculate root mean square error that is to be minimized to zero.
c--------------------------------------------------------------------

      sse = 0.0D0
      gamma_dot_max = abs(gamma_dot(1))
      do k = 1,num_slip_sys
        sse = sse + abs(gamma_dot(k)) * func(k) ** 2
        gamma_dot_max = max(abs(gamma_dot(k)),gamma_dot_max)
      end do

      if (gamma_dot_max .gt. 1.D-12) then
         sse = sse/gamma_dot_max
         sse = sqrt(sse) / (num_slip_sys)
      end if 

      return
      end


c=======================================================================
c=======================================================================
c
c  Evaluate porosity evolution based on porosity f
c
c-----------------------------------------------------------------------


      subroutine get_fdotca(fdotca, c1,bm, pkirch, dtheta,sumSlip,sumG )

      implicit none 

C     output
      real*8 fdotca
C     input
      real*8 c1, bm, pkirch, dtheta, sumSlip, sumG
C     other
      real*8 sinhbmt
C     c1 cocks ashby parameter
C     bm = (n-0.5)/(n+0.5)
C     pkirch = pressure from kirchhoff stress
C     dtheta = 1/(1-f)^n - (1-f)
C     sumSlip = sum of gamma_dot
C     sumG is the weight sum of slip system strengths

      sinhbmt = sinh(bm*pkirch/sumG)
      fdotca = c1*sinhbmt*dtheta*sumSlip

      end 

c=======================================================================
c=======================================================================
c
c  Evaluate average cocks-ashby consistent degration funtion
c
c-----------------------------------------------------------------------

      subroutine get_wmmAve(wmmAve, pf, mExp, bm, pkirch, sumG, 
     &                      pc1, pc2, hh3 )
      implicit none 

C     output
      real*8 wmmAve
C     input
      real*8 pf, mExp, bm, pkirch, sumG, pc1, pc2, hh3
C     internal 
      real*8 dtheta, coshbmt, hh1, am, OmegaS
C     constants
      real*8 nhalf, two3
C     am is leading coefficent in consistent degration funtion
C     bm = (n-0.5)/(n+0.5)
C     pkirch = pressure from kirchhoff stress
C     dtheta = 1/(1-f)^n - (1-f)
C     sumG is the weight sum of slip system strengths

      nhalf = -0.5D0
      two3 = 2.D0/3.D0

      dtheta = (1.D0/(1.D0-pf)**(1.D0/mExp) - (1.D0 -pf))/(1.D0-pf)
      coshbmt = cosh(bm*pkirch/sumG)
      hh1 = 1.D0 + two3*pf
      am = (pc1/pc2)*hh1*sqrt(6.D0)*sqrt(two3)*(2.D0+mExp)/(2.D0-mExp)
      OmegaS = am*coshbmt*dtheta
      wmmAve = hh3*(hh1 + OmegaS)**(nhalf)      

      end 




c=======================================================================
c=======================================================================
c
c  Determine the Jacobian of the peforamtion from porosity Jd
c
c-----------------------------------------------------------------------


      subroutine get_Jd(Jd,fini,f,fn )

      implicit none

C     output
      real*8 Jd
C     input
      real*8 fini, f, fn

      Jd = (1.D0 - fini) / (1.D0 - (f - fn))

      end 


c=======================================================================
c=======================================================================
c
c  determined porosity from Jd
c
c-----------------------------------------------------------------------


      subroutine get_f_fromJd(Jd,fini,f,fn )

      implicit none 

C     output
      real*8 f
C     input
      real*8 fini, Jd, fn

      f = (fini - 1.D0)/Jd + 1.D0 +fn

      end 

c=======================================================================
c=======================================================================
c  Calculate residusls for porosity evalution
C  the is based on cocks ashby porosity kinetics estimates 
c--------------------------------------------------------------------
      subroutine eval_func_pf(func,f,f0,dtime,fdotca)

      implicit none

C     output 
      real*8 func
C     input
      real*8 f,f0,dtime,fdotca
C     residual
      func = (f - f0)/dtime - fdotca

      end 


c=======================================================================
c=======================================================================
c
c  get abs of slip systems
c
c-----------------------------------------------------------------------


      subroutine get_sumSlip(sumSlip,num_slip_sys,gamma_dot)
      
      implicit none

C     output
      real*8 sumSlip
C     input
      real*8 gamma_dot(num_slip_sys)
      integer i, num_slip_sys

      sumSlip = 0.D0
      do i = 1,num_slip_sys
         sumSlip = sumSlip + abs(gamma_dot(i))
      end do

      end


c=======================================================================
c=======================================================================
c
c  get the weighte sum of slip system strengths
c
c-----------------------------------------------------------------------


      subroutine get_sumG(sumG,g,gamma_dot,sumSlip,num_slip_sys,
     &           dsumGdg, dsumGdgdot )
      
      implicit none

C     output
      real*8 sumG
      real*8 dsumGdg(num_slip_sys)
      real*8 dsumGdgdot(num_slip_sys)
C     input
      integer i, num_slip_sys
      real*8 sumSlip, gamma_dot(num_slip_sys), g(num_slip_sys)
      

C     sumSlip = sum of gamma_dot
C     sumG is the weight sum of slip system strengths
C     gamma_dot is the slip rate in each system

C     weight sum based on slip system activity
       sumG = 0.D0
       if (sumSlip >= 1.D-12) then
          do i = 1,num_slip_sys
C             sumG = sumG + abs(gamma_dot(i))*g(i)
             sumG = sumG + g(i)
          end do 

C          sumG = sumG/sumSlip
          sumG = sumG/real(num_slip_sys)
       else
C         probably a hack, but just assume that if there is no slip
C         activity that the the sum of strenghts is just the strenght 
C         of the first system
C         hopefuly this smooths transitions to slip acivity
          sumG = g(1)
                  
       end if

C      calculate derivative of sumG wrt g(alpha)

       do i = 1,num_slip_sys
C          if (sumSlip > 1.D-9) then
C             dsumGdg(i) = abs(gamma_dot(i))/ sumSlip
C          else
C             dsumGdg(i) = 0.D0
C          end if

          dsumGdg(i) = 1.D0


C      calculate derivative of sumG wrt slip rate gammadot

C          dsumGdgdot(i) = -1.D0*g(i)*sumSlip**2.D0
           dsumGdgdot(i) = 0.D0
       end do
  
      end


         SUBROUTINE UVARM(UVAR,DIRECT,T,TIME,DTIME,CMNAME,ORNAME,
     1 NUVARM,NOEL,NPT,LAYER,KSPT,KSTEP,KINC,NDI,NSHR,COORD,
     2 JMAC,JMATYP,MATLAYO,LACCFLA) 
c
C      INCLUDE 'ABA_PARAM.INC'
c
      CHARACTER*80 CMNAME,ORNAME
      CHARACTER*3 FLGRAY(200)
      DIMENSION UVAR(NUVARM),DIRECT(3,3),T(3,3),TIME(2)
      DIMENSION ARRAY(200),JARRAY(200),JMAC(*),JMATYP(*),COORD(*)

C      CALL GETVRM('SDV',ARRAY,JARRAY,FLGRAY,JRCD,JMAC,JMATYP,
C     1 MATLAYO,LACCFLA)

c	Plastic strain

      UVAR(1)  = ARRAY(37) !! plastic strain (1,1)
      UVAR(2)  = ARRAY(38) !! plastic strain (1,2)
      UVAR(3)  = ARRAY(39) !! plastic strain (1,3)
      UVAR(4)  = ARRAY(41) !! plastic strain (2,2)
      UVAR(5)  = ARRAY(42) !! plastic strain (2,3)
      UVAR(6)  = ARRAY(45) !! plastic strain (3,3)
      
c	Fp      

      UVAR(7)  = ARRAY(46) !! eff tot strain
      UVAR(8)  = ARRAY(47) !! eff plastic strain     
      UVAR(9)  = ARRAY(48) !! eff plastic strain increment

      RETURN
      END
C****************************************************************
	SUBROUTINE DLOAD(F,KSTEP,KINC,TIME,NOEL,NPT,LAYER,KSPT,
     & COORDS,JLTYP,SNAME)
  
C
C      INCLUDE 'ABA_PARAM.INC'
C
      DIMENSION TIME(2),COORDS(3)
	CHARACTER*80 SNAME
C
      IF(KSTEP.EQ.1) THEN
		F=1000
		F=F*TIME(1)/10.0*(-1.0)   !!! tensile load (negative)
	END IF
	IF(KSTEP.EQ.2) THEN
	  STF=1000
	  ENF=STF*0.0
	  SLOPE=(STF-ENF)/10.0
	  F=STF-(SLOPE*TIME(1))
        F=F*(-1)
	END IF
C
      RETURN
      END
