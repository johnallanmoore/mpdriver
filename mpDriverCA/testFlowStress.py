#!/usr/bin/python

import matplotlib.pyplot as plt
import numpy as np

sy0 = 260
e0 = 0.004
ns = 0.1
endStrain = 0.2

epb = np.linspace(0,endStrain,100)

sy = sy0*(1.+epb/e0)**ns

plt.plot(epb,sy)
plt.ylim([0,1.1*max(sy)])
plt.show()
