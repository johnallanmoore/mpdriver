C                                                  _______
C                 \    /  |    |   |\  /|    /\       |
C                  \  /   |    |   | \/ |   /__\      |
C                   \/    |____|   |    |  /    \     |
C
C  Updates stresses, internal variables, and energies for 3D solid elements ONLY
C
C  
C     J2-PLASTICITY MATERIAL SUBROUTINE WITH COCKS ASHBY POROSITY EVOLUTION 
C              DEGRADATION FUNCTION BASED ON YIELD MARIN/COCKS YIELD FUN.
C              RADIAL RETURN METHOD (PLUS CONVERSION FROM KIRCHHOFF STRESS)
C              VIA MATRIX INVERSION INSTEAD OF THE CLOSED FORM 
C              SOLUTION FOR THE PLASTIC STRAIN-RATE MULTIPLIER
C              INCREMENT WRITTEN FOR THE ABAQUS\EXPLICIT MODULE
C  
C  ============================================================================
      subroutine vumat(
C Read only -
     1     nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,
     2     stepTime, totalTime, dt, cmname_int, coordMp, charLength,
     3     props, density, strainInc, relSpinInc,
     4     tempOld, stretchOld, defgradOld, fieldOld,
     5     stressOld, stateOld, enerInternOld, enerInelasOld,
     6     tempNew, stretchNew, defgradNew, fieldNew,
C Write only -
     7     stressNew, stateNew, enerInternNew, enerInelasNew)

      implicit none             ! This is used during compilation testing to make

!        When implicit none is used during compilation testing, all the following

!        variables need to be defined.

      integer nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal
      integer km
      real*8  stepTime, totalTime, dt, coordMp, charLength, props,
     +          density, strainInc, relSpinInc, tempOld, stretchOld,

     +          defgradOld, fieldOld, stressOld, stateOld, 

     +          enerInternOld, enerInelasOld, tempNew, stretchNew,

     +          defgradNew, fieldNew, stressNew, stateNew, 

     +          enerInternNew, enerInelasNew

      dimension props(nprops), density(nblock), coordMp(nblock,*),
     1  charLength(nblock), strainInc(nblock,ndir+nshr),
     2  relSpinInc(nblock,nshr), tempOld(nblock),
     3  stretchOld(nblock,ndir+nshr),
     4  defgradOld(nblock,ndir+nshr+nshr),
     5  fieldOld(nblock,nfieldv), stressOld(nblock,ndir+nshr),
     6  stateOld(nblock,nstatev), enerInternOld(nblock),
     7  enerInelasOld(nblock), tempNew(nblock),
     8  stretchNew(nblock,ndir+nshr),
     9  defgradNew(nblock,ndir+nshr+nshr),
     1  fieldNew(nblock,nfieldv),
     2  stressNew(nblock,ndir+nshr), stateNew(nblock,nstatev),
     3  enerInternNew(nblock), enerInelasNew(nblock)

      integer cmname_int,i,j,k,counter
C-----------------------------------------------------------------------------
C                       Start new variable definitions here
C-----------------------------------------------------------------------------
      real*8 sigh,e,G,xnu,tr,sy0,lame1,tsig1,tsig2,dwp1,dwp2
      real*8 tsig3,tsig4,tsig5,tsig6,tsigh,tdev1,tdev2
      real*8 tdev3,tdev4,tdev5,tdev6,tsige,epb0,sy,plmul,plinc
      real*8 epb,H,depb,sige,n,C(6,6),I6(6,6),II(6,6)
      real*8 Ih(6,6),norm(6),Nmat(6,6),Cinv(6,6),sqrt23
      real*8 Ainv(8,8),den,rl(8),rr(8),dep(6), EYE2(2,2)
      real*8 fst,dfstdf,r(6),f0,fult,drdq1(6),drdq2(6)
      real*8 fini,f,trr,sig(6), wmm, drdsig(6,6)
      real*8 sigret,coef8
      real*8 argu,dev(6),Amat(8,8),Ivect(6)
      real*8 IkronI(6,6),df,fncheck,e0,fn, fnuc,ns,fterm
      real*8 sigdev,  mf,h2sigcoef1
      real*8 dh2dsig(6),dh2dq1,dh2dq2, c1, c2, Jd, JdInv
      real*8 T, nhalf, two3, n32,rJ2(6)
      real*8 three2,half, n52,one3
      real*8 am,bm,dtheta,m,hh1,hh3,term1,term2
      real*8 sinhbmt, coshbmt, Aterm, sigInv, X1
      real*8 drdf1, drdf2, drdf3, dhh1df, dhh3df,wmmMax
      real*8 dAddt, dAdhh1, dApdA,ddtdf, OmegaS, c3
      real*8 Bterm,drdsigDev(6,6),drdsigVol(6,6),dAdT,dBdT
      real*8 drdsigDDsig(6),dh1dsig(6),dh1dq1,dh1dq2,dTdsy
      real*8 pi, epsnuc,snuc,Anuc,Bnuc,dfnuc, spall
      logical YIELD

C     varibles for checking derivatives
      real*8 h1Old, h1New, sigOld(6),  dh1dsig_fd(6)
      real*8 rOld(6), drdsig_fd(6,6)
      integer iii,jj

C-----------------------------------------------------------------------------
C     This model implements a yield criteria phi = sige - wmm*sy
C     where sige is the standard J2 equivilent stress
C     sy is the flow stress determined by a swift hardening law
C     and wmm is a degradation function based on a slighty modified 
C     version of the yield function presented in Marin and McDowell
C     International Journal of Plasticity, Vol. 12, No. 5, pp. 629-669, 1996
C     Associative versus non-associative porous viscoplasticity based 
C     on internal state variable concepts
C     this model also uses Cocks-Ashby (Intergranular fracture during 
C     power-law creep under multiaxial stresses, 1980) porosity evolution
C     with calibration paramter c1 added outside the sinh function
C     and calibration paprameter c2 replace the 2 inside the sinh
C
C     the Marin model is further modified to replace sigh/sige with sigh/sy
C     the avoids a singularity for cases of hydrostatic tension
C     and may prove more stable than the sigh/sige version
C-----------------------------------------------------------------------------

C-----------------------------------------------------------------------------
C	          some usefull numbers
C-----------------------------------------------------------------------------
      nhalf = -1.D0/2.D0
      half = 1.D0/2.D0
      sqrt23 = sqrt(2.D0/3.D0)
      two3 = 2.D0/3.D0
      n32 = -3.D0/2.D0
      n52 = -5.D0/2.D0
      one3 = 1.D0/3.D0
      three2 = 3.D0/2.D0
      pi = 4.D0*ATAN(1.D0)
C-----------------------------------------------------------------------------
C	           Initialize material data from input file
C-----------------------------------------------------------------------------
      e    = props(1) ! Young's modulus
      xnu  = props(2) ! Poisson's ratio
      sy0  = props(3) ! initial yield strength
      e0   = props(4) ! initial yield strain
      ns    = props(5)! matrix stress exponent
      c1 = props(6)   ! Cocks Ashby porosity evol coef1
      c2 = props(7)   ! Cocks Ashby porosity evol coef2
      fini  = props(8)! initial porosity
      n    = props(9) ! Cocks Ashby creep exponent 
      fult  = props(10)! failure porosity
      spall = props(11)! spall strength of material

C     Derived Quanitites
      lame1 = e*xnu/((1.D0+xnu)*(1.D0-2.D0*xnu)) ! lame1
      G     = e/(2.D0*(1.D0+xnu)) ! lame2

C     modifed Marin Model parameter
      m     = 1.D0/n
      bm    = c2*(2.D0 - m)/(2.D0 + m)
            
C     nucleation volume fraction (not really implemented)
C     ON
C      fnuc   = 0.003D0
C      epsnuc = 0.01D0
C      snuc   = 0.001D0
C     OFF
      fnuc = 0.D0
      snuc   = 0.001D0
C
C  Determine the inverse of the elastic moduli tensor and
C  get the 4th order projection tensor Ih (P4)
C
      C = 0.
      do i=1,3
         C(i,i) = lame1+2.D0*G
         C(i+3,i+3) = 2.D0*G
      enddo
      C(1,2) = lame1
      C(2,3) = lame1
      C(1,3) = lame1
      C(2,1) = lame1
      C(3,1) = lame1
      C(3,2) = lame1

      I6 = 0.
      do i=1,6
         I6(i,i) = 1.D0
      enddo

      Ivect = 0.
      II = 0.
      do i=1,3
         do j=1,3 
            II(i,j) = -1.D0/3.D0
         enddo
         Ivect(i) = 1.D0
      enddo

      EYE2 = 0.
      do i=1,2
         EYE2(i,i) = 1.D0
      enddo

      Ih = I6 + II

      call invert(C,6,Cinv)

      call kron(Ivect,Ivect,6,IkronI)
C-----------------------------------------------------------------------------           
C	               Start element loop
C-----------------------------------------------------------------------------
      do k = 1,nblock
C-----------------------------------------------------------------------------
C                         Get old internal varibles
C-----------------------------------------------------------------------------
         epb0   = stateOld(k,1)
         f0     = stateOld(k,5)
         f      = f0
C-----------------------------------------------------------------------------
C              Calcualte Jd, Jacobian for converting from Kirch to Cauchy
C-----------------------------------------------------------------------------
         Jd = 1.D0/(1.D0-f)
C-----------------------------------------------------------------------------
C                         Get trial Kirchhoff stress
C-----------------------------------------------------------------------------
         tr    = strainInc(k,1) + strainInc(k,2) + strainInc(k,3)
         tsig1 = Jd*stressOld(k,1) + lame1*tr + 2.D0*G*strainInc(k,1) 
         tsig2 = Jd*stressOld(k,2) + lame1*tr + 2.D0*G*strainInc(k,2)
         tsig3 = Jd*stressOld(k,3) + lame1*tr + 2.D0*G*strainInc(k,3)
         tsig4 = Jd*stressOld(k,4) + 2.D0*G*strainInc(k,4)  
         tsig5 = Jd*stressOld(k,5) + 2.D0*G*strainInc(k,5) 
         tsig6 = Jd*stressOld(k,6) + 2.D0*G*strainInc(k,6)
C-----------------------------------------------------------------------------
C              Get hydrostatic, deviatoric and  equivalent stress
C-----------------------------------------------------------------------------
         tsigh = (tsig1 + tsig2 + tsig3)/3.D0 

         tdev1  = tsig1 - tsigh
         tdev2  = tsig2 - tsigh
         tdev3  = tsig3 - tsigh
         tdev4  = tsig4
         tdev5  = tsig5
         tdev6  = tsig6
C
         tsige  = sqrt(tdev1*tdev1 + tdev2*tdev2+
     1            tdev3*tdev3 + 2.D0*tdev4*tdev4+        
     2            2.D0*tdev5*tdev5 
     3            + 2.D0*tdev6*tdev6)*sqrt(3.D0/2.D0) 
         
C        Swift Hardening Law
         sy = sy0*(1.+epb0/e0)**ns
         H  = (ns/e0)*sy0*(1.D0+epb0/e0)**(ns-1.D0)

C        trial trixaixiality like quantitity
C         c3 = stateOld(k,8)
C         if (c3 < 1e-3) then
C            c3 = 1.D0
C         endif
C        Cap triaxiality based on spall strength
C        so sinh term does not blow up
         c3 = 1.0D0
         if (abs(tsigh) > spall) then
            T = spall/(c3*sy)            
         else
            T = tsigh/(c3*sy)
         end if



C      Does the pressure release with porosity?
C      How did Nathan reduce pressure with porosity
C      need to figure that out



C        catch hydrostatic pressure case
         if (tsige.GT.0.D0) then
            sigInv = tsige**(-1.D0)
         else
            sigInv = 0.D0
         endif
  
C-----------------------------------------------------------------------------
C        Get return direction for stress update
C-----------------------------------------------------------------------------
         if (f0.LE.fini) then
            f0 = fini
            f = f0
         endif
         if (tsigh.GE.0.D0) then
            fst = f0
            dfstdf = 1.D0
         else
            fst = f0 - fnuc
            dfstdf = 1.D0
         endif

C        Initialize degradation function 
         wmmMax = stateOld(k,8)
         
C        modifed Martin damage function parameters that are functions
C        of porosity (called h1 etc in text) and stress
         hh1 = 1 + two3*f
         hh3 = (1.D0 - f)**(1.D0/(m+1.D0))
         am = (c1/c2)*hh1*sqrt(6.D0)*sqrt(two3)*(2.D0+m)/(2.D0-m)
         dtheta = (1.D0/(1.D0-f)**(1.D0/m) - (1.D0 -f))/(1.D0-f)
         sinhbmt = sinh(bm*T)
         coshbmt = cosh(bm*T)
         OmegaS = am*coshbmt*dtheta
C        Degrdation Function
         wmm = hh3*(hh1 + OmegaS)**(nhalf)

C        enforce that damaged material cannot become undamged
C         if (wmm.GE.wmmMax) then
C            wmm = wmmMax
C         endif
            
C        protect against a damage parameter of zero (really this is failure)
         if (wmm.le.0.D0) then
C        this is weird, it runs longer with this stop command
C        its esspestially odd, because i dont think this if triggers
C        confirmed in the debugger, never trigger
C        perhaps some issue with wmm
            STOP
            print *, "counter " , counter
            print *, "wmm " , wmm
         endif
         
C         print *, wmm

         if (wmm < 0.9999) then
C            print *, wmm
         endif


C        Return Direction with spot for pres dependent term
         rJ2(1) = sigInv*three2*tdev1
         rJ2(2) = sigInv*three2*tdev2
         rJ2(3) = sigInv*three2*tdev3
         rJ2(4) = sigInv*three2*tdev4
         rJ2(5) = sigInv*three2*tdev5
         rJ2(6) = sigInv*three2*tdev6
C
         norm(1) = sqrt23*rJ2(1)
         norm(2) = sqrt23*rJ2(2)
         norm(3) = sqrt23*rJ2(3)
         norm(4) = sqrt23*rJ2(4)
         norm(5) = sqrt23*rJ2(5)
         norm(6) = sqrt23*rJ2(6)

C        n x n deviatoric 
         call kron(norm,norm,6,Nmat)

C        Trial return direction
         term1 = hh1+am*dtheta*coshbmt
         term2 = am*bm*dtheta*sinhbmt
         r(1) = rJ2(1) + half*hh3*one3*term1**n32*term2 
         r(2) = rJ2(2) + half*hh3*one3*term1**n32*term2 
         r(3) = rJ2(3) + half*hh3*one3*term1**n32*term2 
         r(4) = rJ2(4) 
         r(5) = rJ2(5)  
         r(6) = rJ2(6)   

C        drdsig debug
         rOld = r

C
C-----------------------------------------------------------------------------
C        Initialize internal variables
C-----------------------------------------------------------------------------
C
         plinc  = 0.D0
C
C-----------------------------------------------------------------------------
C        I use the magnitude of the strain increment to help convergence
C-----------------------------------------------------------------------------
         plmul = sqrt(strainInc(k,1)**2.D0 + strainInc(k,2)**2.D0
     &           + strainInc(k,3)**2.D0 + 2.D0*(strainInc(k,4)**2.D0
     &           + strainInc(k,5)**2.D0 + strainInc(k,6)**2.D0))
         depb  = plmul

C-----------------------------------------------------------------------------
C           Check for yielding
C-----------------------------------------------------------------------------
         YIELD    = .FALSE.
C
            if ( tsige - wmm*sy > 0.D0) then
C
                YIELD = .TRUE.
                sige    = tsige
                sigh    = tsigh
                f       = f0
                sig(1)  = tsig1
                sig(2)  = tsig2
                sig(3)  = tsig3
                sig(4)  = tsig4
                sig(5)  = tsig5
                sig(6)  = tsig6
                dev(1)  = tdev1
                dev(2)  = tdev2
                dev(3)  = tdev3
                dev(4)  = tdev4
                dev(5)  = tdev5
                dev(6)  = tdev6

            else
C
                YIELD = .FALSE.
C
                sig(1)  = tsig1
                sig(2)  = tsig2
                sig(3)  = tsig3
                sig(4)  = tsig4
                sig(5)  = tsig5
                sig(6)  = tsig6
C
                sige  = tsige
                sigh  = tsigh
                epb   = epb0
                f     = f0
C
            end if 
C-----------------------------------------------------------------------------            
C        Begin Minimization loop
C-----------------------------------------------------------------------------
         counter = 0

C        checks of derivatives
C        dh1dsig
         call dblcontr(sig,r,sigret)
         h1Old = sigret/((1-f)*sy)
         h1New = h1Old

         do while (YIELD)
C           
            counter = counter + 1
C-----------------------------------------------------------------------------
C           Put a stopper in case minimzation fails (usually around 50 iterations)
C-----------------------------------------------------------------------------
            if (counter .GE. 50) then
C                print *,'Did not minimize'
C                print *, "T ", T
C                print *, "sige ", sige
C                print *, "epb ", epb 
C                print *, "f ", f
C                print *, "wmm " , wmm
C                print *, "sinhbmt ", sinhbmt
C                print *, "steptime", steptime
C                print *, "--------------"
                go to 100
            end if
C-----------------------------------------------------------------------------
C           Get the plastic increment 
C           (see Box 5.13 of Belytschko et al Book
C           Nonliner FE for Continua and Structure 2nd ed)
C           Determine the inverse of the matrix A
C-----------------------------------------------------------------------------
C           some terms that are functions of f
            fterm = ((1.D0 - f )**(-n) - (1.D0 - f))
            coef8 = 1./((1.D0-f)*sy)
            mf = (1.D0 - f)
c           Double contraction of stress and return direction and sig_ij s_ij
            call dblcontr(sig,r,sigret)
            call dblcontr(sig,dev,sigdev)
C
C           Derivatives for denominator of delta lambda
C-----------------------------------------------------------------------------
C           Calculate drdsig
C-----------------------------------------------------------------------------           
C           leading constants
            X1 = (1.D0/6.D0)*hh3*am*bm*dtheta
C           A = h1 + am*dtheta*cosh(bm*t)
            Aterm = hh1 + am*dtheta*coshbmt
            Bterm = am*dtheta*bm*sinhbmt
C           drij/dsigij deviatoric (correct)
            drdsigDev = three2*sigInv*(Ih - Nmat)
C           dAterm/dT
            dAdT = am*bm*dtheta*sinhbmt
C           dBterm/dT
            dBdT = bm*coshbmt
C           drij/dsigij Volumetic/Spherical
            X1 = X1*(n32*Aterm**n52*dAdT*Bterm + Aterm**n32*dBdT)
            drdsigVol = X1*(one3/sy)*IkronI
C           drdsig
            drdsig = drdsigDev +  drdsigVol  
C           drdsig debug
            do iii=1,6
               do jj=1,6
                  if (abs(sig(jj)- sigOld(jj)) > 1.D-12) then
                     drdsig_fd(iii,jj) = (r(iii) - rOld(iii))
     1               /(sig(jj)-sigOld(jj))
                  else
                     drdsig_fd(iii,jj) = 0.D0
                  end if 
               end do
            end do 

            if (counter.eq.1) then
C            print *, "step ", steptime
C            print *, "counter", counter
C            print *, "drdsig", drdsig
C            print *, "drdsig_fd", drdsig_fd
C            print *, "---------------"
            end if 
            
C-----------------------------------------------------------------------------
C           Calculate drdq1
C-----------------------------------------------------------------------------
            X1 = H*(-1.D0/6.D0)*hh3*am*bm**2.D0*dtheta*sigh/sy**2.D0
            Aterm = hh1 + am*dtheta*coshbmt
            drdq1(1) = X1*(n32*Aterm**n52*am*dtheta*sinhbmt**2.D0
     1               +     Aterm**n32*coshbmt)
            drdq1(2) = X1*(n32*Aterm**n52*am*dtheta*sinhbmt**2.D0
     1               +     Aterm**n32*coshbmt)
            drdq1(3) = X1*(n32*Aterm**n52*am*dtheta*sinhbmt**2.D0
     1               +     Aterm**n32*coshbmt)
            drdq1(4) = 0.D0
            drdq1(5) = 0.D0
            drdq1(6) = 0.D0
C-----------------------------------------------------------------------------
C           Calculate drdq2
C-----------------------------------------------------------------------------
C           leading constants
            X1 = (1.D0/6.D0)*am*bm*sinhbmt
C           derviative of dtheta wrt f
            ddtdf = (1.D0/m + 1.D0)*(1.D0-f)**(-1.D0/m - 2.D0) 
C           derivative of A^-3/2 (ie A to a power or Ap) wrt A
C           A = hh1 + am*dtheta*cosh(bm*T)
            dApdA = n32 * Aterm**(-5.D0/2.D0)
C           derivative of A wrt to h1 (h1 the function of f)
            dAdhh1 = 1.D0
C           derivative of A wrt to dtheta
            dAddt = am*coshbmt
C           derivatie of h1 (the function of f) wrt f
            dhh1df = two3
C           derivative of h3 (the function of f) wrt f
            dhh3df = (-1.D0/(m+1.D0))
     1            *  (1.D0-f)**(1.D0/(m+1.D0) - 1.D0)

C           first term in derivative of rij wrt f
            drdf1   =  dhh3df*dtheta*Aterm**n32
C           second term in derivative of rij wrt f
            drdf2   =  hh3*ddtdf*Aterm**n32
C           third term in derivative of rij wrt f
            drdf3  = hh3*dtheta*dApdA*(dAdhh1*dhh1df + dAddt*ddtdf)
C           drdq2
            drdq2(1) = X1*(drdf1 + drdf2 + drdf3)
            drdq2(2) = X1*(drdf1 + drdf2 + drdf3)              
            drdq2(3) = X1*(drdf1 + drdf2 + drdf3)              
            drdq2(4) = 0.D0               
            drdq2(5) = 0.D0              
            drdq2(6) = 0.D0               
C-----------------------------------------------------------------------------
C           Calculate dhdsig
C-----------------------------------------------------------------------------
            dh1dsig = r/((1.D0 -f)*sy)
            do jj=1,6
               if (sig(jj)- sigOld(jj) > 1.D-12) then
                  dh1dsig_fd(jj) = (h1New - h1Old)/(sig(jj)-sigOld(jj))
               else
                  dh1dsig_fd(jj) = 0.D0
               end if 
            end do 
C           drdsig:sig
            call matmult6(drdsig,sig,drdsigDDsig)
C           leading constants
            X1 = (c1/sy)*dtheta
            h2sigcoef1 = one3*bm*coshbmt/sy

            dh2dsig(1) = X1*(h2sigcoef1 + sinhbmt*drdsigDDsig(1))
            dh2dsig(2) = X1*(h2sigcoef1 + sinhbmt*drdsigDDsig(2))
            dh2dsig(3) = X1*(h2sigcoef1 + sinhbmt*drdsigDDsig(3))
            dh2dsig(4) = X1*(sinhbmt + sinhbmt*drdsigDDsig(4))
            dh2dsig(5) = X1*(sinhbmt + sinhbmt*drdsigDDsig(5))
            dh2dsig(6) = X1*(sinhbmt + sinhbmt*drdsigDDsig(6)) 
C-----------------------------------------------------------------------------
C           Calculate dhdq
C-----------------------------------------------------------------------------
            dh1dq1 = -H/((1.D0-f)*sy**2.)*sigret
            dh1dq2 = -1./(sy*(1.D0-f)**2.)*sigret

            X1 = c1*dtheta*sigret*(-1.D0*H/sy**2.D0)
            dh2dq1 = X1*(bm*sigh**2.D0*coshbmt + sinhbmt)

            X1 = (c1/sy)*sinhbmt
            dh2dq2 = X1*sigret*(-1.D0)*(n+1.D0)*(1.D0-f)**(-n-2.D0)

C-----------------------------------------------------------------------------
C           A matrix for determining dlambda
C-----------------------------------------------------------------------------

            Amat(1:6,1:6) = Cinv + plmul*drdsig
            Amat(1:6,7) = plmul*drdq1
            Amat(1:6,8) = plmul*drdq2
            Amat(7,1:6) = plmul*dh1dsig
            Amat(8,1:6) = plmul*dh2dsig
            Amat(7,7) = plmul*dh1dq1 - 1.D0
            Amat(7,8) = plmul*dh1dq2          
            Amat(8,7) = plmul*dh2dq1
            Amat(8,8) = plmul*dh2dq2  - 1.D0 

            call invert(Amat,8,Ainv)
C
C           Get the left and right multiplied direction vectors
C
C           dphidsig (i.e., r)
            rl(1:6) = r(1:6)

C           dphidq1
            X1 = -hh3*H
            dTdsy = -sigh/sy**2.D0
            rl(7) = X1*(nhalf*Aterm**n32*dTdsy*sy + Aterm**nhalf)
C           dphidq2
            rl(8) = -dhh3df*sy*Aterm**(nhalf)
     1            + half*hh3*sy*Aterm**n32
     2            * (dhh1df + am*coshbmt*ddtdf)
C
            rr(1:6) = r(1:6)
C           h1 = dq1/dDeltalambda
            rr(7)   = coef8*sigret
C           h2 = dq2/dDeltalambda
            rr(8)   = c1*sigret*sinhbmt*fterm/(mf*sy)
C-----------------------------------------------------------------------------
C        Degradation Function (needed for determing plastic inc)
C-----------------------------------------------------------------------------
            OmegaS = am*coshbmt*dtheta
            wmm = hh3*(hh1 + OmegaS)**(nhalf)
C           enforce that damaged material cannot become undamaged
C            if (wmm.GE.wmmMax) then
C               wmm = wmmMax
C            endif
C-----------------------------------------------------------------------------
C        Yield Criteria
C-----------------------------------------------------------------------------
            fn    = sige - wmm*sy
C-----------------------------------------------------------------------------
C        Plastic multiplier increment dlambda
C-----------------------------------------------------------------------------
            call sclrval(rl,rr,Ainv,8,den)
            plinc = fn/den
C-----------------------------------------------------------------------------
C        Update internal variables and plastic multiplier
C-----------------------------------------------------------------------------
            plmul = plmul + plinc
            
C
            depb  = plmul*coef8*sigret
C
            epb = epb0 + depb
C
            df = plmul*c1*sigret*sinhbmt*fterm/(mf*sy)
C           Porosity Nucleation
            Anuc = fnuc/(snuc*sqrt(2.D0*pi))
            Bnuc = (epb - epsnuc)**2.D0 / snuc
            dfnuc = Anuc*EXP(nhalf*Bnuc)*plmul
C          
            f = f0 + df + dfnuc
C
            sy = sy0*(1.+epb/e0)**ns
            H  = (ns/e0)*sy0*(1.+epb/e0)**(ns-1.)
C-----------------------------------------------------------------------------
C           Update the stress measures 
C-----------------------------------------------------------------------------
C           Jacobian debug
            sigOld = sig
            trr = r(1)+r(2)+r(3)
            sig(1) = tsig1 - lame1*plmul*trr - 2.D0*G*plmul*r(1)
            sig(2) = tsig2 - lame1*plmul*trr - 2.D0*G*plmul*r(2)
            sig(3) = tsig3 - lame1*plmul*trr - 2.D0*G*plmul*r(3)
            sig(4) = tsig4 - 2.D0*G*plmul*r(4)
            sig(5) = tsig5 - 2.D0*G*plmul*r(5)
            sig(6) = tsig6 - 2.D0*G*plmul*r(6)
C-----------------------------------------------------------------------------
C           Update the equivalent stress
C-----------------------------------------------------------------------------
            sigh = (sig(1) + sig(2) + sig(3))/3.D0
C
            dev(1)  = sig(1) - sigh
            dev(2)  = sig(2) - sigh
            dev(3)  = sig(3) - sigh
            dev(4)  = sig(4)
            dev(5)  = sig(5)
            dev(6)  = sig(6)

C           Update Equivilent Stress
            sige  = sqrt(dev(1)**2. + dev(2)**2.+
     1            dev(3)**2. + 2.D0*dev(4)**2.+
     2            2.D0*dev(5)**2.
     3            + 2.D0*dev(6)**2.)*sqrt(3.D0/2.D0)

            sigInv = sige**(-1.D0)

C           Update trixaixiality
C           Cap pressure at spall strenght 
C           so Sinhterm does not blow up
            if (abs(sigh) > spall) then
               T = spall/(c3*sy)
            else
               T = sigh/(c3*sy)
            end if

C           Update fstar
            if (sigh.GE.0.D0) then
               fst = f0
            else
               fst = f0 - fnuc
            endif

C-----------------------------------------------------------------------------
C        Get return direction for stress update and the unit normal
C-----------------------------------------------------------------------------
C        update degradation function and related functions of triaxiality and f
C        functions of f
         hh1 = 1.D0 + two3*f
         hh3 = (1.D0 - f)**(1.D0/(m+1.D0))
         am = (c1/c2)*hh1*sqrt(6.D0)*sqrt(two3)*(2.D0+m)/(2.D0-m)
         dtheta = (1.D0/(1.D0-f)**(1.D0/m) - (1.D0 -f))/(1.D0-f)
C        functions of T
         sinhbmt = sinh(bm*T)
         coshbmt = cosh(bm*T)
         OmegaS = am*coshbmt*dtheta
         wmm = hh3*(hh1 + OmegaS)**(nhalf)
C        enforce that damaged material cannot become undamaged
C         if (wmm.GE.wmmMax) then
C            wmm = wmmMax
C         endif


C        Return Direction with spot for pres dependent term
                 
         rJ2(1) = 3.D0*dev(1)/(2.D0*sige)
         rJ2(2) = 3.D0*dev(2)/(2.D0*sige)
         rJ2(3) = 3.D0*dev(3)/(2.D0*sige)
         rJ2(4) = 3.D0*dev(4)/(2.D0*sige)
         rJ2(5) = 3.D0*dev(5)/(2.D0*sige)
         rJ2(6) = 3.D0*dev(6)/(2.D0*sige)
C
         norm(1) = sqrt23*rJ2(1)
         norm(2) = sqrt23*rJ2(2)
         norm(3) = sqrt23*rJ2(3)
         norm(4) = sqrt23*rJ2(4)
         norm(5) = sqrt23*rJ2(5)
         norm(6) = sqrt23*rJ2(6)
C
         call kron(norm,norm,6,Nmat)
C        Return Direction with spot for pres dependent term
C        drdsig debug
         rOld = r 
         term1 = hh1+am*dtheta*coshbmt
         term2 = am*bm*dtheta*sinhbmt
         r(1) = rJ2(1) +half*hh3*one3*term1**n32*term2 
         r(2) = rJ2(2) +half*hh3*one3*term1**n32*term2 
         r(3) = rJ2(3) +half*hh3*one3*term1**n32*term2 
         r(4) = rJ2(4) 
         r(5) = rJ2(5)  
         r(6) = rJ2(6)

         call kron(norm,norm,6,Nmat)
C        Jacobin debug
         h1Old = h1New
         call dblcontr(sig,r,sigret)
         h1New = sigret/((1-f)*sy)
        
C-----------------------------------------------------------------------------
C         Check the yield condition again
C-----------------------------------------------------------------------------
            fncheck = sige/(wmm*sy)-1.D0
            if (abs(fncheck) > 1.0D-12) then
                YIELD = .TRUE.
            else
                YIELD = .FALSE.
            end if
C-----------------------------------------------------------------------------
C        End minimization loop
C-----------------------------------------------------------------------------
         enddo
C-----------------------------------------------------------------------------
C               Exit procedure
C-----------------------------------------------------------------------------
C-----------------------------------------------------------------------------
C              Calcualte Jd, Jacobian for converting from Kirch to Cauchy
C-----------------------------------------------------------------------------
100         Jd = 1.D0/(1.D0-f)
            JdInv = 1.D0/Jd
C-----------------------------------------------------------------------------
C              Update new Cauchy stresses (from Kirchhoff)
C-----------------------------------------------------------------------------
            stressNew(k,1) = JdInv*sig(1)
            stressNew(k,2) = JdInv*sig(2)
            stressNew(k,3) = JdInv*sig(3)
            stressNew(k,4) = JdInv*sig(4)
            stressNew(k,5) = JdInv*sig(5)
            stressNew(k,6) = JdInv*sig(6)
C-----------------------------------------------------------------------------
C              Update the internal variables
C-----------------------------------------------------------------------------
            stateNew(k,1)  = epb
            stateNew(k,2)  = sy
            stateNew(k,3)  = fncheck
            stateNew(k,5)  = f
            stateNew(k,6)  = JdInv*sige
            stateNew(k,7)  = fst
            stateNew(k,8)  = wmm
C Set the criterion for material failure
            if (fst.GE.fult) then
               stateNew(k,10) = 0.
            endif
C-----------------------------------------------------------------------------
C              Verify the increment in plastic work
C-----------------------------------------------------------------------------
            dep(1) = plmul*r(1)
            dep(2) = plmul*r(2)
            dep(3) = plmul*r(3)
            dep(4) = plmul*r(4)
            dep(5) = plmul*r(5) 
            dep(6) = plmul*r(6)
C
            dwp1 = sig(1)*dep(1)+sig(2)*dep(2)+sig(3)*dep(3)
     &             +2.*(sig(4)*dep(4)
     &             +sig(5)*dep(5)+sig(6)*dep(6))
            dwp2 = (1.-f)*sy*depb
            if (epb.GT.0.) then
               stateNew(k,4) = dwp1-dwp2
            endif
C-----------------------------------------------------------------------------
C     End of subroutine
C-----------------------------------------------------------------------------
      enddo
      return
      end
C #============================================================================
C #============================================================================
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine invert(A,n,Ainv)
C A is an nxn matrix, n is the matrix dimension, Ainv is A**(-1)
C this subroutin uses Gaussian elimination to determine the inverse
C of an nxn square matrix
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC


      implicit none


      INTEGER :: i,n,k,counter,j,g
      REAL*8 :: A(n,n),Ainv(n,n),Atemp(n,n),Id(n,n),den,mul

         Id = 0.
         do i=1,n
            Id(i,i) = 1.
         enddo

         Ainv = Id

         Atemp = A

         if (sum(A) .EQ. 0.) then
            Ainv = A;
         else
C zero out lower left diagonal
            do i=1,n
C set the ith column in the ith row equal to 1
               if (Atemp(i,i) .NE. 1.) then
                  if (Atemp(i,i) .NE. 0.) then          
                     den = Atemp(i,i)
                     Atemp(i,:) = Atemp(i,:)/den
                     Ainv(i,:) = Ainv(i,:)/den
                  else
                     if (i .EQ. n) then
                        stop 'matrix is singular'
                     endif
                     do k=i+1,n
                        if (Atemp(k,i) .NE. 0.) then
                           counter = k
                           exit
                        endif
                        if (k .EQ. n) then
                           stop 'matrix is singular'
                        endif
                     enddo
                     den = Atemp(counter,i)
                     Atemp(i,:) = Atemp(counter,:)/den + Atemp(i,:)
                     Ainv(i,:) = Ainv(counter,:)/den + Ainv(i,:)
                  endif
               endif
C set the ith column in all rows > i equal to 0
               do k=i+1,n
                  if (Atemp(k,i) .NE. 0.) then
                     mul = Atemp(k,i)
                     Atemp(k,:) = -mul*Atemp(i,:) + Atemp(k,:)
                     Ainv(k,:) = -mul*Ainv(i,:) + Ainv(k,:)
                  endif
               enddo
           enddo
C zero out upper right diagonal
           do i=2,n
              j = n - i + 2
              do k=1,j-1
                 g = j - k
                 if (Atemp(g,j) .NE. 0.) then
                    mul = Atemp(g,j)
                    Atemp(g,:) = -mul*Atemp(j,:) + Atemp(g,:)
                    Ainv(g,:) = -mul*Ainv(j,:) + Ainv(g,:)
                 endif
              enddo
           enddo
        endif

      return

      end subroutine invert

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine kron(a,b,n,C)

C this subroutine takes the kronecker product
C of two vectors a and b and gives back an nxn
C matrix C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC


      implicit none

      integer j 
      INTEGER :: n,i
      REAL*8 :: a(n),b(n),C(n,n)

         do i=1,n
            do j=1,n
               C(i,j) = a(i)*b(j)
            enddo
         enddo
   
      return
 
      end subroutine kron


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine sclrval(av,bv,Amat,n,sclr)

C     This subroutine takes two vectors av and bv and
C a matrix Amat and determines the scalar product av*Amat*bv
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      implicit none

      integer i , j
      INTEGER :: n
      REAL*8 :: sclr,av(n),bv(n),Amat(n,n),temp(n),elsum

         do i=1,n
            elsum = 0.
            do j=1,n
               elsum = elsum + av(j)*Amat(j,i)
            enddo
            temp(i) = elsum
         enddo

         sclr = 0.
         do i=1,n
            sclr = sclr + temp(i)*bv(i)
         enddo

      return

      end subroutine sclrval
C
C
C
      subroutine dblcontr(A,B,sclr)
 
      implicit none

      integer i 
      REAL*8 :: A(6),B(6),sclr

         sclr = 0.
         do i=1,6
            if (i.LE.3) then
               sclr = sclr + A(i)*B(i)
            else
               sclr = sclr + 2.*A(i)*B(i)
            endif
         enddo

      return

      end subroutine

C
C
C
      subroutine matmult6(A,B,C)
 
      implicit none
      
      integer i,j 
      REAL*8 :: A(6,6),B(6),C(6)

         do i=1,6
            C(i) = 0.D0
            do j=1,6
               C(i) = C(i) + A(i,j)*B(j)
            end do
         enddo

      return

      end subroutine

