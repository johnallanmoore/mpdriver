       program mpDriver

       implicit none

       integer i, j , k, Ninc, startInd, endInd

       integer ntens,nstatev, nprops, ndir, nshr
       integer kinc, kspt, kstep, layer, noel, npt
       
       real*8 coords(3), dfgrd0(3,3), dfgrd1(3,3)
       real*8 dpred(1), drot(3,3), predef(1), time(2)
       real*8 celent, dtime, pnewdt, dtemp, rpl
       real*8 scd, spd, sse, temp
       ! dummy values that may have different dimensions
       ! but that probably wont get used
       real*8 drpldt

       real*8 startTemp
       real*8 strainIncScalarX, strainIncScalarY, strainIncScalarZ
       real*8 strainIncScalarXY, strainIncScalarXZ, strainIncScalarYZ
       real*8 totalStrainX, totalStrainY, totalStrainZ
       real*8 totalStrainXY, totalStrainXZ, totalStrainYZ

       character*80 cmname

       real*8, dimension (:,:), allocatable :: ddsdde 
       real*8, dimension (:), allocatable :: ddsddt 
       real*8, dimension (:), allocatable :: drplde
       real*8, dimension (:), allocatable :: dstrain 
       real*8, dimension (:), allocatable :: props 
       real*8, dimension (:), allocatable :: statev 
       real*8, dimension (:), allocatable :: strain
       real*8, dimension (:), allocatable :: stress
 
       ! variables used in code but not in umat
       real*8, dimension (:), allocatable:: strainState

       ! varibles to calculate tangent stiffness
       real*8 strain0(6), stress0(6), ddsddeFD(6,6),diff(6,6)
       real*8  stressSave0(6,6), stressSave(6,6)

       real*8, dimension (:,:), allocatable :: statevSave0
       real*8, dimension (:,:), allocatable :: statevSave

       logical fdtan

       ! coords,        !  coordinates of Gauss pt. being evaluated
       ! ddsdde,        ! Tangent Stiffness Matrix
       ! ddsddt,	! Change in stress per change in temperature
       ! dfgrd0,	! Deformation gradient at beginning of step
       ! dfgrd1,	! Deformation gradient at end of step
       ! dpred,	        ! Change in predefined state variables
       ! drplde,	! Change in heat generation per change in strain
       ! drot,	        ! Rotation matrix
       ! dstrain,	! Strain increment tensor stored in vector form
       ! predef,	! Predefined state vars dependent on field variables
       ! props,	        ! Material properties passed in
       ! statev,	! State Variables
       ! strain,	! Strain tensor stored in vector form
       ! stress,	! Cauchy stress tensor stored in vector form
       ! time		! Step Time and Total Time
       ! celent         ! Characteristic element length
       ! drpldt         ! Variation of RPL w.r.t temp.
       ! dtemp          ! Increment of temperature
       ! dtime          ! Increment of time
       ! kinc           !Increment number
       ! kspt           ! Section point # in current layer
       ! kstep          ! Step number
       ! layer          ! layer number
       ! noel           ! element number
       ! npt            ! Integration point number
       ! pnewdt         ! Ratio of suggested new time increment/time increment
       ! rpl            ! Volumetric heat generation
       ! scd            ! “creep” dissipation
       ! spd            ! plastic dissipation
       ! sse            ! elastic strain energy
       ! temp           ! temperature

       !logical inputs, should tangent be calculated numerically
       fdtan =.true.

!      Integer Inputs
       ndir = 3
       nshr = 3
       ntens = ndir + nshr
       nstatev = 20
       nprops = 4

       ! limit tangent stiffness calculation
       ! 1-6 give full matrix
       startInd = 1
       endInd = 6

! !     Dimension Reals
       allocate (ddsdde(ntens,ntens) )
       allocate (ddsddt(ntens) )
       allocate (drplde(ntens) )
       allocate (dstrain(ntens) )
       allocate (props(nprops) )
       allocate (statev(nstatev) )
       allocate (strain(ntens) )
       allocate (stress(ntens) )

       ! variables used in code but not in umat
       allocate (strainState(ntens))

       allocate (statevSave(nstatev,6) )
       allocate (statevSave0(nstatev,6) )

       !User Inputs
       ! Ifort
       open(unit=2,file="results.dat",recl=204)
       ! gfortran
       ! open(unit=2,file="resultsX.dat")
       
       ! set approx total strain
       totalStrainX = 0.1D-1
       totalStrainY = -0.03D-1
       totalStrainZ = -0.03D-1

       totalStrainXY = 0.001D-1
       totalStrainXZ = 0.001D-1
       totalStrainYZ = 0.001D-1

       dtime = 0.00001D0
       props(1:nprops) = (/1.D3, 0.5D3, 0.8D3, 0.6D3, 0.3D0/)
       cmname = "Material-1"
       Ninc = 100
       coords(1:ndir) = (/0.0D0,0.0D0,0.0D0  /)
       !strainState(1:ntens) = (/1.D0, -0.0D0, -0.0D0, 0.D0, 0.D0, 0.D0/)
       strainIncScalarX = totalStrainX/Ninc
       strainIncScalarY = totalStrainY/Ninc
       strainIncScalarZ = totalStrainZ/Ninc

       strainIncScalarXY = totalStrainXY/Ninc
       strainIncScalarXZ = totalStrainXZ/Ninc
       strainIncScalarYZ = totalStrainYZ/Ninc

       !tensor component Tij = [T11, T22, T33, T12, T13, T23 ]
       !tensor component Ti  = [T(1), T(2), T(3), T(4), T(5), T(6) ]
       !dstrain(1:3) = (/strainIncScalarX, strainIncScalarY,strainIncScalarZ /)
       !dstrain(4:6) = (/strainIncScalarXY, strainIncScalarXZ,strainIncScalarYZ /)
       dstrain(1) = strainIncScalarX
       dstrain(2) = strainIncScalarY
       dstrain(3) = strainIncScalarZ
       dstrain(4) = strainIncScalarXY
       dstrain(5) = strainIncScalarXZ
       dstrain(6) = strainIncScalarYZ
       ! initalize varibles
       kinc = 1
       kspt = 1
       kstep = 1
       layer = 1
       noel = 1
       npt = 1

       !intialize variables
       startTemp = 300.D0
       temp = startTemp
       stress(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       statev(1:nstatev) =  (/0.D0/)
       ddsdde(:,:) = 0.D0
       sse = 0.D0
       spd = 0.D0
       scd = 0.D0
       rpl = 0.D0
       ddsddt(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       drplde(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       drpldt = 0.D0
       strain(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       time = 0.D0
       dtemp = 0.D0
       dpred = 0.D0
       drot(:,:) = 0.D0
       pnewdt = 1.D0
       celent = 1.D0

       ! no stretch no shear
       dfgrd0(1,1:3) = (/1.D0, 0.D0, 0.D0/)
       dfgrd0(2,1:3) = (/0.D0, 1.D0, 0.D0/)
       dfgrd0(3,1:3) = (/0.D0, 0.D0, 1.D0/)

       dfgrd0 = dfgrd1

       if (fdtan) then
          statevSave0(:,:) = 0.D0
          stressSave0(:,:) = 0.D0
       endif
       
       do i = 1, Ninc
          kinc = i
          kstep = kinc

          !no shear deformtion gradient (approx)
          !dfgrd0(1,1:3) = (/strain(1) + 1.D0, 0.D0, 0.D0/)
          !dfgrd0(2,1:3) = (/0.D0, strain(2) + 1.D0, 0.D0/)
          !dfgrd0(3,1:3) = (/0.D0, 0.D0, strain(3) + 1.D0/)
          
          ! approximate f from strain, and update strain if differences
          call strain2F(strain,dfgrd0)
          call f2Strain(dfgrd0,strain)

          ! increment strain
          strain(1:ntens) = strain(1:ntens) + dstrain(1:ntens)

          ! calculate new F value and update strain if needed
          call strain2f(strain,dfgrd1)
          call f2strain(dfgrd1,strain)

          !dfgrd1(1,1:3) = (/strain(1) + 1.D0, 0.D0, 0.D0/)
          !dfgrd1(2,1:3) = (/0.D0, strain(2) + 1.D0, 0.D0/)
          !dfgrd1(3,1:3) = (/0.D0, 0.D0, strain(3) + 1.D0/)

          !print *, "strain0 ", strain0
          !print *, "strain ", strain
          !print*, "----"



          ! standard call to umat
          call umat( &
               stress,  statev,  ddsdde,  sse,     spd, &
               scd,     rpl,     ddsddt,  drplde,  drpldt, &
               strain,  dstrain, time,    dtime,   temp, &
               dtemp,   predef,  dpred,   cmname,  ndir, &
               nshr,    ntens,   nstatev,  props,   nprops, &
               coords,  drot,    pnewdt,  celent,  dfgrd0, &
               dfgrd1,  noel,    npt,     layer,   kspt, &
               kstep,   kinc )

          time  = time + dtime


          if (fdtan) then
             ! check for divide by zero
             do j = 1,6
                if (abs(dstrain(j)).le.1.D-12) then
                   dstrain(j) = 1.D-6
                endif
             enddo

          !calculate tangent stiffness
             call getDdsddeFD( &
               stress,  statev,  ddsdde,  sse,     spd, &
               scd,     rpl,     ddsddt,  drplde,  drpldt, &
               strain,  dstrain, time,    dtime,   temp, &
               dtemp,   predef,  dpred,   cmname,  ndir, &
               nshr,    ntens,   nstatev,  props,   nprops, &
               coords,  drot,    pnewdt,  celent,  dfgrd0, &
               dfgrd1,  noel,    npt,     layer,   kspt, &
               kstep,   kinc, startInd,endInd,statevSave0, &
               stressSave0, statevSave,stressSave, ddsddeFD )

             ! update saved variables
             statevSave0 = statevSave
             stressSave0 = stressSave
          endif


          ! print to file (ifort)
          write(2,"(E,$)") time(1)
          write(2,"(E,$)") strain(1), strain(2), strain(3)
          write(2,"(E,$)") strain(4), strain(5), strain(6)
          write(2,"(E,$)") stress(1), stress(2), stress(3)
          write(2,"(E,$)") stress(4), stress(5), stress(6)
          write(2,"(E)"  )  

          ! Gfortran
          ! write(2,*) time, strain, stress
          
          if (fdtan) then
               print *, "  ddsdde ", ddsdde
               print *, "ddsddeFD ", ddsddeFD
               print *, "    diff ", ddsdde-ddsddeFD
               print *, "---------------"
          endif

       end do ! end strain increment loop  

       if (fdtan) then
          
          do i = 1,6
            do j = startInd, endInd
               diff(j,i) = (ddsdde(j,i)-ddsddeFD(j,i))/ddsddeFD(j,i)
            end do
          end do
          print *, "  ddsdde ", ddsdde
          print *, "ddsddeFD ", ddsddeFD
          print *, "    diff ", ddsdde-ddsddeFD
          print *, "    diff %", 100.D0*diff
          print *, "---------------"
       endif
       
       end program mpDriver

       ! get an approximate tangent stiffness matrix using forward diff
       ! a large number of substeps may be needed to get accurate values
       subroutine getDdsddeFD( &
            stress,  statev,  ddsdde,  sse,     spd, &
            scd,     rpl,     ddsddt,  drplde,  drpldt, &
            strain,  dstrain, time,    dtime,   temp, &
            dtemp,   predef,  dpred,   cmname,  ndir, &
            nshr,    ntens,   nstatev,  props,   nprops, &
            coords,  drot,    pnewdt,  celent,  dfgrd0, &
            dfgrd1,  noel,    npt,     layer,   kspt, &
            kstep,   kinc, startInd,endInd, statevSave0, &
            stressSave0, statevSave,stressSave, ddsddeFD )

         implicit none

         integer ntens,nstatev, nprops, ndir, nshr
         integer kinc, kspt, kstep, layer, noel, npt
       
         real*8 coords(3), dfgrd0(3,3), dfgrd1(3,3)
         real*8 dpred(1), drot(3,3), predef(1), time(2)
         real*8 celent, dtime, pnewdt, dtemp, rpl
         real*8 scd, spd, sse, temp

         real*8 drpldt

         character*80 cmname

         ! copy values
         integer ntens_,nstatev_, nprops_, ndir_, nshr_
         integer kinc_, kspt_, kstep_, layer_, noel_, npt_
       
         real*8 coords_(3), dfgrd0_(3,3), dfgrd1_(3,3)
         real*8 dpred_(1), drot_(3,3), predef_(1), time_(2)
         real*8 celent_, dtime_, pnewdt_, dtemp_, rpl_
         real*8 scd_, spd_, sse_, temp_

         real*8 drpldt_

         character*80 cmname_

         ! subroutinve local values
         real*8 ddsddeFD(6,6)
         real*8 stressSave(6,6), stressSave0(6,6)
         real*8 stressMat(6,6), eps

         integer i, j , k, startInd, endInd

         ! copy values
         real*8, dimension (:,:), allocatable :: ddsdde_ 
         real*8, dimension (:), allocatable :: ddsddt_ 
         real*8, dimension (:), allocatable :: drplde_
         real*8, dimension (:), allocatable :: dstrain_ 
         real*8, dimension (:), allocatable :: props_ 
         real*8, dimension (:), allocatable :: statev_ 
         real*8, dimension (:), allocatable :: strain_
         real*8, dimension (:), allocatable :: stress_

         real*8 ddsdde(ntens,ntens)
         real*8 ddsddt(ntens) 
         real*8 drplde(ntens) 
         real*8 dstrain(ntens) 
         real*8 props(nprops) 
         real*8 statev(nstatev) 
         real*8 strain(ntens) 
         real*8 stress(ntens)

         real*8 statevSave0(nstatev,6) 
         real*8 statevSave(nstatev,6) 

         !copy values (so I don't mess anything up)

         ntens_      = ntens
         nstatev_    = nstatev
         nprops_     = nprops
         ndir_       = ndir
         nshr_       = nshr
         kinc_       = kinc
         kspt_       = kspt
         kstep_      = kstep
         layer_      = layer
         noel_       = noel
         npt_        = npt
       
         coords_     = coords
         
         dpred_      = dpred
         drot_       = drot
         predef_     = predef
         time_       = time
         celent_     = celent
         dtime_      = dtime
         pnewdt_     = pnewdt
         dtemp_      = dtemp
         rpl_        = rpl
         scd_        = scd
         spd_        = spd
         sse_        = sse
         temp_       = temp

         ddsdde_     = ddsdde 
         ddsddt_     = ddsddt
         drplde_     = drplde
         
         props_      = props
         statev_     = statev
         
         stress_     = stress
         strain_     = strain

         dfgrd0_     = dfgrd0
         dfgrd1_     = dfgrd1
         dstrain_    = dstrain

         drpldt_     = drpldt

         cmname_     = cmname


         ! calcualte F0
         !call strain2f(strain_,dfgrd0_)

         ! Loop through the indices where tangent stiffness is requested
         do j = startInd,endInd

            !  reset strain
            strain_     = strain
            dstrain_(:) = 0.D0
            ! increment strain in only one direction
            eps = 1.D-9
            strain_(j) = strain(j) + eps
            dstrain_(j) = eps

            ! strain is end of increment, calcuate F1
            call strain2F(strain_,dfgrd1_)
            
            ! reset stress
            stress_ = stress
            statev_ = statev


          call umat( &
               stress_,  statev_,  ddsdde_,  sse_,     spd_, &
               scd_,     rpl_,     ddsddt_,  drplde_,  drpldt_, &
               strain_,  dstrain_, time_,    dtime_,   temp_, &
               dtemp_,   predef_,  dpred_,   cmname_,  ndir_, &
               nshr_,    ntens_,   nstatev_,  props_,   nprops_, &
               coords_,  drot_,    pnewdt_,  celent_,  dfgrd0_, &
               dfgrd1_,  noel_,    npt_,     layer_,   kspt_, &
               kstep_,   kinc_ )

          ! save state variables, for all strain = 0 but one
          do i = 1,nstatev_
             statevSave(i,j) = statev_(i)
          end do

          ! save stress 
          do i = 1,6
             stressSave(i,j) = stress_(i)
          end do

         enddo ! end do loop i

         ! calculte the tangent stiffness using finite difference
         ddsddeFD(:,:) = 0.D0
         do j = 1,6
            do i = startInd, endInd
               ddsddeFD(i,j) = (stressSave(i,j) - stress(i))/eps
            end do
         end do

       end subroutine getDdsddeFD

       !-----------------------------------------------------------------
       ! convert deformation gradient F to train Green-Lagrange Strain E
       !-----------------------------------------------------------------
       subroutine f2Strain(dfgrd,strain)
         implicit none

         integer j,k
         real*8 dfgrd(3,3), strain(6), strainMat(3,3)
         real*8 Cmat(3,3)

         ! right Cauchy green tensor, C
         Cmat = matmul(transpose(dfgrd),dfgrd)

         ! Green-Lagrance strain
         do j = 1,3
            do k = 1,3
               if (j.eq.k) then
                  strainMat(j,k) = 0.5*(Cmat(j,k) - 1.D0)
               else
                  strainMat(j,k) = 0.5*Cmat(j,k)
               endif
            end do
         end do

         ! turn in back into Voigt notation
         call tensor2voigt(strainMat, strain)

       end subroutine f2Strain

       !-----------------------------------------------------------------
       !----Approx. Deformation tensor F from Green-Lagrange Strain -----
       !----I think its just an approximatation, but it might be exaxt --
       !-----------------------------------------------------------------
       subroutine strain2F(strain,dfgrd)

       implicit none

       integer j,k
       real*8 strainMat(3,3), Cmat(3,3), Ceig(3,3), CVeig(3,3)
       real*8 sqrtCeig(3,3)
       real*8 strain(6), dfgrd(3,3)

       ! convert stain in Voigt notation to tensor
          call voigt2tensor(strain,strainMat)
          
       ! calculate right Cauchy Green Tensor
          do j = 1,3
             do k = 1,3
                if (j.eq.k) then
                   Cmat(j,k) = 2.D0*strainMat(j,k) + 1.D0
                   else
                   Cmat(j,k) = 2.D0*strainMat(j,k)
                endif
             end do
          end do

          ! Cacluate the eigenvalues Ceig and Vector CVeig of C tensor
          call eig(Cmat,1.D-9,Ceig,CVeig)

          ! take sqrt of diagonal eigenvalues, this is the F approximation
          do j = 1,3
             do k = 1,3
                sqrtCeig(j,k) = sqrt(Ceig(j,k))
             end do
          end do

          ! use eigen vectors to get back to full F tensor
          dfgrd = matmul(matmul(CVeig, sqrtCeig),transpose(CVeig) )

       end subroutine strain2F


       !-----------------------------------------------------------------
       !--------------- turn voigt vector to tensor matrix  ------------
       !--- only works for kinematric tensors (e.g.,, strain, strainrate)
       !-----------------------------------------------------------------
      subroutine voigt2Tensor(Avec,Amat)

        implicit none

        real*8 Avec(6), Amat(3,3)

        ! assuming kinematic tensor
        Amat(1,1) = Avec(1)
        Amat(2,2) = Avec(2)
        Amat(3,3) = Avec(3)
        Amat(1,2) = 0.5D0*Avec(4)
        Amat(2,1) = Amat(1,2)
        Amat(1,3) = 0.5D0*Avec(5)
        Amat(3,1) = Amat(1,3)
        Amat(2,3) = 0.5D0*Avec(6)
        Amat(3,2) = Amat(2,3)

      end subroutine voigt2Tensor


       !-----------------------------------------------------------------
       !--------------- turn tensor matrixx to voigt vector ------------
       !--- only works for kinematric tensors (e.g.,, strain, strainrate)
       !-----------------------------------------------------------------
      subroutine tensor2Voigt(Amat,Avec)

        implicit none

        real*8 Avec(6), Amat(3,3)

        ! assuming kinematic tensor
        Avec(1) = Amat(1,1)
        Avec(2) = Amat(2,2)
        Avec(3) = Amat(3,3)
        Avec(4) = 2.D0*Amat(1,2)
        Avec(5) = 2.D0*Amat(1,3)
        Avec(6) = 2.D0*Amat(2,3)


      end subroutine tensor2Voigt


! Determine eigenvalues, Aeig,  and eigenvectors, Geig, of the matrix A
! to within a tolerance, tol using Jacobi iteration method
!
! to reproduce the orginal matrix:
!
!                A =  matmul(matmul(Geig,Aeig),transpose(Geig))
!
!      Programmed by Jacob Smith, Apple Austin TX (as of 2021), circa 2013
      subroutine eig(A,tol,Aeig,Geig)


        implicit none

        integer i

        real*8 conv,A(3,3),Aeig(3,3),G(3,3),Geig(3,3),tol,maxv,tau,t,c,s
        real*8 Aeig1,Aeig2,Aeig3,Geig1(3),Geig2(3),Geig3(3),Gcount1
        real*8 Gcount2,maxvo
        integer counter

        Gcount1 = 0.
        Gcount2 = 0.

        conv = sqrt(A(1,2)**2. + A(1,3)**2. + A(2,3)**2.)
        Aeig = A
        Geig = 0.
        do i=1,3
           Geig(i,i) = 1.
        enddo
 
        maxvo = 1e25
        counter = 0
        do while(conv.GT.tol)
           counter = counter + 1
           if (counter.GE.50) then
              print *,'eigenvalue minimization failed',conv
              goto 100
           endif
           G = 0.
           maxv = max(abs(Aeig(1,2)),abs(Aeig(1,3)),abs(Aeig(2,3)))
           if (maxv.EQ.abs(Aeig(1,2))) then
              tau = (Aeig(1,1) - Aeig(2,2))/(2.*Aeig(1,2))
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)
              G(1,1) = c
              G(2,2) = c
              G(1,2) = -s
              G(2,1) = s
              G(3,3) = 1.
           elseif (maxv.EQ.abs(Aeig(1,3))) then
              tau = (Aeig(1,1) - Aeig(3,3))/(2.*Aeig(1,3))       
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)        
              G(1,1) = c
              G(3,3) = c
              G(1,3) = -s
              G(3,1) = s
              G(2,2) = 1.        
           else
              tau = (Aeig(2,2) - Aeig(3,3))/(2.*Aeig(2,3))
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)        
              G(2,2) = c
              G(3,3) = c
              G(2,3) = -s
              G(3,2) = s
              G(1,1) = 1.        
           endif
           maxvo = maxv
           Aeig = matmul(matmul(G,Aeig),transpose(G))
           Geig = matmul(Geig,transpose(G))
           conv = sqrt(Aeig(1,2)**2. + Aeig(1,3)**2. + Aeig(2,3)**2.)
        enddo

! reorganize the principal values into max, min, mid

        Aeig1 = max(Aeig(1,1),Aeig(2,2),Aeig(3,3))
        Aeig2 = min(Aeig(1,1),Aeig(2,2),Aeig(3,3))

        do i=1,3
           if ((Aeig1.EQ.Aeig(i,i)).AND.(Gcount1.EQ.0.)) then
              Geig1 = Geig(:,i)
              Gcount1 = 1.
           elseif ((Aeig2.EQ.Aeig(i,i)).AND.(Gcount2.EQ.0.)) then
              Geig2 = Geig(:,i)
              Gcount2 = 1.
           else
              Aeig3 = Aeig(i,i)
              Geig3 = Geig(:,i)
           endif
        enddo

        Aeig = 0.
        Aeig(1,1) = Aeig1
        Aeig(2,2) = Aeig2
        Aeig(3,3) = Aeig3
        Geig(:,1) = Geig1
        Geig(:,2) = Geig2
        Geig(:,3) = Geig3

100     return

      end subroutine eig


