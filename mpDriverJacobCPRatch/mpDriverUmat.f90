program mpDriver

  implicit none

  integer i, Ninc, j, startInd, endInd

  integer ntens,nstatev, nprops, ndir, nshr
  integer kinc, kspt, kstep, layer, noel, npt

  real*8 coords(3), dfgrd0(3,3), dfgrd1(3,3)
  real*8 dpred(1), drot(3,3), predef(1), time(2)
  real*8 celent, dtime, pnewdt, dtemp, rpl
  real*8 scd, spd, sse, temp
  real*8 dtime_initial
  ! dummy values that may have different dimensions
  ! but that probably wont get used
  real*8 drpldt

  real*8 beginning_step_temp
  real*8 beginning_step_sse
  real*8 beginning_step_spd
  real*8 beginning_step_scd
  real*8 beginning_step_rpl
  real*8 beginning_step_drpldt
  real*8 beginning_step_time(2)
  real*8 beginning_step_dtemp
  real*8 beginning_step_dpred(1)
  real*8 beginning_step_drot(3,3)
  real*8 beginning_step_celent
  real*8 beginning_step_dfgrd0(3,3)

  real*8 startTemp
  real*8 strainIncLoadX, strainIncLoadY, strainIncLoadZ
  real*8 strainIncLoadXY, strainIncLoadXZ, strainIncLoadYZ
  real*8 strainIncUnLoadX, strainIncUnLoadY, strainIncUnLoadZ
  real*8 strainIncUnLoadXY, strainIncUnLoadXZ, strainIncUnLoadYZ
  real*8 totalStrainLoadX, totalStrainLoadY, totalStrainLoadZ
  real*8 totalStrainLoadXY, totalStrainLoadXZ, totalStrainLoadYZ
  real*8 totalStrainUnLoadX, totalStrainUnLoadY, totalStrainUnLoadZ
  real*8 totalStrainUnLoadXY, totalStrainUnLoadXZ, totalStrainUnLoadYZ
  real*8 StrainRatioX, StrainRatioY, StrainRatioZ
  real*8 StrainRatioXY, StrainRatioXZ, StrainRatioYZ
  real*8 fMax(3,3), Emat(3,3), df(3,3)

  character*80 cmname

  ! New variables to read in properties
  real*8 C11A,C12A,C44A,C11M,C12M,C44M,alpha_A,alpha_M
  real*8 f_c,gamma_0,Theta_T,Theta_Ref_Low,lambda_T
  real*8 gamma_dot_0,Am,S0,H0,SS,Ahard,QL,Theta_Ref_High
  real*8 EULER1,EULER2,EULER3,Bsat,b1,N_GEN_F,d_ir,dir
  real*8 mu
  real*8 C11A2,C12A2,C44A2
  ! For sequential file numbering with
  ! leading zeros in file name
  ! This is limited to 99,999 elements
  ! since the char length is 5
  ! increase if necessary, will also
  ! have to change the format specifier in
  ! the write command if this changes
  ! e.g. if it is 4, specifier becomes I5.4
  character(len=5) :: filenum
  character(len=5) :: simcountchar
  character(len=255) :: pwd

  ! number of elements, counters, and number
  ! of steps
  integer mpnum, m, k, t, numsteps, a, numsims
  integer eulercount, simcount, Beginning_Simnum
  integer End_Simnum, fileoffset, straincount
  integer kinc_counter

  real*8, dimension (:,:), allocatable :: ddsdde 
  real*8, dimension (:,:), allocatable :: beginning_step_ddsdde
  real*8, dimension (:),   allocatable :: ddsddt 
  real*8, dimension (:),   allocatable :: beginning_step_ddsddt
  real*8, dimension (:),   allocatable :: drplde
  real*8, dimension (:),   allocatable :: beginning_step_drplde
  real*8, dimension (:),   allocatable :: dstran
  real*8, dimension (:),   allocatable :: dstrain
  real*8, dimension (:),   allocatable :: dstrainLoad 
  real*8, dimension (:),   allocatable :: dstrainUnLoad
  real*8, dimension (:),   allocatable :: props 
  real*8, dimension (:),   allocatable :: statev 
  real*8, dimension (:),   allocatable :: beginning_step_statev
  real*8, dimension (:),   allocatable :: stran
  real*8, dimension (:),   allocatable :: beginning_step_stran
  real*8, dimension (:),   allocatable :: strain
  real*8, dimension (:),   allocatable :: beginning_step_strain
  real*8, dimension (:),   allocatable :: stress
  real*8, dimension (:),   allocatable :: beginning_step_stress

  ! variables used in code but not in umat
  real*8, dimension (:), allocatable :: strainState
  real*8, dimension (:,:), allocatable :: EULER
  real*8, dimension (:,:), allocatable :: totalStrain

  ! varibles to calculate tangent stiffness
  real*8 strain0(6), stress0(6), ddsddeFD(6,6)
  real*8  stressSave0(6,6), stressSave(6,6)

  real*8, dimension (:,:), allocatable :: statevSave0
  real*8, dimension (:,:), allocatable :: statevSave

  logical fdtan

  ! coords,     ! coordinates of Gauss pt. being evaluated
  ! ddsdde,     ! Tangent Stiffness Matrix
  ! ddsddt,	! Change in stress per change in temperature
  ! dfgrd0,	! Deformation gradient at beginning of step
  ! dfgrd1,	! Deformation gradient at end of step
  ! dpred,	! Change in predefined state variables
  ! drplde,	! Change in heat generation per change in strain
  ! drot,	! Rotation matrix
  ! dstrain,	! Strain increment tensor stored in vector form
  ! predef,	! Predefined state vars dependent on field variables
  ! props,	! Material properties passed in
  ! statev,	! State Variables
  ! strain,	! Strain tensor stored in vector form
  ! stress,	! Cauchy stress tensor stored in vector form
  ! time	! Step Time and Total Time
  ! celent      ! Characteristic element length
  ! drpldt      ! Variation of RPL w.r.t temp.
  ! dtemp       ! Increment of temperature
  ! dtime       ! Increment of time
  ! kinc        ! Increment number
  ! kspt        ! Section point # in current layer
  ! kstep       ! Step number
  ! layer       ! layer number
  ! noel        ! element number
  ! npt         ! Integration point number
  ! pnewdt      ! Ratio of suggested new time increment/time increment
  ! rpl         ! Volumetric heat generation
  ! scd         ! “creep” dissipation
  ! spd         ! plastic dissipation
  ! sse         ! elastic strain energy
  ! temp        ! temperature

  !logical inputs, should tangent be calculated numerically
  !fdtan =.true.
  fdtan = .false.

  ! limit tangent stiffness calculation
  ! 1-6 give full matrix
  startInd = 1
  endInd = 6

  !      Integer Inputs
  ndir = 3
  nshr = 3
  ntens = ndir + nshr
  nstatev = 234
  nprops = 33
  ! Number of material points in simulation
  ! Make sure mpnum matches the number of rows in
  ! the .inc files. Name results files numerically 
  ! and have spreadsheet in Excel showing what 
  ! simulation corresponds to which proerties
  mpnum = 64
  ! fileoffset is the number of files which are read in
  fileoffset = 4
  ! For cyclic loading need to have at least two steps
  ! Numseteps is 2*Number of cycles (e.g. 4 steps = 2 cycles)
  numsteps = 2

  call getcwd(pwd)

  ! !     Dimension Reals
  ! !     Dimension Reals
  allocate (beginning_step_stress(ntens) )
  allocate (beginning_step_statev(nstatev) )
  allocate (beginning_step_drplde(ntens) )
  allocate (beginning_step_ddsddt(ntens) )
  allocate (beginning_step_ddsdde(ntens,ntens) )

  allocate (ddsdde(ntens,ntens) )
  allocate (ddsddt(ntens) )
  allocate (drplde(ntens) )
  allocate (dstran(ntens) )
  allocate (dstrain(ntens) )
  allocate (dstrainLoad(ntens) )
  allocate (dstrainUnLoad(ntens) )
  allocate (props(nprops) )
  allocate (statev(nstatev) )
  allocate (stran(ntens) )
  allocate (beginning_step_stran(ntens) )
  allocate (strain(ntens) )
  allocate (beginning_step_strain(ntens) )
  allocate (stress(ntens) )
  allocate (EULER(mpnum,3) )

  allocate (totalStrain(mpnum,12) )

  ! variables used in code but not in umat
  allocate (strainState(ntens))
  allocate (statevSave(nstatev,6) )
  allocate (statevSave0(nstatev,6) )

  ! In order to have this be as close to the
  ! ABAQUS simulations as possible, the strains
  ! at the end of the increments from an ABAQUS  
  ! simulation are exported to a text file and
  ! read in here to be used.
  open(4,file="Strain_Values.txt")
  do straincount = 1,mpnum
     read(4,*)totalStrain(straincount,1), &
          totalStrain(straincount,2), &
          totalStrain(straincount,3), &
          totalStrain(straincount,4), &
          totalStrain(straincount,5), &
          totalStrain(straincount,6), &
          totalStrain(straincount,7), &
          totalStrain(straincount,8), &
          totalStrain(straincount,9), &
          totalStrain(straincount,10), &
          totalStrain(straincount,11), &
          totalStrain(straincount,12)
  enddo

  open(3,file="Beginning_and_Ending_Simnum.inc")
  read(3,*)Beginning_Simnum,End_Simnum
  numsims = End_Simnum-Beginning_Simnum+1
  !print *, numsims

  ! Get texture from file
  open(2,file="texture.inc")
  do eulercount = 1,mpnum
     read(2,*)EULER1,EULER2,EULER3
     EULER(eulercount,1)  = EULER1
     EULER(eulercount,2)  = EULER2
     EULER(eulercount,3)  = EULER3
  enddo

  ! print *, EULER

  ! Start loop which reads properties and opens
  ! file location to store results for each
  ! simulation
  do simcount = Beginning_Simnum,End_Simnum

     ! Euler angles and properties should be in a
     ! text file where each row contains the values
     ! for each material point/simulation
     !open(1,file="Properties_mpDriver.inc")
     open(1,file="Props.inc")

     ! Read in material properties
     ! Note the Fortran read() statement reads a 
     ! row of values and then advances to the
     ! beginning of the next line.
     read(1,*)C11A,C12A,C44A,C11M,C12M,C44M,alpha_A,alpha_M, &
          f_c,gamma_0,Theta_T,Theta_Ref_Low,lambda_T,C11A2,C12A2,C44A2, &
          gamma_dot_0,Am,S0,H0,SS,Ahard,QL,Theta_Ref_High, &
          Bsat,b1,N_GEN_F,d_ir,dir,mu

     ! Assigning properties which were read in from
     ! the .inc file
     props(1)  = C11A
     props(2)  = C12A
     props(3)  = C44A
     props(4)  = C11M
     props(5)  = C12M
     props(6)  = C44M
     props(7)  = alpha_A
     props(8)  = alpha_M
     props(9)  = f_c
     props(10) = gamma_0
     props(11) = Theta_T
     props(12) = Theta_Ref_Low
     props(13) = lambda_T
     props(14) = C11A2
     props(15) = C12A2
     props(16) = C44A2
     props(17) = gamma_dot_0
     props(18) = Am
     props(19) = S0
     props(20) = H0
     props(21) = SS
     props(22) = Ahard
     props(23) = QL
     props(24) = Theta_Ref_High
     !props(25) = EULER1
     !props(26) = EULER2
     !props(27) = EULER3
     props(28) = Bsat
     props(29) = b1
     props(30) = N_GEN_F
     props(31) = d_ir
     props(32) = dir
     props(33) = mu 

     do m = 1,mpnum

        ! Convert index m into file number string
        ! "(I5)" allocates a file name that can be
        ! 5 characters long. Can change to "(I5.5)"
        ! to put leading zeros in front of number
        ! if wanted. 
        ! e.g.: write(filenum,"(I5)") 1 --> 1
        ! e.g.: write(filenum,"(I5.5)") 1 --> 00001
        write(filenum,"(I5)") m
        write(simcountchar,"(I5)") simcount

        ! Convert index simcount to string

        ! Uncomment if using external file for texture
        !read(2,*)EULER1,EULER2,EULER3

        ! Open a file to allocate where the results
        ! will be stored. In this code, it will make
        ! a file for each material point (mpnum)
        ! Note: // joins two strings together
        open(unit=m+fileoffset,file=trim(pwd)//"/"//trim(adjustl(simcountchar))//"/"//trim(adjustl(filenum))//".dat",recl=204)

        ! Assign texture
        props(25) = EULER(m,1)
        props(26) = EULER(m,2)
        props(27) = EULER(m,3)

        cmname = "Material-1"

        ! For uniaxial tensile test:
        ! Stress Ratio: S1:S2:S3=1:0:0
        ! Strain Ratio: E1:E2:E3=1:-0.5:-0.5 (no shear)
        ! Strains imported from the text file are logarithmic
        ! strains output from Abaqus sim. Need to get them back
        ! to regular strain values.
        ! totalStrainLoadX  = exp(totalStrain(m,1))-1.D0
        ! totalStrainLoadY  = exp(totalStrain(m,2))-1.D0
        ! totalStrainLoadZ  = exp(totalStrain(m,3))-1.D0
        ! totalStrainLoadXY = exp(totalStrain(m,4))-1.D0
        ! totalStrainLoadXZ = exp(totalStrain(m,5))-1.D0
        ! totalStrainLoadYZ = exp(totalStrain(m,6))-1.D0

        ! totalStrainUnLoadX  = exp(totalStrain(m,7))-1.D0
        ! totalStrainUnLoadY  = exp(totalStrain(m,8))-1.D0
        ! totalStrainUnLoadZ  = exp(totalStrain(m,9))-1.D0
        ! totalStrainUnLoadXY = exp(totalStrain(m,10))-1.D0
        ! totalStrainUnLoadXZ = exp(totalStrain(m,11))-1.D0
        ! totalStrainUnLoadYZ = exp(totalStrain(m,12))-1.D0


        totalStrainLoadX  = totalStrain(m,1)
        totalStrainLoadY  = totalStrain(m,2)
        totalStrainLoadZ  = totalStrain(m,3)
        totalStrainLoadXY = totalStrain(m,4)
        totalStrainLoadXZ = totalStrain(m,5)
        totalStrainLoadYZ = totalStrain(m,6)

        totalStrainUnLoadX  = totalStrain(m,7)
        totalStrainUnLoadY  = totalStrain(m,8)
        totalStrainUnLoadZ  = totalStrain(m,9)
        totalStrainUnLoadXY = totalStrain(m,10)
        totalStrainUnLoadXZ = totalStrain(m,11)
        totalStrainUnLoadYZ = totalStrain(m,12)

        dtime_initial = 0.01D0
        dtime = dtime_initial

        ! Number of increments in each Step
        ! Setting it equal to inverse of dtime since that makes
        ! it easier to deal with for Objective 1 in thesis
        Ninc = 1/dtime
        coords(1:ndir) = (/0.0D0,0.0D0,0.0D0 /)
        !strainState(1:ntens) = (/1.D0, -0.0D0, -0.0D0, 0.D0, 0.D0, 0.D0/)
        ! Strain increments
        strainIncLoadX   = totalStrainLoadX!*dtime
        strainIncLoadY   = totalStrainLoadY!*dtime
        strainIncLoadZ   = totalStrainLoadZ!*dtime
        strainIncLoadXY  = totalStrainLoadXY!*dtime
        strainIncLoadXZ  = totalStrainLoadXZ!*dtime
        strainIncLoadYZ  = totalStrainLoadYZ!*dtime

        strainIncUnLoadX   = (totalStrainLoadX -totalStrainUnLoadX)!*dtime
        strainIncUnLoadY   = (totalStrainLoadY -totalStrainUnLoadY)!*dtime
        strainIncUnLoadZ   = (totalStrainLoadZ -totalStrainUnLoadZ)!*dtime
        strainIncUnLoadXY  = (totalStrainLoadXY-totalStrainUnLoadXY)!*dtime
        strainIncUnLoadXZ  = (totalStrainLoadXZ-totalStrainUnLoadXZ)!*dtime
        strainIncUnLoadYZ  = (totalStrainLoadYZ-totalStrainUnLoadYZ)!*dtime

        dstrainLoad(1:3) = (/strainIncLoadX*dtime, strainIncLoadY*dtime,strainIncLoadZ*dtime /)
        dstrainLoad(4:6) = (/strainIncLoadXY*dtime, strainIncLoadXZ*dtime,strainIncLoadYZ*dtime /)

        dstrainUnLoad(1:3) = (/strainIncUnLoadX*dtime, strainIncUnLoadY*dtime,strainIncUnLoadZ*dtime /)
        dstrainUnLoad(4:6) = (/strainIncUnLoadXY*dtime, strainIncUnLoadXZ*dtime,strainIncUnLoadYZ*dtime /)

        ! initalize varibles
        kinc = 1
        kspt = 1
        kstep = 1
        layer = 1
        ! noel = 1
        noel = m
        npt = 1

        !intialize variables
        startTemp = 0.D0
        temp = startTemp
        stress(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
        statev(1:nstatev) =  (/0.D0/)
        ddsdde(:,:) = 0.D0
        sse = 0.D0
        spd = 0.D0
        scd = 0.D0
        rpl = 0.D0
        ddsddt(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
        drplde(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
        drpldt = 0.D0
        strain(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
        stran(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
        time(1) = 0.D0
        time(2) = 0.D0
        dtemp = 0.D0
        dpred = 0.D0
        drot(:,:) = 0.D0
        pnewdt = 1.D0
        celent = 1.D0

        beginning_step_strain(1:ntens) = (/0.D0, 0.D0, 0.D0,&
             0.D0, 0.D0, 0.D0/)
        beginning_step_time(1) = 0.D0
        beginning_step_time(2) = 0.D0

        ! no stretch no shear
        dfgrd0(1,1:3) = (/1.D0, 0.D0, 0.D0/)
        dfgrd0(2,1:3) = (/0.D0, 1.D0, 0.D0/)
        dfgrd0(3,1:3) = (/0.D0, 0.D0, 1.D0/)

        dfgrd1 = dfgrd0

        if (fdtan) then
          statevSave0(:,:) = 0.D0
          stressSave0(:,:) = 0.D0
        endif

        do t = 1,numsteps
           time(1) = 0.D0
           beginning_step_time(1) = 0.D0
           dtime = dtime_initial
           kinc_counter = 1
           kstep = t
           ! do while time.le.t (time <= t) instead of using Ninc
           ! Each step will be 1 second, therefore the loop will go
           ! until a whole number is reached for the time step
           !do i = 1, Ninc
           do while (time(2).lt.dble(t))
           !do while ((strain(1).le.totalStrainLoad).and.&
           !     (strain(1).ge.totalStrainUnLoad))
              kinc = kinc_counter

              ! approximate f from strain, and update strain if differences
              call strain2F(strain,dfgrd0)
              call f2Strain(dfgrd0,strain)

              if (MOD(t,2).eq.1) then
                 ! if 1 it is an odd step which loads the material point
                 ! in tension
                 dstrain(1)= strainIncLoadX*dtime
                 dstrain(2)= strainIncLoadY*dtime
                 dstrain(3)= strainIncLoadZ*dtime
                 dstrain(4)= strainIncLoadXY*dtime
                 dstrain(5)= strainIncLoadXZ*dtime
                 dstrain(6)= strainIncLoadYZ*dtime
                 
                 strain(1:ntens) = strain(1:ntens) + dstrain(1:ntens)

                 !print *, strain(1)
                 !if(strain(3).ge.totalStrainLoad) exit
              else
                 ! if 0 it is an even step which unloads the material point
                 ! back to a 0 strain
                 dstrain(1)= strainIncUnLoadX*dtime
                 dstrain(2)= strainIncUnLoadY*dtime
                 dstrain(3)= strainIncUnLoadZ*dtime
                 dstrain(4)= strainIncUnLoadXY*dtime
                 dstrain(5)= strainIncUnLoadXZ*dtime
                 dstrain(6)= strainIncUnLoadYZ*dtime
                 
                 strain(1:ntens) = strain(1:ntens) - dstrain(1:ntens)
                 !print *, strain(1)
                 !if(strain(3).le.totalStrainUnLoad) exit
              endif

              ! calculate new F value and update strain if needed
              call strain2f(strain,dfgrd1)
              call f2strain(dfgrd1,strain)

              if (fdtan) then
                 ! check for divide by zero
                 do j = 1,6
                    if (abs(dstrain(j)).le.1.D-12) then
                       dstrain(j) = 1.D-6
                    endif
                 enddo

                 !calculate tangent stiffness
                 call getDdsddeFD( &
                      stress,  statev,  ddsdde,  sse,     spd, &
                      scd,     rpl,     ddsddt,  drplde,  drpldt, &
                      strain,  dstrain, time,    dtime,   temp, &
                      dtemp,   predef,  dpred,   cmname,  ndir, &
                      nshr,    ntens,   nstatev,  props,   nprops, &
                      coords,  drot,    pnewdt,  celent,  dfgrd0, &
                      dfgrd1,  noel,    npt,     layer,   kspt, &
                      kstep,   kinc, startInd,endInd,statevSave0, &
                      stressSave0, statevSave,stressSave, ddsddeFD )

                 ! update saved variables
                 statevSave0 = statevSave
                 stressSave0 = stressSave
              endif

              call umat( &
                   stress,  statev,  ddsdde,  sse,     spd, &
                   scd,     rpl,     ddsddt,  drplde,  drpldt, &
                   strain,  dstrain, time,    dtime,   temp, &
                   dtemp,   predef,  dpred,   cmname,  ndir, &
                   nshr,    ntens,   nstatev,  props,   nprops, &
                   coords,  drot,    pnewdt,  celent,  dfgrd0, &
                   dfgrd1,  noel,    npt,     layer,   kspt, &
                   kstep,   kinc )

              if (pnewdt.eq.1.0)then
                 ! can proceed to next increment
                 time(1)  = time(1) + dtime
                 time(2)  = time(2) + dtime
                 kinc_counter = kinc_counter + 1
                 ! Save time and strain so that if the next step has a decrease 
                 ! in time increment, the time and strain can be reset to 
                 ! whatever the previous increment values were.
                 beginning_step_temp = temp
                 beginning_step_stress(1:ntens)=stress(1:ntens)
                 do i = 1, nstatev
                    beginning_step_statev(i) =  statev(i)
                 enddo
                 do i = 1,ntens
                    do j = 1,ntens
                       beginning_step_ddsdde(i,j) = ddsdde(i,j)
                    enddo
                 enddo
                 beginning_step_sse = sse
                 beginning_step_spd = spd
                 beginning_step_scd = scd
                 beginning_step_rpl = rpl
                 beginning_step_ddsddt(1:ntens) = ddsddt(1:ntens)
                 beginning_step_drplde(1:ntens) = drplde(1:ntens)
                 beginning_step_drpldt = drpldt
                 beginning_step_strain(1:ntens) = strain(1:ntens)
                 beginning_step_stran(1:ntens) = stran(1:ntens)
                 beginning_step_time(1) = time(1)
                 beginning_step_time(2) = time(2)
                 beginning_step_dtemp = dtemp
                 beginning_step_dpred = dpred
                 do i = 1,3
                    do j = 1,3
                       beginning_step_drot(i,j) = drot(i,j)
                    enddo
                 enddo
                 ! Reset pnewdt to 1
                 pnewdt = 1.D0
                 ! New time increment
                 dtime = dtime*pnewdt
                 beginning_step_celent = celent

                 ! no stretch no shear
                 beginning_step_dfgrd0(1,1:3) = dfgrd0(1,1:3)
                 beginning_step_dfgrd0(2,1:3) = dfgrd0(2,1:3)
                 beginning_step_dfgrd0(3,1:3) = dfgrd0(3,1:3)

                 ! pnewdt is usually 1.0, but if an internal error in the Umat
                 ! is tripped, it often reduces the timestep
                 write(m+fileoffset,"(E,$)") time(2)
                 write(m+fileoffset,"(E,$)") strain(1), strain(2), strain(3)
                 write(m+fileoffset,"(E,$)") strain(4), strain(5), strain(6)
                 write(m+fileoffset,"(E,$)") stress(1), stress(2), stress(3)
                 write(m+fileoffset,"(E,$)") stress(4), stress(5), stress(6)
                 write(m+fileoffset,"(E,$)") statev(1), statev(177), statev(39)
                 write(m+fileoffset,"(E,$)") strain(1), time(1)
                 write(m+fileoffset,"(E)"  )  
                 !write(*,*)'pnewdt=', pnewdt
                 !print *, strain
                 !print *, stress
                 !print *, "statev(79)", statev(79)
                 !print *, 'time=',time
                 !dtime = dtime_initial

              else
                 write(*,*)'PNEWDT less than 1. Time step change', pnewdt
                 ! Return to time and strain at beginning of step (i.e. the
                 ! values at the end of the previous step)
                 temp = beginning_step_temp
                 stress(1:ntens) = beginning_step_stress(1:ntens)
                 do i = 1, nstatev
                    statev(i) =  beginning_step_statev(i)
                 enddo
                 do i = 1,ntens
                    do j = 1,ntens
                       ddsdde(i,j) = beginning_step_ddsdde(i,j)
                    enddo
                 enddo
                 sse = beginning_step_sse
                 spd = beginning_step_spd
                 scd = beginning_step_scd
                 rpl = beginning_step_rpl
                 ddsddt(1:ntens) = beginning_step_ddsddt(1:ntens)
                 drplde(1:ntens) = beginning_step_drplde(1:ntens)
                 drpldt = beginning_step_drpldt
                 strain(1:ntens) = beginning_step_strain(1:ntens)
                 stran(1:ntens) = beginning_step_stran(1:ntens)
                 time(1) = beginning_step_time(1)
                 time(2) = beginning_step_time(2)
                 dtemp = beginning_step_dtemp
                 dpred = beginning_step_dpred
                 do i = 1,3
                    do j = 1,3
                       drot(i,j) = beginning_step_drot(i,j)
                    enddo
                 enddo
                 ! New time increment
                 dtime = dtime*pnewdt
                 ! Reset pnewdt to 1
                 pnewdt = 1.D0
                 celent = beginning_step_celent

                 ! no stretch no shear
                 dfgrd0(1,1:3) = beginning_step_dfgrd0(1,1:3)
                 dfgrd0(2,1:3) = beginning_step_dfgrd0(2,1:3)
                 dfgrd0(3,1:3) = beginning_step_dfgrd0(3,1:3)
              endif

              if(dtime.lt.0.00000001)then
                 write(*,*)'dtime=',dtime
                 write(*,*)'pnewdt=',pnewdt
                 ! The stop command ends the whole program
                 stop 'TIME INCREMENT TOO SMALL - mpDriver'
              endif
              
              if (fdtan) then
                 print *, "  ddsdde ", ddsdde
                 print *, "ddsddeFD ", ddsddeFD
                 print *, "    diff ", ddsdde-ddsddeFD
                 print *, "---------------"
              endif

              if (time(1).gt.t)then
                 exit
              endif

           end do ! end strain increment loop  
        end do ! end of step
     end do ! end of material point loop
  end do ! end of numsim loop

end program mpDriver


! get an approximate tangent stiffness matrix using forward diff
! a large number of substeps may be needed to get accurate values
subroutine getDdsddeFD( &
     stress,  statev,  ddsdde,  sse,     spd, &
     scd,     rpl,     ddsddt,  drplde,  drpldt, &
     strain,  dstrain, time,    dtime,   temp, &
     dtemp,   predef,  dpred,   cmname,  ndir, &
     nshr,    ntens,   nstatev,  props,   nprops, &
     coords,  drot,    pnewdt,  celent,  dfgrd0, &
     dfgrd1,  noel,    npt,     layer,   kspt, &
     kstep,   kinc, startInd,endInd, statevSave0, &
     stressSave0, statevSave,stressSave, ddsddeFD )

  implicit none

  integer ntens,nstatev, nprops, ndir, nshr
  integer kinc, kspt, kstep, layer, noel, npt

  real*8 coords(3), dfgrd0(3,3), dfgrd1(3,3)
  real*8 dpred(1), drot(3,3), predef(1), time(2)
  real*8 celent, dtime, pnewdt, dtemp, rpl
  real*8 scd, spd, sse, temp

  real*8 drpldt

  character*80 cmname

  ! copy values
  integer ntens_,nstatev_, nprops_, ndir_, nshr_
  integer kinc_, kspt_, kstep_, layer_, noel_, npt_

  real*8 coords_(3), dfgrd0_(3,3), dfgrd1_(3,3)
  real*8 dpred_(1), drot_(3,3), predef_(1), time_(2)
  real*8 celent_, dtime_, pnewdt_, dtemp_, rpl_
  real*8 scd_, spd_, sse_, temp_

  real*8 drpldt_

  character*80 cmname_

  ! subroutinve local values
  real*8 ddsddeFD(6,6)
  real*8 stressSave(6,6), stressSave0(6,6)
  real*8 stressMat(6,6)

  integer i, j , k, startInd, endInd

  ! copy values
  real*8, dimension (:,:), allocatable :: ddsdde_ 
  real*8, dimension (:), allocatable :: ddsddt_ 
  real*8, dimension (:), allocatable :: drplde_
  real*8, dimension (:), allocatable :: dstrain_ 
  real*8, dimension (:), allocatable :: props_ 
  real*8, dimension (:), allocatable :: statev_ 
  real*8, dimension (:), allocatable :: strain_
  real*8, dimension (:), allocatable :: stress_

  real*8 ddsdde(ntens,ntens)
  real*8 ddsddt(ntens) 
  real*8 drplde(ntens) 
  real*8 dstrain(ntens) 
  real*8 props(nprops) 
  real*8 statev(nstatev) 
  real*8 strain(ntens) 
  real*8 stress(ntens)

  real*8 statevSave0(nstatev,6) 
  real*8 statevSave(nstatev,6) 

  !copy values (so I don't mess anything up)

  ntens_      = ntens
  nstatev_    = nstatev
  nprops_     = nprops
  ndir_       = ndir
  nshr_       = nshr
  kinc_       = kinc
  kspt_       = kspt
  kstep_      = kstep
  layer_      = layer
  noel_       = noel
  npt_        = npt

  coords_     = coords

  dpred_      = dpred
  drot_       = drot
  predef_     = predef
  time_       = time
  celent_     = celent
  dtime_      = dtime
  pnewdt_     = pnewdt
  dtemp_      = dtemp
  rpl_        = rpl
  scd_        = scd
  spd_        = spd
  sse_        = sse
  temp_       = temp

  ddsdde_     = ddsdde 
  ddsddt_     = ddsddt
  drplde_     = drplde

  props_      = props
  statev_     = statev

  stress_     = stress
  strain_     = strain

  dfgrd0_     = dfgrd0
  dfgrd1_     = dfgrd1
  dstrain_    = dstrain

  drpldt_     = drpldt

  cmname_     = cmname


  ! Loop through the indices where tangent stiffness is requested
  do i = startInd,endInd
     ! build a strain in only one direction
     do j = 1,6
        if (i.eq.j) then
           strain_(j) = strain(i)
           dstrain_(j) = dstrain(i)
        else
           strain_(j) = 0.D0
           dstrain_(j) = 0.D0
        endif
     enddo

     ! strain is end of increment, calcuate F1
     call strain2F(strain_,dfgrd1_)

     ! begingin of increment strain
     strain_(1:ntens_) = strain_(1:ntens) - dstrain_(1:ntens)

     ! calcualte F0
     call strain2f(strain_,dfgrd0_)

     ! get end of increment strain back
     strain_(1:ntens_) = strain_(1:ntens) + dstrain_(1:ntens)

     ! update stress/state inputs for single strain inputs
     !stress_ = stressSave0(:,i)
     !statev_ = statevSave0(:,i)

     stress_  = stress
     statev_  = statev 

     call umat( &
          stress_,  statev_,  ddsdde_,  sse_,     spd_, &
          scd_,     rpl_,     ddsddt_,  drplde_,  drpldt_, &
          strain_,  dstrain_, time_,    dtime_,   temp_, &
          dtemp_,   predef_,  dpred_,   cmname_,  ndir_, &
          nshr_,    ntens_,   nstatev_,  props_,   nprops_, &
          coords_,  drot_,    pnewdt_,  celent_,  dfgrd0_, &
          dfgrd1_,  noel_,    npt_,     layer_,   kspt_, &
          kstep_,   kinc_ )

     ! save state variables, for all strain = 0 but one
     do k = 1,nstatev_
        statevSave(k,i) = statev_(k)
     end do

     ! save stress for all strain = 0 but one
     do k = 1,6
        stressSave(k,i) = stress_(k)
     end do

  enddo

  do i = 1,6
     stressMat(:,i) = stress(:)
  end do

  ! calculte the tangent stiffness using finite difference
  ddsddeFD(:,:) = 0.D0
  do i = 1,6
     do j = startInd, endInd
        !ddsddeFD(j,i) = (stressSave(j,i) - stressSave0(j,i))/dstrain(i)
        ddsddeFD(j,i) = (stressSave(j,i) - stressMat(j,i))/dstrain(i)
     end do
  end do

end subroutine getDdsddeFD

!-----------------------------------------------------------------
! convert deformation gradient F to train Green-Lagrange Strain E
!-----------------------------------------------------------------
subroutine f2Strain(dfgrd,strain)
  implicit none

  integer j,k
  real*8 dfgrd(3,3), strain(6), strainMat(3,3)
  real*8 Cmat(3,3)

  ! right Cauchy green tensor, C
  Cmat = matmul(transpose(dfgrd),dfgrd)

  ! Green-Lagrance strain
  do j = 1,3
     do k = 1,3
        if (j.eq.k) then
           strainMat(j,k) = 0.5*(Cmat(j,k) - 1.D0)
        else
           strainMat(j,k) = 0.5*Cmat(j,k)
        endif
     end do
  end do

  ! turn in back into Voigt notation
  call tensor2voigt(strainMat, strain)

end subroutine f2Strain

!-----------------------------------------------------------------
!----Approx. Deformation tensor F from Green-Lagrange Strain -----
!----I think its just an approximatation, but it might be exaxt --
!-----------------------------------------------------------------
subroutine strain2F(strain,dfgrd)

  implicit none

  integer j,k
  real*8 strainMat(3,3), Cmat(3,3), Ceig(3,3), CVeig(3,3)
  real*8 sqrtCeig(3,3)
  real*8 strain(6), dfgrd(3,3)

  ! convert stain in Voigt notation to tensor
  call voigt2tensor(strain,strainMat)

  ! calculate right Cauchy Green Tensor
  do j = 1,3
     do k = 1,3
        if (j.eq.k) then
           Cmat(j,k) = 2.D0*strainMat(j,k) + 1.D0
        else
           Cmat(j,k) = 2.D0*strainMat(j,k)
        endif
     end do
  end do

  ! Cacluate the eigenvalues Ceig and Vector CVeig of C tensor
  call eig(Cmat,1.D-9,Ceig,CVeig)

  ! take sqrt of diagonal eigenvalues, this is the F approximation
  do j = 1,3
     do k = 1,3
        sqrtCeig(j,k) = sqrt(Ceig(j,k))
     end do
  end do

  ! use eigen vectors to get back to full F tensor
  dfgrd = matmul(matmul(CVeig, sqrtCeig),transpose(CVeig) )

end subroutine strain2F


!-----------------------------------------------------------------
!--------------- turn voigt vector to tensor matrix  ------------
!--- only works for kinematric tensors (e.g.,, strain, strainrate)
!-----------------------------------------------------------------
subroutine voigt2Tensor(Avec,Amat)

  implicit none

  real*8 Avec(6), Amat(3,3)

  ! assuming kinematic tensor
  Amat(1,1) = Avec(1)
  Amat(2,2) = Avec(2)
  Amat(3,3) = Avec(3)
  Amat(1,2) = 0.5D0*Avec(4)
  Amat(2,1) = Amat(1,2)
  Amat(1,3) = 0.5D0*Avec(5)
  Amat(3,1) = Amat(1,3)
  Amat(2,3) = 0.5D0*Avec(6)
  Amat(3,2) = Amat(2,3)

end subroutine voigt2Tensor


!-----------------------------------------------------------------
!--------------- turn tensor matrixx to voigt vector ------------
!--- only works for kinematric tensors (e.g.,, strain, strainrate)
!-----------------------------------------------------------------
subroutine tensor2Voigt(Amat,Avec)

  implicit none

  real*8 Avec(6), Amat(3,3)

  ! assuming kinematic tensor
  Avec(1) = Amat(1,1)
  Avec(2) = Amat(2,2)
  Avec(3) = Amat(3,3)
  Avec(4) = 2.D0*Amat(1,2)
  Avec(5) = 2.D0*Amat(1,3)
  Avec(6) = 2.D0*Amat(2,3)


end subroutine tensor2Voigt


! Determine eigenvalues, Aeig,  and eigenvectors, Geig, of the matrix A
! to within a tolerance, tol using Jacobi iteration method
!
! to reproduce the orginal matrix:
!
!                A =  matmul(matmul(Geig,Aeig),transpose(Geig))
!
!      Programmed by Jacob Smith, Apple Austin TX (as of 2021), circa 2013
subroutine eig(A,tol,Aeig,Geig)


  implicit none

  integer i

  real*8 conv,A(3,3),Aeig(3,3),G(3,3),Geig(3,3),tol,maxv,tau,t,c,s
  real*8 Aeig1,Aeig2,Aeig3,Geig1(3),Geig2(3),Geig3(3),Gcount1
  real*8 Gcount2,maxvo
  integer counter

  Gcount1 = 0.
  Gcount2 = 0.

  conv = sqrt(A(1,2)**2. + A(1,3)**2. + A(2,3)**2.)
  Aeig = A
  Geig = 0.
  do i=1,3
     Geig(i,i) = 1.
  enddo

  maxvo = 1e25
  counter = 0
  do while(conv.GT.tol)
     counter = counter + 1
     if (counter.GE.50) then
        print *,'eigenvalue minimization failed',conv
        goto 100
     endif
     G = 0.
     maxv = max(abs(Aeig(1,2)),abs(Aeig(1,3)),abs(Aeig(2,3)))
     if (maxv.EQ.abs(Aeig(1,2))) then
        tau = (Aeig(1,1) - Aeig(2,2))/(2.*Aeig(1,2))
        if (maxvo.EQ.maxv) then
           t = tau + sqrt(1.+tau**2.)
        else
           t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
        endif
        c = 1./sqrt(1. + t**2.)
        s = t/sqrt(1. + t**2.)
        G(1,1) = c
        G(2,2) = c
        G(1,2) = -s
        G(2,1) = s
        G(3,3) = 1.
     elseif (maxv.EQ.abs(Aeig(1,3))) then
        tau = (Aeig(1,1) - Aeig(3,3))/(2.*Aeig(1,3))       
        if (maxvo.EQ.maxv) then
           t = tau + sqrt(1.+tau**2.)
        else
           t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
        endif
        c = 1./sqrt(1. + t**2.)
        s = t/sqrt(1. + t**2.)        
        G(1,1) = c
        G(3,3) = c
        G(1,3) = -s
        G(3,1) = s
        G(2,2) = 1.        
     else
        tau = (Aeig(2,2) - Aeig(3,3))/(2.*Aeig(2,3))
        if (maxvo.EQ.maxv) then
           t = tau + sqrt(1.+tau**2.)
        else
           t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
        endif
        c = 1./sqrt(1. + t**2.)
        s = t/sqrt(1. + t**2.)        
        G(2,2) = c
        G(3,3) = c
        G(2,3) = -s
        G(3,2) = s
        G(1,1) = 1.        
     endif
     maxvo = maxv
     Aeig = matmul(matmul(G,Aeig),transpose(G))
     Geig = matmul(Geig,transpose(G))
     conv = sqrt(Aeig(1,2)**2. + Aeig(1,3)**2. + Aeig(2,3)**2.)
  enddo

  ! reorganize the principal values into max, min, mid

  Aeig1 = max(Aeig(1,1),Aeig(2,2),Aeig(3,3))
  Aeig2 = min(Aeig(1,1),Aeig(2,2),Aeig(3,3))

  do i=1,3
     if ((Aeig1.EQ.Aeig(i,i)).AND.(Gcount1.EQ.0.)) then
        Geig1 = Geig(:,i)
        Gcount1 = 1.
     elseif ((Aeig2.EQ.Aeig(i,i)).AND.(Gcount2.EQ.0.)) then
        Geig2 = Geig(:,i)
        Gcount2 = 1.
     else
        Aeig3 = Aeig(i,i)
        Geig3 = Geig(:,i)
     endif
  enddo

  Aeig = 0.
  Aeig(1,1) = Aeig1
  Aeig(2,2) = Aeig2
  Aeig(3,3) = Aeig3
  Geig(:,1) = Geig1
  Geig(:,2) = Geig2
  Geig(:,3) = Geig3

100 return

end subroutine eig

