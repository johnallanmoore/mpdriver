#!/usr/local/bin/python3

import numpy as np
import matplotlib.pyplot as plt

#resultsFileVec = ['00001.dat','00002.dat','00003.dat','00004.dat','00005.dat','00006.dat','00007.dat','00008.dat','00009.dat','00010.dat','00011.dat','00012.dat','00013.dat','00014.dat','00015.dat','00016.dat','00017.dat','00018.dat','00019.dat','00020.dat','00021.dat','00022.dat','00023.dat','00024.dat','00025.dat','00026.dat','00027.dat']

numelm = 27
resultsFileVec =[0 for x in range(numelm)]
for i in range(numelm):
    resultsFileVec[i]=str(i+1)+'.dat'


#resultsFileVec = ['00001.dat']
#colors = ['k-','r--']

for j in range(len(resultsFileVec)):
    resultsFile = resultsFileVec[j]
    data = np.loadtxt(resultsFile)

    time = data[:,0]

#    strainV = data[:,1:7]
#    stressV = data[:,7:13]
    strainV = data[:,1:7]
    stressV = data[:,7:13]

    strain11 = data[:,1]
    stress11 = data[:,7]

    sige = np.zeros(len(time))

    for i in range(len(time)):
        stressTensor = np.zeros((3,3))
        stressTensor[0,0] = stressV[i,0]# 11
        stressTensor[1,1] = stressV[i,1]# 22
        stressTensor[2,2] = stressV[i,2]# 33
        stressTensor[0,1] = stressV[i,3]# 12
        stressTensor[0,2] = stressV[i,4]# 13
        stressTensor[1,2] = stressV[i,5]# 23
        stressTensor[1,0] = stressTensor[0,1]
        stressTensor[1,2] = stressTensor[2,1]
        stressTensor[0,2] = stressTensor[2,0]

        strainTensor = np.zeros((3,3))
        strainTensor[0,0] = strainV[i,0]# 11
        strainTensor[1,1] = strainV[i,1]# 22
        strainTensor[2,2] = strainV[i,2]# 33
        strainTensor[0,1] = strainV[i,3]# 12
        strainTensor[0,2] = strainV[i,4]# 13
        strainTensor[1,2] = strainV[i,5]# 23
        strainTensor[1,0] = strainTensor[0,1]
        strainTensor[1,2] = strainTensor[2,1]
        strainTensor[0,2] = strainTensor[2,0]
        p = (1./3.)*(stressV[i,0] + stressV[i,1] + stressV[i,2])
        s = stressTensor
        s[0,0] = stressTensor[0,0] - p
        s[1,1] = stressTensor[1,1] - p
        s[2,2] = stressTensor[2,2] - p
        sige[i] = np.sqrt((3./2.)*np.tensordot(s,s,axes=2))
        
    
#    plt.plot(strain11,sige,colors[j])
#    plt.plot(strain11,stress11,colors[j])
    plt.plot(strain11,sige)
#    plt.plot(strain11,stress11)

plt.show()
