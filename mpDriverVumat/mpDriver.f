       program mpDriver

       implicit none

       integer  nprops, nblock, ndir, nshr
       integer  nstatev, nfieldv
       real*8 props
       real*8  density, coordMp
       real*8 charLength, strainInc
       real*8 relSpinInc, tempOld
       real*8 stretchOld
       real*8 defgradOld
       real*8 fieldOld, stressOld
       real*8 stateOld, enerInternOld
       real*8 enerInelasOld, tempNew
       real*8 stretchNew
       real*8 defgradNew
       real*8 fieldNew
       real*8 stressNew, stateNew
       real*8 enerInternNew, enerInelasNew
       real*8 dt,lanneal,stepTime,totalTime
       character*80 cmname

C       real*8 dimension (:), allocatable ::  props
       
C      nprops = User-specified number of user-defined material properties
C      nblock = Number of material points to be processed in this call to VUMAT
C      ndir = Number of direct components in a symmetric tensor
C      nshr = Number of indirect components in a symmetric tensor
C      nstatev = Number of user-defined state variables
C      nfieldv = Number of user-defined external field variables
C      lanneal = Flag indicating whether the routine is being called during an annealing process
C      stepTime = Value of time since the step began
C      totalTime = Value of total time
C      dt = Time increment size
C      cmname = User-specified material name, left justified
C      coordMp(nblock,*) Material point coordinates
C      charLength(nblock) Characteristic element length
C      props(nprops) User-supplied material properties
C      density(nblock) Current density at the material points 
C      strainInc (nblock, ndir+nshr) Strain increment tensor at each material point
C      relSpinInc (nblock, nshr) Incremental relative rotation vector
C      tempOld(nblock) Temperatures at beginning of the increment.
C      stretchOld (nblock, ndir+nshr) Stretch tensor, U
C      defgradOld (nblock,ndir+2*nshr) Deformation gradient tensor
C      fieldOld (nblock, nfieldv) Values of the user-defined field variables beginning of the increment
C      stressOld (nblock, ndir+nshr) Stress tensor at each material point at the beginning of the increment
C      stateOld (nblock, nstatev) State variables at each material point at the beginning of the increment.
C      enerInternOld (nblock) Internal energy per unit mass at each material point at the beginning of the increment.
C      enerInelasOld (nblock) Dissipated inelastic energy per unit mass at each material point at the beginning of the increment.
C      tempNew(nblock) Temperatures at each material point at the end of the increment.
C      stretchNew (nblock, ndir+nshr) Stretch tensor,U , at each material point at the end of the increment
C      defgradNew (nblock,ndir+2*nshr) tion gradient tensor  at the end of the increment
C      fieldNew (nblock, nfieldv) Values of the user-defined field variables  end of the increment.

C      Integer Inputs
       nblock = 1
       ndir = 3
       nshr = 3
       nstatev = 0
       nfieldv = 0
       nprops = 2
       lanneal = 0

C     Dimension Reals
C       allocate (props(nprops))
C      density(nblock), coordMp(nblock,*)
C      dimension charLength(nblock), strainInc(nblock,ndir+nshr)
C      dimension relSpinInc(nblock,nshr), tempOld(nblock)
C      dimension   stretchOld(nblock,ndir+nshr)
C      dimension defgradOld(nblock,ndir+nshr+nshr)
C      dimension fieldOld(nblock,nfieldv), stressOld(nblock,ndir+nshr)
C      dimension stateOld(nblock,nstatev), enerInternOld(nblock)
C      dimension enerInelasOld(nblock), tempNew(nblock)
C      dimension stretchNew(nblock,ndir+nshr)
C      dimension defgradNew(nblock,ndir+nshr+nshr)
C      dimension fieldNew(nblock,nfieldv)
C      dimension stressNew(nblock,ndir+nshr) 
C      dimension stateNew(nblock,nstatev)
C      dimension enerInternNew(nblock), enerInelasNew(nblock)
C     Real Inputs
C       charLength(nblock) = 
      
       call vumat(
C Read only (unmodifiable)variables -
     1  nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,
     2  stepTime, totalTime, dt, cmname, coordMp, charLength,
     3  props, density, strainInc, relSpinInc,
     4  tempOld, stretchOld, defgradOld, fieldOld,
     5  stressOld, stateOld, enerInternOld, enerInelasOld,
     6  tempNew, stretchNew, defgradNew, fieldNew,
C Write only (modifiable) variables -
     7  stressNew, stateNew, enerInternNew, enerInelasNew )
       end program mpDriver



