C                                                  _______
C                 \    /  |    |   |\  /|    /\       |
C                  \  /   |    |   | \/ |   /__\      |
C                   \/    |____|   |    |  /    \     |
C
C   Updates stresses, internal variables, and energies for 3D solid elements ONLY
C
C  
C     SIMPLE LINEAR ELASTIC ISOTROPIC MATERIAL, SMALL STRAIN
C  
C  ============================================================================
      subroutine vumat(
C Read only (unmodifiable)variables -
     1  nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,
     2  stepTime, totalTime, dt, cmname, coordMp, charLength,
     3  props, density, strainInc, relSpinInc,
     4  tempOld, stretchOld, defgradOld, fieldOld,
     5  stressOld, stateOld, enerInternOld, enerInelasOld,
     6  tempNew, stretchNew, defgradNew, fieldNew,
C Write only (modifiable) variables -
     7  stressNew, stateNew, enerInternNew, enerInelasNew )
C
C      include 'vaba_param.inc'
C

       implicit none
        
       integer  nprops, nblock, ndir, nshr
       integer  nstatev, nfieldv 
      
       real*8 dt,lanneal,stepTime,totalTime

       real*8 density, coordMp,props, 
     1  charLength, strainInc,relSpinInc, tempOld,
     2  stretchOld,defgradOld,fieldOld, stressOld,
     3  stateOld, enerInternOld,enerInelasOld, tempNew,
     4  stretchNew,defgradNew,fieldNew,stressNew, stateNew,
     5  enerInternNew, enerInelasNew

       real*8 tr,sig(6)

       dimension
     1  density(nblock), coordMp(nblock,*),props(nprops), 
     1  charLength(nblock), strainInc(nblock,ndir+nshr),
     2  relSpinInc(nblock,nshr), tempOld(nblock),
     3  stretchOld(nblock,ndir+nshr),
     4  defgradOld(nblock,ndir+nshr+nshr),
     5  fieldOld(nblock,nfieldv), stressOld(nblock,ndir+nshr),
     6  stateOld(nblock,nstatev), enerInternOld(nblock),
     7  enerInelasOld(nblock), tempNew(nblock),
     8  stretchNew(nblock,ndir+nshr),
     8  defgradNew(nblock,ndir+nshr+nshr),
     9  fieldNew(nblock,nfieldv),
     1  stressNew(nblock,ndir+nshr), stateNew(nblock,nstatev),
     2  enerInternNew(nblock), enerInelasNew(nblock)
C
      integer km, i 
      character*80 cmname

      real*8 e,nu, lame1, G

C-----------------------------------------------------------------------------
C	           Initialize material data from input file
C-----------------------------------------------------------------------------
      e    = props(1) ! Young's modulus
      nu  = props(2) ! Poisson's ratio

      lame1 = e*nu/((1.D0+nu)*(1.D0-2.D0*nu)) ! lame1
      G     = e/(2.D0*(1.D0+nu)) ! lame2

      do km = 1,nblock
C-----------------------------------------------------------------------------
C                         Get stress
C-----------------------------------------------------------------------------
         tr    = strainInc(km,1) + strainInc(km,2) + strainInc(km,3)
         sig(1) = stressOld(km,1) + lame1*tr + 2.D0*G*strainInc(km,1) 
         sig(2) = stressOld(km,2) + lame1*tr + 2.D0*G*strainInc(km,2)
         sig(3) = stressOld(km,3) + lame1*tr + 2.D0*G*strainInc(km,3)
         sig(4) = stressOld(km,4) + 2.D0*G*strainInc(km,4)  
         sig(5) = stressOld(km,5) + 2.D0*G*strainInc(km,5) 
         sig(6) = stressOld(km,6) + 2.D0*G*strainInc(km,6)

         stressNew(km,1) = sig(1)
         stressNew(km,2) = sig(2)
         stressNew(km,3) = sig(3)
         stressNew(km,4) = sig(4)  
         stressNew(km,5) = sig(5) 
         stressnew(km,6) = sig(6) 
      enddo

      return
      end
