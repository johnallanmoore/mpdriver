C                                                  _______
C                 \    /  |    |   |\  /|    /\       |
C                  \  /   |    |   | \/ |   /__\      |
C                   \/    |____|   |    |  /    \     |
C
C  Updates stresses, internal variables, and energies for 3D solid elements ONLY
C
C  
C     J2-PLASTICITY MATERIAL SUBROUTINE WITH DAMAGE - RADIAL RETURN METHOD
C              VIA MATRIX INVERSION INSTEAD OF THE CLOSED FORM 
C              SOLUTION FOR THE PLASTIC STRAIN-RATE MULTIPLIER
C              INCREMENT WRITTEN FOR THE ABAQUS\EXPLICIT MODULE
C  
C  ============================================================================
      subroutine vumat(
C Read only -
     1     nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,
     2     stepTime, totalTime, dt, cmname, coordMp, charLength,
     3     props, density, strainInc, relSpinInc,
     4     tempOld, stretchOld, defgradOld, fieldOld,
     5     stressOld, stateOld, enerInternOld, enerInelasOld,
     6     tempNew, stretchNew, defgradNew, fieldNew,
C Write only -
     7     stressNew, stateNew, enerInternNew, enerInelasNew)
C
C      include 'vaba_param.inc'
C

       implicit none
 
       character*80 cmname
        
       integer  nprops, nblock, ndir, nshr
       integer  nstatev, nfieldv
      
       real*8 dt,lanneal,stepTime,totalTime

       real*8 density, coordMp,props, 
     1  charLength, strainInc,relSpinInc, tempOld,
     2  stretchOld,defgradOld,fieldOld, stressOld,
     3  stateOld, enerInternOld,enerInelasOld, tempNew,
     4  stretchNew,defgradNew,fieldNew,stressNew, stateNew,
     5  enerInternNew, enerInelasNew


       dimension
     1  density(nblock), coordMp(nblock,*),props(nprops), 
     1  charLength(nblock), strainInc(nblock,ndir+nshr),
     2  relSpinInc(nblock,nshr), tempOld(nblock),
     3  stretchOld(nblock,ndir+nshr),
     4  defgradOld(nblock,ndir+nshr+nshr),
     5  fieldOld(nblock,nfieldv), stressOld(nblock,ndir+nshr),
     6  stateOld(nblock,nstatev), enerInternOld(nblock),
     7  enerInelasOld(nblock), tempNew(nblock),
     8  stretchNew(nblock,ndir+nshr),
     8  defgradNew(nblock,ndir+nshr+nshr),
     9  fieldNew(nblock,nfieldv),
     1  stressNew(nblock,ndir+nshr), stateNew(nblock,nstatev),
     2  enerInternNew(nblock), enerInelasNew(nblock)

        integer i, j, k, counter
C-----------------------------------------------------------------------------
C                       Start new variable definitions here
C-----------------------------------------------------------------------------
      logical YIELD
      real*8 e,xnu,sy0,e0,n,kexp,cys,cpre,alpha1,alpha2,beta1,beta2
      real*8 beta3,gamma1,gamma2,gamma3,tol,lame1,G,m,coef1,coef2
      real*8 coef3,coef4,coef5,coef6,coef7,coef8,coef9,L(6,6),C(6,6)
      real*8 I6(6,6),Ivect(6),II(6,6),I2(2,2),Ih(6,6),IkronI(6,6)
      real*8 Cinv(6,6),tr,tsig(6),epb0,sy,H,tIPEvoigt(6),tIPEmat(3,3)
      real*8 tIPEeval(3,3),IPEevec(3,3),tIPE(3),S1,S2,S3,phihat1
      real*8 phihat2,phihat,f,dphihat1d1,dphihat1d2,dphihat1d3
      real*8 dphihat2d1,dphihat2d2,dphihat2d3,dphihat(3),premult1
      real*8 rvec(6),plmul,depb,IPE(3),epb,dphihatkron(3,3)
      real*8 d2phihat1d11,d2phihat1d12,d2phihat1d13,d2phihat1d21
      real*8 d2phihat1d22,d2phihat1d23,d2phihat1d31,d2phihat1d32
      real*8 d2phihat1d33,d2phihat2d11,d2phihat2d22,d2phihat2d33
      real*8 d2phihat(3,3),premult2,premult3,drdsig(6,6)
      real*8 dqdlam,Amat(7,7),Ainv(7,7),r(6),rl(7),rr(7),fn,den
      real*8 plinc,trr,dIPE(3),fncheck,rmat(3,3),dpPIPEmat(3,3)
      real*8 DP(6),sig(6),alpha,tpress,press,tPIPE(3),PIPE(3),dPIPE(3)
      real*8 I3vect(3),tPT(3),PT(3),Lst(6,6),I3mat(3,3),dpPIPE(6)
      real*8 I1st, f0, fini, fc, fst, wg, fyield, ret, sigret
      real*8 argu, coefGTN2, coefGTN3, coefGTN4, coefGTN5, coefGTN6 
      real*8 coefGTN7, coefGTN8, coefGTN9, coefGTN10, coefGTN11
      real*8 q1,q2, fult, drdq1(6),drdq2(6),df, phistar,dfstdf
      real*8 sinhcoef, coshcoef, phibar, dhdsig(6),dhdq, dfdq
C-----------------------------------------------------------------------------
C	           Initialize material data from input file
C-----------------------------------------------------------------------------
      e    = props(1) ! Young's modulus
      xnu  = props(2) ! Poisson's ratio
      sy0  = props(3) ! initial yield strength
      e0   = props(4) ! initial yield strain
      n    = props(5) ! matrix stress exponent
C     Pressure Depedent Properties
      alpha = props(6) ! pressure-dependence term
      kexp = props(7) ! yield surface exponent
      cys  = props(8) ! yield surface weighting coefficient
      cpre = props(9) ! premultiplier of the linear transformation tensor
C	Anisotropic Properties
      alpha1 = props(10) ! the rest are elements of the linear transformation tensor
      alpha2 = props(11)
      beta1  = props(12)
      beta2  = props(13)
      beta3  = props(14)
      gamma1 = props(15)
      gamma2 = props(16)
      gamma3 = props(17)
      tol    = props(18)
C
      lame1 = e*xnu/((1.D0+xnu)*(1.D0-2.D0*xnu)) ! lame1
      G     = e/(2.D0*(1.D0+xnu)) ! lame2
      m = (3.D0**(2.D0*kexp))/(2.D0**(2.D0*kexp-1.D0) + 1.D0)
      coef1 = 2.D0*kexp
      coef2 = 2.D0*kexp-1.D0
      coef3 = 1.D0/(2.D0*kexp)
      coef4 = 1.D0/(2.D0*kexp) - 1.D0
      coef5 = 1.D0/(4.D0*kexp)
      coef6 = 1.D0/(2.D0*kexp) - 2.D0
      coef7 = 4.D0*kexp**2.D0 - 2.D0*kexp
      coef8 = 2.D0*kexp-2.D0
      coef9 = 1.D0 - 2.D0*kexp
C
C  Determine the inverse of the elastic moduli tensor and
C  get the 4th order projection tensor Ih
C
      L = 0.
      L(1,1) = 1.D0
      L(2,2) = alpha1
      L(3,3) = alpha2
      L(1,2) = beta1
      L(2,1) = beta1
      L(1,3) = beta2
      L(3,1) = beta2
      L(2,3) = beta3
      L(3,2) = beta3
      L(4,4) = gamma1
      L(5,5) = gamma2
      L(6,6) = gamma3
      L = cpre*L
C
      C = 0.
      do i=1,3
         C(i,i) = lame1+2.*G
         C(i+3,i+3) = 2.*G
      enddo
      C(1,2) = lame1
      C(2,3) = lame1
      C(1,3) = lame1
      C(2,1) = lame1
      C(3,1) = lame1
      C(3,2) = lame1
C
      I6 = 0.
      do i=1,6
         I6(i,i) = 1.
      enddo
C
      Ivect = 0.D0
      II = 0.D0
      I3mat = 0.D0
      do i=1,3
         do j=1,3 
            II(i,j) = -1.D0/3.D0
         enddo
         Ivect(i) = 1.D0
         I3vect(i) = 1.D0
         I3mat(i,i) = 1.D0
      enddo
C
      I2 = 0.
      do i=1,2
         I2(i,i) = 1.D0
      enddo
C
      Ih = I6 + II
C
      call invert(C,6,Cinv)
      call kron(Ivect,Ivect,6,IkronI)
      Lst = L - II
C-----------------------------------------------------------------------------           
C	               Start element loop
C-----------------------------------------------------------------------------
      do k = 1,nblock
C-----------------------------------------------------------------------------
C                         Get trial stress
C-----------------------------------------------------------------------------
         tr    = strainInc(k,1) + strainInc(k,2) + strainInc(k,3)
         tsig(1) = stressOld(k,1) + lame1*tr + 2.D0*G*strainInc(k,1) 
         tsig(2) = stressOld(k,2) + lame1*tr + 2.D0*G*strainInc(k,2)
         tsig(3) = stressOld(k,3) + lame1*tr + 2.D0*G*strainInc(k,3)
         tsig(4) = stressOld(k,4) + 2.D0*G*strainInc(k,4)  
         tsig(5) = stressOld(k,5) + 2.D0*G*strainInc(k,5) 
         tsig(6) = stressOld(k,6) + 2.D0*G*strainInc(k,6)
C-----------------------------------------------------------------------------
C              Bring in the state variables and initialize
C-----------------------------------------------------------------------------
         epb0 = stateOld(k,1)
         sy = sy0*(1.D0+epb0/e0)**n
         H  = (n/e0)*sy0*(1.D0+epb0/e0)**(n-1.D0)
C-----------------------------------------------------------------------------
C              Get the trial pressure tensor
C-----------------------------------------------------------------------------
         tpress = -(1.D0/3.D0)*(tsig(1)+tsig(2)+tsig(3))
         tPT = -tpress*I3vect
C-----------------------------------------------------------------------------
C              Get trial IPE tensor
C-----------------------------------------------------------------------------
         tIPEvoigt = matmul(L,tsig)
         tIPEmat(1,1) = tIPEvoigt(1)
         tIPEmat(2,2) = tIPEvoigt(2)
         tIPEmat(3,3) = tIPEvoigt(3)
         tIPEmat(1,2) = tIPEvoigt(4)
         tIPEmat(2,1) = tIPEvoigt(4)
         tIPEmat(2,3) = tIPEvoigt(5)
         tIPEmat(3,2) = tIPEvoigt(5)
         tIPEmat(1,3) = tIPEvoigt(6)
         tIPEmat(3,1) = tIPEvoigt(6)
C-----------------------------------------------------------------------------
C              Get trial IPE principal values
C-----------------------------------------------------------------------------
         call eig(tIPEmat,tol,tIPEeval,IPEevec)
C
         tIPE(1) = tIPEeval(1,1)
         tIPE(2) = tIPEeval(2,2)
         tIPE(3) = tIPEeval(3,3)       
         S1 = tIPE(1)/sy
         S2 = tIPE(2)/sy
         S3 = tIPE(3)/sy 
C-----------------------------------------------------------------------------
C              Get trial PIPE tensor
C-----------------------------------------------------------------------------
         tPIPE = tIPE + tPT
Cc-----------------------------------------------------------------------------
C              Develop the yield criterion
C-----------------------------------------------------------------------------
         phihat1 = (S1 - S2)**coef1+(S2 - S3)**coef1+(S3 - S1)**coef1
         phihat2 = S1**coef1 + S2**coef1 + S3**coef1
         phihat = (1.D0-cys)*phihat1 + cys*m*phihat2
C
         phibar = sy*(phihat/2.D0)**coef3 
Cc-----------------------------------------------------------------------------
C              Define Mean Stress (I1*)
C-----------------------------------------------------------------------------
	 I1st = tPIPE(1) + tPIPE(2) + tPIPE(3)
C-----------------------------------------------------------------------------
C        Get return direction for stress update and the unit normal
C-----------------------------------------------------------------------------
C
	 fyield = phibar - sy
C
         dphihat1d1 = coef1*((S1 - S2)**coef2 - (S3 - S1)**coef2)
         dphihat1d2 = coef1*((S2 - S3)**coef2 - (S1 - S2)**coef2)
         dphihat1d3 = coef1*((S3 - S1)**coef2 - (S2 - S3)**coef2)
C
         dphihat2d1 = coef1*S1**coef2
         dphihat2d2 = coef1*S2**coef2
         dphihat2d3 = coef1*S3**coef2
C
         dphihat(1) = (1.D0-cys)*dphihat1d1 + cys*m*dphihat2d1
         dphihat(2) = (1.D0-cys)*dphihat1d2 + cys*m*dphihat2d2
         dphihat(3) = (1.D0-cys)*dphihat1d3 + cys*m*dphihat2d3

         premult1 = coef5*(phihat/2.D0)**coef4

         rvec(1) = premult1*dphihat(1)
         rvec(2) = premult1*dphihat(2)
         rvec(3) = premult1*dphihat(3)
C
C 	   AMMENDED RETURN DIRECTIONS TO ENSURE IPE IS DEVIATORIC
C
         trr = rvec(1)+rvec(2)+rvec(3)
         rvec(1) = rvec(1) - trr/3.D0 
         rvec(2) = rvec(2) - trr/3.D0 
         rvec(3) = rvec(3) - trr/3.D0 
C
C-----------------------------------------------------------------------------
C        I use the magnitude of the strain increment to help convergence
C-----------------------------------------------------------------------------
C         call dblcontr(strainInc(k,:),strainInc(k,:),effstr)
         plmul = sqrt(strainInc(k,1)**2.D0 + strainInc(k,2)**2.D0
     &           + strainInc(k,3)**2.D0 + 2.D0*(strainInc(k,4)**2.D0
     &           + strainInc(k,5)**2.D0 + strainInc(k,6)**2.D0))
         depb  = plmul
C-----------------------------------------------------------------------------
C           Check for yielding
C-----------------------------------------------------------------------------
         YIELD    = .FALSE.
C
            if ( fyield > 0.D0) then
C
                YIELD = .TRUE.
                IPE  = tIPE
                PIPE = tPIPE
C
            else
C
                YIELD = .FALSE.
C
                sig  = tsig
                epb   = epb0
C
            end if 
C-----------------------------------------------------------------------------            
C        Begin Minimization loop
C-----------------------------------------------------------------------------
         counter = 0
         do while (YIELD)
C           
            counter = counter + 1
C-----------------------------------------------------------------------------
C           Put a stopper in case minimzation fails (usually around 50 iterations)
C-----------------------------------------------------------------------------
            if (counter .GE. 50) then
                print *,'Did not minimize'
                go to 100
            end if
C-----------------------------------------------------------------------------
C           Get the plastic increment
C-----------------------------------------------------------------------------
C          Determine the inverse of the matrix A

            call kron(dphihat,dphihat,3,dphihatkron)
C    
            d2phihat1d11 = coef7*((S1 - S2)**coef8 + (S3 - S1)**coef8)
            d2phihat1d12 = -coef7*(S1 - S2)**coef8
            d2phihat1d13 = -coef7*(S3 - S1)**coef8
            d2phihat1d21 = -coef7*(S1 - S2)**coef8
            d2phihat1d22 = coef7*((S2 - S3)**coef8 + (S1 - S2)**coef8)
            d2phihat1d23 = -coef7*(S2 - S3)**coef8
            d2phihat1d31 = -coef7*(S3 - S1)**coef8
            d2phihat1d32 = -coef7*(S2 - S3)**coef8
            d2phihat1d33 = coef7*((S3 - S1)**coef8 + (S2 - S3)**coef8)

            d2phihat2d11 = coef7*S1**coef2
            d2phihat2d22 = coef7*S2**coef2
            d2phihat2d33 = coef7*S3**coef2

            d2phihat(1,1) = (1.D0-cys)*d2phihat1d11 + cys*m*d2phihat2d11
            d2phihat(1,2) = (1.D0-cys)*d2phihat1d12
            d2phihat(1,3) = (1.D0-cys)*d2phihat1d13
            d2phihat(2,1) = (1.D0-cys)*d2phihat1d21
            d2phihat(2,2) = (1.D0-cys)*d2phihat1d22 + cys*m*d2phihat2d22
            d2phihat(2,3) = (1.D0-cys)*d2phihat1d23
            d2phihat(3,1) = (1.D0-cys)*d2phihat1d31
            d2phihat(3,2) = (1.D0-cys)*d2phihat1d32
            d2phihat(3,3) = (1.D0-cys)*d2phihat1d33 + cys*m*d2phihat2d33
C-----------------------------------------------------------------------------
C           dr/dsig
C-----------------------------------------------------------------------------						
C		GTN parameters
		call dblcontr3x6(PIPE,rvec,sigret)
		
	
            premult2 = (coef9/(16.D0*sy*kexp**2.D0))*
     1               (phihat/2.D0)**coef6
            premult3 = (1.D0/(4.D0*sy*kexp))*(phihat/2.D0)**coef4

c$$$            premult2 = (coef9/(16.D0*kexp**2.D0))*
c$$$     1               (phihat/2.D0)**coef6
c$$$            premult3 = (1.D0/(4.D0*kexp))*(phihat/2.D0)**coef4

C           had this commented out, this was the error, erg!
            drdsig = 0.D0

C		KB93 Part (need to initiaize if you add, this was the error)
            drdsig(1:3,1:3) = drdsig(1:3,1:3) + premult2*dphihatkron 
     1           + premult3*d2phihat 
C-----------------------------------------------------------------------------
C           dr/dq1 
C-----------------------------------------------------------------------------
            drdq1(1:6) = 0.D0

C-----------------------------------------------------------------------------
C           dh/dsigma
C-----------------------------------------------------------------------------
            dhdsig(1:6) = 0.D0

C-----------------------------------------------------------------------------
C           dhdq
C-----------------------------------------------------------------------------
            dhdq = 0.D0

C-----------------------------------------------------------------------------
C           dfdq
C-----------------------------------------------------------------------------
            dfdq = -1.D0*H

C-----------------------------------------------------------------------------
C           h
C-----------------------------------------------------------------------------
            dqdlam = 1.D0

C-----------------------------------------------------------------------------
C            A matrix
C-----------------------------------------------------------------------------
           		
            Amat(1:6,1:6) = Cinv + plmul*drdsig
            Amat(1:6,7) = plmul*drdq1(1:6)       
C           dh/dsigma h = dq/dlambda
            Amat(7,1:6) = plmul*dhdsig(1:6) 
C           dh/dq - 1
            Amat(7,7) = dhdq - 1.D0

            call invert(Amat,7,Ainv)

C          Change this to the transpose based
C          on what i beleive is correct from anisoCA.f
           Ainv = transpose(Ainv)

C ------------------------------------------------------------
C          Get the left and right multiplied direction vectors
C ------------------------------------------------------------
            rl(1:6) = rvec(1:6)
            rl(7) = dfdq
C
            rr(1:6) = rvec(1:6)
C           h (lower case) dq/dlambda
            rr(7)   = dqdlam

C           yield criteria
            fn    = fyield
C           matrix multiplier to get Jacobian denominator
            call sclrval(rl,rr,Ainv,7,den)
C
            plinc = fn/den
C
C-----------------------------------------------------------------------------
C        Update state variables and plastic multiplier
C-----------------------------------------------------------------------------
            plmul = plmul + plinc
C
C           is this right?
C            depb  = plmul*sigret/sy
            depb = plmul*dqdlam
C
            epb = epb0 + depb
C
            sy = sy0*(1.D0+epb/e0)**n
            H  = (n/e0)*sy0*(1.D0+epb/e0)**(n-1.D0)
C-----------------------------------------------------------------------------
C           Update the stress measures
C-----------------------------------------------------------------------------
            trr = rvec(1) + rvec(2) + rvec(3)
            dPIPE(1) = plmul*lame1*trr + 2.*plmul*G*rvec(1)
            dPIPE(2) = plmul*lame1*trr + 2.*plmul*G*rvec(2)
            dPIPE(3) = plmul*lame1*trr + 2.*plmul*G*rvec(3)

            PIPE = tPIPE - dPIPE

C-----------------------------------------------------------------------------
C              Get the trial pressure tensor
C-----------------------------------------------------------------------------
            press = -(1.D0/3.D0)*(PIPE(1)+PIPE(2)+PIPE(3))
            PT = -press*I3vect
C-----------------------------------------------------------------------------
C              Get the trial pressure tensor
C-----------------------------------------------------------------------------
            IPE = PIPE - PT
            S1 = IPE(1)/sy
            S2 = IPE(2)/sy
            S3 = IPE(3)/sy
Cc-----------------------------------------------------------------------------
C              Develop the yield criterion
C-----------------------------------------------------------------------------
            phihat1 = (S1 - S2)**coef1+(S2 - S3)**coef1+(S3 - S1)**coef1
            phihat2 = S1**coef1 + S2**coef1 + S3**coef1
            phihat = (1.D0-cys)*phihat1 + cys*m*phihat2
C
            phibar = sy*(phihat/2.D0)**coef3 

Cc-----------------------------------------------------------------------------
C              Define Mean Stress (I1*)
C-----------------------------------------------------------------------------
	      I1st = PIPE(1)+PIPE(2)+PIPE(3)
C-----------------------------------------------------------------------------
C        Get return direction for stress update and the unit normal
C-----------------------------------------------------------------------------
C
C
C
C	    Yield Criteria
	    fyield = phibar - sy
			
            dphihat1d1 = coef1*((S1 - S2)**coef2 - (S3 - S1)**coef2)
            dphihat1d2 = coef1*((S2 - S3)**coef2 - (S1 - S2)**coef2)
            dphihat1d3 = coef1*((S3 - S1)**coef2 - (S2 - S3)**coef2)
C
            dphihat2d1 = coef1*S1**coef2
            dphihat2d2 = coef1*S2**coef2
            dphihat2d3 = coef1*S3**coef2
C
            dphihat(1) = (1.D0-cys)*dphihat1d1 + cys*m*dphihat2d1
            dphihat(2) = (1.D0-cys)*dphihat1d2 + cys*m*dphihat2d2
            dphihat(3) = (1.D0-cys)*dphihat1d3 + cys*m*dphihat2d3

            premult1 = coef5*(phihat/2.D0)**coef4
C
            rvec(1) = premult1*dphihat(1)
            rvec(2) = premult1*dphihat(2)
            rvec(3) = premult1*dphihat(3)
C
C AMMENDED RETURN DIRECTIONS TO ENSURE IPE IS DEVIATORIC
C
            trr = rvec(1)+rvec(2)+rvec(3)
            rvec(1) = rvec(1) - trr/3.D0
            rvec(2) = rvec(2) - trr/3.D0
            rvec(3) = rvec(3) - trr/3.D0
C
C-----------------------------------------------------------------------------
C         Check the yield condition again
C-----------------------------------------------------------------------------
            fncheck = abs(fyield/sy)
C
            if (fncheck > 1.D-9) then
                YIELD = .TRUE.
            else
                YIELD = .FALSE.
            end if

            if (isnan(PIPE(1))) THEN
               print *, counter
               print *, totalTime
               print *, Cinv(1,1)
               print *, drdsig(1,1)
               print *, Amat(1,1)
               print *, rl
               print *, rr
               print *, den
               print *, fn
               print *, plinc
               print *, plmul
               print *, epb
               print *, sy
               print *, S1
               print *, phibar
               print *, dphihat1d1
               print *, dphihat(1)
               print *, premult1
               print *, rvec
            endif 

C-----------------------------------------------------------------------------
C        End minimization loop
C-----------------------------------------------------------------------------
         enddo
C-----------------------------------------------------------------------------
C               Exit procedure
C-----------------------------------------------------------------------------
C-----------------------------------------------------------------------------
C               Transform IPE to Cauchy
C-----------------------------------------------------------------------------
         if (counter.GT.0.) then
            rmat = 0.D0
            rmat(1,1) = rvec(1)
            rmat(2,2) = rvec(2)
            rmat(3,3) = rvec(3)
C           This (with Q') only works becuase stress is symmetric
            dpPIPEmat = 
     &          plmul*matmul(matmul(IPEevec,rmat),transpose(IPEevec))
C     The above is correct because it is convering it back to non-eigenvalues
C            dpPIPEmat = 
C     &          plmul*matmul(transpose(IPEevec),matmul(rmat,IPEevec))
            dpPIPE(1) = dpPIPEmat(1,1)
            dpPIPE(2) = dpPIPEmat(2,2)
            dpPIPE(3) = dpPIPEmat(3,3)
            dpPIPE(4) = dpPIPEmat(1,2)
            dpPIPE(5) = dpPIPEmat(2,3)
            dpPIPE(6) = dpPIPEmat(3,1)
C
            DP = matmul(Lst,dpPIPE)
C
            sig = tsig - matmul(C,DP)

         endif
C-----------------------------------------------------------------------------
C              Update new stresses 
C-----------------------------------------------------------------------------
100         stressNew(k,1) = sig(1)
            stressNew(k,2) = sig(2)
            stressNew(k,3) = sig(3)
            stressNew(k,4) = sig(4)
            stressNew(k,5) = sig(5)
            stressNew(k,6) = sig(6)
C-----------------------------------------------------------------------------
C              Update the internal variables
C-----------------------------------------------------------------------------

            stateNew(k,1)  = epb
            stateNew(k,2)  = sy
            stateNew(k,3)  = fncheck
            stateNew(k,5)  = 0.0D0
            stateNew(k,6)  = phibar
            stateNew(k,7)  = 0.D0

C-----------------------------------------------------------------------------
C              Verify the increment in plastic work
C-----------------------------------------------------------------------------
C            dep(1) = plmul*r(1)
C            dep(2) = plmul*r(2)
C            dep(3) = plmul*r(3)
C            dep(4) = plmul*r(4)
C            dep(5) = plmul*r(5) 
C            dep(6) = plmul*r(6)
C
C            dwp1 = sig(1)*dep(1)+sig(2)*dep(2)+sig(3)*dep(3)
C     &             +2.*(sig(4)*dep(4)
C     &             +sig(5)*dep(5)+sig(6)*dep(6))
C            dwp2 = (1.-f)*sy*depb
C            if (epb.GT.0.) then
C               stateNew(k,4) = dwp1-dwp2
C            endif
C-----------------------------------------------------------------------------
C     End of subroutine
C-----------------------------------------------------------------------------
      enddo
      return
      end
C #============================================================================
C #============================================================================
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine invert(A,n,Ainv)
C A is an nxn matrix, n is the matrix dimension, Ainv is A**(-1)
C this subroutin uses Gaussian elimination to determine the inverse
C of an nxn square matrix
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C      include 'vaba_param.inc'
      implicit none

      INTEGER :: i,n,k,counter,j,g
      REAL*8 :: A(n,n),Ainv(n,n),Atemp(n,n),Id(n,n),den,mul

         Id = 0.
         do i=1,n
            Id(i,i) = 1.
         enddo

         Ainv = Id

         Atemp = A

         if (sum(A) .EQ. 0.) then
            Ainv = A;
         else
C zero out lower left diagonal
            do i=1,n
C set the ith column in the ith row equal to 1
               if (Atemp(i,i) .NE. 1.) then
                  if (Atemp(i,i) .NE. 0.) then          
                     den = Atemp(i,i)
                     Atemp(i,:) = Atemp(i,:)/den
                     Ainv(i,:) = Ainv(i,:)/den
                  else
                     if (i .EQ. n) then
                        stop 'matrix is singular'
                     endif
                     do k=i+1,n
                        if (Atemp(k,i) .NE. 0.) then
                           counter = k
                           exit
                        endif
                        if (k .EQ. n) then
                           stop 'matrix is singular'
                        endif
                     enddo
                     den = Atemp(counter,i)
                     Atemp(i,:) = Atemp(counter,:)/den + Atemp(i,:)
                     Ainv(i,:) = Ainv(counter,:)/den + Ainv(i,:)
                  endif
               endif
C set the ith column in all rows > i equal to 0
               do k=i+1,n
                  if (Atemp(k,i) .NE. 0.) then
                     mul = Atemp(k,i)
                     Atemp(k,:) = -mul*Atemp(i,:) + Atemp(k,:)
                     Ainv(k,:) = -mul*Ainv(i,:) + Ainv(k,:)
                  endif
               enddo
           enddo
C zero out upper right diagonal
           do i=2,n
              j = n - i + 2
              do k=1,j-1
                 g = j - k
                 if (Atemp(g,j) .NE. 0.) then
                    mul = Atemp(g,j)
                    Atemp(g,:) = -mul*Atemp(j,:) + Atemp(g,:)
                    Ainv(g,:) = -mul*Ainv(j,:) + Ainv(g,:)
                 endif
              enddo
           enddo
        endif

      return

      end subroutine invert

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine kron(a,b,n,C)

C this subroutine takes the kronecker product
C of two vectors a and b and gives back an nxn
C matrix C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C      include 'vaba_param.inc'

      implicit none

      INTEGER :: n,i,j
      REAL*8 :: a(n),b(n),C(n,n)

         do i=1,n
            do j=1,n
               C(i,j) = a(i)*b(j)
            enddo
         enddo
   
      return
 
      end subroutine kron


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine sclrval(av,bv,Amat,n,sclr)

C     This subroutine takes two vectors av and bv and
C a matrix Amat and determines the scalar product av*Amat*bv
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C      include 'vaba_param.inc'
      implicit none

      INTEGER :: n, i , j
      REAL*8 :: sclr,av(n),bv(n),Amat(n,n),temp(n),elsum

         do i=1,n
            elsum = 0.
            do j=1,n
               elsum = elsum + av(j)*Amat(j,i)
            enddo
            temp(i) = elsum
         enddo

         sclr = 0.
         do i=1,n
            sclr = sclr + temp(i)*bv(i)
         enddo

      return

      end subroutine sclrval
C
C
C
      subroutine dblcontr(A,B,sclr)
 
C      include 'vaba_param.inc'
      implicit none

      integer i
      REAL*8 :: A(6),B(6),sclr

         sclr = 0.
         do i=1,6
            if (i.LE.3) then
               sclr = sclr + A(i)*B(i)
            else
               sclr = sclr + 2.*A(i)*B(i)
            endif
         enddo

      return

      end subroutine

      subroutine dblcontr3x6(A,B,sclr)
 
C      include 'vaba_param.inc'
      implicit none

      integer i
      REAL*8 :: A(3),B(6),sclr

         sclr = 0.
         do i=1,3
            sclr = sclr + A(i)*B(i)
         enddo

      return

      end subroutine


C Determine eigenvalues, Aeig,  and eigenvectors, Geig, of the matrix A
C to within a tolerance, tol using Jacobi iteration method
C
C to reproduce the orginal matrix:
C
C                A =  matmul(matmul(Geig,Aeig),transpose(Geig))
C
      subroutine eig(A,tol,Aeig,Geig)

C        include 'vaba_param.inc'
        implicit none

        integer i

        real*8 conv,A(3,3),Aeig(3,3),G(3,3),Geig(3,3),tol,maxv,tau,t,c,s
        real*8 Aeig1,Aeig2,Aeig3,Geig1(3),Geig2(3),Geig3(3),Gcount1
        real*8 Gcount2,maxvo
        integer counter

        Gcount1 = 0.
        Gcount2 = 0.

        conv = sqrt(A(1,2)**2. + A(1,3)**2. + A(2,3)**2.)
        Aeig = A
        Geig = 0.
        do i=1,3
           Geig(i,i) = 1.
        enddo
 
        maxvo = 1e25
        counter = 0
        do while(conv.GT.tol)
           counter = counter + 1
           if (counter.GE.50) then
              print *,'eigenvalue minimization failed',conv
              goto 100
           endif
           G = 0.
           maxv = max(abs(Aeig(1,2)),abs(Aeig(1,3)),abs(Aeig(2,3)))
           if (maxv.EQ.abs(Aeig(1,2))) then
              tau = (Aeig(1,1) - Aeig(2,2))/(2.*Aeig(1,2))
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)
              G(1,1) = c
              G(2,2) = c
              G(1,2) = -s
              G(2,1) = s
              G(3,3) = 1.
           elseif (maxv.EQ.abs(Aeig(1,3))) then
              tau = (Aeig(1,1) - Aeig(3,3))/(2.*Aeig(1,3))       
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)        
              G(1,1) = c
              G(3,3) = c
              G(1,3) = -s
              G(3,1) = s
              G(2,2) = 1.        
           else
              tau = (Aeig(2,2) - Aeig(3,3))/(2.*Aeig(2,3))
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)        
              G(2,2) = c
              G(3,3) = c
              G(2,3) = -s
              G(3,2) = s
              G(1,1) = 1.        
           endif
           maxvo = maxv
           Aeig = matmul(matmul(G,Aeig),transpose(G))
           Geig = matmul(Geig,transpose(G))
           conv = sqrt(Aeig(1,2)**2. + Aeig(1,3)**2. + Aeig(2,3)**2.)
        enddo

C reorganize the principal values into max, min, mid

        Aeig1 = max(Aeig(1,1),Aeig(2,2),Aeig(3,3))
        Aeig2 = min(Aeig(1,1),Aeig(2,2),Aeig(3,3))

        do i=1,3
           if ((Aeig1.EQ.Aeig(i,i)).AND.(Gcount1.EQ.0.)) then
              Geig1 = Geig(:,i)
              Gcount1 = 1.
           elseif ((Aeig2.EQ.Aeig(i,i)).AND.(Gcount2.EQ.0.)) then
              Geig2 = Geig(:,i)
              Gcount2 = 1.
           else
              Aeig3 = Aeig(i,i)
              Geig3 = Geig(:,i)
           endif
        enddo

        Aeig = 0.
        Aeig(1,1) = Aeig1
        Aeig(2,2) = Aeig2
        Aeig(3,3) = Aeig3
        Geig(:,1) = Geig1
        Geig(:,2) = Geig2
        Geig(:,3) = Geig3

100     return

      end subroutine eig
