       program mpDriver

       implicit none

       integer i, Ninc

       integer  nprops, nblock, ndir, nshr
       integer  nstatev, nfieldv 
       real*8 dt,lanneal,stepTime,totalTime
       real*8 startTemp, totalStrain
       real*8 strainIncScalar, pi, s, c, theta
       real*8 Ry(3,3), strainStateTen(3,3), sigP(3,3)
       character*80 cmname

       real*8, dimension (:), allocatable ::  props
       real*8, dimension (:), allocatable ::  density
       real*8, dimension (:,:), allocatable:: coordMp
       real*8, dimension (:), allocatable:: charLength
       real*8, dimension (:,:), allocatable:: strainInc
       real*8, dimension (:,:), allocatable:: relSpinInc
       real*8, dimension (:), allocatable::  tempOld
       real*8, dimension (:,:), allocatable:: stretchOld
       real*8, dimension (:,:), allocatable:: defgradOld
       real*8, dimension (:,:), allocatable:: fieldOld
       real*8, dimension (:,:), allocatable:: stressOld
       real*8, dimension (:,:), allocatable:: stateOld
       real*8, dimension (:), allocatable:: enerInternOld
       real*8, dimension (:), allocatable:: enerInelasOld
       real*8, dimension (:), allocatable::  tempNew
       real*8, dimension (:,:), allocatable:: stretchNew
       real*8, dimension (:,:), allocatable:: defgradNew
       real*8, dimension (:,:), allocatable:: fieldNew
       real*8, dimension (:,:), allocatable:: stressNew
       real*8, dimension (:,:), allocatable:: stateNew
       real*8, dimension (:), allocatable:: enerInternNew 
       real*8, dimension (:), allocatable:: enerInelasNew
       ! variables used in code but not in vumat
       real*8, dimension (:,:), allocatable:: strainState
       real*8, dimension (:,:), allocatable:: strainOld
       real*8, dimension (:,:), allocatable:: strainNew

       real*8, dimension (:,:), allocatable:: strainStateLocal

!      nprops = User-specified number of user-defined material properties
!      nblock = Number of material points to be processed in this call to VUMAT
!      ndir = Number of direct components in a symmetric tensor
!      nshr = Number of indirect components in a symmetric tensor
!      nstatev = Number of user-defined state variables
!     nfieldv = Number of user-defined external field variables
!      lanneal = Flag indicating whether the routine is being called during an annealing process
!      stepTime = Value of time since the step began
!      totalTime = Value of total time
!      dt = Time increment size
!      cmname = User-specified material name, left justified
!      coordMp(nblock,*) Material point coordinates
!      charLength(nblock) Characteristic element length
!      props(nprops) User-supplied material properties
!      density(nblock) Current density at the material points 
!      strainInc (nblock, ndir+nshr) Strain increment tensor at each material point
!      relSpinInc (nblock, nshr) Incremental relative rotation vector
!      tempOld(nblock) Temperatures at beginning of the increment.
!      stretchOld (nblock, ndir+nshr) Stretch tensor, U
!      defgradOld (nblock,ndir+2*nshr) Deformation gradient tensor
!      fieldOld (nblock, nfieldv) Values of the user-defined field variables beginning of the increment
!      stressOld (nblock, ndir+nshr) Stress tensor at each material point at the beginning of the increment
!      stateOld (nblock, nstatev) State variables at each material point at the beginning of the increment.
!      enerInternOld (nblock) Internal energy per unit mass at each material point at the beginning of the increment.
!      enerInelasOld (nblock) Dissipated inelastic energy per unit mass at each material point at the beginning of the increment.
!      tempNew(nblock) Temperatures at each material point at the end of the increment.
!      stretchNew (nblock, ndir+nshr) Stretch tensor,U , at each material point at the end of the increment
!      defgradNew (nblock,ndir+2*nshr) tion gradient tensor  at the end of the increment
!      fieldNew (nblock, nfieldv) Values of the user-defined field variables  end of the increment.

!      Integer Inputs
       nblock = 1
       ndir = 3
       nshr = 3
       nstatev = 10
       nfieldv = 1
       nprops = 24
       lanneal = 0

! !     Dimension Reals
       allocate (props(nprops))
       allocate (density(nblock)) 
       allocate (coordMp(nblock,ndir))
       allocate (charLength(nblock))
       allocate (strainInc(nblock,ndir+nshr))
       allocate (relSpinInc(nblock,nshr))
       allocate (tempOld(nblock))
       allocate (stretchOld(nblock,ndir+nshr))
       allocate (defgradOld(nblock,ndir+nshr+nshr))
       allocate (fieldOld(nblock,nfieldv)) 
       allocate (stressOld(nblock,ndir+nshr))
       allocate (stateOld(nblock,nstatev))
       allocate (enerInternOld(nblock))
       allocate (enerInelasOld(nblock))
       allocate (tempNew(nblock))
       allocate (stretchNew(nblock,ndir+nshr))
       allocate (defgradNew(nblock,ndir+nshr+nshr))
       allocate (fieldNew(nblock,nfieldv))
       allocate (stressNew(nblock,ndir+nshr)) 
       allocate (stateNew(nblock,nstatev))
       allocate (enerInternNew(nblock))
       allocate (enerInelasNew(nblock))
       ! variables used in code but not in vumat
       allocate (strainState(nblock,ndir+nshr))
       allocate (strainOld(nblock,ndir+nshr))
       allocate (strainNew(nblock,ndir+nshr))

       allocate (strainStateLocal(nblock,ndir+nshr))

       !User Inputs
       open(unit=2,file="results.dat",recl=204)
       totalStrain = 0.1D0
       dt = 0.001D0
       Ninc = 10000
       charLength(nblock) = 1.D0 
       cmname = "Material-1"
       coordMP(nblock,1:ndir) = (/0.0D0,0.0D0,0.0D0  /)
       ! E,v, sy0, e0, nflow,alpha,kexp,cys,cpre
       ! alpha1, alpha2,beta1,beta2,beta3, gamma1,gamma2,gamma3,tol
       !c1, c2,fini, n,fult,spall
       !! similar to CA simulations Isotropic
       !props(1:nprops) = (/ 75000.,    0.33,     260., 0.004,     0.1,    0.0, 1.0,    0.0, & 
       !0.667, 1.00,    1.00,  -0.5, -0.5, -0.5,  1.5, 1.5, &
       !1.5,   1.0e-6,    0.1,   0.5,   0.005,     10.,	0.5, 10000./)
       !! similar to CA simulations
       !props(1:nprops) = (/ 75000.,    0.33,    260., 0.004,     0.1, 	  0.1, 1.0,    0.85, &	
       !0.639,     1.103,    1.12,  -0.492,  -0.509,  -0.612,  1.5,     1.5, &
       !0.73,    1.0e-6,    0.1,   0.5,   0.005,     10.,	0.5, 10000./)
       !! old params
       !props(1:nprops) = (/ 73312.1,    0.33,    260., 0.00355,     0.1, 	  0.1, 1.0,    0.85, &	
       !0.639,     1.103,    1.12,  -0.492,  -0.509,  -0.612,  1.5,     1.5, &
       !0.73,    1.0e-6,    1.29,   0.982,   0.005,     10.,	0.8, 30000./)
       ! props for Ti64 CP simulation
       !props(1:nprops) = (/ 73312.1,    0.33,    530., 0.00056928,     0.16661238, 	  0.1, 1.0,    0.85, &	
       !0.639,     1.103,    1.12,  -0.492,  -0.509,  -0.612,  1.5,     1.5, &
       !0.73,    1.0e-6,    1.29,   0.982,   0.01,     2.,	0.8, 30000./)

       !! old props for Ti64 CP simulation with alpha, beta, gamma
       !props(1:nprops) = (/ 73312.1,    0.33,    530., 0.00056928,     0.16661238, 	  0.1, 1.0,    0.85, &	
       !1.0,  2.1333,    1.5333, -0.8000,   -0.2000,   -1.3333,  1.5,     1.5, &
 !-6.4222,    1.0e-6,    1.29,   0.982,   0.0001,     2.,	0.8, 30000./)

       !! new props for Ti64 CP simulation with alpha, beta, gamma
       !props(1:nprops) = (/ 73312.1,    0.33,    530., 0.00056928,     0.16661238, 	  0.1, 1.0,    0.85, &	
       !0.77,  2.0593,    1.4674, -0.7959,   -0.2041,   -1.2634,  1.5,     1.5, &
 !-4.2371,    1.0e-6,    1.29,   0.982,   1.0e-12,     2.,	0.8, 30000./)

       !! play with e0,Cpre,n Ti64 CP simulation with alpha, beta, gamma
       !props(1:nprops) = (/ 73312.1,    0.33,    530., 0.0004,     0.15, 	  0.1, 1.0,    0.85, &	
       !0.77,  2.0593,    1.4674, -0.7959,   -0.2041,   -1.2634,  1.5,     1.5, &
 !-4.2371,    1.0e-6,    1.29,   0.982,   1.0e-12,     2.,	0.8, 30000./)

       !! play with kexp Ti64 CP simulation with alpha, beta, gamma
       props(1:nprops) = (/ 73312.1,    0.33,    530., 0.0004,     0.15, 	  0.1, 2.0,    0.85, &	
       0.667,  1.0,    1.0, -0.5,   -0.5,   -0.5,  1.5,     1.5, &
 1.5,    1.0e-6,    1.29,   0.982,   1.0e-12,     2.,	0.8, 30000./)

       density(nblock) = 0.000277D0
       ! abaqus stress convention: sig: 11, 22,33, 12, 23, 31
       ! x
       !strainState(1,1:ndir+nshr) = (/1.D0, -0.5D0, -0.5D0, 0.D0, 0.D0, 0.D0/)
       !strainState(1,1:ndir+nshr) = (/1.D0, -0.35D0, -0.35D0, 0.D0, 0.D0, 0.D0/)
       !strainState(1,1:ndir+nshr) = (/1.D0, -0.306D0, -0.6705D0, 0.D0, 0.D0, 0.D0/)
       ! y
       !strainState(1,1:ndir+nshr) = (/ -0.5D0, 1.D0, -0.5D0, 0.D0, 0.D0, 0.D0/)
       !strainState(1,1:ndir+nshr) = (/ -0.35D0, 1.D0, -0.35D0, 0.D0, 0.D0, 0.D0/)
       ! z
       !strainState(1,1:ndir+nshr) = (/-0.5D0, -0.5D0, 1.D0, 0.D0, 0.D0, 0.D0/)
       !strainState(1,1:ndir+nshr) = (/-0.35D0, -0.35D0, 1.D0, 0.D0, 0.D0, 0.D0/)
       !strainState(1,1:ndir+nshr) = (/-0.349D0, -0.540077D0, 1.D0, 0.D0, 0.D0, 0.D0/)
       !strainState(1,1:ndir+nshr) = (/1.D0, -0.0D0, -0.0D0, 0.D0, 0.D0, 0.D0/)
       ! 45 deg between x and z
       PI=4.D0*DATAN(1.D0)
       theta = 45.D0*PI/180.D0
       c = cos(theta)
       s = sin(theta)
       ! simple rotation about y for [1,0,0,0,0,0] strain state
       ! old strain state with possible sign error
       !strainState(1,1:ndir+nshr) = (/c**2.D0, -0.0D0, -s**2.D0, 0.D0, 0.D0, -s*c/)
       ! new strain state with correct sign
       strainState(1,1:ndir+nshr) = (/c**2.D0, -0.0D0, s**2.D0, 0.D0, 0.D0, -s*c/)
       strainStateLocal(1,1:ndir+nshr) = (/1.D0, 0.0D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       ! local strain state  0 degrees
       !strainState(1,1:ndir+nshr) = (/1.D0, -0.306D0, -0.6705D0, 0.D0, 0.D0, 0.D0/)
       ! local strain state 90 degrees
       !strainState(1,1:ndir+nshr) = (/1.D0, -0.540077D0, -0.349D0, 0.D0, 0.D0, 0.D0/)
       ! local strain state 45 degrees
       strainStateLocal(1,1:ndir+nshr) = (/1.D0, -1.0535849056D0, 0.31471698113D0, 0.D0, 0.D0, 0.D0/)
       !https://en.wikipedia.org/wiki/Rotation_matrix
       Ry(1,1) = c
       Ry(1,2) = 0.D0
       Ry(1,3) = s
       Ry(2,1) = 0.D0
       Ry(2,2) = 1.D0
       Ry(2,3) = 0.D0
       Ry(3,1) = -s
       Ry(3,2) = 0.D0
       Ry(3,3) = c

       strainStateTen(1:3,1:3) = 0.D0
       strainStateTen(1,1) = strainStateLocal(1,1)
       strainStateTen(2,2) = strainStateLocal(1,2)
       strainStateTen(3,3) = strainStateLocal(1,3)

       sigP = matmul(matmul(Ry,strainStateTen),transpose(Ry))
       strainState(1,1) = sigP(1,1)
       strainState(1,2) = sigP(2,2)
       strainState(1,3) = sigP(3,3)
       strainState(1,4) = sigP(1,2)
       strainState(1,5) = sigP(2,3)
       strainState(1,6) = sigP(3,1)

       print *, strainState

       !Calculated values
       strainIncScalar = totalStrain/Ninc
       strainInc = strainIncScalar*strainState
      
       !intialize variables
       totalTime = 0.D0
       startTemp = 300.D0
       tempOld(nblock) = startTemp
       stressOld(nblock,1:ndir+nshr) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       stateOld(nblock,1:nstatev) = (/0.D0/) 
       enerInternOld(nblock) = 0.D0
       enerInelasOld(nblock) = 0.D0

       strainOld(nblock,1:ndir+nshr) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)

       ! dummy values
       relSpinInc(nblock,1:nshr) = (/0.D0, 0.D0, 0.D0/)
       stretchOld(nblock,1:ndir+nshr) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       defgradOld(nblock,1:ndir+nshr+nshr)= (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0 ,0.D0, 0.D0, 0.D0/)
       fieldOld(nblock,1:nfieldv) = (/0.D0/)

       tempNew(nblock) = tempOld(nblock)
       stretchNew(nblock,1:ndir+nshr) = stretchOld(nblock,1:ndir+nshr)
       defgradNew(nblock,1:ndir+nshr+nshr) = defgradOld(nblock,1:ndir+nshr+nshr)
       fieldNew(nblock,1:nfieldv) = fieldOld(nblock,1:nfieldv)

       ! start strain increment loop
       do i = 1, Ninc
          call vumat( &
          !Read only (unmodifiable) variables -
          nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,&
          stepTime, totalTime, dt, cmname, coordMp, charLength, &
          props, density, strainInc, relSpinInc,                &
          tempOld, stretchOld, defgradOld, fieldOld,            &
          stressOld, stateOld, enerInternOld, enerInelasOld,    &
          tempNew, stretchNew, defgradNew, fieldNew,            &
          ! Write only (modifiable) variables -
          stressNew, stateNew, enerInternNew, enerInelasNew )  

          ! time update
          totalTime = totalTime + dt
          ! update strain
          strainNew(nblock,1:ndir+nshr) = strainOld(nblock,1:ndir+nshr) + strainInc(nblock,1:ndir+nshr)
          strainOld(nblock,1:ndir+nshr) = strainNew(nblock,1:ndir+nshr)
          
          write(2,"(E,$)") totalTime
          write(2,"(E,$)") strainNew(1,1), strainNew(1,2), strainNew(1,3)
          write(2,"(E,$)") strainNew(1,4), strainNew(1,5), strainNew(1,6)
          write(2,"(E,$)") stressNew(1,1), stressNew(1,2), stressNew(1,3)
          write(2,"(E,$)") stressNew(1,4), stressNew(1,5), stressNew(1,6)
          write(2,"(E,$)") stateNew(1,1)
          write(2,"(E)"  )

          !print *, stateNew(nblock,8)
          ! changed in vumat
          stressOld(nblock,1:ndir+nshr) = stressNew(nblock,1:ndir+nshr)
          stateOld(nblock,1:nstatev) = stateNew(nblock,1:nstatev)
          enerInternOld(nblock) = enerInternNew(nblock) 
          enerInelasOld(nblock) =  enerInelasNew(nblock)

       end do ! end strain increment loop  
  
       close(2)

       end program mpDriver



