#!/usr/local/bin/python3

import numpy as np
import matplotlib.pyplot as plt

#resultsFile = 'results.dat'

##resultsFileNames = ['resultsX.dat', 'resultsY.dat', 'resultsZ.dat']
#resultsFileNames = ['resultsX.dat', 'results45.dat', 'resultsZ.dat']
##resultsFileNames = ['resultsX.dat', 'resultsY.dat', 'resultsZ.dat','results45.dat']
resultsFileNames = ['results.dat']
#colors = ['r','m','g','k']
colors = ['r','k','g']
Cpre = 1.0
##### load data
resultsFile = 'testElem64kTi64-S-Mises.txt'
sigeAnisoX = np.loadtxt(resultsFile)
#sigeAnisoX = np.insert(sigeAnisoX,0,0.0)

resultsFile = 'testElem64kTi64-SDV71.txt'
epsbAnisoX = np.loadtxt(resultsFile)
#epsbAnisoX = np.insert(epsbAnisoX,0,0.0)

resultsFile = 'testElem64kTi64-Z-S-Mises.txt'
sigeAnisoZ = np.loadtxt(resultsFile)
#sigeAnisoZ = np.insert(sigeAnisoZ,0,0.0)

resultsFile = 'testElem64kTi64-Z-SDV71.txt'
epsbAnisoZ = np.loadtxt(resultsFile)
#epsbAnisoZ = np.insert(epsbAnisoZ,0,0.0)

resultsFile = 'testElem64kTi64-45-S-Mises.txt'
sigeAnisoN45 = np.loadtxt(resultsFile)
#sigeAnisoN45 = np.insert(sigeAnisoN45,0,0.0)

resultsFile = 'testElem64kTi64-45-SDV71.txt'
epsbAnisoN45 = np.loadtxt(resultsFile)
#epsbAnisoN45 = np.insert(epsbAnisoN45,0,0.0)

plt.rcParams.update({'font.size': 16})
plt.plot(epsbAnisoX,sigeAnisoX,'r-')
plt.plot(epsbAnisoN45,sigeAnisoN45,'k-')
plt.plot(epsbAnisoZ,sigeAnisoZ,'g-')

for k in range(len(resultsFileNames)):
    resultsFile = resultsFileNames[k]

    data = np.loadtxt(resultsFile)

    time = data[:,0]

    strainV = data[:,1:7]
    stressV = data[:,7:13]

    strain11 = data[:,1]
    stress11 = data[:,7]
    
    sige = np.zeros(len(time))
    #eqps = np.zeros(len(time))
    eqps = data[:,-1]
    for i in range(len(time)):
        stressTensor = np.zeros((3,3))
        stressTensor[0,0] = stressV[i,0]# 11
        stressTensor[1,1] = stressV[i,1]# 22
        stressTensor[2,2] = stressV[i,2]# 33
        stressTensor[0,1] = stressV[i,3]# 12
        stressTensor[0,2] = stressV[i,4]# 13
        stressTensor[1,2] = stressV[i,5]# 23
        stressTensor[1,0] = stressTensor[0,1]
        stressTensor[1,2] = stressTensor[2,1]
        stressTensor[0,2] = stressTensor[2,0]

        strainTensor = np.zeros((3,3))
        strainTensor[0,0] = strainV[i,0]# 11
        strainTensor[1,1] = strainV[i,1]# 22
        strainTensor[2,2] = strainV[i,2]# 33
        strainTensor[0,1] = 0.5*strainV[i,3]# 12
        strainTensor[0,2] = 0.5*strainV[i,4]# 13
        strainTensor[1,2] = 0.5*strainV[i,5]# 23
        strainTensor[1,0] = strainTensor[0,1]
        strainTensor[1,2] = strainTensor[2,1]
        strainTensor[0,2] = strainTensor[2,0]

        # Hydrostatic stress
        p = (1./3.)*(stressV[i,0] + stressV[i,1] + stressV[i,2])
        
        # Deviatoric Stress
        s = stressTensor
        s[0,0] = stressTensor[0,0] - p
        s[1,1] = stressTensor[1,1] - p
        s[2,2] = stressTensor[2,2] - p

        # Hydrostatic stress
        veps = (1./3.)*(strainV[i,0] + strainV[i,1] + strainV[i,2])
        
        # Deviatoric Stress
        epsd = strainTensor
        epsd[0,0] = strainTensor[0,0] - veps
        epsd[1,1] = strainTensor[1,1] - veps
        epsd[2,2] = strainTensor[2,2] - veps

        # Equivalent Stress and Strain
        sige[i] = np.sqrt((3./2.)*np.tensordot(s,s,axes=2))
        #eqps[i] = np.sqrt((2./3.)*np.tensordot(strainTensor,strainTensor,axes=2))
        #eqps[i] = np.sqrt((2./3.)*np.tensordot(epsd,epsd,axes=2))
    plt.rcParams.update({'font.size': 16})
    plt.plot(eqps,Cpre*sige,colors[k])
    #plt.plot(eqps,stressV[:,1])
plt.ylim([-48., 980.])
plt.xlim([0., 0.021])
plt.rcParams.update({'font.size': 16})
plt.xlabel(' Equivalent Strain')
plt.ylabel('von Mises Stress, MPa')
plt.gcf().subplots_adjust(left=0.15)
plt.gcf().subplots_adjust(bottom=0.15)
if len(resultsFileNames) > 1:
    #plt.legend(['x','y','z'])
    plt.legend(['0',r'$45^o$','90'])
    #plt.legend(['x','y','z',r'$45^o$'])
plt.show()
