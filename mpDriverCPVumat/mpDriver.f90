       program mpDriver

       implicit none

       integer i, Ninc

       integer  nprops, nblock, ndir, nshr
       integer  nstatev, nfieldv 
       real*8 dt,lanneal,stepTime,totalTime
       real*8 startTemp, totalStrain
       real*8 strainIncScalar
       character*80 cmname

       real*8, dimension (:), allocatable ::  props
       real*8, dimension (:), allocatable ::  density
       real*8, dimension (:,:), allocatable:: coordMp
       real*8, dimension (:), allocatable:: charLength
       real*8, dimension (:,:), allocatable:: strainInc
       real*8, dimension (:,:), allocatable:: relSpinInc
       real*8, dimension (:), allocatable::  tempOld
       real*8, dimension (:,:), allocatable:: stretchOld
       real*8, dimension (:,:), allocatable:: defgradOld
       real*8, dimension (:,:), allocatable:: fieldOld
       real*8, dimension (:,:), allocatable:: stressOld
       real*8, dimension (:,:), allocatable:: stateOld
       real*8, dimension (:), allocatable:: enerInternOld
       real*8, dimension (:), allocatable:: enerInelasOld
       real*8, dimension (:), allocatable::  tempNew
       real*8, dimension (:,:), allocatable:: stretchNew
       real*8, dimension (:,:), allocatable:: defgradNew
       real*8, dimension (:,:), allocatable:: fieldNew
       real*8, dimension (:,:), allocatable:: stressNew
       real*8, dimension (:,:), allocatable:: stateNew
       real*8, dimension (:), allocatable:: enerInternNew 
       real*8, dimension (:), allocatable:: enerInelasNew
       ! variables used in code but not in vumat
       real*8, dimension (:,:), allocatable:: strainState
       real*8, dimension (:,:), allocatable:: strainOld
       real*8, dimension (:,:), allocatable:: strainNew

       real*8 dfgrd0(3,3), dfgrd1(3,3)

!      nprops = User-specified number of user-defined material properties
!      nblock = Number of material points to be processed in this call to VUMAT
!      ndir = Number of direct components in a symmetric tensor
!      nshr = Number of indirect components in a symmetric tensor
!      nstatev = Number of user-defined state variables
!     nfieldv = Number of user-defined external field variables
!      lanneal = Flag indicating whether the routine is being called during an annealing process
!      stepTime = Value of time since the step began
!      totalTime = Value of total time
!      dt = Time increment size
!      cmname = User-specified material name, left justified
!      coordMp(nblock,*) Material point coordinates
!      charLength(nblock) Characteristic element length
!      props(nprops) User-supplied material properties
!      density(nblock) Current density at the material points 
!      strainInc (nblock, ndir+nshr) Strain increment tensor at each material point
!      relSpinInc (nblock, nshr) Incremental relative rotation vector
!      tempOld(nblock) Temperatures at beginning of the increment.
!      stretchOld (nblock, ndir+nshr) Stretch tensor, U
!      defgradOld (nblock,ndir+2*nshr) Deformation gradient tensor
!      fieldOld (nblock, nfieldv) Values of the user-defined field variables beginning of the increment
!      stressOld (nblock, ndir+nshr) Stress tensor at each material point at the beginning of the increment
!      stateOld (nblock, nstatev) State variables at each material point at the beginning of the increment.
!      enerInternOld (nblock) Internal energy per unit mass at each material point at the beginning of the increment.
!      enerInelasOld (nblock) Dissipated inelastic energy per unit mass at each material point at the beginning of the increment.
!      tempNew(nblock) Temperatures at each material point at the end of the increment.
!      stretchNew (nblock, ndir+nshr) Stretch tensor,U , at each material point at the end of the increment
!      defgradNew (nblock,ndir+2*nshr) tion gradient tensor  at the end of the increment
!      fieldNew (nblock, nfieldv) Values of the user-defined field variables  end of the increment.

!      Integer Inputs
       nblock = 1
       ndir = 3
       nshr = 3
       nstatev = 200
       nfieldv = 1
       nprops = 22
       lanneal = 0

! !     Dimension Reals
       allocate (props(nprops))
       allocate (density(nblock)) 
       allocate (coordMp(nblock,ndir))
       allocate (charLength(nblock))
       allocate (strainInc(nblock,ndir+nshr))
       allocate (relSpinInc(nblock,nshr))
       allocate (tempOld(nblock))
       allocate (stretchOld(nblock,ndir+nshr))
       allocate (defgradOld(nblock,ndir+nshr+nshr))
       allocate (fieldOld(nblock,nfieldv)) 
       allocate (stressOld(nblock,ndir+nshr))
       allocate (stateOld(nblock,nstatev))
       allocate (enerInternOld(nblock))
       allocate (enerInelasOld(nblock))
       allocate (tempNew(nblock))
       allocate (stretchNew(nblock,ndir+nshr))
       allocate (defgradNew(nblock,ndir+nshr+nshr))
       allocate (fieldNew(nblock,nfieldv))
       allocate (stressNew(nblock,ndir+nshr)) 
       allocate (stateNew(nblock,nstatev))
       allocate (enerInternNew(nblock))
       allocate (enerInelasNew(nblock))
       ! variables used in code but not in vumat
       allocate (strainState(nblock,ndir+nshr))
       allocate (strainOld(nblock,ndir+nshr))
       allocate (strainNew(nblock,ndir+nshr))

       !User Inputs
       !Ifort
       !open(unit=2,file="results.dat",recl=204)
       ! gfortran
       open(unit=2,file="results.dat")
       totalStrain = 0.15D0
       dt = 1D-5
       Ninc = 100
       charLength(nblock) = 1.D0 
       cmname = "Material-1"
       coordMP(nblock,1:ndir) = (/0.0D0,0.0D0,0.0D0  /)
       props(1:nprops) = (/80869.,40356., &
            20257., 0.002, 10., 320. ,0. ,0., 0.,1.0, &
            0.0, 500.,0.0, 0., 2., 45. ,45. ,45., &
            3933000., 1.5, 2.0, 0.000277  /)
       density(nblock) = 0.000277D0
       ! abaqus stress convention: sig: 11, 22,33, 12, 23, 31
       !strainState(1,1:ndir+nshr) = (/1.D0, -0.0D0, -0.0D0, 0.D0, 0.D0, 0.D0/)
       !strainState(1,1:ndir+nshr) = (/1.D0, -0.5D0, -0.5D0, 0.D0, 0.D0, 0.D0/)
       strainState(1,1:ndir+nshr) = (/1.D0, -0.4D0, -0.4D0, 0.D0, 0.D0, 0.D0/)
       strainState(1,1:ndir+nshr) = (/-1.D0, 0.4D0, 0.4D0, 0.D0, 0.D0, 0.D0/)
       !strainState(1,1:ndir+nshr) = (/1.D0, -0.33D0, -0.93D0, 0.D0, 0.D0, 0.D0/)
       !Calculated values
       strainIncScalar = totalStrain/Ninc
       strainInc = strainIncScalar*strainState
      
       !intialize variables
       totalTime = 0.D0
       startTemp = 300.D0
       tempOld(nblock) = startTemp
       stressOld(nblock,1:ndir+nshr) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       stateOld(nblock,1:nstatev) = (/0.D0/) 
       enerInternOld(nblock) = 0.D0
       enerInelasOld(nblock) = 0.D0

       strainOld(nblock,1:ndir+nshr) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)

       ! dummy values
       relSpinInc(nblock,1:nshr) = (/0.D0, 0.D0, 0.D0/)
       stretchOld(nblock,1:ndir+nshr) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)

       fieldOld(nblock,1:nfieldv) = (/0.D0/)

       tempNew(nblock) = tempOld(nblock)
       stretchNew(nblock,1:ndir+nshr) = stretchOld(nblock,1:ndir+nshr)
       fieldNew(nblock,1:nfieldv) = fieldOld(nblock,1:nfieldv)

       ! start strain increment loop
       do i = 1, Ninc

          ! get F0 tensor
          call strain2F(strainOld(nblock,:),dfgrd0)
          call nonSym2Voigt(defgradOld(nblock,:),dfgrd0)

          !update strain
          strainNew(nblock,1:ndir+nshr) = strainOld(nblock,1:ndir+nshr) + strainInc(nblock,1:ndir+nshr)

          ! get F1 tensor
          call strain2F(strainNew(nblock,:),dfgrd1)
          call nonSym2Voigt(defgradNew(nblock,:),dfgrd1)

          call vumat( &
          !Read only (unmodifiable) variables -
          nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,&
          stepTime, totalTime, dt, cmname, coordMp, charLength, &
          props, density, strainInc, relSpinInc,                &
          tempOld, stretchOld, defgradOld, fieldOld,            &
          stressOld, stateOld, enerInternOld, enerInelasOld,    &
          tempNew, stretchNew, defgradNew, fieldNew,            &
          ! Write only (modifiable) variables -
          stressNew, stateNew, enerInternNew, enerInelasNew )  

          ! time update
          totalTime = totalTime + dt
          ! update strain  variable

          strainOld(nblock,1:ndir+nshr) = strainNew(nblock,1:ndir+nshr)
          
          ! Ifotr
          write(2,"(E,$)") totalTime
          write(2,"(E,$)") strainNew(1,1), strainNew(1,2), strainNew(1,3)
          write(2,"(E,$)") strainNew(1,4), strainNew(1,5), strainNew(1,6)
          write(2,"(E,$)") stressNew(1,1), stressNew(1,2), stressNew(1,3)
          write(2,"(E,$)") stressNew(1,4), stressNew(1,5), stressNew(1,6)
          write(2,"(E)"  )

          ! Gfortran
          !write(2,*) totalTime, strainNew, stressNew


          ! changed in vumat
          stressOld(nblock,1:ndir+nshr) = stressNew(nblock,1:ndir+nshr)
          stateOld(nblock,1:nstatev) = stateNew(nblock,1:nstatev)
          enerInternOld(nblock) = enerInternNew(nblock) 
          enerInelasOld(nblock) =  enerInelasNew(nblock)

       end do ! end strain increment loop  
  
       close(2)

       end program mpDriver


       !-----------------------------------------------------------------
       ! convert deformation gradient F to train Green-Lagrange Strain E
       !-----------------------------------------------------------------
       subroutine f2Strain(dfgrd,strain)
         implicit none

         integer j,k
         real*8 dfgrd(3,3), strain(6), strainMat(3,3)
         real*8 Cmat(3,3)

         ! right Cauchy green tensor, C
         Cmat = matmul(transpose(dfgrd),dfgrd)

         ! Green-Lagrance strain
         do j = 1,3
            do k = 1,3
               if (j.eq.k) then
                  strainMat(j,k) = 0.5*(Cmat(j,k) - 1.D0)
               else
                  strainMat(j,k) = 0.5*Cmat(j,k)
               endif
            end do
         end do

         ! turn in back into Voigt notation
         call tensor2voigt(strainMat, strain)

       end subroutine f2Strain

       !-----------------------------------------------------------------
       !----Approx. Deformation tensor F from Green-Lagrange Strain -----
       !----I think its just an approximatation, but it might be exaxt --
       !-----------------------------------------------------------------
       subroutine strain2F(strain,dfgrd)

       implicit none

       integer j,k
       real*8 strainMat(3,3), Cmat(3,3), Ceig(3,3), CVeig(3,3)
       real*8 sqrtCeig(3,3)
       real*8 strain(6), dfgrd(3,3)

       ! convert stain in Voigt notation to tensor
          call voigt2tensor(strain,strainMat)
          
       ! calculate right Cauchy Green Tensor
          do j = 1,3
             do k = 1,3
                if (j.eq.k) then
                   Cmat(j,k) = 2.D0*strainMat(j,k) + 1.D0
                   else
                   Cmat(j,k) = 2.D0*strainMat(j,k)
                endif
             end do
          end do

          ! Cacluate the eigenvalues Ceig and Vector CVeig of C tensor
          call eig(Cmat,1.D-9,Ceig,CVeig)

          ! take sqrt of diagonal eigenvalues, this is the F approximation
          do j = 1,3
             do k = 1,3
                sqrtCeig(j,k) = sqrt(Ceig(j,k))
             end do
          end do

          ! use eigen vectors to get back to full F tensor
          dfgrd = matmul(matmul(CVeig, sqrtCeig),transpose(CVeig) )

       end subroutine strain2F


       !-----------------------------------------------------------------
       !--------------- turn voigt vector to tensor matrix  ------------
       !--- only works for kinematric tensors (e.g.,, strain, strainrate)
       !-----------------------------------------------------------------
      subroutine voigt2Tensor(Avec,Amat)

        implicit none

        real*8 Avec(6), Amat(3,3)

        ! assuming kinematic tensor
        Amat(1,1) = Avec(1)
        Amat(2,2) = Avec(2)
        Amat(3,3) = Avec(3)
        Amat(1,2) = 0.5D0*Avec(4)
        Amat(2,1) = Amat(1,2)
        Amat(1,3) = 0.5D0*Avec(5)
        Amat(3,1) = Amat(1,3)
        Amat(2,3) = 0.5D0*Avec(6)
        Amat(3,2) = Amat(2,3)

      end subroutine voigt2Tensor


       !-----------------------------------------------------------------
       !--------------- turn tensor matrixx to voigt vector ------------
       !--- only works for kinematric tensors (e.g.,, strain, strainrate)
       !-----------------------------------------------------------------
      subroutine tensor2Voigt(Amat,Avec)

        implicit none

        real*8 Avec(6), Amat(3,3)

        ! assuming kinematic tensor
        Avec(1) = Amat(1,1)
        Avec(2) = Amat(2,2)
        Avec(3) = Amat(3,3)
        Avec(4) = 2.D0*Amat(1,2)
        Avec(5) = 2.D0*Amat(1,3)
        Avec(6) = 2.D0*Amat(2,3)


      end subroutine tensor2Voigt


! Determine eigenvalues, Aeig,  and eigenvectors, Geig, of the matrix A
! to within a tolerance, tol using Jacobi iteration method
!
! to reproduce the orginal matrix:
!
!                A =  matmul(matmul(Geig,Aeig),transpose(Geig))
!
!      Programmed by Jacob Smith, Apple Austin TX (as of 2021), circa 2013
      subroutine eig(A,tol,Aeig,Geig)


        implicit none

        integer i

        real*8 conv,A(3,3),Aeig(3,3),G(3,3),Geig(3,3),tol,maxv,tau,t,c,s
        real*8 Aeig1,Aeig2,Aeig3,Geig1(3),Geig2(3),Geig3(3),Gcount1
        real*8 Gcount2,maxvo
        integer counter

        Gcount1 = 0.
        Gcount2 = 0.

        conv = sqrt(A(1,2)**2. + A(1,3)**2. + A(2,3)**2.)
        Aeig = A
        Geig = 0.
        do i=1,3
           Geig(i,i) = 1.
        enddo
 
        maxvo = 1e25
        counter = 0
        do while(conv.GT.tol)
           counter = counter + 1
           if (counter.GE.50) then
              print *,'eigenvalue minimization failed',conv
              goto 100
           endif
           G = 0.
           maxv = max(abs(Aeig(1,2)),abs(Aeig(1,3)),abs(Aeig(2,3)))
           if (maxv.EQ.abs(Aeig(1,2))) then
              tau = (Aeig(1,1) - Aeig(2,2))/(2.*Aeig(1,2))
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)
              G(1,1) = c
              G(2,2) = c
              G(1,2) = -s
              G(2,1) = s
              G(3,3) = 1.
           elseif (maxv.EQ.abs(Aeig(1,3))) then
              tau = (Aeig(1,1) - Aeig(3,3))/(2.*Aeig(1,3))       
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)        
              G(1,1) = c
              G(3,3) = c
              G(1,3) = -s
              G(3,1) = s
              G(2,2) = 1.        
           else
              tau = (Aeig(2,2) - Aeig(3,3))/(2.*Aeig(2,3))
              if (maxvo.EQ.maxv) then
                 t = tau + sqrt(1.+tau**2.)
              else
                 t = min(tau+sqrt(1.+tau**2.),tau-sqrt(1.+tau**2.))
              endif
              c = 1./sqrt(1. + t**2.)
              s = t/sqrt(1. + t**2.)        
              G(2,2) = c
              G(3,3) = c
              G(2,3) = -s
              G(3,2) = s
              G(1,1) = 1.        
           endif
           maxvo = maxv
           Aeig = matmul(matmul(G,Aeig),transpose(G))
           Geig = matmul(Geig,transpose(G))
           conv = sqrt(Aeig(1,2)**2. + Aeig(1,3)**2. + Aeig(2,3)**2.)
        enddo

! reorganize the principal values into max, min, mid

        Aeig1 = max(Aeig(1,1),Aeig(2,2),Aeig(3,3))
        Aeig2 = min(Aeig(1,1),Aeig(2,2),Aeig(3,3))

        do i=1,3
           if ((Aeig1.EQ.Aeig(i,i)).AND.(Gcount1.EQ.0.)) then
              Geig1 = Geig(:,i)
              Gcount1 = 1.
           elseif ((Aeig2.EQ.Aeig(i,i)).AND.(Gcount2.EQ.0.)) then
              Geig2 = Geig(:,i)
              Gcount2 = 1.
           else
              Aeig3 = Aeig(i,i)
              Geig3 = Geig(:,i)
           endif
        enddo

        Aeig = 0.
        Aeig(1,1) = Aeig1
        Aeig(2,2) = Aeig2
        Aeig(3,3) = Aeig3
        Geig(:,1) = Geig1
        Geig(:,2) = Geig2
        Geig(:,3) = Geig3

100     return

      end subroutine eig

!     ===============================================================
!     ================== convert nonsymetric voigt to tensor==============
!     ===============================================================

      subroutine voigt2NonSym(tensor,voigt)

      implicit none

      real*8 tensor(3,3)
      real*8 voigt(9)

      tensor(1,1) = voigt(1)
      tensor(2,2) = voigt(2)
      tensor(3,3) = voigt(3)
      tensor(1,2) = voigt(4)
      tensor(2,3) = voigt(5)
      tensor(3,1) = voigt(6)
      tensor(2,1) = voigt(7)
      tensor(3,2) = voigt(8)
      tensor(1,3) = voigt(9)

      end subroutine voigt2NonSym

!     ===============================================================
!     ================== convert voigt to nonsymetric tensor==============
!     ===============================================================

      subroutine nonSym2Voigt(voigt,tensor)

      implicit none

      real*8 tensor(3,3)
      real*8 voigt(9)

      voigt(1) = tensor(1,1)
      voigt(2) = tensor(2,2)
      voigt(3) = tensor(3,3)
      voigt(4) = tensor(1,2)
      voigt(5) = tensor(2,3)
      voigt(6) = tensor(3,1)
      voigt(7) = tensor(2,1)
      voigt(8) = tensor(3,2)
      voigt(9) = tensor(1,3)

      end subroutine nonSym2Voigt
