c===================================================================
c
c  This UMAT subroutine implements the following model in ABAQUS.
c
c-------------------------------------------------------------------
C******************************************************************
C This code is being modified to incorporate the effects of BCT crystal
C plasticity and for Fe-C materials. Modification start date 10/2006
C incorporate the slip effects of BCC and FCC together

      subroutine umat (stress,  statev,  ddsdde,  sse,     spd,
     &			scd,     rpl,     ddsddt,  drplde,  drpldt,
     &			strain,  dstrain, time,    dtime,   temp,
     &			dtemp,   predef,  dpred,   cmname,  ndi,
     &			nshr,    ntens,   nstatv,  props,   nprops,
     &			coords,  drot,    pnewdt,  celent,  dfgrd0,
     &			dfgrd1,  noel,    npt,     layer,   kspt,
     &			kstep,   kinc )

C      implicit double precision (a-h,o-z)
      implicit none

      real*8 stress,  statev,  ddsdde,  sse,     spd
      real*8 scd,     rpl,     ddsddt,  drplde,  drpldt
      real*8 strain,  dstrain, time,    dtime,   temp
      real*8 dtemp,   predef,  dpred,  props
      real*8 coords,  drot,    pnewdt,  celent,  dfgrd0
      real*8 dfgrd1

      integer ndi, nshr, ntens, nstatv, nprops, npt, layer
      integer kspt, kstep, kinc, noel

      integer i,j,k,m ,n,l, N_incr, ia, ib, N_ctr 
      integer max_grains, max_loops, num_slip_sys
      integer num_slip_sys1
      real*8 Pi, Tolerance, C11, C12, C44
      real*8 gamma_dot_zero, flow_exp, g_zero
      real*8 Hdir, igrn, xLatent, Adir, iopt
      real*8 mat_data, theta_ang
      real*8 y,z, a_zero, psi_ang, phi_ang, del
      real*8 sum, psi, g0,  E_p, E_p_eff
      real*8 s1, c1,  a0, F_p_inv_0, E_eff
      real*8 E_p_eff_cum, s2, c2,s3,c3
      real*8 dir_cos,C0, sig_avg, C, xs0,xm0
      real*8 g, a, F0, F1, F_el, F_el_inv
      real*8 xs, xm, array1, E_el, Spk2
      real*8 array2, sig,  tau, gamma_dot
      real*8 dTaudGd, F_p_inv, g_sat, func
      real*8 xL_p, sse_ref, t1, dgadgb, daadga
      real*8 array3, grad,d_gamma_dot, sse_old
      real*8 gamma_try, C_avg, ddpdsig, E_tot
      real*8 trace, sig_eff, array6, Array4, Array5
      real*8 delta_gamma, delta_E_p, sum1, ddsdde_4th
      real*8 sum2, array7, dlpdsd, F_dot, F_inv, sig0, xL 
C     Functions
      real*8 determinant, power, sgn
C     IS Fp, g_sat, xL_p, Array7, dlpdsd, 
C     F_dot, F_inv, sig0, xL never defined
C     What is C_avg needed
      parameter(max_grains	= 1,
     &		max_loops	= 10,
     &		Tolerance	= 0.00001,
     &        num_slip_sys	= 12,
     &        num_slip_sys1=12)

      character*8 cmname, ans
      logical Converged, Improved

c-------------------------------------------------------------------
c  Dimension arrays passed into the UMAT sub
c-------------------------------------------------------------------

      dimension
     &	coords(3),	! Coordinates of Gauss pt. being evaluated
     &	ddsdde(ntens,ntens), ! Tangent Stiffness Matrix
     &	ddsddt(ntens),	! Change in stress per change in temperature
     &	dfgrd0(3,3),	! Deformation gradient at beginning of step
     &	dfgrd1(3,3),	! Deformation gradient at end of step
     &	dpred(1),	! Change in predefined state variables
     &	drplde(ntens),	! Change in heat generation per change in strain
     &	drot(3,3),	! Rotation matrix
     &	dstrain(ntens),	! Strain increment tensor stored in vector form
     &	predef(1),	! Predefined state vars dependent on field variables
     &	props(nprops),	! Material properties passed in
     &	statev(nstatv),	! State Variables
     &	strain(ntens),	! Strain tensor stored in vector form
     &	stress(ntens),	! Cauchy stress tensor stored in vector form
     &	time(2)		! Step Time and Total Time
                                                                                
c-------------------------------------------------------------------            
c  Dimension other arrays used in this UMAT
c-------------------------------------------------------------------            

      dimension
     &	array1	(3,3),		! Dummy array
     &	array2	(3,3),		! Dummy array
     &	array3	(num_slip_sys,num_slip_sys), ! A_matrix of Newton Raphson
     &	array4	(6,6),		! Dummy array used in Voigt notation
     &	array5	(6,6),		! Inverse of array4().
     &	array6	(3,3,3,3),	! 4th rank dummy array
     &	array7	(3,3,3,3),	! Another 4th rank dummy array
     &	a0	(num_slip_sys,max_grains), ! Kinematic stress at beginning of step
     &	a	(num_slip_sys,max_grains), ! Kinematic stress at end of step
     &	C0	(3,3,3,3),	! Local  4th rank elastic stiffness tensor
     &	C	(3,3,3,3),	! Global 4th rank elastic stiffness tensor
     &	C_avg	(3,3,3,3),	! Average over all grains
     &	del	(3,3),		! Kronecker delta tensor
     &	ddpdsig	(3,3,3,3),	! deriv of D_p wrt sig * dt
     &	ddsdde_4th(3,3,3,3),	! 4th rank tangent stiffness tensor.
     &	daadga	(num_slip_sys),	! deriv of a_alpha wrt del_gamma_alpha
     &	dgadgb	(num_slip_sys,num_slip_sys),! deriv of g_alpha wrt del_gamma_beta
     &	d_gamma_dot(num_slip_sys),! Gamma_dot step from Newt-Raphson
     &	dlpdsd	(3,3,3,3),	! deriv of xL_p wrt sigma_dot
     &	dir_cos	(3,3,max_grains),! Direction cosines 
     &	dTaudGd	(num_slip_sys,num_slip_sys), ! Deriv of Tau wrt Gamma_dot
     &	E_el	(3,3),		! Elastic Green strain tensor
     &	E_tot	(3,3),		! Green strain tensor E_el + E_p
     &	F0	(3,3),		! F at beginning of sub increment
     &	F1	(3,3),		! F at end of sub increment
     &	F_el	(3,3),		! Elastic part of F
     &	F_el_inv(3,3),		! Inverse of elastic part of F
     &	F_dot	(3,3),		! F-dot
     &	F_inv	(3,3),		! Inverse of deformation gradient
     &	F_p_inv_0(3,3,max_grains),! Inverse of plastic part of F at beginning
     &	F_p_inv(3,3,max_grains),! Inverse of plastic part of F at end of step

     &	func	(num_slip_sys),	! Function that is solved to get gamma_dot(k)
     &	g0	(num_slip_sys,max_grains), ! Ref stress at beginning of step
     &	g	(num_slip_sys,max_grains), ! Ref stress at end of step
     &	gamma_dot(num_slip_sys,max_grains),! shear strain rate on system
     &	gamma_try(num_slip_sys),! Candidate gamma_dots for line search
     &	grad(num_slip_sys),	! gradient of sum-sqares-error
     &	psi	(3,max_grains),	! Euler angles
     &	sig0	(3,3),		! Stress tensor at beginning of step.
     &	sig	(3,3),		! Stress tensor
     &	sig_avg	(3,3),		! Rate of stress change for a grain
     &	Spk2	(3,3),		! 2nd Piola Kirkhoff stress
     &	tau	(num_slip_sys,max_grains),! Resolved shear stress for slip dir.
     &	xL	(3,3),		! Eulerian velocity gradient
     &	xL_p	(3,3),		! Plastic vel grad in current configuration
     &	xs0	(3,num_slip_sys),! Inter config slip directions in global coords
     &	xs	(3,num_slip_sys),! Current config slip directions in global coords
     &	xm0	(3,num_slip_sys),! Inter config plane normals in global coords
     &	xm	(3,num_slip_sys),! Current config plane normals in global coords
     &	y	(3,num_slip_sys),! Miller indices of slip plane normals 
     &	z	(3,num_slip_sys), ! Miller indices of slip directions
     &  delta_E_p(3,3),         !increment of plastic strain tensor
     &  delta_gamma(num_slip_sys), !increment of shear strain on each slip sys
     &	E_p(3,3)
      REAL*8 omega, det,dt_incr, Hdyn,Adyn
      INTEGER N_incr_total,num_grains
C     Adyn is never defined
C     Hdyn is never defined
C     why is is looping over grains
C     why is it storing 4th order C tensor
C     why is it calculating a 4th order c tensor
C     begin loop over grains needs removed
C     what is N_incr_total
C     How is F0 and F1 calculated
      Pi = 4. * atan(1.)
c-------------------------------------------------------------------
c   Determine number of grains.  Divide the total number of ISVs
c   by the number of ISVs per grain.
c   3 - Euler angles
c   9 - F_p(i,j)
c  12 - g(i) for 12 slip systems
c  12 - a(i) for 12 slip systems
c ----
c  36
c-------------------------------------------------------------------

c      num_grains = nstatv / 40
        num_grains = 1
      print *, "------------"
      print *, "stress" , stress
      print *, "statev" , statev
      print *, "ddsdde" , ddsdde
      print *, "sse "  , sse 
      print *, "spd", spd
      print *, "scd", scd
      print *, "rpl", rpl
      print *, "ddsddt", ddsddt
      print *, "drplde", drplde
      print *, "drpldt", drpldt
      print *, "strain", strain 
      print *, "dstrain", dstrain
      print *, "time" , time
      print *, "dtime", dtime
      print *, "temp", temp
      print *, "dtemp" , dtemp
      print *, "predef" , predef
      print *, "dpred" , dpred
      print *, "props" , props
      print *, "coords" , coords
      print *, "drot" , drot
      print *, "pnewdt" , pnewdt
      print *, "celent", celent
      print *, "dfgrd0", dfgrd0
      print *, "dfgrd1", dfgrd1
c-------------------------------------------------------------------
c  Assign props() array to logical variable names
C... Props(16) is the parameter to identify whether BCC or FCC
C.... Props(8) parameter for multiple elements per grain
C     Props(8)=0 single element in one grain
C     Props(8)=1 and above for multple element for one grain
C... Props(12) parameter to identify for polycrystal and single cryst anal
c    Props(12)=0 polycrystal with orientations for subroutine RANGRN
C....Props(12)=1 Polycrystal with multiple elements per grain
C....Props(12)=2 polycrystal with orientations from .inp file
C... Props(12)=3 single crystal analysis
c-------------------------------------------------------------------

      C11	= props(1)
      C12	= props(2)
      C44	= props(3)
      gamma_dot_zero= props(4)
      flow_exp	= props(5)
      g_zero	= props(6)
      Hdir	= props(7)
      Hdyn      = props(8)
      igrn	= props(9)
      xLatent	= props(10)
      a_zero    = props(11)
      Adir      = props(12)
      Adyn      = props(13)
      iopt= props(14) !!! options for different grain analysis
      mat_data=props(15) ! 1 for FCC and 2 for BCC
      psi_ang   = props(16)
      theta_ang = props(17)
      phi_ang   = props(18)
	
c... assign the number of slip systems
c      if(mat_data.eq.1)  num_slip_sys1	= 12 !!! FCC
c	if(mat_data.eq.2) num_slip_sys1	= 48 !!! BCC
c.... added for polycrystal analysis
 
c-------------------------------------------------------------------
c  Initialize Kronnecker delta tensor
c-------------------------------------------------------------------

      do i = 1,3
         do j = 1,3
            del(i,j) = 0.0
         end do
         del(i,i) = 1.0
      end do

c-------------------------------------------------------------------
c  Assign slip system normals and slip directions for an FCC.
c-------------------------------------------------------------------
c.... for NiTi system 6<100>{110} and 6 <100>{010} systems are considered 

C.... system 1 (010)[100]
      y(1,1)=0
	y(2,1)=1
	y(3,1)=0
	z(1,1)=1
	z(2,1)=0
	z(3,1)=0
C.... system 2  (100) [010]
      y(1,2)=1
	y(2,2)=0
	y(3,2)=0
	z(1,2)=0
	z(2,2)=1
	z(3,2)=0
C.... system 3 (001)[100]
      y(1,3)=0
	y(2,3)=0
	y(3,3)=1
	z(1,3)=1
	z(2,3)=0
	z(3,3)=0
C.... system 4 (100)[001]
      y(1,4)=1
	y(2,4)=0
	y(3,4)=0
	z(1,4)=0
	z(2,4)=0
	z(3,4)=1
C.... system 5 (001)[010]
      y(1,5)=0
	y(2,5)=0
	y(3,5)=1
	z(1,5)=0
	z(2,5)=1
	z(3,5)=0
C.... system 6 (010)[001]
      y(1,6)=0
	y(2,6)=1
	y(3,6)=0
	z(1,6)=0
	z(2,6)=0
	z(3,6)=1
C.... system 7 (011)[100]
      y(1,7)=0
	y(2,7)=1
	y(3,7)=1
	z(1,7)=1
	z(2,7)=0
	z(3,7)=0
C.... system 8 (110)[001]
      y(1,8)=1
	y(2,8)=1
	y(3,8)=0
	z(1,8)=0
	z(2,8)=0
	z(3,8)=1
C.... system 9 (01-1)[100]
      y(1,9)=0
	y(2,9)=1
	y(3,9)=-1
	z(1,9)=1
	z(2,9)=0
	z(3,9)=0
C.... system 10(-110)[001]
      y(1,10)=-1
	y(2,10)=1
	y(3,10)=0
	z(1,10)=0
	z(2,10)=0
	z(3,10)=1
C.... system 11(101)[010]
      y(1,11)=1
	y(2,11)=0
	y(3,11)=1
	z(1,11)=0
	z(2,11)=1
	z(3,11)=0
C.... system 12 (10-1)[010]
      y(1,12)=1
	y(2,12)=0
	y(3,12)=-1
	z(1,12)=0
	z(2,12)=1
	z(3,12)=0

c-------------------------------------------------------------------
c  Normalise the Miller indices to length one.
c-------------------------------------------------------------------

      do i = 1,num_slip_sys
         call normalize_vector( y(1,i), y(2,i), y(3,i) )
         call normalize_vector( z(1,i), z(2,i), z(3,i) )
      end do

c-------------------------------------------------------------------
c  Initialize internal variables for initial time step
c-------------------------------------------------------------------

      if (time(2) .eq. 0.0) then

c-------------------------------------------------------------------
c  Check for normality of Miller indices.
c-------------------------------------------------------------------

      do k = 1,num_slip_sys
        sum = 0.0
        do i = 1,3
          sum = sum + y(i,k) * z(i,k)
        end do
        if (abs(sum) .gt. tolerance) then
          write(7,*) 'The Miller indices are WRONG!!!'
          write(7,*) 'on slip system # ',k
          STOP
        end if
      end do

c-------------------------------------------------------------------
c  Generate Euler angles  1-3
c-------------------------------------------------------------------
c..... modification for polycrystal analysis 
c.... single element is one grain from subroutine RANGRN

        do i=1,num_grains
           psi(1,i) = psi_ang*Pi/180.0
           psi(2,i) = theta_ang*Pi/180.0
           psi(3,i) = phi_ang*Pi/180.0
        end do
c
c.... 
c-------------------------------------------------------------------
c  Initialize reference shear stress for each slip system of
c  each grain.  13-24

         do m = 1,num_grains
           do n = 1,num_slip_sys
             g0(n,m) = g_zero
           end do
         end do
            
c-------------------------------------------------------------------
c  Initialize kinematic stress for each slip system of
c  each grain.   25-36
c-------------------------------------------------------------------

         do m = 1,num_grains
           do n = 1,num_slip_sys
             a0(n,m) = a_zero
           end do
         end do
            
c-------------------------------------------------------------------
c  Initialize F_p_inv_0    4-12
c-------------------------------------------------------------------

         do m = 1,num_grains
           do i = 1,3
             do j = 1,3
               F_p_inv_0(i,j,m) = 0.0
             end do
             F_p_inv_0(i,i,m) = 1.0
           end do
         end do
c-------------------------------------------------------------------
c  Initialize E_p  37-45
c-------------------------------------------------------------------

        do i = 1,3
	    do j = 1,3
           E_p(i,j) = 0.
          end do
	end do  
c-------------------------------------------------------------------
c  Initialize E_eff   46
c-------------------------------------------------------------------

        E_eff = 0.

c-------------------------------------------------------------------
c  Initialize E_p_eff    47
c-------------------------------------------------------------------

        E_p_eff = 0.

c-------------------------------------------------------------------
c  Initialize E_p_eff_cum   48
c-------------------------------------------------------------------

        E_p_eff_cum = 0.          
c-------------------------------------------------------------------
c  End of initializations.  Read in internal variables.
c-------------------------------------------------------------------

      else  ! time<>0

         n = 0
            
c-------------------------------------------------------------------
c  Read in Euler Angles 1-3
c-------------------------------------------------------------------

         do m = 1,num_grains
           do i = 1,3
             n = n + 1
             psi(i,m) = statev(n)
           end do
         end do
            
c-------------------------------------------------------------------
c  Read inverse of the plastic part of F     4-12
c-------------------------------------------------------------------

         do m = 1,num_grains
           do j = 1,3
             do i = 1,3
               n = n + 1
               F_p_inv_0(i,j,m) = statev(n)
             end do
           end do
         end do
           
c-------------------------------------------------------------------
c  Read reference shear values       13-24
c-------------------------------------------------------------------

         do m = 1,num_grains
           do i = 1,num_slip_sys
             n = n + 1
             g0(i,m) = statev(n)
           end do
         end do

c-------------------------------------------------------------------
c  Read kinematic stress values     25-36
c-------------------------------------------------------------------

         do m = 1,num_grains
           do i = 1,num_slip_sys
             n = n + 1
             a0(i,m) = statev(n)
           end do
         end do
c-------------------------------------------------------------------
c  Read E_p        37-45
c-------------------------------------------------------------------

        do i = 1,3
         do j = 1,3
          n = n + 1
          E_p(i,j) = statev(n)
         end do
        end do
          
c--state variables 37-45
c-------------------------------------------------------------------
c  Read E_eff      46
c-------------------------------------------------------------------
        n=n+1
	  E_eff = statev(n)

c--- state variables 46
c-------------------------------------------------------------------
c  Read E_p_eff      47
c-------------------------------------------------------------------
        n=n+1
	  E_p_eff = statev(n)

c--- state variables 47
c-------------------------------------------------------------------
c  Read E_p_eff_cum   48
c-------------------------------------------------------------------
        n = n+1
        E_p_eff_cum = statev(n)  
c-------------------------------------------------------------------
c  End of initializations
c-------------------------------------------------------------------

      end if ! (time = 0)

c-------------------------------------------------------------------
c  Calculate direction cosines based on Euler angles
c-------------------------------------------------------------------
c.... using XZX rotation for the direction cosines to alinng 111 direction parallel to 010 (tube axis direction)

      do i = 1,num_grains

        s1 = sin(psi(1,i))
        c1 = cos(psi(1,i))
        s2 = sin(psi(2,i))
        c2 = cos(psi(2,i))
        s3 = sin(psi(3,i))
        c3 = cos(psi(3,i))
            
        dir_cos(1,1,i) = c2
        dir_cos(2,1,i) = c1*s2
        dir_cos(3,1,i) = s1*s2
        dir_cos(1,2,i) = -1*c3*s2
        dir_cos(2,2,i) = c1*c2*c3-s1*s3
        dir_cos(3,2,i) = c1*s3+c3*c2*s1
        dir_cos(1,3,i) = s3*s2
        dir_cos(2,3,i) = -1*c2*c1*s3-c3*s1
        dir_cos(3,3,i) = c1*c3-c2*s1*s3

      end do
      

c-------------------------------------------------------------------
c  Initialize ANISOTROPIC 4th rank elastic stiffness tensor
c-------------------------------------------------------------------

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          C0(i,j,k,l) = C12 * del(i,j) * del(k,l) +
     &		  C44 * (del(i,k)*del(j,l)+del(i,l)*del(k,j))
         end do
        end do
       end do
      end do
      C0(1,1,1,1) = C11
      C0(2,2,2,2) = C11
      C0(3,3,3,3) = C11
 
c--------------------------------------------------------------------
c  Initialize arrays for averaging over grains
c--------------------------------------------------------------------

      do i = 1,3
       do j = 1,3
        sig_avg(i,j) = 0.0
       end do
      end do

c====================================================================
c  Begin loop over grains 
c====================================================================

      do m = 1,num_grains

c--------------------------------------------------------------------
c  Rotate local anisotropic elasticity tensor to
c  global coordinates.
c--------------------------------------------------------------------

      call rotate_4th(dir_cos(1,1,m),C0,C)

c--------------------------------------------------------------------
c  Convert Miller Indices to global coordinates
c--------------------------------------------------------------------

      do n = 1,num_slip_sys
        do i = 1,3
          xs0(i,n) = 0.0
          xm0(i,n) = 0.0
          do j = 1,3
            xs0(i,n) = xs0(i,n) + dir_cos(i,j,m) * z(j,n)
            xm0(i,n) = xm0(i,n) + dir_cos(i,j,m) * y(j,n)
          end do
        end do
      end do

c--------------------------------------------------------------------
c  Initialize number of subincrements.  Note that the
c  subincrement initially equals the total increment.  This
c  remains the case unless the process starts to diverge.
c--------------------------------------------------------------------

      N_incr       = 1
      N_incr_total = 1

c====================================================================
c  Top of Subincrement Time Step Integration Loop.
c  Calculate subincrement time step size.
c====================================================================

  100 dt_incr = dtime / N_incr_total
c      print*,N_incr_total

c-------------------------------------------------------------------
c  Initialize ref shear stress
c-------------------------------------------------------------------

      do n = 1,num_slip_sys
        g(n,m) = g0(n,m)
      end do

c-------------------------------------------------------------------
c  Initialize back stress
c-------------------------------------------------------------------

      do n = 1,num_slip_sys
        a(n,m) = a0(n,m)
      end do

c--------------------------------------------------------------------
c  Initialize deformation gradients for beginning and
c  end of subincrement.
c--------------------------------------------------------------------

      do i = 1,3
        do j = 1,3
          F0(i,j) = dfgrd0(i,j) + (dfgrd1(i,j) - dfgrd0(i,j)) *
     &					(N_incr - 1) / N_incr_total
          F1(i,j) = dfgrd0(i,j) + (dfgrd1(i,j) - dfgrd0(i,j)) *
     &						N_incr / N_incr_total
        end do
      end do

c--------------------------------------------------------------------
c  Multiply F() by F_p_inv() to get F_el()
c--------------------------------------------------------------------

      call aa_dot_bb(3,F0,F_p_inv_0(1,1,m),F_el)
      call inverse_3x3(F_el,F_el_inv)

c--------------------------------------------------------------------
c  Rotate xs0 and xm0 to current coordinates, called xs and xm.
c--------------------------------------------------------------------

      do n = 1,num_slip_sys
        do i = 1,3
          xs(i,n) = 0.0
          xm(i,n) = 0.0
          do j = 1,3
            xs(i,n) = xs(i,n) + F_el(i,j) * xs0(j,n)
            xm(i,n) = xm(i,n) + xm0(j,n)  * F_el_inv(j,i)
          end do
        end do
      end do

c--------------------------------------------------------------------
c  Calculate elastic Green Strain
c--------------------------------------------------------------------

      call transpose(3,F_el,array1)
      call aa_dot_bb(3,array1,F_el,E_el)
      do i = 1,3
        E_el(i,i) = E_el(i,i) - 1
        do j = 1,3
          E_el(i,j) = E_el(i,j) / 2
        end do
      end do 
c--------------------------------------------------------------------
c  Multiply the anisotropic stiffness tensor by the Green strain 
c  to get the 2nd Piola Kirkhhoff stress
c--------------------------------------------------------------------

      call aaaa_dot_dot_bb(3,C,E_el,Spk2)

c--------------------------------------------------------------------
c  Convert from PK2 stress to Cauchy stress
c--------------------------------------------------------------------

      det = determinant(F_el)
      call transpose(3,F_el,array2)
      call aa_dot_bb(3,F_el,Spk2,array1)
      call aa_dot_bb(3,array1,array2,sig)
      do i = 1,3
       do j = 1,3
        sig(i,j) = sig(i,j) / det
       end do
      end do

c--------------------------------------------------------------------
c  Calculate resolved shear stress for each slip system.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        tau(k,m) = 0.0
        do j = 1,3
          do i = 1,3
            tau(k,m) = tau(k,m) + xs(i,k) * xm(j,k) * sig(i,j)
          end do
        end do
      end do
       
c--------------------------------------------------------------------
c  Calculate 1st estimate of gamma_dot for each slip system.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
       gamma_dot(k,m)=gamma_dot_zero*power((tau(k,m)-a(k,m))/
     &						g(k,m),flow_exp)
      end do
   
c--------------------------------------------------------------------
c  Calculate d(Tau)/d(Gamma_dot)
c--------------------------------------------------------------------

      do ia = 1,num_slip_sys
       do ib = 1,num_slip_sys
        dTaudGd(ia,ib) = 0.0

        do i = 1,3
         do j = 1,3
          array1(i,j) = xs0(i,ib) * xm0(j,ib)
         end do
        end do
        call aaaa_dot_dot_bb(3,C0,array1,array2)

        do i = 1,3
         do j = 1,3
          array1(i,j) = xs0(i,ia) * xm0(j,ia)
         end do
        end do
        call aa_dot_dot_bb(3,array1,array2,dTaudGd(ia,ib))

        dTaudGd(ia,ib) = dTaudGd(ia,ib) * dt_incr
       end do !ib
      end do !ia

c====================================================================
c  Begin Newton-Raphson iterative loops.
c====================================================================

      converged = .false.

      do while (.not.converged)

      converged = .true.
  
c--------------------------------------------------------------------
c  Calculate g, F_p_inv, F_el, sig, tau, func, sse.
c--------------------------------------------------------------------

      call eval_func(xs0,	dt_incr,	gamma_dot(1,m),	
     &		     xm0,	F1,		num_slip_sys,
     &		     C,		F_p_inv(1,1,m),	F_p_inv_0(1,1,m),
     &		     g0(1,m),	Hdir,		Hdyn,
     &		     a0(1,m),	Adir,		Adyn,
     &		     g_sat,	F_el,		flow_exp,
     &		     sig,	tau(1,m),	gamma_dot_zero,
     &		     g(1,m),	func,		xL_p,
     &		     xLatent,	sse,		a(1,m))

      sse_ref = sse

c--------------------------------------------------------------------
c  Begin calculation of the partial derivatives needed for 
c  the Newton-Raphson step!!!
c  Calculate derivative of the hardening variable, g-alpha,
c  w.r.t. gamma-dot-beta.
c--------------------------------------------------------------------

      sum = 0
      do ia = 1,num_slip_sys
        sum = sum + abs(gamma_dot(ia,m))
      end do
      do ia = 1,num_slip_sys
       do ib = 1,num_slip_sys
         t1 = xLatent
         if (ia .eq. ib) t1 = 1.0
         dgadgb(ia,ib) = (Hdir * t1 - Hdyn*g(ia,m)) * dt_incr / 
     &			(1 + Hdyn * sum * dt_incr)
         if(gamma_dot(ib,m).lt.0.0)dgadgb(ia,ib)=-dgadgb(ia,ib)
       end do
      end do

c--------------------------------------------------------------------
c  Calculate derivative of kinematic stress, a-alpha
c  w.r.t. gamma-dot-beta.
c--------------------------------------------------------------------

      do ia = 1,num_slip_sys
         daadga(ia) = (Adir - Adyn * A(ia,m) * sgn(gamma_dot(ia,m)))
     &       * dt_incr / (1 + Adyn * dt_incr * abs(gamma_dot(ia,m)))
      end do

c--------------------------------------------------------------------
c  Form "A-matrix" of derivatives wrt d_gamma_beta
c--------------------------------------------------------------------

      do ia = 1,num_slip_sys
        do ib = 1,num_slip_sys
          array3(ia,ib) = dgadgb(ia,ib) * (tau(ia,m)-a(ia,m))/g(ia,m)
        end do
        array3(ia,ia) = array3(ia,ia) + 
     &		g(ia,m) / (flow_exp * gamma_dot_zero) *
     &		abs(power((tau(ia,m)-a(ia,m))/g(ia,m),(1.-flow_exp)))
      end do

c--------------------------------------------------------------------
c  Add d(Tau)/d(Gamma_dot) to the A-matrix for the Newton-
c  Raphson iteration.
c--------------------------------------------------------------------

      do ia = 1,num_slip_sys
        do ib = 1,num_slip_sys
          array3(ia,ib) = array3(ia,ib) + dTaudGd(ia,ib)
        end do
      end do

c--------------------------------------------------------------------
c  Add d(a-alpha)/d(Gamma_dot) to the A-matrix for the Newton-
c  Raphson iteration.
c--------------------------------------------------------------------

      do ia = 1,num_slip_sys
        array3(ia,ia) = array3(ia,ia) + daadga(ia)
      end do

c--------------------------------------------------------------------
c  Calculate the gradient of sse wrt gamma_dot().  Will be used
c  later to ensure that line search is in the correct direction.
c--------------------------------------------------------------------

      do j = 1,num_slip_sys
        grad(j) = 0.0
        do i = 1,num_slip_sys
          grad(j) = grad(j) + func(i) * array3(i,j)
        end do
        grad(j) = 2 * grad(j)
      end do

c--------------------------------------------------------------------
c  Solve for increment of gamma_dot.  Solution is returned 
c  in the func() array.
c--------------------------------------------------------------------

      call simeq(num_slip_sys,array3,func)
 
c--------------------------------------------------------------------
c  Store offset in d_gamma_dot(k) 
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
         d_gamma_dot(k) = func(k)
      end do

c--------------------------------------------------------------------
c  Check to make sure that N-R step leads 'down hill' the 
c  sse surface.
c--------------------------------------------------------------------

      sum = 0.0
      do i = 1,num_slip_sys
        sum = sum - grad(i) * d_gamma_dot(i)
      end do

      if (sum .gt. 0.0) then
        do i = 1,num_slip_sys
          d_gamma_dot(i) = -d_gamma_dot(i)
        end do
      end if

c--------------------------------------------------------------------
c  Multiply step size by two 'cause next loop will divide it by 2.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        d_gamma_dot(k) = d_gamma_dot(k) * 2
      end do
      
c====================================================================
c  Begin line search.
c====================================================================

      improved = .false.

      do N_ctr = 1,max_loops

      sse_old = sse

c--------------------------------------------------------------------
c  Divide step size by two.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        d_gamma_dot(k) = d_gamma_dot(k) / 2
        gamma_try(k)   = gamma_dot(k,m) + d_gamma_dot(k)
      end do
      
c--------------------------------------------------------------------
c  Calculate g, F_p_inv, F_el, sig, tau, func, and sse based
c  on gamma_try(k)
c--------------------------------------------------------------------

      call eval_func(xs0,	dt_incr,	gamma_try,	
     &		     xm0,	F1,		num_slip_sys,
     &		     C,		F_p_inv(1,1,m),	F_p_inv_0(1,1,m),
     &		     g0(1,m),	Hdir,		Hdyn,
     &		     a0(1,m),	Adir,		Adyn,
     &		     g_sat,	F_el,		flow_exp,
     &		     sig,	tau(1,m),	gamma_dot_zero,
     &		     g(1,m),	func,		xL_p,
     &		     xLatent,	sse,		a(1,m))

c--------------------------------------------------------------------
c  Check for 'convergence' of the line search.  Note that the line
c  search doesn't waste time converging closely.  This is 'cause
c  the N-R step does so much better.
c--------------------------------------------------------------------

      if ((sse_old.le.sse_ref).and.(sse.ge.sse_old).and.
     &					(N_ctr.gt.1)) improved=.true.

      if (improved) go to 200

      end do ! Linear Search

c--------------------------------------------------------------------
c  Add "d_gamma_dot" to gamma_dot to get new values for
c  this iteration. 
c--------------------------------------------------------------------

  200 do k = 1,num_slip_sys
        gamma_dot(k,m) = gamma_dot(k,m) + d_gamma_dot(k) * 2.0
      end do

c--------------------------------------------------------------------
c  If (sse_old > tolerance) then this step has not converged.
c--------------------------------------------------------------------

      if (sse_old .gt. tolerance) converged = .false.

c--------------------------------------------------------------------
c  If (sse_old > sse_ref/2) then convergence is too slow and
c  increment is divided into two subincrements.
c--------------------------------------------------------------------
  
      if ((sse_old.gt.sse_ref/2.0).and.(.not.converged)) then
        N_incr = 2 * N_incr - 1
        N_incr_total = 2 * N_incr_total
        go to 100
      end if

c--------------------------------------------------------------------
c  End iterative loop.
c--------------------------------------------------------------------

      end do ! 'Newton Raphson Iterative Loop'

c--------------------------------------------------------------------
c  If another subincrement remains to be done, then reinitialize
c  F_p_inv_0 and g0.  F0 and F1 gets reinitialized back at the
c  top of this loop.
c--------------------------------------------------------------------

      if (N_incr .lt. N_incr_total) then
        if (N_incr .eq. (N_incr/2)*2) then	! N_incr is 'even'
          N_incr = N_incr / 2 + 1
          N_incr_total = N_incr_total / 2
        else					! N_incr is 'odd'
          N_incr = N_incr + 1
        end if
        do i = 1,3
          do j = 1,3
            F_p_inv_0(i,j,m) = F_p_inv(i,j,m)
          end do
        end do
        do i = 1,num_slip_sys
          g0(i,m) = g(i,m)
          a0(i,m) = a(i,m)
        end do
        go to 100
      end if

c--------------------------------------------------------------------
c  Calculate the average Cauchy stress.
c--------------------------------------------------------------------

      do i = 1,3
        do j = 1,3
          sig_avg(i,j) = sig_avg(i,j) + sig(i,j)
        end do
      end do

c--------------------------------------------------------------------
c  Write out Euler Angles in Kocks Convention.
c--------------------------------------------------------------------

      call aa_dot_bb(3,F_el,dir_cos(1,1,m),array1)
      call kocks_angles(npt,m,time(2),array1)

c--------------------------------------------------------------------
c  Calculate the average elasticity tensor.
c--------------------------------------------------------------------

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          if (m .eq. 1) C_avg(i,j,k,l) = 0.0
          C_avg(i,j,k,l) = C_avg(i,j,k,l) + C(i,j,k,l)
         end do ! l
        end do ! k
       end do ! j
      end do ! i

c--------------------------------------------------------------------
c  Rotate xs0 and xm0 to current coordinates, called xs and xm.
c--------------------------------------------------------------------

      call inverse_3x3(F_el,F_el_inv)
      do n = 1,num_slip_sys
        do i = 1,3
          xs(i,n) = 0.0
          xm(i,n) = 0.0
          do j = 1,3
            xs(i,n) = xs(i,n) + F_el(i,j) * xs0(j,n)
            xm(i,n) = xm(i,n) + xm0(j,n)  * F_el_inv(j,i)
          end do
        end do
      end do

c--------------------------------------------------------------------
c  Calculate the derivative of the plastic part of the rate of
c  deformation tensor in the current configuration wrt sigma.
c--------------------------------------------------------------------

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          if (m .eq. 1) ddpdsig(i,j,k,l) = 0.0
          do n = 1,num_slip_sys
           ddpdsig(i,j,k,l) = ddpdsig(i,j,k,l) + (xs(i,n)*xm(j,n) +
     &       xm(i,n) * xs(j,n)) * (xs(k,n) * xm(l,n) + xm(k,n) *
     &       xs(l,n)) * abs(power((tau(n,m)-a(n,m))/g(n,m),
     &		flow_exp-1.0)) / g(n,m)
          end do ! num_slip_sys
         end do ! l
        end do ! k
       end do ! j
      end do ! i

c      write(7,'(12f7.3)')(gamma_dot(i,m)/0.0004,i=1,12)

c--------------------------------------------------------------------
c  End loop over all the grains
c--------------------------------------------------------------------

      end do ! m  = 1,num_grains
c--------------------------------------------------------------------
c  Calculate Green Strain
c--------------------------------------------------------------------
      call transpose(3,F1,array1)
	call aa_dot_bb(3,array1,F1,E_tot)

      do i = 1,3
        E_tot(i,i) = E_tot(i,i) - 1
        do j = 1,3
          E_tot(i,j) = E_tot(i,j) / 2
        end do 
      end do 
c====================================================================
c  Begin calculation of the Jacobian (the tangent stiffness matrix).
c====================================================================

c--------------------------------------------------------------------
c  Store sig_avg() into sig()
c--------------------------------------------------------------------

      do i = 1,3
        do j = 1,3
          sig(i,j) = sig_avg(i,j) / num_grains
        end do
      end do

c--------------------------------------------------------------------
c  Divide C_avg by num_grains to get correct average.
c--------------------------------------------------------------------

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          C_avg(i,j,k,l) = C_avg(i,j,k,l) / num_grains
         end do ! l
        end do ! k
       end do ! j
      end do ! i

c--------------------------------------------------------------------
c  Output stress, strain, etc. for post processing.
c--------------------------------------------------------------------

      sum = 0.0
      do i = 1,3
       do j = 1,3
        sum = sum + sig(i,j) * sig(i,j)
       end do
      end do
      trace = sig(1,1) + sig(2,2) + sig(3,3)
      sig_eff=sqrt(1.5 * sum - 0.5 * trace**2)

c      write(7,'(a3,i5,5f14.6)')'BM1',ninc,time(2),
c     &	abs(log(dfgrd1(3,3)))+dfgrd1(1,3)/dfgrd1(3,3)/1.7320508,
c     &	sig_eff,sig(3,3),sig(1,3)

c      write(7,'(a3,i5,5f14.6)')'BM1',ninc,time(2),sig(1,2),sig(2,3)
c--------------------------------------------------------------------
c  Calculate the inverse of F_el
c--------------------------------------------------------------------

      call inverse_3x3(F_el,F_el_inv)

c--------------------------------------------------------------------
c  Scale by appropriate constants and divide by num_grains to
c  get the average value.  ALSO multiply by 'dtime' which is
c  d(sig)/d(sig_dot).
c--------------------------------------------------------------------

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          ddpdsig(i,j,k,l) = ddpdsig(i,j,k,l) * dtime * flow_exp *
     &		gamma_dot_zero / 4. / num_grains
         end do ! l
        end do ! k
       end do ! j
      end do ! i

c--------------------------------------------------------------------
c  Multiply the 4th rank elastic stiffness tensor by the derivative
c  of the plastic part of the rate of deformation tensor wrt sig_dot.
c--------------------------------------------------------------------

      call aaaa_dot_dot_bbbb(3,C_avg,ddpdsig,array6)

c--------------------------------------------------------------------
c  Add 4th rank identity tensor to array6()
c--------------------------------------------------------------------

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          array6(i,j,k,l) = array6(i,j,k,l) + 0.5 * 
     &		(del(i,k) * del(j,l) + del(i,l) * del(j,k))
         end do
        end do
       end do
      end do

c--------------------------------------------------------------------
c  Need to take the inverse of Array4.  Since it relates two 2nd
c  rank tensors that are both symmetric, Array4 can be xformed to 
c  Voigt notation style in order to do the inverse, then xformed back.
c--------------------------------------------------------------------

      call forth_to_Voigt (Array6,Array4)
      call inverse        (6,Array4,Array5)
      call Voigt_to_forth (Array5,Array6)

c--------------------------------------------------------------------
c  Multiply Array6 by C, the elastic stiffness matrix to
c  finally get the Jocobian, but as a 4th rank tensor.
c--------------------------------------------------------------------

      call aaaa_dot_dot_bbbb (3,Array6,C_avg,ddsdde_4th)

c--------------------------------------------------------------------
c  Store the stress tensor in the ABAQUS stress 'vector'
c--------------------------------------------------------------------

      do i = 1,ndi
         stress(i) = sig(i,i)
      end do
      if (nshr .eq. 1) stress(ndi+1) = sig(1,2)
      if (nshr .eq. 3) then
         stress(4) = sig(1,2)
         stress(5) = sig(1,3)
         stress(6) = sig(2,3)
      end if

c--------------------------------------------------------------------
c  Store the Jocbian in Voigt notation form.
c--------------------------------------------------------------------

      do i = 1,3
       do j = i,3   ! not 1 !!!
        ia = i
        if (i.ne.j) ia=i+j+1
        do k = 1,3
         do l = k,3 ! not 1 !!!
          ib = k
          if (k.ne.l) ib=k+l+1
          array4(ia,ib) = ddsdde_4th(i,j,k,l)
          IF(IB.GE.4) ARRAY4(IA,IB) = 2 * ARRAY4(IA,IB)
         end do
        end do
       end do
      end do

c      call print_array(6,array4)

      do i =1,6
        do j = 1,6
          ddsdde(i,j) = 0.0
        end do
      end do

      if (ndi .eq. 1) then			! 1-D
         ddsdde(1,1) = array4(1,1)
      else if (ndi .eq. 2) then			! 2-D plane stress & axi
         do i = 1,2
            do j = 1,2
               ddsdde(i,j) = array4(i,j)
            end do
         end do
         ddsdde(1,3) = array4(1,4)
         ddsdde(2,3) = array4(2,4)
         ddsdde(3,1) = array4(4,1)
         ddsdde(3,2) = array4(4,2)
         ddsdde(3,3) = array4(4,4)
      else if (ndi .eq. 3 .and. nshr .eq. 1) then ! plane strain
         do i = 1,4
            do j = 1,4
               ddsdde(i,j) = array4(i,j)
            end do
         end do
      else					! Full 3-D
         do i = 1,6
            do j = 1,6
               ddsdde(i,j) = array4(i,j)
            end do
         end do
      end if

c-------------------------------------------------------------------
c  Store the internal variables in the statev() array
c-------------------------------------------------------------------

         n = 0

            
c-------------------------------------------------------------------
c  Store the Euler Angles  1-3
c-------------------------------------------------------------------

         do m = 1,num_grains
           do i = 1,3
             n = n + 1
             statev(n) = psi(i,m)
           end do
         end do
            
c-------------------------------------------------------------------
c  Store the plastic part of F   4-12
c-------------------------------------------------------------------

         do m = 1,num_grains
            do j = 1,3
               do i = 1,3
                  n = n + 1
                  statev(n) = F_p_inv(i,j,m)
               end do
            end do
         end do

c-------------------------------------------------------------------
c  Store the reference shear stresses.     13-24
c-------------------------------------------------------------------

         do m = 1,num_grains
           do i = 1,num_slip_sys
             n = n + 1
             statev(n) = g(i,m)
           end do
         end do

c-------------------------------------------------------------------
c  Store the back stresses.     25-36
c-------------------------------------------------------------------

         do m = 1,num_grains
           do i = 1,num_slip_sys
             n = n + 1
             statev(n) = a(i,m)
           end do
         end do

c-------------------------------------------------------------------
c  Plastic Strain Calculations
c-------------------------------------------------------------------
c  Increment of plastic shear strain accumulated on each slip system
c  over this time step.
      do i = 1,num_slip_sys
	  delta_gamma(i) = gamma_dot(i,1)*dtime
	end do

	do j = 1,3
	  do k = 1,3
         delta_E_p(j,k) =  0.0
	  end do
	end do

c  Increment of the plastic strain tensor
	do j = 1,3
	  do k = 1,3
	    do l = 1,num_slip_sys
	      delta_E_p(j,k) = delta_E_p(j,k) + 0.5*delta_gamma(l)*
     &     (xs0(j,l)*xm0(k,l)+xs0(k,l)*xm0(j,l))
          end do
	  end do
	end do 

c  Plastic strain tensor
      do i = 1,3
	  do j = 1,3
	    E_p(i,j) = E_p(i,j)+delta_E_p(i,j)
	  end do
	end do 
    
	do i = 1,3
	 do j = 1,3
	   n = n+1
	   statev(n) = E_p(i,j)
	 end do
	end do		      
c  state variables 37-45
 
c  Effective total strain
      call aa_dot_dot_bb(3,E_tot,E_tot,sum)
	E_eff = sqrt(2./3.*sum)
      
	n = n+1
      statev(n)= E_eff
c   state variables 46

c  Effective Plastic Strain
      call aa_dot_dot_bb(3,E_p,E_p,sum1)
	E_p_eff = sqrt(2./3.*sum1)

      if (E_p_eff.lt.0) E_p_eff = 0.
		     
	n = n+1
      statev(n)= E_p_eff
c   state variables 47

c  Effective plastic strain increment
      sum2 = 0.
      call aa_dot_dot_bb(3,delta_E_p,delta_E_p,sum2)
      sum2=sqrt(2./3.*sum2)
      E_p_eff_cum = sum2 + E_p_eff_cum

      n = n+1       
      statev(n) = E_p_eff_cum
c   state variables 48

      return
      end
c====================================================================
c====================================================================
c====================== S U B R O U T I N E S =======================
c====================================================================
c====================================================================
c
c  Calculate a vector cross product.
c
c  c = a cross b
c
c--------------------------------------------------------------------

      subroutine cross_product(a1,a2,a3,b1,b2,b3,c1,c2,c3)
      
      implicit none
      real*8 a1, a2, a3, b1,b2,b3,c1,c2,c3
      
      c1 = a2 * b3 - a3 * b2
      c2 = a3 * b1 - a1 * b3
      c3 = a1 * b2 - a2 * b1

      return
      end

c====================================================================
c====================================================================
c
c  Normalize the length of a vector to one.
c
c--------------------------------------------------------------------

      subroutine normalize_vector(x,y,z)

      implicit double precision (a-h,o-z)
	REAL*8 xlength

      xlength = sqrt(x*x+y*y+z*z)
      x = x / xlength
      y = y / xlength
      z = z / xlength

      return
      end

c====================================================================
c====================================================================
c
c  Transpose an ( n x n ) tensor.
c
c--------------------------------------------------------------------

      subroutine transpose(n,a,b)

      implicit none
      
      integer i,j,n
      real*8 a(n,n), b(n,n)

      do i = 1,n
         do j = 1,n
            b(i,j) = a(j,i)
         end do
      end do

      return
      end


c====================================================================
c====================================================================
c
c  Calculate the dot product of two 2nd rank tensors.
c  Result is stored in cc(i,j)
c
c--------------------------------------------------------------------

      subroutine aa_dot_bb(n,a,b,c)

      implicit none
      integer i,j,k,n

      real*8 a(n,n), b(n,n), c(n,n)

      do i = 1,n
         do j = 1,n
            c(i,j) = 0
            do k = 1,n
               c(i,j) = c(i,j) + a(i,k) * b(k,j)
            end do
         end do
      end do

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the double dot product of two 2nd rank tensors.
c
c--------------------------------------------------------------------

      subroutine aa_dot_dot_bb(n,a,b,sum)

      implicit none
      integer i,j,n

      real*8 a(n,n), b(n,n), sum

      sum = 0.0
      do i = 1,n
         do j = 1,n
            sum = sum + a(i,j) * b(i,j)
         end do
      end do

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the double dot product of two 4th rank tensors.
c  Result is stored in c(i,j,k,l)
c
c--------------------------------------------------------------------

      subroutine aaaa_dot_dot_bbbb(n,a,b,c)

      implicit none

      integer i,j,k,l,n, m1, m2 
      real*8 a(n,n,n,n), b(n,n,n,n), c(n,n,n,n)

      do i = 1,n
       do j = 1,n
        do k = 1,n
         do l = 1,n
          c(i,j,k,l) = 0
          do m1 = 1,n
           do m2 = 1,n
            c(i,j,k,l) = c(i,j,k,l) + a(i,j,m1,m2) * b(m1,m2,k,l)
           end do !m2
          end do !m1
         end do !l
        end do !k
       end do !j
      end do !i

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the double dot product of a 4th rank tensor and
c  a 2nd rank tensor.  Result is stored in c(i,j).
c
c--------------------------------------------------------------------

      subroutine aaaa_dot_dot_bb(n,a,b,c)

      implicit none
      integer i,j,k,l,n

      real*8 a(n,n,n,n), b(n,n), c(n,n)

      do i = 1,n
       do j = 1,n
        c(i,j) = 0
        do k = 1,n
         do l = 1,n
          c(i,j) = c(i,j) + a(i,j,k,l) * b(k,l)
         end do !l
        end do !k
       end do !j
      end do !i

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the double dot product of a 2nd rank tensor and
c  a 4th rank tensor.  Result is stored in c(i,j).
c
c--------------------------------------------------------------------

      subroutine aa_dot_dot_bbbb(n,a,b,c)

      implicit none
      
      integer i,j,k,l,n
      real*8 a(n,n), b(n,n,n,n), c(n,n)

      do i = 1,n
       do j = 1,n
        c(i,j) = 0
        do k = 1,n
         do l = 1,n
          c(i,j) = c(i,j) + a(k,l) * b(k,l,i,j)
         end do !l
        end do !k
       end do !j
      end do !i

      return
      end

c====================================================================
c====================================================================
c
c  Rotates any 3x3x3x3 tensor by a rotation matrix.
c
c  c(i,j,k,l) = a(i,m) * a(j,n) * a(k,p) * a(l,q) * b(m,n,p,q)
c
c--------------------------------------------------------------------

      subroutine rotate_4th(a,b,c)

      implicit none

      integer i,j,m,n,k,l
      real*8 a(3,3), b(3,3,3,3), c(3,3,3,3), d(3,3,3,3)

      do m = 1,3
       do n = 1,3
        do k = 1,3
         do l = 1,3
          d(m,n,k,l) = a(k,1) * (a(l,1) * b(m,n,1,1) + 
     &		a(l,2) * b(m,n,1,2) + a(l,3) * b(m,n,1,3)) +
     &		a(k,2) * (a(l,1) * b(m,n,2,1) + 
     &		a(l,2) * b(m,n,2,2) + a(l,3) * b(m,n,2,3)) +
     &		a(k,3) * (a(l,1) * b(m,n,3,1) + 
     &		a(l,2) * b(m,n,3,2) + a(l,3) * b(m,n,3,3))
         end do
        end do
       end do
      end do

      do i = 1,3
       do j = 1,3
        do k = 1,3
         do l = 1,3
          c(i,j,k,l) = a(i,1) * (a(j,1) * d(1,1,k,l) + 
     &		a(j,2) * d(1,2,k,l) + a(j,3) * d(1,3,k,l)) +
     &		a(i,2) * (a(j,1) * d(2,1,k,l) + 
     &		a(j,2) * d(2,2,k,l) + a(j,3) * d(2,3,k,l)) +
     &		a(i,3) * (a(j,1) * d(3,1,k,l) + 
     &		a(j,2) * d(3,2,k,l) + a(j,3) * d(3,3,k,l))
         end do
        end do
       end do
      end do

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the inverse of a 3 x 3 matrix.
c
c--------------------------------------------------------------------

      subroutine inverse_3x3(a,b)

      implicit none

      integer i,j
      real*8 a(3,3), b(3,3), det

      b(1,1) = a(2,2) * a(3,3) - a(3,2) * a(2,3)
      b(1,2) = a(3,2) * a(1,3) - a(1,2) * a(3,3)
      b(1,3) = a(1,2) * a(2,3) - a(2,2) * a(1,3)
      b(2,1) = a(3,1) * a(2,3) - a(2,1) * a(3,3)
      b(2,2) = a(1,1) * a(3,3) - a(3,1) * a(1,3)
      b(2,3) = a(2,1) * a(1,3) - a(1,1) * a(2,3)
      b(3,1) = a(2,1) * a(3,2) - a(3,1) * a(2,2)
      b(3,2) = a(3,1) * a(1,2) - a(1,1) * a(3,2)
      b(3,3) = a(1,1) * a(2,2) - a(2,1) * a(1,2)

      det = a(1,1) * b(1,1) + a(1,2) * b(2,1) + a(1,3) * b(3,1)

      do i = 1,3
         do j = 1,3
            b(i,j) = b(i,j) / det
         end do
      end do

      return
      end


c====================================================================
c====================================================================
c
c  Solve simultaneous equations using LU decomposition (Crout's method)
c  Result is stored in b(i)
c
c  Reference: "Numerical Recipes" Section 2.3  p. 31
c
c--------------------------------------------------------------------

      subroutine simeq(n,a,b)

      implicit none

      integer n
      real*8 a(n,n), b(n), index(n)

      call LU_Decomp(n,a,index)
      call LU_BackSub(n,a,index,b)

      return
      end

c====================================================================
c====================================================================
c
c  Calculate the inverse of a matrix using 
c  LU decomposition (Crout's method)
c
c  Reference: "Numerical Recipes" Section 2.3  p. 31
c
c--------------------------------------------------------------------

      subroutine inverse(n,a,b)

      implicit none

      integer i,j,n
      real*8 a(n,n), b(n,n), c(n,n), index(n)

      do i = 1,n
         do j = 1,n
            c(i,j) = a(i,j)
         end do
      end do

      do i = 1,n
         do j = 1,n
            b(i,j) = 0.0
         end do
         b(i,i) = 1.0
      end do

      call LU_Decomp(n,c,index)
      do j = 1,n
         call LU_BackSub(n,c,index,b(1,j))
      end do

      return
      end

c====================================================================
c====================================================================
c
c  This sub performs an LU Decomposition (Crout's method) on the 
c  matrix "a". It uses partial pivoting for stability. The index()
c  vector is used for the partial pivoting.  The v() vector is 
c  a dummy work area.
c
c  Reference: "Numerical Recipes" Section 2.3  p. 31
c
c--------------------------------------------------------------------

      subroutine LU_Decomp(n,a,index)

      implicit none

      integer i,j,k,n, imax
      real*8 a(n,n), index(n), v(n), tiny, dummy
      real*8 a_max, sum

      tiny = 1.0e-20

c--------------------------------------------------------------------
c  Loop over the rows to get the implicit scaling info.
c--------------------------------------------------------------------

      do i = 1,n
         a_max = 0.0
         do j = 1,n
            a_max = max(a_max,abs(a(i,j)))
         end do !j
         v(i) = 1.0 / a_max
      end do !i

c--------------------------------------------------------------------
c  Begin big loop over all the columns.
c--------------------------------------------------------------------

      do j = 1,n

         do i = 1,j-1
            sum = a(i,j)
            do k = 1,i-1
               sum = sum - a(i,k) * a(k,j)
            end do
            a(i,j) = sum
         end do

         a_max = 0.0
         do i = j,n
            sum = a(i,j)
            do k = 1,j-1
               sum = sum - a(i,k) * a(k,j)
            end do
            a(i,j) = sum
            dummy = v(i) * abs(sum)
            if ( dummy .gt. a_max ) then
               imax = i
               a_max = dummy
            end if
         end do

c--------------------------------------------------------------------
c  Pivot rows if necessary.
c--------------------------------------------------------------------

         if ( j .ne. imax ) then
            do k = 1,n
               dummy = a(imax,k)
               a(imax,k) = a(j,k)
               a(j,k) = dummy
            end do
            v(imax) = v(j)
         end if
         index(j) = imax

c--------------------------------------------------------------------
c  Divide by the pivot element.
c--------------------------------------------------------------------

         if ( a(j,j) .eq. 0.0 ) a(j,j) = tiny
         if ( j .ne. n ) then
            dummy = 1.0 / a(j,j)
            do i = j+1,n
               a(i,j) = a(i,j) * dummy
            end do
         end if

      end do !j

      return
      end

c====================================================================
c====================================================================
c
c  Solves a set of simultaneous equations by doing back substitution.
c  The answer in returned in the b() vector.  The a(,) matrix
c  must have already been "LU Decomposed" by the above subroutine.
c
c  Reference: "Numerical Recipes" Section 2.3  p. 31
c
c--------------------------------------------------------------------

      subroutine LU_BackSub(n,a,index,b)

      implicit none

      integer i,j,m,n, ii
      real*8 a(n,n), index(n), b(n)
      real*8 sum

      ii = 0

c--------------------------------------------------------------------
c  Do the forward substitution.
c--------------------------------------------------------------------

      do i = 1,n
         m = index(i)
         sum = b(m)
         b(m) = b(i)
         if ( ii .ne. 0 ) then
            do j = ii,i-1
               sum = sum - a(i,j) * b(j)
            end do
         else if ( sum .ne. 0.0 ) then
            ii = i
         end if
         b(i) = sum
      end do

c--------------------------------------------------------------------
c  Do the back substitution.
c--------------------------------------------------------------------

      do i = n,1,-1
         sum = b(i)
         if ( i .lt. n ) then
            do j = i+1,n
               sum = sum - a(i,j) * b(j)
            end do
         end if
         b(i) = sum / a(i,i)
      end do

      return
      end
      
c====================================================================
c====================================================================
c
c  Restore a symmetric 4th rank tensor stored in Voigt notation 
c  back to its 4th rank form.
c
c--------------------------------------------------------------------

      subroutine Voigt_to_forth(b,a)

      implicit none

      integer i,j,k,l, ia , ib
      real*8 a(3,3,3,3), b(6,6)

      do i = 1,3
       do j = 1,3
        ia = i
        if (i.ne.j) ia=9-i-j
        do k = 1,3
         do l = 1,3
          ib = k
          if (k.ne.l) ib=9-k-l
          a(i,j,k,l) = b(ia,ib)
          if (ia.gt.3) a(i,j,k,l) = a(i,j,k,l) / 2
          if (ib.gt.3) a(i,j,k,l) = a(i,j,k,l) / 2
         end do
        end do
       end do
      end do

      return
      end


c====================================================================
c====================================================================
c
c  Store a SYMMETRIC 4th rank tensor in Voigt notation.
c
c--------------------------------------------------------------------

      subroutine forth_to_Voigt(a,b)

      implicit none

      integer i,j,k,l, ia, ib
      real*8 a(3,3,3,3), b(6,6)

      do i = 1,3
       do j = i,3   ! not 1 !!!
        ia = i
        if (i.ne.j) ia=9-i-j
        do k = 1,3
         do l = k,3 ! not 1 !!!
          ib = k
          if (k.ne.l) ib=9-k-l
          b(ia,ib) = a(i,j,k,l)
         end do
        end do
       end do
      end do

      return
      end



c====================================================================
c====================================================================
c
c  Perform x**y but while retaining the sign of x.
c
c--------------------------------------------------------------------

      function power(x,y)

      implicit double precision (a-h,o-z)

      if (x.eq.0.0) then
        if (y.gt.0.0) then
          power = 0.0
        else if (y .lt. 0.0) then
          power = 1.0d+300
        else
          power = 1.0
        end if
      else
         power = y * log10(abs(x))
         if (power .gt. 300.) then
           power = 1.d+300
         else
           power = 10.d0 ** power
         end if
         if (x .lt. 0.0) power = -power
      end if

      return
      end

c====================================================================
c====================================================================
c
c  Return the sign of a number.
c
c--------------------------------------------------------------------

      function sgn(a)

      implicit double precision (a-h,o-z)

      sgn = 1.0
      if (a .lt. 0.0) sgn = -1.0

      return
      end 

c====================================================================
c====================================================================
c
c  Calculate the determinant of a 3 x 3 matrix.
c
c--------------------------------------------------------------------

      function determinant(a)

      implicit double precision (a-h,o-z)

      real*8 a(3,3)

      b1 = a(2,2) * a(3,3) - a(3,2) * a(2,3)
      b2 = a(3,1) * a(2,3) - a(2,1) * a(3,3)
      b3 = a(2,1) * a(3,2) - a(3,1) * a(2,2)

      determinant = a(1,1) * b1 + a(1,2) * b2 + a(1,3) * b3

      return
      end

c===================================================================
c===================================================================
c
c  Print out an array.
c
c-------------------------------------------------------------------

      subroutine print_array(n,a)

      implicit none

      integer n,i,j
      real*8 a(n,n)

      do i = 1,n
         write(6,'(10f12.3)')(a(i,j),j=1,n)
      end do
      print*,' '

      return
      end

c===================================================================
c===================================================================
c
c  Print out Euler angles in Kocks notation.
c
c-------------------------------------------------------------------

      subroutine kocks_angles(npt,m,time,array1)

      implicit none

      integer npt,m
      real*8 array1(3,3), time, pi
      real*8 psi, theta, phi

      pi = 4 * atan(1.0)

      if (abs(array1(3,3)) .gt. 0.99999) then
        psi   = atan2(array1(2,1),array1(1,1))
        theta = 0.0
        phi   = 0.0
      else
        psi   = atan2(array1(2,3),array1(1,3))
        theta = acos(array1(3,3))
        phi   = atan2(array1(3,2),-array1(3,1))
      end if

      psi   = 180 * psi   / pi
      theta = 180 * theta / pi
      phi   = 180 * phi   / pi


c      write(7,'(a3,2i5,4f10.3)')'BM2',ninc,m,time,psi,theta,phi

      return
      end

c=======================================================================
c=======================================================================
c
c  Evaluate function to be minimized to zero.  gamma_dot()'s are
c  input and several things are output.
c
c-----------------------------------------------------------------------


      subroutine eval_func( xs0,	dtime,		gamma_dot,	
     &			    xm0,	F1,		num_slip_sys,
     &			    C,		F_p_inv,	F_p_inv_0,
     &			    g0,		Hdir,		Hdyn,
     &			    a0,		Adir,		Adyn,
     &			    g_sat,	F_el,		flow_exp,
     &			    sig,	tau,		gamma_dot_zero,
     &			    g,		func,		xL_p,
     &			    xLatent,	sse,		a)

      implicit none
      
      integer ii, ia, ib, k,n, num_slip_sys 
      real*8 dtime, sum, det, determinant, sum00
      real*8 xLatent, Hdir, HDyn, Adir, power
      real*8 Adyn, sse, gamma_dot_zero, flow_exp
      real*8 gamma_dot_max, g_sat
      

      real*8
     &	xs0(3,num_slip_sys),	xm0(3,num_slip_sys),	F_p_inv_0(3,3),
     &	F1(3,3),		C(3,3,3,3),		F_p_inv(3,3),
     &	g0(num_slip_sys),	xL_p_inter(3,3),	F_el(3,3),
     &	E_el(3,3),		Spk2(3,3),		sig(3,3),
     &	tau(num_slip_sys),	g(num_slip_sys),	array1(3,3),
     &	func(num_slip_sys),	gamma_dot(num_slip_sys),array2(3,3),
     &	F_el_inv(3,3),		xL_p(3,3),
     &	xs(3,num_slip_sys),	xm(3,num_slip_sys),
     &	a0(num_slip_sys),	a(num_slip_sys)
	REAL*8 omega
	INTEGER i,j
c*** Note that xs0 and xm0 are in INTERMEDIATE configuration!!!

c--------------------------------------------------------------------
c  Calculate the plastic part of the
c  velocity gradient in the intermediate configuration.
c--------------------------------------------------------------------

      do i = 1,3
        do j = 1,3
          xL_p_inter(i,j) = 0.0
          do k = 1,num_slip_sys
            xL_p_inter(i,j) = xL_p_inter(i,j) + 
     &			xs0(i,k) * xm0(j,k) * gamma_dot(k)
          end do
        end do
      end do

c--------------------------------------------------------------------
c  Begin calculation process of F_p_n+1 = exp(xL_p_inter*dt).F_p_n
c--------------------------------------------------------------------

      do i = 1,3
        do j = 1,3
          array1(i,j) = xL_p_inter(i,j) * dtime
        end do
      end do

c--------------------------------------------------------------------
c  Calculate omega.
c--------------------------------------------------------------------

      sum = 0
      do i = 1,3
        do j = 1,3
          sum = sum + array1(i,j) * array1(i,j)
        end do
      end do
      omega = sqrt(0.5 * sum)

c--------------------------------------------------------------------
c  Continue calculating intermediate stuff needed for F_p_n+1
c--------------------------------------------------------------------
      call aa_dot_bb(3,array1,array1,array2)
      do i = 1,3
        if (abs(omega).ge.0.00000001) then   ! if omega=0 then no need
          do j = 1,3
            array1(i,j) = array1(i,j) * sin(omega) / omega +
     &			array2(i,j) * (1-cos(omega)) / omega**2
	    	if (j .eq. 4) then
			print *,omega
		end if 
          end do
        end if
        array1(i,i) = 1 + array1(i,i)
      end do
c--------------------------------------------------------------------
c   Finally multiply arrays to get F_p_inv at end of time step.
c--------------------------------------------------------------------

      call inverse_3x3(array1,array2)
      call aa_dot_bb(3,F_p_inv_0,array2,F_p_inv)

c--------------------------------------------------------------------
c  Multiply F() by F_p_inv() to get F_el()
c--------------------------------------------------------------------
      call aa_dot_bb(3,F1,F_p_inv,F_el)
      call inverse_3x3(F_el,F_el_inv)

c--------------------------------------------------------------------
c  Rotate director vectors from intermediate configuration to
c  the current configuration.
c--------------------------------------------------------------------
      do n = 1,num_slip_sys
        do i = 1,3
          xs(i,n) = 0.0
          xm(i,n) = 0.0
          do j = 1,3
            xs(i,n) = xs(i,n) + F_el(i,j) * xs0(j,n)
            xm(i,n) = xm(i,n) + xm0(j,n)  * F_el_inv(j,i)
          end do
        end do
      end do
c--------------------------------------------------------------------
c  Calculate elastic Green Strain
c--------------------------------------------------------------------

      call transpose(3,F_el,array1)
      call aa_dot_bb(3,array1,F_el,E_el)
      do i = 1,3
        E_el(i,i) = E_el(i,i) - 1
        do j = 1,3
          E_el(i,j) = E_el(i,j) / 2
        end do
      end do 

c--------------------------------------------------------------------
c  Multiply the stiffness tensor by the Green strain to get
c  the 2nd Piola Kirkhhoff stress
c--------------------------------------------------------------------

      call aaaa_dot_dot_bb(3,C,E_el,Spk2)

c--------------------------------------------------------------------
c  Convert from PK2 stress to Cauchy stress
c--------------------------------------------------------------------

      det = determinant(F_el)
      call transpose(3,F_el,array2)
      call aa_dot_bb(3,F_el,Spk2,array1)
      call aa_dot_bb(3,array1,array2,sig)
      do i = 1,3
        do j = 1,3
          sig(i,j) = sig(i,j) / det
        end do
      end do

c--------------------------------------------------------------------
c  Calculate resolved shear stress for each slip system.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        tau(k) = 0.0
        do j = 1,3
          do i = 1,3
            tau(k) = tau(k) + xs(i,k) * xm(j,k) * sig(i,j)
          end do 
        end do
      end do
      
c--------------------------------------------------------------------
c  Calculate hardening law for ref shear stress
c--------------------------------------------------------------------

      sum = 0
      do i = 1,num_slip_sys
        sum = sum + abs(gamma_dot(i))
      end do
      do k = 1,num_slip_sys
        sum00 = xLatent * sum - (xLatent - 1) * abs(gamma_dot(k))
        g(k) = (g0(k) + Hdir*sum00*dtime) / (1 + Hdyn*sum*dtime)
      end do

c--------------------------------------------------------------------
c  Calculate hardening law for back stress
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        a(k) = (a0(k) + Adir * dtime * gamma_dot(k))
     &			 / (1 + Adyn * dtime * abs(gamma_dot(k)))
      end do

c--------------------------------------------------------------------
c  Calculate function values.
c--------------------------------------------------------------------

      do k = 1,num_slip_sys
        func(k) = tau(k)- a(k) - g(k) * power( (gamma_dot(k)/
     &			gamma_dot_zero), (1./flow_exp) )
      end do

c--------------------------------------------------------------------
c  Calculate root mean square error that is to be minimized to zero.
c--------------------------------------------------------------------

      sse = 0.0
      gamma_dot_max = abs(gamma_dot(1))
      do k = 1,num_slip_sys
        sse = sse + abs(gamma_dot(k)) * func(k) ** 2
        gamma_dot_max = max(abs(gamma_dot(k)),gamma_dot_max)
      end do

      if (gamma_dot_max .gt. 0.0) sse = sqrt(sse / gamma_dot_max) / 
     &							num_slip_sys

      return
      end

         SUBROUTINE UVARM(UVAR,DIRECT,T,TIME,DTIME,CMNAME,ORNAME,
     1 NUVARM,NOEL,NPT,LAYER,KSPT,KSTEP,KINC,NDI,NSHR,COORD,
     2 JMAC,JMATYP,MATLAYO,LACCFLA) 
c
C      INCLUDE 'ABA_PARAM.INC'
c
      CHARACTER*80 CMNAME,ORNAME
      CHARACTER*3 FLGRAY(200)
      DIMENSION UVAR(NUVARM),DIRECT(3,3),T(3,3),TIME(2)
      DIMENSION ARRAY(200),JARRAY(200),JMAC(*),JMATYP(*),COORD(*)

C      CALL GETVRM('SDV',ARRAY,JARRAY,FLGRAY,JRCD,JMAC,JMATYP,
C     1 MATLAYO,LACCFLA)

c	Plastic strain

      UVAR(1)  = ARRAY(37) !! plastic strain (1,1)
      UVAR(2)  = ARRAY(38) !! plastic strain (1,2)
      UVAR(3)  = ARRAY(39) !! plastic strain (1,3)
      UVAR(4)  = ARRAY(41) !! plastic strain (2,2)
      UVAR(5)  = ARRAY(42) !! plastic strain (2,3)
      UVAR(6)  = ARRAY(45) !! plastic strain (3,3)
      
c	Fp      

      UVAR(7)  = ARRAY(46) !! eff tot strain
      UVAR(8)  = ARRAY(47) !! eff plastic strain     
      UVAR(9)  = ARRAY(48) !! eff plastic strain increment

      RETURN
      END
C****************************************************************
	SUBROUTINE DLOAD(F,KSTEP,KINC,TIME,NOEL,NPT,LAYER,KSPT,
     & COORDS,JLTYP,SNAME)
  
C
C      INCLUDE 'ABA_PARAM.INC'
C
      DIMENSION TIME(2),COORDS(3)
	CHARACTER*80 SNAME
C
      IF(KSTEP.EQ.1) THEN
		F=1000
		F=F*TIME(1)/10.0*(-1.0)   !!! tensile load (negative)
	END IF
	IF(KSTEP.EQ.2) THEN
	  STF=1000
	  ENF=STF*0.0
	  SLOPE=(STF-ENF)/10.0
	  F=STF-(SLOPE*TIME(1))
        F=F*(-1)
	END IF
C
      RETURN
      END
