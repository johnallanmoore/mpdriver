C                                                  _______
C                 \    /  |    |   |\  /|    /\       |
C                  \  /   |    |   | \/ |   /__\      |
C                   \/    |____|   |    |  /    \     |
C
C  Updates stresses, internal variables, and energies for 3D solid elements ONLY
C
C  
C     SIMPLE LINEAR ELASTIC ISOTROPIC MATERIAL, SMALL STRAIN
C     Rate depedent plasticity
C     microinertia
C  ============================================================================
      subroutine vumat(
C Read only (unmodifiable)variables -
     1  nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,
     2  stepTime, totalTime, dt, cmname, coordMp, charLength,
     3  props, density, strainInc, relSpinInc,
     4  tempOld, stretchOld, defgradOld, fieldOld,
     5  stressOld, stateOld, enerInternOld, enerInelasOld,
     6  tempNew, stretchNew, defgradNew, fieldNew,
C Write only (modifiable) variables -
     7  stressNew, stateNew, enerInternNew, enerInelasNew )
C
C      include 'vaba_param.inc'
C

       implicit none
        
       integer  nprops, nblock, ndir, nshr
       integer  nstatev, nfieldv 
      
       real*8 dt,lanneal,stepTime,totalTime

       real*8 density, coordMp,props, 
     1  charLength, strainInc,relSpinInc, tempOld,
     2  stretchOld,defgradOld,fieldOld, stressOld,
     3  stateOld, enerInternOld,enerInelasOld, tempNew,
     4  stretchNew,defgradNew,fieldNew,stressNew, stateNew,
     5  enerInternNew, enerInelasNew

       real*8 sig(6)

       dimension
     1  density(nblock), coordMp(nblock,*),props(nprops), 
     1  charLength(nblock), strainInc(nblock,ndir+nshr),
     2  relSpinInc(nblock,nshr), tempOld(nblock),
     3  stretchOld(nblock,ndir+nshr),
     4  defgradOld(nblock,ndir+nshr+nshr),
     5  fieldOld(nblock,nfieldv), stressOld(nblock,ndir+nshr),
     6  stateOld(nblock,nstatev), enerInternOld(nblock),
     7  enerInelasOld(nblock), tempNew(nblock),
     8  stretchNew(nblock,ndir+nshr),
     8  defgradNew(nblock,ndir+nshr+nshr),
     9  fieldNew(nblock,nfieldv),
     1  stressNew(nblock,ndir+nshr), stateNew(nblock,nstatev),
     2  enerInternNew(nblock), enerInelasNew(nblock)
C
      integer km, i ,j, dim, nItr
      character*80 cmname

      real*8 A, L, rhoM, rhoBar, E
      real*8 n(36,3), sqrt2o2, Ai(36), Li(36)
      real*8 sumTerm(3,3), volTerm, Al3
      real*8 ni(1,3), nxn(3,3), niT(3,1)
      real*8 strainIncTensor(3,3), strainDDot(3,3)
      real*8 strainDDotScalar(1,1), strainDDot3x1(3,1)
      real*8 scalarTerm, sigMI(3,3), Chat(6,6), one6, one12
      real*8 dstress(6,1), dstrain(6,1), theta, thetaCoef
      real*8 dstrain0(1,6), dstrainDot0(1,6)
      real*8 dstrain0T(3,3), dstrainDot0T(3,3)
      real*8 dstrainDot(1,6), dstrainDotT(3,3)
      real*8 dstrainT(3,3), x(7), Rsig(6,1), Rsigbar(1,1)
      real*8 R(7,1), tol, depsb0, stress(6,1),sigbar
      real*8 strainIncP(6,1), dsigbar, term1(6,1)
      real*8 term2(6,6), term4(6,1), strainInci
      real*8 ChatInv(6,6), Cinvsig(6,1), nxnV(6,1)
      real*8 stressi(1,1), nxnVT(1,6), ratio, ratio2
      real*8 H, nExp, sigy0, term0, term3(6,1), term3T(1,6)
      real*8 trTerm, tr, sigbar0, deps(6,1), strainIncT(6,1)
      real*8 Jac(7,7), term1T(1,6), Chatepsp(6,1), term4T(1,6)
      real*8 dx(7,1), JacInv(7,7), depsb, sige, miFlag, sigMI0(1,6)
C     State variables are
C     1 = equivlent plastic strain
C     2 = flow stress
C     3 = equivlent stress
C     4-9 previous strain increment
C     10-15 previous rate of strain incremt (accelration)


C-----------------------------------------------------------------------------
C	           Initialize material data from input file
C-----------------------------------------------------------------------------
      E      = props(1) ! youngs modulus of material
      A      = props(2) ! Area
      L      = props(3) ! strut Length
      rhoM   = props(4) ! material density
      rhoBar = props(5) ! relative density (fraction)
      theta  = props(6) ! factor in strain rate rate
C     plasticty parameters
      H      = props(7) ! hardening modulus
      nExp   = props(8) ! strain-rate exponent
      sigy0  = props(9) ! initial yield stress
C     flag to turn microinertia on (1) and off (0)
      miFlag = props(10)! microinertia flag
C     useful constants
      sqrt2o2 = sqrt(2.D0)/2.D0;

C     dimension of problem
      dim = 3

C     strut normals
      n(1,1:3) = (/  sqrt2o2,  0.0D0 , sqrt2o2 /)
      n(2,1:3) = (/  -sqrt2o2,  0.0D0       , sqrt2o2 /)
      n(3,1:3) = (/   sqrt2o2,   0.0D0      , sqrt2o2 /)
      n(4,1:3) = (/  -sqrt2o2,   0.0D0      , sqrt2o2 /)
C      % -------------------------
      n(5,1:3) = (/ 0.0D0    , -sqrt2o2, sqrt2o2 /)
      n(6,1:3) = (/ 0.0D0    ,  sqrt2o2, sqrt2o2 /)
      n(7,1:3) = (/ 0.0D0    , -sqrt2o2, sqrt2o2 /)
      n(8,1:3) = (/ 0.0D0    ,  sqrt2o2, sqrt2o2 /)
C      % -------------------------
      n(9,1:3) = (/   sqrt2o2,  sqrt2o2, 0.0D0 /)
      n(10,1:3) = (/  -sqrt2o2,  sqrt2o2, 0.0D0 /)
      n(11,1:3) = (/   sqrt2o2,  sqrt2o2, 0.0D0 /)
      n(12,1:3) = (/  -sqrt2o2,  sqrt2o2, 0.0D0 /)
C      % -------------------------
      n(13,1:3) = (/   sqrt2o2,  0.0D0    , -sqrt2o2 /)
      n(14,1:3) = (/  0.0D0   ,  sqrt2o2, -sqrt2o2 /)
      n(15,1:3) = (/   sqrt2o2,  0.0D0    ,  sqrt2o2 /)
      n(16,1:3) = (/  0.0D0   ,  sqrt2o2,  sqrt2o2 /)
C      % -------------------------
      n(17,1:3) = (/ sqrt2o2,  0.0D0    ,  sqrt2o2 /)
      n(18,1:3) = (/ 0.0D0  ,  sqrt2o2,  sqrt2o2 /)
      n(19,1:3) = (/ sqrt2o2,  0.0D0    , -sqrt2o2 /)
      n(20,1:3) = (/ 0.0D0  ,  sqrt2o2, -sqrt2o2 /)
C      % -------------------------
      n(21,1:3) = (/ sqrt2o2,  sqrt2o2,  0.0D0 /)
      n(22,1:3) = (/ sqrt2o2, -sqrt2o2,  0.0D0 /)
      n(23,1:3) = (/ sqrt2o2,  sqrt2o2,  0.0D0 /)
      n(24,1:3) = (/ sqrt2o2, -sqrt2o2,  0.0D0 /)
C       % -------------------------
      n(25,1:3) = (/  sqrt2o2,  0.0D0 , sqrt2o2 /)
      n(26,1:3) = (/ -sqrt2o2, 0.0D0  , sqrt2o2 /)
      n(27,1:3) = (/  sqrt2o2,  0.0D0 , sqrt2o2 /)
      n(28,1:3) = (/ -sqrt2o2, 0.0D0  , sqrt2o2  /)
C       % -------------------------
      n(29,1:3) = (/  0.0D0  , -sqrt2o2, sqrt2o2 /)
      n(30,1:3) = (/  0.0D0  ,  sqrt2o2, sqrt2o2 /)
      n(31,1:3) = (/  0.0D0  , -sqrt2o2, sqrt2o2 /)
      n(32,1:3) = (/  0.0D0  ,  sqrt2o2, sqrt2o2 /)
C      % -------------------------
      n(33,1:3) = (/  sqrt2o2,  sqrt2o2, 0.0D0 /)
      n(34,1:3) = (/ -sqrt2o2,  sqrt2o2, 0.0D0 /)
      n(35,1:3) = (/  sqrt2o2,  sqrt2o2, 0.0D0 /)
      n(36,1:3) = (/ -sqrt2o2,  sqrt2o2, 0.0D0 /)   

C     length and area of each strut
      Ai(:)  = A
      Li(:)  = L 


      do km = 1,nblock

C-----------------------------------------------------------------------------
C                         Static Stress
C----------------------------------------------------------------------------
C     octet stiffness tensor
      one6 = 1.D0/6.D0
      one12 = 1.D0/12.D0
      Chat(:,:) = 0.D0
      Chat(1,1) = one6
      Chat(2,2) = one6
      Chat(3,3) = one6
      Chat(1,2) = one12
      Chat(2,1) = one12
      Chat(1,3) = one12
      Chat(3,1) = one12
      Chat(2,3) = one12
      Chat(3,2) = one12
      Chat(4,4) = one12
      Chat(5,5) = one12
      Chat(6,6) = one12

      call invert(Chat,6,ChatInv)

C      no plasticity
C      dstress = matmul(Chat,dstrain)

C     inital things for plasticity loop
C     stressUpdate comes from 
C     M.C. Messner et al. / Int. J. Solids and Structures 73–74 (2015) 55–66
C    --------------------------------------------------------------

C     unknown DOFs
      sigMI0(1,:) = stateOld(km,16:21)
      x(1:6) = stressOld(km,1:6) - sigMI0(1,1:6)
      if (sigMI0(1,1) > 1.D-2) then
C         print *, "-------------------"
C         print *, "j ", j
C         print *, "stress (start) ", x(1:6)
C         print *, "sigMI (start) ", sigMI0(1,1:6)
C         print *, "total stress (start) ", stressOld(km,1:6)
C         print *, "eeeeeeeeeeeeeeeeeee"
      end if
      
      if (stateOld(km,2) < sigy0) then
            x(7) = sigy0
            sigbar0 = sigy0
      else
            x(7) = stateOld(km,2)
            sigbar0 = stateOld(km,2)
      endif

C     initial residual set to super high
      Rsig(:,1) = 1.D6
      Rsigbar(1,1) = 1.D6
      R(1:6,1) = Rsig(:,1)
      R(7,1) = Rsigbar(1,1)

C     tolerances for N-R solver
      tol = 1.D-6
      nItr = 100

C     elastic predictor (and reference strainrate)
      call getEpsb(strainInc,depsb0)
      
C     start plastic loop j
      do j = 1,nItr
C        extract uknowns
         stress(:,1) = x(1:6)
         sigbar = x(7)
C         print *, "stress (start) ", stress

C        initial sums
         strainIncP(:,1) = 0.D0
         dsigbar    = 0.D0
         term1(:,1) = 0.D0
         term2(:,:) = 0.D0
         term4(:,1) = 0.D0
         strainInci = 0.D0

C        sum over each strut
         do i = 1,size(n,1)
C           strut normal i
            ni(1,1:3) = n(i,1:3)
            niT = transpose(ni)
            nxn = matmul(niT,ni)

C           stress in strut i
            Cinvsig = matmul(ChatInv, stress)
            call tensor2voigt(nxn,nxnV,1)
            nxnVT = transpose(nxnV)
            stressi = matmul(nxnVT,Cinvsig)
            stressi = stressi/rhoBar

C           plastic strain increment
            ratio = abs(stressi(1,1)/sigbar)**(nExp - 1.D0)
            ratio = ratio*(stressi(1,1)/sigbar)
            strainIncP(:,1) = strainIncP(:,1) + depsb0*ratio*nxnV(:,1);

C           strain increment stum for struts
            strainInci = strainInci + abs(depsb0*ratio)

C           flow stress increment
            ratio2 = abs(stressi(1,1)/sigbar)**nExp
            dsigbar = dsigbar + H*depsb0*ratio2

C           sum terms for the jacobian
            term0 = abs(stressi(1,1))**(nExp - 1.D0)
            term3 = matmul(ChatInv,nxnV)
            term3T = transpose(term3)
            term1 = term1 + term0*term3
            term2 = term2 + term0*matmul(nxnV,term3T)
            term4 = term4 + stressi(1,1)*term3*
     1              abs(stressi(1,1))**(nExp - 2.D0)
            
         end do ! end stut loop i 

C     Residual
      trTerm = 1.D0
      tr = strainInc(km,1) + strainInc(km,2) + strainInc(km,3)
      Rsig(:,1) = (1.D0 + trTerm*tr)*stress(:,1)
C      if I dont have this sigmi0 term here it converges
C     not sure what is the correct thing to have
C     but the microinertia should slow down plasticity?
      Rsig(:,1) = Rsig(:,1) - stressOlD(km,1:6) + sigMI0(1,1:6)
      strainIncT = transpose(strainInc)
      deps = strainIncT - strainIncP

      Rsig = Rsig - rhoBar*E* matmul(Chat,deps)
      Rsigbar = sigbar - sigbar0 - dsigbar
      
      R(1:6,1) = Rsig(:,1)
      R(7,1) = Rsigbar(1,1)

C     I think it needs to be here
      if (norm2(R) < tol) then
         stress(:,1) = x(1:6)
         sigbar = x(7)
         exit
      endif

C     Jacobian
C      J11
      term1T = transpose(term1)
      Jac(1:6,1:6) = trTerm*(nExp*depsb0)/(rhoBar*sigbar**nExp)
     1             * matmul(stress,term1T) 
     2             + E*nExp*depsb0/(sigbar**nExp)*matmul(Chat,term2)

      Jac(1,1) = Jac(1,1) + (1.D0 + trTerm*tr)
      Jac(2,2) = Jac(2,2) + (1.D0 + trTerm*tr)
      Jac(3,3) = Jac(3,3) + (1.D0 + trTerm*tr)
      Jac(4,4) = Jac(4,4) + (1.D0 + trTerm*tr)
      Jac(5,5) = Jac(5,5) + (1.D0 + trTerm*tr)
      Jac(6,6) = Jac(6,6) + (1.D0 + trTerm*tr)

C       J12
      Chatepsp = matmul(Chat,strainIncP)
      Jac(1:6,7) = - (nExp/sigbar)*(trTerm*stress(1:6,1)*strainInci
     1           + rhoBar*E*Chatepsp(1:6,1))

C       J21
      term4T = transpose(term4)
      Jac(7,1:6) = -(H*nExp*depsb0/(sigbar**nExp*rhoBar)*term4T(1,1:6))

C       J22
      Jac(7,7) = 1.D0 +(H*nExp/sigbar)*strainInci

C     Update
      call invert(Jac,7,JacInv)
C     this should be JacInv
      dx = matmul(-1.D0*Jac(:,:),R)

      x = x + dx(1:7,1)
C      This checks the residual at the
C      beginning of time step
C      not at the current time
C      if (norm2(R) < tol) then
C         stress(:,1) = x(1:6)
C         sigbar = x(7)
C         exit
C      endif

      end do ! end plasticity loop j

      if (j.eq.nItr+1) then
         stop "did not converge"
      else 
         dstress = stress
         call getEpsb(strainIncP,depsb)
         stateNew(km,1) = stateOld(km,1) + depsb
         stateNew(km,2) = sigbar

      endif

C     strainInc Tensor
      call voigt2Tensor(strainInc(km,:), strainIncTensor, 1) 
C-----------------------------------------------------------------------------
C                         Microinertia
C-----------------------------------------------------------------------------
      if (miFlag > 0.1D0) then
C        strain increment vector
C        dstrain = transpose(strainInc)
C        dstrain = transpose(strainIncP) ! not sure this gets used

C     strain increment rate base on
C     N. Jacques et al. / Mechanics of Materials 80 (2015) 311–323
         dstrain0(1,1:6) = stateOld(1,4:9)
         dstrainDot0(1,1:6) = stateOld(1,10:15)

C      old strain rate based on total strain
C        call voigt2tensor(strainInc,dstrainT,1)
C        call voigt2tensor(dstrain0,dstrain0T,1)
C        call voigt2tensor(dstrainDot0,dstrainDot0T,1)

C     strain rate based on plastic strain
         call voigt2tensor(strainIncP,dstrainT,1)
         call voigt2tensor(dstrain0,dstrain0T,1)
         call voigt2tensor(dstrainDot0,dstrainDot0T,1) 

         thetaCoef = (1.D0-theta)/theta
         if (totalTime >= 1.D-12) then
            dstrainDotT = (1.D0/(theta*dt))* (dstrainT - dstrain0T)
         else
            dstrainDotT(1:3,1:3) = 0.D0
         end if 
         dstrainDotT = dstrainDotT - thetaCoef*dstrainDot0T
         
C       for Debugging
c$$$         dstrainDotT(1,1) = 100.D0
c$$$         dstrainDotT(1,2) = 25.D0
c$$$         dstrainDotT(1,3) = 11.D0
c$$$         dstrainDotT(2,1) = 25.D0  
c$$$         dstrainDotT(2,2) = -50.D0
c$$$         dstrainDotT(2,3) = 2.5D0
c$$$         dstrainDotT(3,1) = 11.D0
c$$$         dstrainDotT(3,2) = 2.5D0
c$$$         dstrainDotT(3,3) = -50.D0

         call tensor2voigt(dstrainDotT,dstrainDot,1)

C     start strain-rate rate loop
         sumTerm(:,:) = 0.D0
         do i = 1,size(n,1)
            ni(1,1:3) = n(i,1:3)
            niT = transpose(ni)
            strainDDot3x1 =  matmul(dstrainDotT,niT)
            strainDDotScalar =  matmul(ni,strainDDot3x1)
            nxn = matmul(niT,ni)
            Al3 = Ai(i)*Li(i)**3.D0
            sumTerm(:,:) = sumTerm(:,:) 
     1                    + Al3*strainDDotScalar(1,1)*nxn
         end do

C    volume of lattice
         volTerm = 0.D0
         do i = 1,size(n,1)
            volTerm = volTerm + Ai(i)*Li(i)
         end do 

         scalarTerm = (1.D0/3.D0)*rhoM/volTerm;

         sigMI(1:3,1:3) = scalarTerm*sumTerm

      else
         
         sigMI(1:3,1:3) = 0.D0

      end if ! end miFlag if
         

      print *, "sigMI ", sigMI
C-----------------------------------------------------------------------------
C                         Get stress
C-----------------------------------------------------------------------------
c$$$         sig(1) = stressOld(km,1) + dstress(1,1) !+ sigMI(1,1)
c$$$         sig(2) = stressOld(km,2) + dstress(2,1) !+ sigMI(2,2)
c$$$         sig(3) = stressOld(km,3) + dstress(3,1) !+ sigMI(3,3)
c$$$         sig(4) = stressOld(km,4) + dstress(4,1) !+ sigMI(1,2)  
c$$$         sig(5) = stressOld(km,5) + dstress(5,1) !+ sigMI(2,3)
c$$$         sig(6) = stressOld(km,6) + dstress(6,1) !+ sigMI(1,3)

         sig(1) =  stress(1,1) + sigMI(1,1)
         sig(2) =  stress(2,1) + sigMI(2,2)
         sig(3) =  stress(3,1) + sigMI(3,3)
         sig(4) =  stress(4,1) + sigMI(1,2)  
         sig(5) =  stress(5,1) + sigMI(2,3)
         sig(6) =  stress(6,1) + sigMI(1,3)        

         stressNew(km,1) = sig(1)
         stressNew(km,2) = sig(2)
         stressNew(km,3) = sig(3)
         stressNew(km,4) = sig(4)  
         stressNew(km,5) = sig(5) 
         stressnew(km,6) = sig(6)

         call getSige(sig,sige)

         stateNew(km,3) = sige

C         stateNew(km,4) = strainInc(km,1)
C         stateNew(km,5) = strainInc(km,2)
C         stateNew(km,6) = strainInc(km,3)
C         stateNew(km,7) = strainInc(km,4)
C         stateNew(km,8) = strainInc(km,5)
C         stateNew(km,9) = strainInc(km,6)

         stateNew(km,4) = strainIncP(1,1)
         stateNew(km,5) = strainIncP(2,1)
         stateNew(km,6) = strainIncP(3,1)
         stateNew(km,7) = strainIncP(4,1)
         stateNew(km,8) = strainIncP(5,1)
         stateNew(km,9) = strainIncP(6,1)

         stateNew(km,10) = dstrainDot(1,1)
         stateNew(km,11) = dstrainDot(1,2)
         stateNew(km,12) = dstrainDot(1,3)
         stateNew(km,13) = dstrainDot(1,4)
         stateNew(km,14) = dstrainDot(1,5)
         stateNew(km,15) = dstrainDot(1,6)

         if (sigMI(1,1) > 1.0D-2) then
C            print *, "------------------"
C            print *, "j = " , j
C            print *, "stress (end) ", stress
C            print *, "sigMI ", sigMI
C            print *, "total stress", sig
C            print *, "eeeeeeeeeeeeeeeeeee"
         end if


         stateNew(km,16) = sigMI(1,1)
         stateNew(km,17) = sigMI(2,2)
         stateNew(km,18) = sigMI(3,3)
         stateNew(km,19) = sigMI(1,2)
         stateNew(km,20) = sigMI(2,3)
         stateNew(km,21) = sigMI(1,3)
      enddo

      return
      end

      subroutine voigt2Tensor(voigt,tensor,type)
C        convert voigt notation to tensor
C        type = 1 = kinematic
C        type = 0 = kinetic
         implicit none
         integer type
         real*8 voigt(1,6), tensor(3,3)

         tensor(1,1) = voigt(1,1)
         tensor(2,2) = voigt(1,2)
         tensor(3,3) = voigt(1,3)
         if (type.eq.1) then
            tensor(1,2) = 0.5D0*voigt(1,4)
            tensor(2,3) = 0.5D0*voigt(1,5)
            tensor(3,1) = 0.5D0*voigt(1,6)         
         else
            tensor(1,2) = voigt(1,4)
            tensor(2,3) = voigt(1,5)
            tensor(3,1) = voigt(1,6)         
         end if
         tensor(2,1) = tensor(1,2)
         tensor(3,2) = tensor(2,3)
         tensor(1,3) = tensor(3,1)                  
      end

      subroutine tensor2Voigt(tensor,voigt,type)
C        convert voigt notation to tensor
C        type = 1 = kinematic
C        type = 0 = kinetic
         implicit none
         integer type
         real*8 voigt(1,6), tensor(3,3)

         voigt(1,1) = tensor(1,1)
         voigt(1,2) = tensor(2,2)
         voigt(1,3) = tensor(3,3)
         if (type.eq.1) then
            voigt(1,4) = 2.D0*tensor(1,2)
            voigt(1,5) = 2.D0*tensor(2,3)
            voigt(1,6) = 2.D0*tensor(3,1)        
         else
            voigt(1,4) = tensor(1,2)
            voigt(1,5) = tensor(2,3)
            voigt(1,6) = tensor(3,1)           
         end if                 
      end

      subroutine getEpsb(epsv,epsb)
C     calculate equivlent plastic strain epsb
      implicit none

      real*8 epsv(6),epsb

      epsb = epsv(1)*epsv(1) + epsv(2)*epsv(2) + epsv(3)*epsv(3)
     1 + 2.0*epsv(4)*epsv(4) + 2.0*epsv(5)*epsv(5) + 2.0*epsv(6)*epsv(6) 
      epsb = sqrt((2.D0/3.D0)*epsb)
      end subroutine getEpsb


      subroutine getSige(sigv,sige)
C     calculate equivlent plastic stress
      implicit none

      real*8 sigv(6),sige, s(6), tr

      tr = sigv(1) + sigv(2) + sigv(3)

      s = sigv

      s(1) = s(1)  - (1.D0/3.D0)*tr;
      s(2) = s(2)  - (1.D0/3.D0)*tr;
      s(3) = s(3)  - (1.D0/3.D0)*tr;

      sige = s(1)*s(1) + s(2)*s(2) + s(3)*s(3)
     1 + 2.0*s(4)*s(4) + 2.0*s(5)*s(5) + 2.0*s(6)*s(6) 
      sige = sqrt((3.D0/2.D0)*sige)

      end subroutine getSige

      subroutine invert(A,n,Ainv)

C A is an nxn matrix, n is the matrix dimension, Ainv is A**(-1)
C this subroutin uses Gaussian elimination to determine the inverse
C of an nxn square matrix
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C      include 'vaba_param.inc'
      implicit none

      INTEGER :: i,n,k,counter,j,g
      REAL*8 :: A(n,n),Ainv(n,n),Atemp(n,n),Id(n,n),den,mul

         Id = 0.
         do i=1,n
            Id(i,i) = 1.
         enddo

         Ainv = Id

         Atemp = A

         if (sum(A) .EQ. 0.) then
            Ainv = A;
         else
C zero out lower left diagonal
            do i=1,n
C set the ith column in the ith row equal to 1
               if (Atemp(i,i) .NE. 1.) then
                  if (Atemp(i,i) .NE. 0.) then          
                     den = Atemp(i,i)
                     Atemp(i,:) = Atemp(i,:)/den
                     Ainv(i,:) = Ainv(i,:)/den
                  else
                     if (i .EQ. n) then
                        stop 'matrix is singular'
                     endif
                     do k=i+1,n
                        if (Atemp(k,i) .NE. 0.) then
                           counter = k
                           exit
                        endif
                        if (k .EQ. n) then
                           stop 'matrix is singular'
                        endif
                     enddo
                     den = Atemp(counter,i)
                     Atemp(i,:) = Atemp(counter,:)/den + Atemp(i,:)
                     Ainv(i,:) = Ainv(counter,:)/den + Ainv(i,:)
                  endif
               endif
C set the ith column in all rows > i equal to 0
               do k=i+1,n
                  if (Atemp(k,i) .NE. 0.) then
                     mul = Atemp(k,i)
                     Atemp(k,:) = -mul*Atemp(i,:) + Atemp(k,:)
                     Ainv(k,:) = -mul*Ainv(i,:) + Ainv(k,:)
                  endif
               enddo
           enddo
C zero out upper right diagonal
           do i=2,n
              j = n - i + 2
              do k=1,j-1
                 g = j - k
                 if (Atemp(g,j) .NE. 0.) then
                    mul = Atemp(g,j)
                    Atemp(g,:) = -mul*Atemp(j,:) + Atemp(g,:)
                    Ainv(g,:) = -mul*Ainv(j,:) + Ainv(g,:)
                 endif
              enddo
           enddo
        endif

      return

      end subroutine invert
