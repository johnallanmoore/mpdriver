       program mpDriver

       implicit none

       integer i, Ninc

       integer  nprops, nblock, ndir, nshr
       integer  nstatev, nfieldv 
       real*8 dt,lanneal,stepTime,totalTime
       real*8 startTemp, totalStrain
       real*8 strainIncScalar, x
       character*80 cmname

       real*8, dimension (:), allocatable ::  props
       real*8, dimension (:), allocatable ::  density
       real*8, dimension (:,:), allocatable:: coordMp
       real*8, dimension (:), allocatable:: charLength
       real*8, dimension (:,:), allocatable:: strainInc
       real*8, dimension (:,:), allocatable:: relSpinInc
       real*8, dimension (:), allocatable::  tempOld
       real*8, dimension (:,:), allocatable:: stretchOld
       real*8, dimension (:,:), allocatable:: defgradOld
       real*8, dimension (:,:), allocatable:: fieldOld
       real*8, dimension (:,:), allocatable:: stressOld
       real*8, dimension (:,:), allocatable:: stateOld
       real*8, dimension (:), allocatable:: enerInternOld
       real*8, dimension (:), allocatable:: enerInelasOld
       real*8, dimension (:), allocatable::  tempNew
       real*8, dimension (:,:), allocatable:: stretchNew
       real*8, dimension (:,:), allocatable:: defgradNew
       real*8, dimension (:,:), allocatable:: fieldNew
       real*8, dimension (:,:), allocatable:: stressNew
       real*8, dimension (:,:), allocatable:: stateNew
       real*8, dimension (:), allocatable:: enerInternNew 
       real*8, dimension (:), allocatable:: enerInelasNew
       ! variables used in code but not in vumat
       real*8, dimension (:,:), allocatable:: strainState
       real*8, dimension (:,:), allocatable:: strainOld
       real*8, dimension (:,:), allocatable:: strainNew

!      nprops = User-specified number of user-defined material properties
!      nblock = Number of material points to be processed in this call to VUMAT
!      ndir = Number of direct components in a symmetric tensor
!      nshr = Number of indirect components in a symmetric tensor
!      nstatev = Number of user-defined state variables
!     nfieldv = Number of user-defined external field variables
!      lanneal = Flag indicating whether the routine is being called during an annealing process
!      stepTime = Value of time since the step began
!      totalTime = Value of total time
!      dt = Time increment size
!      cmname = User-specified material name, left justified
!      coordMp(nblock,*) Material point coordinates
!      charLength(nblock) Characteristic element length
!      props(nprops) User-supplied material properties
!      density(nblock) Current density at the material points 
!      strainInc (nblock, ndir+nshr) Strain increment tensor at each material point
!      relSpinInc (nblock, nshr) Incremental relative rotation vector
!      tempOld(nblock) Temperatures at beginning of the increment.
!      stretchOld (nblock, ndir+nshr) Stretch tensor, U
!      defgradOld (nblock,ndir+2*nshr) Deformation gradient tensor
!      fieldOld (nblock, nfieldv) Values of the user-defined field variables beginning of the increment
!      stressOld (nblock, ndir+nshr) Stress tensor at each material point at the beginning of the increment
!      stateOld (nblock, nstatev) State variables at each material point at the beginning of the increment.
!      enerInternOld (nblock) Internal energy per unit mass at each material point at the beginning of the increment.
!      enerInelasOld (nblock) Dissipated inelastic energy per unit mass at each material point at the beginning of the increment.
!      tempNew(nblock) Temperatures at each material point at the end of the increment.
!      stretchNew (nblock, ndir+nshr) Stretch tensor,U , at each material point at the end of the increment
!      defgradNew (nblock,ndir+2*nshr) tion gradient tensor  at the end of the increment
!      fieldNew (nblock, nfieldv) Values of the user-defined field variables  end of the increment.

!      Integer Inputs
       nblock = 1
       ndir = 3
       nshr = 3
       nstatev = 21
       nfieldv = 1
       nprops = 10
       lanneal = 0

! !     Dimension Reals
       allocate (props(nprops))
       allocate (density(nblock)) 
       allocate (coordMp(nblock,ndir))
       allocate (charLength(nblock))
       allocate (strainInc(nblock,ndir+nshr))
       allocate (relSpinInc(nblock,nshr))
       allocate (tempOld(nblock))
       allocate (stretchOld(nblock,ndir+nshr))
       allocate (defgradOld(nblock,ndir+nshr+nshr))
       allocate (fieldOld(nblock,nfieldv)) 
       allocate (stressOld(nblock,ndir+nshr))
       allocate (stateOld(nblock,nstatev))
       allocate (enerInternOld(nblock))
       allocate (enerInelasOld(nblock))
       allocate (tempNew(nblock))
       allocate (stretchNew(nblock,ndir+nshr))
       allocate (defgradNew(nblock,ndir+nshr+nshr))
       allocate (fieldNew(nblock,nfieldv))
       allocate (stressNew(nblock,ndir+nshr)) 
       allocate (stateNew(nblock,nstatev))
       allocate (enerInternNew(nblock))
       allocate (enerInelasNew(nblock))
       ! variables used in code but not in vumat
       allocate (strainState(nblock,ndir+nshr))
       allocate (strainOld(nblock,ndir+nshr))
       allocate (strainNew(nblock,ndir+nshr))

       !User Inputs
       open(unit=2,file="results.dat",recl=204)
       totalStrain = 0.2D0
       dt = 0.1D-3
       Ninc = 500
       charLength(nblock) = 1.D0 
       cmname = "Material-1"
       coordMP(nblock,1:ndir) = (/0.0D0,0.0D0,0.0D0  /)
       props(1:nprops) = (/1780.D0, 0.0032D0, 0.207D0, 1180.D0, 0.1D0, 0.75D0, 100.D0, 20.D0, &
            40.D0, 0.0D0/)
       density(nblock) = 0.000277D0
       ! abaqus stress convention: sig: 11, 22,33, 12, 23, 31
       !strainState(1,1:ndir+nshr) = (/1.D0, -0.5D0, -0.5D0, 0.D0, 0.D0, 0.D0/)
       strainState(1,1:ndir+nshr) = (/1.D0, -0.0D0, -0.0D0, 0.D0, 0.D0, 0.D0/)
       !Calculated values
       strainIncScalar = totalStrain/Ninc
       strainInc = strainIncScalar*strainState
      
       !intialize variables
       totalTime = 0.D0
       startTemp = 300.D0
       tempOld(nblock) = startTemp
       stressOld(nblock,1:ndir+nshr) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       stateOld(nblock,1:nstatev) = 0.D0 
       enerInternOld(nblock) = 0.D0
       enerInelasOld(nblock) = 0.D0

       strainOld(nblock,1:ndir+nshr) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)

       ! dummy values
       relSpinInc(nblock,1:nshr) = (/0.D0, 0.D0, 0.D0/)
       stretchOld(nblock,1:ndir+nshr) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       defgradOld(nblock,1:ndir+nshr+nshr)= (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0 ,0.D0, 0.D0, 0.D0/)
       fieldOld(nblock,1:nfieldv) = (/0.D0/)

       tempNew(nblock) = tempOld(nblock)
       stretchNew(nblock,1:ndir+nshr) = stretchOld(nblock,1:ndir+nshr)
       defgradNew(nblock,1:ndir+nshr+nshr) = defgradOld(nblock,1:ndir+nshr+nshr)
       fieldNew(nblock,1:nfieldv) = fieldOld(nblock,1:nfieldv)

       ! start strain increment loop
       do i = 1, Ninc

          x = real(i)/Ninc
          !print *, i
          !print *, "x ", x
          strainIncScalar = 2.D0*x*totalStrain/Ninc
          strainInc = strainIncScalar*strainState
          !print *, "strainIncScalar", strainIncScalar

          call vumat( &
          !Read only (unmodifiable) variables -
          nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,&
          stepTime, totalTime, dt, cmname, coordMp, charLength, &
          props, density, strainInc, relSpinInc,                &
          tempOld, stretchOld, defgradOld, fieldOld,            &
          stressOld, stateOld, enerInternOld, enerInelasOld,    &
          tempNew, stretchNew, defgradNew, fieldNew,            &
          ! Write only (modifiable) variables -
          stressNew, stateNew, enerInternNew, enerInelasNew )  

          ! time update
          totalTime = totalTime + dt
          ! update strain
          strainNew(nblock,1:ndir+nshr) = strainOld(nblock,1:ndir+nshr) + strainInc(nblock,1:ndir+nshr)
          strainOld(nblock,1:ndir+nshr) = strainNew(nblock,1:ndir+nshr)
          
          !write(2,*) totalTime,strainNew,stressNew
          !print *, totalTime
          !print *, strainNew
          !print *, stressNew

          write(2,"(E,$)") totalTime
          write(2,"(E,$)") strainNew(1,1), strainNew(1,2), strainNew(1,3)
          write(2,"(E,$)") strainNew(1,4), strainNew(1,5), strainNew(1,6)
          write(2,"(E,$)") stressNew(1,1), stressNew(1,2), stressNew(1,3)
          write(2,"(E,$)") stressNew(1,4), stressNew(1,5), stressNew(1,6)
          write(2,"(E)"  )

          ! changed in vumat
          stressOld(nblock,1:ndir+nshr) = stressNew(nblock,1:ndir+nshr)
          stateOld(nblock,1:nstatev) = stateNew(nblock,1:nstatev)
          enerInternOld(nblock) = enerInternNew(nblock) 
          enerInelasOld(nblock) =  enerInelasNew(nblock)

       end do ! end strain increment loop  
  
       close(2)

       end program mpDriver



