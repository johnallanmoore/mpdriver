C                                          _______
C                 |    |   |\  /|    /\       |
C                 |    |   | \/ |   /__\      |
C                 |____|   |    |  /    \     |
C
C  Updates stresses, Jacobin, state, and energy for 3D solid elements ONLY
C
C  
C     ISOTROPIC MATERIAL WITH J2 RADIAL RETURN PLASTICITY, SMALL STRAIN
C  
C  =====================================================================================
      subroutine umat (stress,  statev,  ddsdde,  sse,     spd,
     &                 scd,     rpl,     ddsddt,  drplde,  drpldt,
     &                 strain,  dstrain, time,    dtime,   temp,
     &                 dtemp,   predef,  dpred,   cmname,  ndi,
     &                 nshr,    ntens,   nstatv,  props,   nprops,
     &                 coords,  drot,    pnewdt,  celent,  dfgrd0,
     &                 dfgrd1,  noel,    npt,     layer,   kspt,
     &                 kstep,   kinc )
C
C      include 'vaba_param.inc'
C

      implicit none

      CHARACTER*80 CMNAME

      integer ntens,nstatv, nprops, ndi, nshr
      integer i,j ,k

      real*8 STRESS,STATEV,DDSDDE,SSE,SPD,SCD
      real*8 RPL,DDSDDT,DRPLDE,DRPLDT
      real*8 STRAIN,DSTRAIN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED
      real*8 PROPS,COORDS,DROT,PNEWDT
      real*8 CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,KSTEP,KINC
      real*8 dStress

      real*8 nxn(6,6), gamma, ddsddeElas(6,6), I6(6,6), IkronI(6,6)
      real*8 Ivect(6), bulk, Idev(6,6), beta

      DIMENSION STRESS(NTENS),STATEV(NSTATV),
     1 DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS),
     2 STRAIN(NTENS),DSTRAIN(NTENS),TIME(2),PREDEF(1),DPRED(1),
     3 PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3),
     4 dstress(NTENS)
     
      real*8 E,nu, lame1, G, C11, C12, C44, Y, H, p, J2, VMT
      real*8 S(3,3), f, del_del_lambda, del_lambda, VM, S_mag
      real*8 n_hat(6), strain_new(6), trial_stress(6)

C-----------------------------------------------------------------------
C     Defining the elastic modulus and poisson ratio
C     as well as the yield strength
C-----------------------------------------------------------------------
      E      = props(1)
      nu     = props(2)
      Y      = props(3)
      H      = 0.D0
C-----------------------------------------------------------------------
C    Calculating the lame parameters for the tangent stiffness matrix
C         Lame constant 1 (lambda)
C         Lame constant 2 (G=mu)
C-----------------------------------------------------------------------
      lame1 = E*nu/((1.D0+nu)*(1.D0-2.D0*nu))
      G     = E/(2.D0*(1.D0+nu))
      bulk  = E/(3.D0*(1.D0 - 2.D0*nu))
C-----------------------------------------------------------------------
C     Assigning values to variables to be put into the stiffness matrix
C-----------------------------------------------------------------------
      C11 = lame1 + 2.D0*G
      C12 = lame1
      C44 = G
C-----------------------------------------------------------------------
C     Assembling the transversely isotropic material stiffness matrix
C-----------------------------------------------------------------------
      ddsddeElas(1,1) = C11 
      ddsddeElas(1,2) = C12  
      ddsddeElas(1,3) = C12
      ddsddeElas(1,4) = 0.D0  
      ddsddeElas(1,5) = 0.D0 
      ddsddeElas(1,6) = 0.D0
C
      ddsddeElas(2,2) = C11 
      ddsddeElas(2,3) = C12
      ddsddeElas(2,4) = 0.D0 
      ddsddeElas(2,5) = 0.D0
      ddsddeElas(2,6) = 0.D0
C  
      ddsddeElas(3,3) = C11
      ddsddeElas(3,4) = 0.D0  
      ddsddeElas(3,5) = 0.D0
      ddsddeElas(3,6) = 0.D0
C
      ddsddeElas(4,4) = C44  
      ddsddeElas(4,5) = 0.D0
      ddsddeElas(4,6) = 0.D0
C   
      ddsddeElas(5,5) = C44
      ddsddeElas(5,6) = 0.D0
C
      ddsddeElas(6,6) = C44

C-----------------------------------------------------------------------
C     The stiffness matrix is symmetric
C-----------------------------------------------------------------------
      ddsddeElas(2,1) = ddsddeElas(1,2)  
      ddsddeElas(3,1) = ddsddeElas(1,3)
      ddsddeElas(4,1) = ddsddeElas(1,4)
      ddsddeElas(5,1) = ddsddeElas(1,5)
      ddsddeElas(6,1) = ddsddeElas(1,6)

      ddsddeElas(3,2) = ddsddeElas(2,3)
      ddsddeElas(4,2) = ddsddeElas(2,4)
      ddsddeElas(5,2) = ddsddeElas(2,5)
      ddsddeElas(6,2) = ddsddeElas(2,6)
   
      ddsddeElas(4,3) = ddsddeElas(3,4)  
      ddsddeElas(5,3) = ddsddeElas(3,5)
      ddsddeElas(6,3) = ddsddeElas(3,6)

      ddsddeElas(5,4) = ddsddeElas(4,5)
      ddsddeElas(6,4) = ddsddeElas(4,6)
   
      ddsddeElas(5,6) = ddsddeElas(6,5)


C-----------------------------------------------------------------------
C     Calculating the incremental stress (dStress) done by multiplying the
C     stiffness matrix (DDSDDE) by the strain vector supplied from ABAQUS
C-----------------------------------------------------------------------
      dStress = matmul(ddsddeElas,dstrain)    
C-----------------------------------------------------------------------
C     Calculating and storing the stress
C-----------------------------------------------------------------------
      trial_stress(1:ntens) = stress(1:ntens) + dStress(1:ntens)
C-----------------------------------------------------------------------
C     The material yields when it reaches the yield surface (use von Mises)
C     This method is taken from :Nonlinear Finite Elements for Continua and 
C     Structures Second Edition ISBN 978-1-118-63270-3
C     Logic used is from Box 5.14 Radial Return Method from the text
C
C     Can compute the volumetric part of the stress tensor
C     This is usually multiplied by the kronecker delta (identity matrix),
C     but it is easier to leave it as a scalar and move on to the next step
C-----------------------------------------------------------------------
      p=(1.D0/3.D0)*(trial_stress(1)+trial_stress(2)+trial_stress(3))
C-----------------------------------------------------------------------
C     Can now compute the deviatoric stress tensor (which is symmetric)
C-----------------------------------------------------------------------
      S(1,1)=trial_stress(1) - p
      S(2,2)=trial_stress(2) - p
      S(3,3)=trial_stress(3) - p
      S(1,2)=trial_stress(4)
      S(1,3)=trial_stress(5)
      S(2,1)=trial_stress(4)
      S(2,3)=trial_stress(6)
      S(3,1)=trial_stress(5)
      S(3,2)=trial_stress(6)
C-----------------------------------------------------------------------     
C     Can now calculate the second invariant of the stress tensor (J2)
C-----------------------------------------------------------------------
      J2=(1.D0/2.D0)*(S(1,1)**2.D0+S(2,2)**2.D0+S(3,3)**2.D0
     &   +2.D0*S(1,2)**2.D0+2.D0*S(1,3)**2.D0+2.D0*S(2,3)**2.D0)
C-----------------------------------------------------------------------
C     Using J2, can calculate the equivalent (von Mises) trial stress
C-----------------------------------------------------------------------
      VMT=sqrt(3.D0*J2)
C-----------------------------------------------------------------------
C     Need to figure out if the material is actually yielding by
C     comparing the calculated equivalent stress to the yield criterion
C     The yield criterion is just the static yield for this particular UMAT
C     Could use a hardening law (such as Holloman, ludwik, Voce, etc) to 
C     calculate the flow stress as well.
C-----------------------------------------------------------------------     
      del_lambda = 0.D0
      f = VMT - Y
      IF(f .GE. 10.D0**-7.D0) THEN
C-----------------------------------------------------------------------
C        Solving for the delta_lambda to figure out the plastic strain that is
C        required to get the stress back ON TO (no inside or outside, but ON) 
C        the yield surface. This is a nonlinear solve for delta_lambda which
C        should converge in a fairly small amount of steps (k)
C-----------------------------------------------------------------------      
         do k=1, 50
C-----------------------------------------------------------------------      
C        Compute the increment in plasticity parameter
C-----------------------------------------------------------------------
         del_del_lambda=f/(3.D0*G+H)
C-----------------------------------------------------------------------      
C        Computing the increment in plastic strain
C-----------------------------------------------------------------------
         del_lambda=del_lambda+del_del_lambda
C-----------------------------------------------------------------------
C        ----Perfect Plasticity----
C        No update to yield surface
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C        Calculating the equivalent von mises stress by taking the trial
C        stress and subtracting off the plastic stress
C-----------------------------------------------------------------------
         VM=VMT-del_lambda*3.D0*G
C-----------------------------------------------------------------------     
C        Checking the yield criterion again
C-----------------------------------------------------------------------
         f=abs(VM-Y)
         IF(f .LE. 10.D0**-7.D0) THEN
C-----------------------------------------------------------------------
C        Now need to update the stress when the plastic stress has been found
C        Need to find the unit normal vector in the radial direction of
C        plastic flow (n_hat)
C----------------------------------------------------------------------- 
         S_mag = sqrt(S(1,1)**2.D0+S(2,2)**2.D0+S(3,3)**2.D0
     &           +2.D0*S(1,2)**2.D0+2.D0*S(1,3)**2.D0
     &           +2.D0*S(2,3)**2.D0)
         n_hat(1) = S(1,1)/S_mag
         n_hat(2) = S(2,2)/S_mag
         n_hat(3) = S(3,3)/S_mag
         n_hat(4) = S(1,2)/S_mag
         n_hat(5) = S(2,3)/S_mag
         n_hat(6) = S(3,1)/S_mag

         call kron(n_hat,n_hat,6,nxn)

         Ivect = 0.D0

         do i=1,3
            Ivect(i) = 1.D0
         enddo

         I6 = 0.D0
         do i=1,6
            I6(i,i) = 1.D0
         enddo

         call kron(Ivect,Ivect,6,IkronI)

         Idev = I6 - (1.D0/3.D0)*IkronI

         beta = VM/(VM + 3.D0*G*del_lambda)

         gamma = 1.D0/(1.D0 + (H/(3.D0*G)))
         gamma = gamma - (1.D0 - beta)

C         DDSDDE = DDSDDEelas - 2*G*gamma*nxn
C         DDSDDE = lame1*IkronI + 2.D0*G*I6 - 2*G*gamma*nxn
         do i = 1,6
            do j = 1,6
               DDSDDE(i,j) = bulk*IkronI(i,j) + 
     &                  2.D0*G*beta*Idev(i,j) - 
     &                  2.D0*G*gamma*nxn(i,j)
            end do
         end do
C-----------------------------------------------------------------------
C        Calculating and storing the stress
C-----------------------------------------------------------------------
         stress(1) = trial_stress(1) - (2.D0)*G*del_lambda
     &        *sqrt(3.D0/2.D0)*n_hat(1)
         stress(2) = trial_stress(2) - (2.D0)*G*del_lambda
     &        *sqrt(3.D0/2.D0)*n_hat(2)
         stress(3) = trial_stress(3) - (2.D0)*G*del_lambda
     &        *sqrt(3.D0/2.D0)*n_hat(3)
         stress(4) = trial_stress(4) - (2.D0)*G*del_lambda
     &        *sqrt(3.D0/2.D0)*n_hat(4)
         stress(5) = trial_stress(5) - (2.D0)*G*del_lambda
     &        *sqrt(3.D0/2.D0)*n_hat(5)
         stress(6) = trial_stress(6) - (2.D0)*G*del_lambda
     &        *sqrt(3.D0/2.D0)*n_hat(6)
         GO TO 18
      ENDIF   
      ENDDO
      ELSE
C-----------------------------------------------------------------------
C        if the material is not actively yielding there is no plastic strain. 
C        Therefore the trial stress is the stress.
C-----------------------------------------------------------------------
         stress(1:ntens) = trial_stress(1:ntens)
         ddsdde = ddsddeElas
      ENDIF
 18      CONTINUE
      return
      end

      subroutine kron(a,b,n,C)

C this subroutine takes the kronecker product
C of two vectors a and b and gives back an nxn
C matrix C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C      include 'vaba_param.inc'

      implicit none

      INTEGER :: n,i,j
      REAL*8 :: a(n),b(n),C(n,n)

         do i=1,n
            do j=1,n
               C(i,j) = a(i)*b(j)
            enddo
         enddo
   
      return
 
      end subroutine kron
