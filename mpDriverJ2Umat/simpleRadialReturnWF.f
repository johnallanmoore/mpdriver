C                                          _______
C                 |    |   |\  /|    /\       |
C                 |    |   | \/ |   /__\      |
C                 |____|   |    |  /    \     |
C
C  Updates stresses, Jacobin, state, and energy for 3D solid elements ONLY
C
C  
C     ISOTROPIC MATERIAL WITH J2 RADIAL RETURN PLASTICITY, SMALL STRAIN
C  
C  =====================================================================================
      subroutine umat (stress,  statev,  ddsdde,  sse,     spd,
     &                 scd,     rpl,     ddsddt,  drplde,  drpldt,
     &                 strain,  dstrain, time,    dtime,   temp,
     &                 dtemp,   predef,  dpred,   cmname,  ndi,
     &                 nshr,    ntens,   nstatv,  props,   nprops,
     &                 coords,  drot,    pnewdt,  celent,  dfgrd0,
     &                 dfgrd1,  noel,    npt,     layer,   kspt,
     &                 kstep,   kinc )
C
C      include 'vaba_param.inc'
C

      implicit none

      CHARACTER*80 CMNAME

      integer ntens,nstatv, nprops, ndi, nshr
      integer i,j ,k, ii, jj

      real*8 STRESS,STATEV,DDSDDE,SSE,SPD,SCD
      real*8 RPL,DDSDDT,DRPLDE,DRPLDT
      real*8 STRAIN,DSTRAIN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED
      real*8 PROPS,COORDS,DROT,PNEWDT
      real*8 CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,KSTEP,KINC

      real*8 nxn(6,6), gamma, ddsddeElas(6,6), I1(6,6), I2(6,6)
      real*8 bulk, Idev(6,6), beta, pre, gammab

      DIMENSION STRESS(NTENS),STATEV(NSTATV),
     1 DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS),
     2 STRAIN(NTENS),DSTRAIN(NTENS),TIME(2),PREDEF(1),DPRED(1),
     3 PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3)
     
      real*8 E,nu, lame1, mu, C11, C12, C44, sy, H, press, epsbd
      real*8 s(6), stressT(6), sigeT, smag,  depsbd
      real*8 nhat(6), FT(3,3), strainMat(3,3), strainGL(6), detJ
      real*8 PK2(3,3), cauchy(3,3)

C-----------------------------------------------------------------------
C     Defining the elastic modulus and poisson ratio
C     as well as the yield strength
C-----------------------------------------------------------------------
      E      = props(1)
      nu     = props(2)
      sy     = props(3)
C-----------------------------------------------------------------------
C    Calculating the lame parameters for the tangent stiffness matrix
C         Lame constant 1 (lambda)
C         Lame constant 2 (G=mu)
C-----------------------------------------------------------------------
      lame1 = E*nu/((1.D0+nu)*(1.D0-2.D0*nu))
      mu    = E/(2.D0*(1.D0+nu))
      bulk  = E/(3.D0*(1.D0 - 2.D0*nu))
C-----------------------------------------------------------------------
C     Assigning values to variables to be put into the stiffness matrix
C-----------------------------------------------------------------------
      C11 = lame1 + 2.D0*mu
      C12 = lame1
      C44 = mu
C-----------------------------------------------------------------------
C     Assembling the transversely isotropic material stiffness matrix
C-----------------------------------------------------------------------
      ddsddeElas(1,1) = C11 
      ddsddeElas(1,2) = C12  
      ddsddeElas(1,3) = C12
      ddsddeElas(1,4) = 0.D0  
      ddsddeElas(1,5) = 0.D0 
      ddsddeElas(1,6) = 0.D0
C
      ddsddeElas(2,2) = C11 
      ddsddeElas(2,3) = C12
      ddsddeElas(2,4) = 0.D0 
      ddsddeElas(2,5) = 0.D0
      ddsddeElas(2,6) = 0.D0
C  
      ddsddeElas(3,3) = C11
      ddsddeElas(3,4) = 0.D0  
      ddsddeElas(3,5) = 0.D0
      ddsddeElas(3,6) = 0.D0
C
      ddsddeElas(4,4) = C44  
      ddsddeElas(4,5) = 0.D0
      ddsddeElas(4,6) = 0.D0
C   
      ddsddeElas(5,5) = C44
      ddsddeElas(5,6) = 0.D0
C
      ddsddeElas(6,6) = C44

C-----------------------------------------------------------------------
C     The stiffness matrix is symmetric
C-----------------------------------------------------------------------
      ddsddeElas(2,1) = ddsddeElas(1,2)  
      ddsddeElas(3,1) = ddsddeElas(1,3)
      ddsddeElas(4,1) = ddsddeElas(1,4)
      ddsddeElas(5,1) = ddsddeElas(1,5)
      ddsddeElas(6,1) = ddsddeElas(1,6)

      ddsddeElas(3,2) = ddsddeElas(2,3)
      ddsddeElas(4,2) = ddsddeElas(2,4)
      ddsddeElas(5,2) = ddsddeElas(2,5)
      ddsddeElas(6,2) = ddsddeElas(2,6)
   
      ddsddeElas(4,3) = ddsddeElas(3,4)  
      ddsddeElas(5,3) = ddsddeElas(3,5)
      ddsddeElas(6,3) = ddsddeElas(3,6)

      ddsddeElas(5,4) = ddsddeElas(4,5)
      ddsddeElas(6,4) = ddsddeElas(4,6)
   
      ddsddeElas(5,6) = ddsddeElas(6,5)

C-----------------------------------------------------------------------
C     Green Lagrange Strain from Gradient
C-----------------------------------------------------------------------
      FT = transpose(DFGRD1)
      strainMat = MATMUL(DFGRD1,FT)
      strainGL(1) = 0.5D0*(strainMat(1,1) - 1.D0)
      strainGL(2) = 0.5D0*(strainMat(2,2) - 1.D0)
      strainGL(3) = 0.5D0*(strainMat(2,2) - 1.D0)
      strainGL(4) = 0.5D0*2.D0*strainMat(1,2)
      strainGL(5) = 0.5D0*2.D0*strainMat(1,3)
      strainGL(6) = 0.5D0*2.D0*strainMat(2,3)
C-----------------------------------------------------------------------
C     Trial Stress
C-----------------------------------------------------------------------
      stressT = matmul(ddsddeElas,strainGL)    


c$$$      PK2(1,1) = stressT(1)
c$$$      PK2(2,2) = stressT(2)
c$$$      PK2(3,3) = stressT(3)
c$$$      PK2(1,2) = stressT(4)
c$$$      PK2(1,3) = stressT(5)
c$$$      PK2(2,3) = stressT(6)
c$$$      
c$$$      PK2(2,1) = PK2(1,2)
c$$$      PK2(3,1) = PK2(1,3)
c$$$      PK2(3,2) = PK2(2,3)
c$$$
c$$$      cauchy = MATMUL(PK2,FT)
c$$$      cauchy = MATMUL(dfgrd1,cauchy)
c$$$
c$$$      call det3x3(detJ,dfgrd1)
c$$$
c$$$      stressT(1) = cauchy(1,1)/detJ
c$$$      stressT(2) = cauchy(2,2)/detJ
c$$$      stressT(3) = cauchy(3,3)/detJ
c$$$      stressT(4) = cauchy(1,2)/detJ
c$$$      stressT(5) = cauchy(1,3)/detJ
c$$$      stressT(6) = cauchy(2,3)/detJ



C-----------------------------------------------------------------------
C     von Mises trial Stress
C-----------------------------------------------------------------------
      call vonMises(sigeT,s,stressT)
C-----------------------------------------------------------------------
C     return direction unit normal
C-----------------------------------------------------------------------
      call mag(smag,s)
      nhat(1) = s(1)/smag
      nhat(2) = s(2)/smag
      nhat(3) = s(3)/smag
      nhat(4) = s(4)/smag
      nhat(5) = s(5)/smag
      nhat(6) = s(6)/smag
C-----------------------------------------------------------------------
C     check yield
C-----------------------------------------------------------------------
      if ((sigeT-sy).le.0.D0) then
         stress = stressT
         ddsdde = ddsddeElas
         epsbd = 0.D0
      else
         epsbd = statev(1)
C        plastic multipler incrment
         depsbd = ((sigeT - 3.D0*mu*epsbd) - sy)/(3.D0*mu)
C        plastic multiplier
         epsbd = epsbd + depsbd
C        deviatoris stress
         pre = sqrt(2.D0/3.D0)*sigeT - 2*mu*epsbd*sqrt(3.D0/2.D0)
         s(1) = pre*nhat(1)
         s(2) = pre*nhat(2)
         s(3) = pre*nhat(3)
         s(4) = pre*nhat(4)
         s(5) = pre*nhat(5)
         s(6) = pre*nhat(6)
C        hydro static stress
         press = (1.D0/3.D0)*(stressT(1) + stressT(2) + stressT(3))
C        stress update
         stress(1) = s(1) + press
         stress(2) = s(2) + press
         stress(3) = s(3) + press
         stress(4) = s(4) 
         stress(5) = s(5) 
         stress(6) = s(6) 
C        algorithmic modulus
         I1(:,:) = 0.D0
         do ii = 1,3
            do jj = 1,3
               I1(ii,jj) = 1.D0
            enddo
         enddo

         I2(:,:) = 0.D0
         I2(1,1) = 2.D0
         I2(2,2) = 2.D0
         I2(3,3) = 2.D0
         I2(4,4) = 1.D0
         I2(5,5) = 1.D0
         I2(6,6) = 1.D0
         I2 = 0.5D0*I2

         Idev = I2 - (1.D0/3.D0)*I1

         gamma = 1.D0

         beta = sy/sigeT

         gammab = gamma - (1 - beta)

         call kron(nhat,nhat,6,nxn)

         ddsdde = bulk*I1 + 2.D0*mu*beta*Idev - 2.D0*mu*gammab*nxn

      endif
      statev(1) = epsbd
      


      

      return
      end




C-----------------------------------------------------------------------
C        von Mises Stress from Stress Vector
C-----------------------------------------------------------------------
      subroutine vonMises(sige,s, stress)

      implicit none

      INTEGER :: n,i,j
      REAL*8 :: stress(6), sige, s(6), dd, press

      press = (1.D0/3.D0)*( stress(1) + stress(2) + stress(3))
      s = stress
      s(1) = stress(1) - press
      s(2) = stress(2) - press
      s(3) = stress(3) - press

      call doubleDot(dd,s)

      sige = sqrt((3.D0/2.D0)*dd)

      return
 
      end subroutine vonMises


C-----------------------------------------------------------------------
C        tensor double dot
C-----------------------------------------------------------------------
      subroutine doubleDot(dd,a)

      implicit none

      INTEGER :: n,i,j
      REAL*8 :: dd,a(6)

      dd = a(1)*a(1) + a(2)*a(2) + a(3)*a(3) + 2.D0*a(4)*a(4)
     1   + 2.D0*a(5)*a(5) + 2.D0*a(6)*a(6)

      return
 
      end subroutine doubleDot

C-----------------------------------------------------------------------
C        magnitude of tensor
C-----------------------------------------------------------------------
      subroutine mag(m,a)

      implicit none

      INTEGER :: n,i,j
      REAL*8 :: m,a(6)

      call doubleDot(m,a)
      m = sqrt(m)

      return
 
      end subroutine mag


      subroutine kron(a,b,n,C)

C this subroutine takes the kronecker product
C of two vectors a and b and gives back an nxn
C matrix C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C      include 'vaba_param.inc'

      implicit none

      INTEGER :: n,i,j
      REAL*8 :: a(n),b(n),C(n,n)

         do i=1,n
            do j=1,n
               C(i,j) = a(i)*b(j)
            enddo
         enddo
   
      return
 
      end subroutine kron

C-----------------------------------------------------------------------
C        determinant of 3x3
C-----------------------------------------------------------------------
      subroutine det3x3(d,Amat)

      implicit none

      REAL*8 :: Amat(3,3)
      REAL*8 :: a,b,c,d,e,f,g,h,i
      
      a = Amat(1,1)
      b = Amat(1,2)
      c = Amat(1,3)
      d = Amat(2,1)
      e = Amat(2,2)
      f = Amat(2,3)
      g = Amat(3,1)
      h = Amat(3,2)
      i = Amat(3,3)

      d = a*(e*i-h*f) - b*(d*i-g*f) + c*(d*h - g*e)

      return
 
      end subroutine det3x3
