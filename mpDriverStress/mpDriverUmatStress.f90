       program mpDriver

       implicit none

       integer i, Ninc, fDrive, j

       integer ntens,nstatev, nprops, ndir, nshr
       integer kinc, kspt, kstep, layer, noel, npt
       
       real*8 coords(3), dfgrd0(3,3), dfgrd1(3,3)
       real*8 dpred(1), drot(3,3), predef(1), time(2)
       real*8 celent, dtime, pnewdt, dtemp, rpl
       real*8 scd, spd, sse, temp
       ! dummy values that may have different dimensions
       ! but that probably wont get used
       real*8 drpldt

       real*8 startTemp
       real*8 stressIncScalarX, stressIncScalarY, stressIncScalarZ
       real*8 stressIncScalarXY, stressIncScalarXZ, stressIncScalarYZ
       real*8 totalStressX, totalStressY, totalStressZ
       real*8 totalStressXY, totalStressXZ, totalStressYZ

       real*8 fMax(3,3), Emat(3,3), df(3,3)

       character*80 cmname

       real*8, dimension (:,:), allocatable :: ddsdde 
       real*8, dimension (:), allocatable :: ddsddt 
       real*8, dimension (:), allocatable :: drplde
       real*8, dimension (:), allocatable :: dstrain 
       real*8, dimension (:), allocatable :: props 
       real*8, dimension (:), allocatable :: statev 
       real*8, dimension (:), allocatable :: strain
       real*8, dimension (:), allocatable :: stran
       real*8, dimension (:), allocatable :: stress
 
       ! variables used in code but not in umat
       real*8, dimension (:), allocatable:: strainState

       ! coords,        !  coordinates of Gauss pt. being evaluated
       ! ddsdde,        ! Tangent Stiffness Matrix
       ! ddsddt,	! Change in stress per change in temperature
       ! dfgrd0,	! Deformation gradient at beginning of step
       ! dfgrd1,	! Deformation gradient at end of step
       ! dpred,	        ! Change in predefined state variables
       ! drplde,	! Change in heat generation per change in strain
       ! drot,	        ! Rotation matrix
       ! dstrain,	! Strain increment tensor stored in vector form
       ! predef,	! Predefined state vars dependent on field variables
       ! props,	        ! Material properties passed in
       ! statev,	! State Variables
       ! strain,	! Strain tensor stored in vector form
       ! stress,	! Cauchy stress tensor stored in vector form
       ! time		! Step Time and Total Time
       ! celent         ! Characteristic element length
       ! drpldt         ! Variation of RPL w.r.t temp.
       ! dtemp          ! Increment of temperature
       ! dtime          ! Increment of time
       ! kinc           !Increment number
       ! kspt           ! Section point # in current layer
       ! kstep          ! Step number
       ! layer          ! layer number
       ! noel           ! element number
       ! npt            ! Integration point number
       ! pnewdt         ! Ratio of suggested new time increment/time increment
       ! rpl            ! Volumetric heat generation
       ! scd            ! “creep” dissipation
       ! spd            ! plastic dissipation
       ! sse            ! elastic strain energy
       ! temp           ! temperature

!      Integer Inputs
       ndir = 3
       nshr = 3
       ntens = ndir + nshr
       nstatev = 300
       nprops = 27

! !     Dimension Reals
       allocate (ddsdde(ntens,ntens) )
       allocate (ddsddt(ntens) )
       allocate (drplde(ntens) )
       allocate (dstrain(ntens) )
       allocate (props(nprops) )
       allocate (statev(nstatev) )
       allocate (strain(ntens) )
       allocate (stran(ntens) )
       allocate (stress(ntens) )

       ! variables used in code but not in umat
       allocate (strainState(ntens))

       !User Inputs
       open(unit=2,file="results.dat",recl=204)
       !totalStrainX = 0.1D0
       !totalStrainY = -0.0D0
       !totalStrainZ = -0.0D0

       ! stress similar to element 2d
       !totalStressX  = 0.6336523285135445E+03
       !totalStressX = 0.5417316332775515E+03
       totalStressX = 500.000973982845
       !totalStressY  = 0.4268623565408842E+02
       !totalStressY  = 0.1283653357213246E+03
       totalStressY  = 88.2397138697507 
       !totalStressZ  = 0.1965275147314812E+03
       !totalStressZ  = 0.1948629372009245E+03
       totalStressZ  = 171.232532489677
       totalStressXY = 0.0
       totalStressXZ = 0.0
       totalStressYZ = 0.0

       !Single step
       totalStressX = 539.539044897622
       totalStressY  = 122.558904378318 
       totalStressZ  =  231.342948090321     




       dtime = 0.001

       !C11A(1), C12A(2), C44A(3), C11M(4), C12M(5), C44M(6), ALPHAA(7), ALPHAM(8)
       !TRANSRESIS(9), GAMMA0(10), THETA_TRANS(11), THETA_REF_LOW(12), HEAT_LATENT(13), 14, 15, 16
       !GDOT0(17), AM(18), S0(19), H0(20), SS(21), AHARD(22),QL(23),THETA_REF_HIGH(24)
       props(1:nprops) = (/143000.0,107800.0,37400.0,71500.0,53900.0,18700.0, 1.1e-05, 6.6e-06, &
8.5,  0.1,    257.,    277.,    130., 143000.0,107800.0, 37400.0, &
0.002,    0.02,    210.,   500.,   900.,     0.125,     1.4,    277., &
            45.,0.,0.     /)

       !for test
       !props(1:nprops) = (/200e3,  0.3/) 

       cmname = "Material-1"
       Ninc = 10000
       coords(1:ndir) = (/0.0D0,0.0D0,0.0D0/)
       
       stressIncScalarX = totalStressX/Ninc
       stressIncScalarY = totalStressY/Ninc
       stressIncScalarZ = totalStressZ/Ninc
       stressIncScalarXY = totalStressXY/Ninc
       stressIncScalarXZ = totalStressXZ/Ninc
       stressIncScalarYZ = totalStressYZ/Ninc
       
       dstrain(1:3) = (/0.D0, 0.D0,0.D0 /)
       
       ! initalize varibles
       kinc = 1
       kspt = 1
       kstep = 1
       layer = 1
       noel = 1
       npt = 1

       !intialize variables
       startTemp = 300.D0
       temp = startTemp
       stress(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       do i = 1, nstatev
          statev(i) =  0.D0
       end do
       ddsdde(:,:) = 0.D0
       sse = 0.D0
       spd = 0.D0
       scd = 0.D0
       rpl = 0.D0
       ddsddt(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       drplde(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       drpldt = 0.D0
       strain(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       time = 0.D0
       dtemp = 0.D0
       dpred = 0.D0
       drot(:,:) = 0.D0
       pnewdt = 1.D0
       celent = 1.D0

       ! dummy
       dfgrd0(1,1:3) = (/1.D0, 0.D0, 0.D0/)
       dfgrd0(2,1:3) = (/0.D0, 1.D0, 0.D0/)
       dfgrd0(3,1:3) = (/0.D0, 0.D0, 1.D0/)

       dfgrd1 = dfgrd0
       
       do i = 1, Ninc
          kinc = i
          kstep = kinc


          ! MIDAS strain convention (but I use different Voigt)
          stress(1) = stress(1) + stressIncScalarX
          stress(2) = stress(2) + stressIncScalarY
          stress(3) = stress(3) + stressIncScalarZ
          stress(4) = stress(4) + stressIncScalarXY
          stress(5) = stress(5) + stressIncScalarXZ
          stress(6) = stress(6) + stressIncScalarYZ


          stran = strain

          call umat( &
               stress,  statev,  ddsdde,  sse,     spd, &
               scd,     rpl,     ddsddt,  drplde,  drpldt, &
               stran,  dstrain, time,    dtime,   temp, &
               dtemp,   predef,  dpred,   cmname,  ndir, &
               nshr,    ntens,   nstatev,  props,   nprops, &
               coords,  drot,    pnewdt,  celent,  dfgrd0, &
               dfgrd1,  noel,    npt,     layer,   kspt, &
               kstep,   kinc )
          strain = stran
          time  = time + dtime

          write(2,"(E,$)") time(1)
          write(2,"(E,$)") strain(1), strain(2), strain(3)
          write(2,"(E,$)") strain(4), strain(5), strain(6)
          write(2,"(E,$)") stress(1), stress(2), stress(3)
          write(2,"(E,$)") stress(4), stress(5), stress(6)
          write(2,"(E,$)") statev(202),statev(300), statev(134)
          write(2,"(E,$)") statev(299), statev(298)
          write(2,"(E)"  )  
          !print *, time
          !print *, strain
          !print *, stress
          !print *, "statev(79)", statev(79)
       end do ! end stress increment loop  

       end program mpDriver



