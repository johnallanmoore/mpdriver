#!/usr/local/bin/python3

import numpy as np
import matplotlib.pyplot as plt

resultsFileVec = ['results.dat','resultsStrainDrivenNoHard.dat']
colors = ['k-','r--']

for j in range(len(resultsFileVec)):
    resultsFile = resultsFileVec[j]
    data = np.loadtxt(resultsFile)

    time = data[:,0]

    strainV = data[:,1:7]
    stressV = data[:,7:13]
        
    stateX = data[:,13]

    tau0  = data[:,14]

    Fp11  = data[:,15]
    deltagamma1  = data[:,16]
    tau1  = data[:,17]

    strain11 = data[:,1]
    stress11 = data[:,7]

    sige = np.zeros(len(time))

    for i in range(len(time)):
        stressTensor = np.zeros((3,3))
        stressTensor[0,0] = stressV[i,0]# 11
        stressTensor[1,1] = stressV[i,1]# 22
        stressTensor[2,2] = stressV[i,2]# 33
        stressTensor[0,1] = stressV[i,3]# 12
        stressTensor[0,2] = stressV[i,4]# 13
        stressTensor[1,2] = stressV[i,5]# 23
        stressTensor[1,0] = stressTensor[0,1]
        stressTensor[1,2] = stressTensor[2,1]
        stressTensor[0,2] = stressTensor[2,0]

        strainTensor = np.zeros((3,3))
        strainTensor[0,0] = strainV[i,0]# 11
        strainTensor[1,1] = strainV[i,1]# 22
        strainTensor[2,2] = strainV[i,2]# 33
        strainTensor[0,1] = strainV[i,3]# 12
        strainTensor[0,2] = strainV[i,4]# 13
        strainTensor[1,2] = strainV[i,5]# 23
        strainTensor[1,0] = strainTensor[0,1]
        strainTensor[1,2] = strainTensor[2,1]
        strainTensor[0,2] = strainTensor[2,0]
        p = (1./3.)*(stressV[i,0] + stressV[i,1] + stressV[i,2])
        s = stressTensor
        s[0,0] = stressTensor[0,0] - p
        s[1,1] = stressTensor[1,1] - p
        s[2,2] = stressTensor[2,2] - p
        sige[i] = np.sqrt((3./2.)*np.tensordot(s,s,axes=2))
        
    
    #plt.plot(strain11,sige,colors[j])
    
    plt.figure(1)
    plt.plot(strain11,stateX,colors[j])
    plt.xlabel('Total Strain')
    plt.ylabel('Plastic Strain')
    plt.legend(['Stress Driven','Strain Driven'])
    plt.figure(2)
    plt.plot(strain11,stress11,colors[j])
    plt.xlabel('Strain')
    plt.ylabel('Stress, MPa')
    plt.legend(['Stress Driven','Strain Driven'])

    plt.figure(3)
    plt.plot(strain11,tau0,colors[j])
    plt.xlabel('Strain')
    plt.ylabel('Reference Stress, MPa')
    plt.legend(['Stress Driven','Strain Driven'])

    plt.figure(4)
    plt.plot(strain11,Fp11,colors[j])
    plt.xlabel('Strain')
    plt.ylabel(r'$F^p_{11}$')
    plt.legend(['Stress Driven','Strain Driven'])

    plt.figure(5)
    plt.plot(strain11,deltagamma1,colors[j])
    plt.xlabel('Strain')
    plt.ylabel(r'$\Delta \gamma$')
    plt.legend(['Stress Driven','Strain Driven'])

    plt.figure(6)
    plt.plot(deltagamma1,tau1,colors[j])
    plt.ylabel(r'$\tau_1$, MPa')
    plt.xlabel(r'$\Delta \gamma_1$')
    plt.legend(['Stress Driven','Strain Driven'])

plt.show()
