#!/usr/local/bin/python3

import numpy as np
import matplotlib.pyplot as plt

resultsFile = 'results.dat'

data = np.loadtxt(resultsFile)

time = data[:,0]

strainV = data[:,1:7]
stressV = data[:,7:13]

strain11 = data[:,1]
stress11 = data[:,7]

sige = np.zeros(len(time))

for i in range(len(time)):
    stressTensor = np.zeros((3,3))
    stressTensor[0,0] = stressV[i,0]# 11
    stressTensor[1,1] = stressV[i,1]# 22
    stressTensor[2,2] = stressV[i,2]# 33
    stressTensor[0,1] = stressV[i,3]# 12
    stressTensor[0,2] = stressV[i,4]# 13
    stressTensor[1,2] = stressV[i,5]# 23
    stressTensor[1,0] = stressTensor[0,1]
    stressTensor[1,2] = stressTensor[2,1]
    stressTensor[0,2] = stressTensor[2,0]

    strainTensor = np.zeros((3,3))
    strainTensor[0,0] = strainV[i,0]# 11
    strainTensor[1,1] = strainV[i,1]# 22
    strainTensor[2,2] = strainV[i,2]# 33
    strainTensor[0,1] = strainV[i,3]# 12
    strainTensor[0,2] = strainV[i,4]# 13
    strainTensor[1,2] = strainV[i,5]# 23
    strainTensor[1,0] = strainTensor[0,1]
    strainTensor[1,2] = strainTensor[2,1]
    strainTensor[0,2] = strainTensor[2,0]

    p = (1./3.)*(stressV[i,0] + stressV[i,1] + stressV[i,2])
    s = stressTensor
    s[0,0] = stressTensor[0,0] - p
    s[1,1] = stressTensor[1,1] - p
    s[2,2] = stressTensor[2,2] - p
    sige[i] = np.sqrt((3./2.)*np.tensordot(s,s,axes=2))

plt.plot(strain11,sige)
plt.plot(strain11,stress11)
print 'sige: ' + str(sige[len(time)-1])
plt.show()
