C                                                  _______
C                 \    /  |    |   |\  /|    /\       |
C                  \  /   |    |   | \/ |   /__\      |
C                   \/    |____|   |    |  /    \     |
C
C  Updates stresses, internal variables, and energies for 3D truss elements
C
C  
C     SIMPLE LINEAR ELASTIC ISOTROPIC MATERIAL, SMALL STRAIN
C     Rate depedent plasticity
C     microinertia
C  ============================================================================
      subroutine vumat(
C Read only (unmodifiable)variables -
     1  nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,
     2  stepTime, totalTime, dt, cmname, coordMp, charLength,
     3  props, density, strainInc, relSpinInc,
     4  tempOld, stretchOld, defgradOld, fieldOld,
     5  stressOld, stateOld, enerInternOld, enerInelasOld,
     6  tempNew, stretchNew, defgradNew, fieldNew,
C Write only (modifiable) variables -
     7  stressNew, stateNew, enerInternNew, enerInelasNew )
C
C      include 'vaba_param.inc'
C

       implicit none
        
       integer  nprops, nblock, ndir, nshr
       integer  nstatev, nfieldv 
      
       real*8 dt,lanneal,stepTime,totalTime

       real*8 density, coordMp,props, 
     1  charLength, strainInc,relSpinInc, tempOld,
     2  stretchOld,defgradOld,fieldOld, stressOld,
     3  stateOld, enerInternOld,enerInelasOld, tempNew,
     4  stretchNew,defgradNew,fieldNew,stressNew, stateNew,
     5  enerInternNew, enerInelasNew

       dimension
     1  density(nblock), coordMp(nblock,*),props(nprops), 
     1  charLength(nblock), strainInc(nblock,ndir+nshr),
     2  relSpinInc(nblock,nshr), tempOld(nblock),
     3  stretchOld(nblock,ndir+nshr),
     4  defgradOld(nblock,ndir+nshr+nshr),
     5  fieldOld(nblock,nfieldv), stressOld(nblock,ndir+nshr),
     6  stateOld(nblock,nstatev), enerInternOld(nblock),
     7  enerInelasOld(nblock), tempNew(nblock),
     8  stretchNew(nblock,ndir+nshr),
     8  defgradNew(nblock,ndir+nshr+nshr),
     9  fieldNew(nblock,nfieldv),
     1  stressNew(nblock,ndir+nshr), stateNew(nblock,nstatev),
     2  enerInternNew(nblock), enerInelasNew(nblock)
C
      integer km, i ,j, dim, nItr
      character*80 cmname

      real*8 A, L, rhoM, rhoBar, E
      real*8 theta
      real*8 x(2), Rsig(1,1), Rsigbar(1,1)
      real*8 R(2,1), tol, depsb0, stress(1,1),sigbar
      real*8 strainIncP(1,1), dsigbar 
      real*8 stressi(1,1),  ratio, ratio2
      real*8 H, nExp, sigy0
      real*8 trTerm, tr, sigbar0, deps(1,1)
      real*8 Jac(2,2)
      real*8 dx(2,1), JacInv(2,2), depsb, sige, miFlag
C     State variables are
C     1 = equivlent plastic strain
C     2 = flow stress
C     3 = equivlent stress

C-----------------------------------------------------------------------------
C	           Initialize material data from input file
C-----------------------------------------------------------------------------
      E      = props(1) ! youngs modulus of material
C     plasticty parameters
      H      = props(2) ! hardening modulus
      nExp   = props(3) ! strain-rate exponent
      sigy0  = props(4) ! initial yield stress

      do km = 1,nblock

C     unknown DOFs
      x(1) = stressOld(km,1)
      
      if (stateOld(km,2) < sigy0) then
            x(2) = sigy0
            sigbar0 = sigy0
      else
            x(2) = stateOld(km,2)
            sigbar0 = stateOld(km,2)
      endif

C     initial residual set to super high
      Rsig(:,1) = 1.D6
      Rsigbar(1,1) = 1.D6
      R(1,1) = Rsig(1,1)
      R(2,1) = Rsigbar(1,1)

C     tolerances for N-R solver
      tol = 1.D-6
      nItr = 100

C     elastic predictor (and reference strainrate)
      call getEpsb(strainInc,depsb0)

C     start plastic loop j
      do j = 1,nItr
C        extract uknowns
         stress(1,1) = x(1)
         sigbar = x(2)

C        stress in strut (x direction in co-rotational element)
         stressi = stress(1,1)

C        plastic strain increment
         ratio = abs(stressi(1,1)/sigbar)**(nExp - 1.D0)
         ratio = ratio*(stressi(1,1)/sigbar)
         strainIncP(1,1) =  depsb0*ratio;

C        flow stress increment
         ratio2 = abs(stressi(1,1)/sigbar)**nExp
         dsigbar = H*depsb0*ratio2

C        Residual
         trTerm = 1.D0
         tr = strainInc(km,1)
         deps = strainInc(km,1) - strainIncP(1,1)

         Rsig(1,1) = (1.D0 + trTerm*tr)*stress(1,1)
         Rsig(1,1) = Rsig(1,1) - stressOlD(km,1)
         Rsig(1,1) = Rsig(1,1) - E*deps(1,1)

         Rsigbar = sigbar - sigbar0 - H*dsigbar
      
         R(1,1) = Rsig(1,1)
         R(2,1) = Rsigbar(1,1)

C        check tolerance
         if (norm2(R) < tol) then
            stress(1,1) = x(1)
            sigbar = x(2)
            exit
         endif

C        Jacobian
C        J11
         Jac(1,1) = trTerm*(nExp*depsb0*stressi(1,1)/(sigbar))
     1          * ratio              
     2          - E*(nExp*depsb0/(sigbar))*ratio

         Jac(1,1) = Jac(1,1) + trTerm*(1.D0 + tr)

C        J12
         Jac(1,2) = -(nExp/sigbar)*(stressi(1,1)*strainIncP(1,1)
     1            + E*strainIncP(1,1))
C        J21
         Jac(2,1) = -(H*nExp*depsb0/sigbar)*ratio

C        J22
         Jac(2,2) = 1.D0 +(H*nExp/sigbar)*depsb0*ratio

C        Update
C        Check this in lattice File
         call invert(Jac,2,JacInv)
         dx = matmul(-1.D0*JacInv(:,:),R)

         x = x + dx(1:2,1)
C         print *, "x: " , x
C         print *, "R: " , R

      end do ! end plasticity loop j

      if (j.eq.nItr+1) then
         stop "*****Did Not Converge*****"
      else 
         call getEpsb(strainIncP,depsb)
         stateNew(km,1) = stateOld(km,1) + depsb
         stateNew(km,2) = sigbar

      endif


C-----------------------------------------------------------------------------
C                         Get stress
C-----------------------------------------------------------------------------

         stressNew(km,1) = stress(1,1)
         stressNew(km,2:6) = 0.D0
      enddo

      return
      end

      subroutine getEpsb(epsv,epsb)
C     calculate equivlent plastic strain epsb
      implicit none

      real*8 epsv(6),epsb

      epsb = epsv(1)*epsv(1) + epsv(2)*epsv(2) + epsv(3)*epsv(3)
     1 + 2.0*epsv(4)*epsv(4) + 2.0*epsv(5)*epsv(5) + 2.0*epsv(6)*epsv(6) 
      epsb = sqrt((2.D0/3.D0)*epsb)
      end subroutine getEpsb




      subroutine invert(A,n,Ainv)

C A is an nxn matrix, n is the matrix dimension, Ainv is A**(-1)
C this subroutin uses Gaussian elimination to determine the inverse
C of an nxn square matrix
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

C      include 'vaba_param.inc'
      implicit none

      INTEGER :: i,n,k,counter,j,g
      REAL*8 :: A(n,n),Ainv(n,n),Atemp(n,n),Id(n,n),den,mul

         Id = 0.
         do i=1,n
            Id(i,i) = 1.
         enddo

         Ainv = Id

         Atemp = A

         if (sum(A) .EQ. 0.) then
            Ainv = A;
         else
C zero out lower left diagonal
            do i=1,n
C set the ith column in the ith row equal to 1
               if (Atemp(i,i) .NE. 1.) then
                  if (Atemp(i,i) .NE. 0.) then          
                     den = Atemp(i,i)
                     Atemp(i,:) = Atemp(i,:)/den
                     Ainv(i,:) = Ainv(i,:)/den
                  else
                     if (i .EQ. n) then
                        stop 'matrix is singular'
                     endif
                     do k=i+1,n
                        if (Atemp(k,i) .NE. 0.) then
                           counter = k
                           exit
                        endif
                        if (k .EQ. n) then
                           stop 'matrix is singular'
                        endif
                     enddo
                     den = Atemp(counter,i)
                     Atemp(i,:) = Atemp(counter,:)/den + Atemp(i,:)
                     Ainv(i,:) = Ainv(counter,:)/den + Ainv(i,:)
                  endif
               endif
C set the ith column in all rows > i equal to 0
               do k=i+1,n
                  if (Atemp(k,i) .NE. 0.) then
                     mul = Atemp(k,i)
                     Atemp(k,:) = -mul*Atemp(i,:) + Atemp(k,:)
                     Ainv(k,:) = -mul*Ainv(i,:) + Ainv(k,:)
                  endif
               enddo
           enddo
C zero out upper right diagonal
           do i=2,n
              j = n - i + 2
              do k=1,j-1
                 g = j - k
                 if (Atemp(g,j) .NE. 0.) then
                    mul = Atemp(g,j)
                    Atemp(g,:) = -mul*Atemp(j,:) + Atemp(g,:)
                    Ainv(g,:) = -mul*Ainv(j,:) + Ainv(g,:)
                 endif
              enddo
           enddo
        endif

      return

      end subroutine invert
