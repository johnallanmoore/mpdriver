C
C     UMAT
C      
      SUBROUTINE UMAT(STRESS,STATEV,DDSDDE,SSE,SPD,SCD,
     1     RPL,DDSDDT,DRPLDE,DRPLDT,
     2     STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED, CMNAME,
     3     NDI,NSHR,NTENS,NSTATV,PROPS,NPROPS,COORDS,DROT,PNEWDT,
     4     CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,KSTEP,KINC)
C     
C       IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I, J, NDI,NSHR,NTENS,NSTATV,NPROPS
      INTEGER NOEL,NPT,LAYER,KSPT,KSTEP,KINC
      INTEGER NSTATEV_TRANS, ISYS, ITER, ITERMAX, IACTIVE
      INTEGER NTRANS, NSLIP, NPROPS_TRANS, ICP, NACTIVE, ITRANS

      REAL*8 STRESS,STATEV,DDSDDE,SSE,SPD,SCD,
     1     RPL,DDSDDT,DRPLDE,DRPLDT,
     2     STRAN,DSTRAN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED,
     3     PROPS,COORDS,DROT,PNEWDT,
     4     CELENT,DFGRD0,DFGRD1
      REAL*8 TRANSRESIS, ALPHAM, ALPHAA, C44M, HEAT_LATENT
      REAL*8 TEHTA_REF_LOW, THETA_TRANS, GAMMA0, THETA_REF
      REAL*8 THETA_REF_HIGH, THETA_REF_LOW, CM, CB, TR, PTCON
      REAL*8 GM, GB, AMATA, DDM_GG_2D, DDA_GG_2D, DDM_GG, AM
      REAL*8 GDOT0, HINT, AMATM, QL, AHARD, SS, H0, GBSLIP
      REAL*8 CMSLIP, PROPST, TOT_ZETA_TAU, S0ALPHASLIP, GMSLIP
      REAL*8 CBSLIP, TIME_HT, DELTAGAMMA_TRIAL, EQPLASTICOLD
      REAL*8 NSTATEVOFFSET, ITIMETEMP1, ITIMETEMP2, TIME_RT
      REAL*8 GAMMANORM, GAMMATOL, ZETATOL, FTRANSOLD, FP_LP
      REAL*8 SLIPLP, ZETANORM, DDEQ_T, FTRANSNEW, CAUCHY2D
      REAL*8 DGAMMAPT_DTSTAR, C_ALPHA_1D, DELTA_GAMMAPT, ZETA_TAU
      REAL*8 FTR_LTR, TRLP, FDRIVE_TRANS, DELTAGAMMA
      REAL*8 FTRANSFORMATIONNEW, FPOLD, TPK1D_TAU, IERRORCP
      REAL*8 DGAMMA_DTSTAR, C_ALPHA_1DSLIP, SHARD_TAU
      REAL*8 ERRORDGAMMA, FPLASTICNEW, FP_LP_POST, FPEFFNEW
      REAL*8 ERRORZETA, ZETA_TRIAL, TAU, FET_TAU, FE_TAU
      REAL*8 DETFPEFFNEW, FPEFFNEWINV, DELTAIJ, CPLASTIC
      REAL*8 FPLASTICNEWT, CE, TPK2D_TAU, ESTRAIN1D, EPLASTIC2D
      REAL*8 ESTRAIN2D, DDEQ_T4D, DET_FE_TAU, FE_INV_TAU, TOTALGAMMA0
      REAL*8 DELTA_GAMMACP, TPK2D_FET_TAU, SHARD_TRIAL, FORCEINT
      REAL*8 DDA_GG, DDM_4D, TOTALGAMMA, DDA_4D, DDA_2D, DDM_2D
      REAL*8 S0ALPHA, C12M, C11M, C11A, C12A, C44A, SHARD_T
      REAL*8 S0, DELTAGAMMA_T, TOT_ZETA_T, ZETA_T
      REAL*8 FPLASTICOLD, FTRANSFORMATIONOLD, FPEFFOLD, EULER

C      INCLUDE 'aba_param.inc'
C       INCLUDE 'globals2.inc'
C     globals2.inc information here
C     Number of slip systems
      PARAMETER(NSLIP  = 12)
C     number of elements in mesh JAM
C      PARAMETER(NELX   = 1)
C      PARAMETER(NGAU   = 216)
C     number of variants
      PARAMETER(NTRANS = 24)
      CHARACTER*80 CMNAME
      DIMENSION STRESS(NTENS),STATEV(NSTATV),
     1     DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS),
     2     STRAN(NTENS),DSTRAN(NTENS),TIME(2),PREDEF(1),DPRED(1),
     3     PROPS(NPROPS),COORDS(3),DROT(3,3),
     4     DFGRD0(3,3),DFGRD1(3,3)
 
      DIMENSION CB(3,NTRANS),CM(3,NTRANS),S0ALPHA(3,3,NTRANS),
     &     EULER(3),TR(3,3),GB(3,NTRANS),GM(3,NTRANS),
     &     DDA_2D(6,6),DDM_2D(6,6),DDA_4D(3,3,3,3),DDM_4D(3,3,3,3),
     &     DDA_GG(3,3,3,3),DDM_GG(3,3,3,3),
     &     DDA_GG_2D(6,6),DDM_GG_2D(6,6),
     &     AMATA(6),AMATM(6),
     &     FPEFFOLD(3,3),FPEFFNEW(3,3),
     &     ZETA_T(NTRANS),ZETA_TAU(NTRANS),
     &     CAUCHY2D(3,3),FTRANSNEW(3,3),DDEQ_T(6,6),
     &     DELTA_GAMMAPT(NTRANS),C_ALPHA_1D(6,NTRANS),
     &     DGAMMAPT_DTSTAR(3,3,NTRANS),TPK1D_TAU(6),
C     &     S0ALPHA_STORE(3,3,NTRANS,NGAU,NELX),
C     &     FPEFFOLD_STORE(3,3,NGAU,NELX),
C     &     FPEFFNEW_STORE(3,3,NGAU,NELX),
C     &     ZETAOLD_STORE(NTRANS,NGAU,NELX),
C     &     ZETANEW_STORE(NTRANS,NGAU,NELX),
C     &     TOTALZETAOLD_STORE(NGAU,NELX),
C     &     TOTALZETANEW_STORE(NGAU,NELX),
C     &     DDA_GG_2D_STORE(6,6,NGAU,NELX),
C     &     DDM_GG_2D_STORE(6,6,NGAU,NELX),
     &     DDEQ_T4D(3,3,3,3),FDRIVE_TRANS(NTRANS)

C PLASTICITY
C      DIMENSION SHARDOLD_STORE(NSLIP,NGAU,NELX),
C     &     SHARDNEW_STORE(NSLIP,NGAU,NELX),
C     &     DELTAGAMMAOLD_STORE(NSLIP,NGAU,NELX),
C     &     DELTAGAMMANEW_STORE(NSLIP,NGAU,NELX),
      DIMENSION    CBSLIP(3,NSLIP),CMSLIP(3,NSLIP),
     &     GBSLIP(3,NSLIP),GMSLIP(3,NSLIP),
     &     S0ALPHASLIP(3,3,NSLIP),
C     &     S0ALPHASLIP_STORE(3,3,NSLIP,NGAU,NELX),
     &     DELTAGAMMA_T(NSLIP),DELTAGAMMA_TRIAL(NSLIP),
     &     DELTAGAMMA(NSLIP),
     &     SHARD_T(NSLIP),SHARD_TRIAL(NSLIP),SHARD_TAU(NSLIP),
     &     SLIPLP(3,3),FP_LP(3,3),FTRANSOLD(3,3),TRLP(3,3),
     &     FPOLD(3,3),FTR_LTR(3,3),
     &     ERRORDGAMMA(NSLIP),ZETA_TRIAL(NTRANS),ERRORZETA(NTRANS),
     &     FPEFFNEWINV(3,3),FE_TAU(3,3),FET_TAU(3,3),CE(3,3),
     &     ESTRAIN2D(3,3),ESTRAIN1D(6),
     &     TPK2D_TAU(3,3),TPK2D_FET_TAU(3,3),
     &     FE_INV_TAU(3,3),
     &     C_ALPHA_1DSLIP(6,NSLIP),
     &     DGAMMA_DTSTAR(3,3,NSLIP),DELTA_GAMMACP(NSLIP),TAU(NSLIP)
   

C     INTERACTION
      DIMENSION HINT(NTRANS,NTRANS),FORCEINT(NTRANS)!,
C     &     HINT_STORE(NTRANS,NTRANS,NELX)
C      COMMON/HARDINT/HINT_STORE

      DIMENSION PROPST(10)
C      COMMON/PROPST/C11A,C12A,C44A,GDOT0,AM,S0,H0,SS,AHARD,QL
C      COMMON IREAD
C      COMMON IKINC
C      DATA IREAD/0/
C      DATA IKINC/-10/
C      COMMON/READSTORE/DDA_GG_2D_STORE,DDM_GG_2D_STORE,
C     &     S0ALPHA_STORE,AMATA,AMATM

      DIMENSION PTCON(7)
C      COMMON/PTCON/
C     &     TRANSRESIS,GAMMA0,THETA_TRANS,THETA_REF,
C     &     HEAT_LATENT,THETA_REF_HIGH,THETA_REF_LOW

C      COMMON/STATEVSTORE/FPEFFOLD_STORE,FPEFFNEW_STORE,
C     &     ZETAOLD_STORE,ZETANEW_STORE,TOTALZETAOLD_STORE,
C     &     TOTALZETANEW_STORE

C      COMMON/CP1/oDELTAGAMMAOLD_STORE,DELTAGAMMANEW_STORE,
C     &     SHARDOLD_STORE,SHARDNEW_STORE,S0ALPHASLIP_STORE

C      DIMENSION FPLASTICOLD_STORE(3,3,NGAU,NELX),
C     &     FPLASTICNEW_STORE(3,3,NGAU,NELX),
C     &     FTRANSFORMATIONOLD_STORE(3,3,NGAU,NELX),
C     &     FTRANSFORMATIONNEW_STORE(3,3,NGAU,NELX),
      DIMENSION     FPLASTICOLD(3,3),FPLASTICNEW(3,3),
     &     FTRANSFORMATIONOLD(3,3),
     &     FTRANSFORMATIONNEW(3,3),
     &     FPLASTICNEWT(3,3),CPLASTIC(3,3),EPLASTIC2D(3,3), 
     &     FP_LP_POST(3,3)

C      COMMON/POST/FPLASTICOLD_STORE,FPLASTICNEW_STORE,
C     & FTRANSFORMATIONOLD_STORE,FTRANSFORMATIONNEW_STORE

      
C      WRITE(*,*)DFGRD0
C      WRITE(*,*)DTIME
C      WRITE(*,*)'UMAT STARTED'

C     NEED TO SORT OUT IF(ITRANS.EQ.1)THEN
C     NEED TO SORT OUT IF(ICP.EQ.1)THEN

      ITRANS=1
      NACTIVE=0
      ICP=1

C     read euler angles from prop definition
      EULER(1) = PROPS(25)
      EULER(2) = PROPS(26)
      EULER(3) = PROPS(27) 

C     unpack needed state/history variables
      FPEFFOLD(1,1) = STATEV(101)
      FPEFFOLD(1,2) = STATEV(102)
      FPEFFOLD(1,3) = STATEV(103)
      FPEFFOLD(2,1) = STATEV(104)
      FPEFFOLD(2,2) = STATEV(105)
      FPEFFOLD(2,3) = STATEV(106)
      FPEFFOLD(3,1) = STATEV(107)
      FPEFFOLD(3,2) = STATEV(108)
      FPEFFOLD(3,3) = STATEV(109)

      FPLASTICOLD(1,1) = STATEV(109+2*NSLIP+1)
      FPLASTICOLD(1,2) = STATEV(109+2*NSLIP+2)
      FPLASTICOLD(1,3) = STATEV(109+2*NSLIP+3)
      FPLASTICOLD(2,1) = STATEV(109+2*NSLIP+4)
      FPLASTICOLD(2,2) = STATEV(109+2*NSLIP+5)
      FPLASTICOLD(2,3) = STATEV(109+2*NSLIP+6)
      FPLASTICOLD(3,1) = STATEV(109+2*NSLIP+7)
      FPLASTICOLD(3,2) = STATEV(109+2*NSLIP+8)
      FPLASTICOLD(3,3) = STATEV(109+2*NSLIP+9)

      FTRANSFORMATIONOLD(1,1) = STATEV(109+2*NSLIP+9+1)
      FTRANSFORMATIONOLD(1,2) = STATEV(109+2*NSLIP+9+2)
      FTRANSFORMATIONOLD(1,3) = STATEV(109+2*NSLIP+9+3)
      FTRANSFORMATIONOLD(2,1) = STATEV(109+2*NSLIP+9+4)
      FTRANSFORMATIONOLD(2,2) = STATEV(109+2*NSLIP+9+5)
      FTRANSFORMATIONOLD(2,3) = STATEV(109+2*NSLIP+9+6)
      FTRANSFORMATIONOLD(3,1) = STATEV(109+2*NSLIP+9+7)
      FTRANSFORMATIONOLD(3,2) = STATEV(109+2*NSLIP+9+8)
      FTRANSFORMATIONOLD(3,3) = STATEV(109+2*NSLIP+9+9)

      IF (KSTEP.EQ.1) THEN
         IF (KINC.EQ.1) THEN
            DO I=1,3
               DO J=1,3
                  IF (I.EQ.J) THEN
                     FPEFFOLD(I,J)= 1.D0
                     FPLASTICOLD(I,J)= 1.D0
                     FTRANSFORMATIONOLD(I,J) = 1.D0
                  ELSE
                     FPEFFOLD(I,J)= 0.D0
                     FPLASTICOLD(I,J)= 0.D0
                     FTRANSFORMATIONOLD(I,J) = 0.D0
                  ENDIF
               ENDDO
            ENDDO
         ENDIF
      ENDIF

      DO ISYS=1,NTRANS
         ZETA_T(ISYS)=STATEV(1+ISYS)
      ENDDO                 

      TOT_ZETA_T = STATEV(1)

      DO ISYS=1,NSLIP
         DELTAGAMMA_T(ISYS)=STATEV(109 + ISYS)
      ENDDO

      NPROPS_TRANS=13
      S0    = PROPS(NPROPS_TRANS+6)  
      IF(ICP.EQ.1)THEN
         DO ISYS=1,NSLIP
            IF (KSTEP.EQ.1) THEN
C              IF (abs(STATEV(109+NSLIP+ISYS)).LT.1.D-12) THEN
               IF (KINC.EQ.1) THEN
                  SHARD_T(ISYS) = S0
               ELSE
                  SHARD_T(ISYS) = STATEV(109+NSLIP+ISYS)
               ENDIF
            ELSE
               SHARD_T(ISYS) = STATEV(109+NSLIP+ISYS)
            ENDIF
         ENDDO
      ENDIF


      IF(ITRANS.EQ.1)THEN
            
         C11A=PROPS(1)
         C12A=PROPS(2)
         C44A=PROPS(3)
         C11M=PROPS(4)
         C12M=PROPS(5)
         C44M=PROPS(6)
         ALPHAA=PROPS(7)
         ALPHAM=PROPS(8)
         TRANSRESIS=PROPS(9)
         GAMMA0=PROPS(10)
         GAMMA0 = 0.D0
         H0 = 0.0
         IF (KINC.EQ.1) THEN
            print *, "Transformation Turned Off"

            IF (H0.EQ.0.0) THEN
               print *, "Hardening Turned Off"
            endif
         endif


         THETA_TRANS=PROPS(11)
         THETA_REF_LOW=PROPS(12)
         HEAT_LATENT=PROPS(13)    
         THETA_REF_HIGH=PROPS(24)
            
C         NPROPS_TRANS=13
         NSTATEV_TRANS=2     
 
C        Pack phase transformtion consitutive props
         IF (KSTEP.EQ.1) THEN
C            if (abs(STATEV(78)).LE.1.D-12) THEN
            IF (KINC.EQ.1) THEN
               THETA_REF=THETA_REF_HIGH
            ELSE
               THETA_REF = STATEV(78)
            ENDIF          
         ELSE
            THETA_REF = STATEV(78)
         ENDIF
         PTCON(1) = TRANSRESIS
         PTCON(2) = GAMMA0
         PTCON(3) = THETA_TRANS
         PTCON(4) = THETA_REF
         PTCON(5) = HEAT_LATENT
         PTCON(6) = THETA_REF_HIGH
         PTCON(7) = THETA_REF_LOW

         CALL INICRYS(CB,CM)
         CALL EULER_SLIP(EULER,TR)

         CALL ROTATE_CRYS_VECTOR(CB,TR,GB,NTRANS)
         CALL ROTATE_CRYS_VECTOR(CM,TR,GM,NTRANS)
            
         DO ISYS=1,NTRANS
            DO I=1,3
               DO J=1,3
                  S0ALPHA(I,J,ISYS)=GB(I,ISYS)*GM(J,ISYS)
               ENDDO
            ENDDO
         ENDDO

         CALL MAKE_ELASTIC_STIFFNESS(DDA_2D,C11A,C12A,C44A)
         CALL MAKE_ELASTIC_STIFFNESS(DDM_2D,C11M,C12M,C44M)
         CALL TR2TO4(DDA_2D,DDA_4D)
         CALL TR2TO4(DDM_2D,DDM_4D)
         CALL ROTATE4D(DDA_4D,TR,DDA_GG)
         CALL ROTATE4D(DDM_4D,TR,DDM_GG)
         CALL TR4TO2(DDA_GG,DDA_GG_2D)
         CALL TR4TO2(DDM_GG,DDM_GG_2D)
         DO I=1,6
            AMATA(I)=ALPHAA
            AMATM(I)=ALPHAM
         ENDDO
         CALL MAKE_INT_HARD(HINT,PROPS(3),CB,CM)
      ELSE
         NPROPS_TRANS=13
         NSTATEV_TRANS=0
      ENDIF

         
      IF(ICP.EQ.1)THEN
         C11A  = PROPS(NPROPS_TRANS+1)
         C12A  = PROPS(NPROPS_TRANS+2)   
         C44A  = PROPS(NPROPS_TRANS+3)   
         GDOT0 = PROPS(NPROPS_TRANS+4)   
         AM    = PROPS(NPROPS_TRANS+5)     
         S0    = PROPS(NPROPS_TRANS+6)  
         H0    = PROPS(NPROPS_TRANS+7)  
         SS    = PROPS(NPROPS_TRANS+8)  
         AHARD = PROPS(NPROPS_TRANS+9)  
         QL    = PROPS(NPROPS_TRANS+10)

         PROPST(1) = C11A
         PROPST(2) = C12A
         PROPST(3) = C44A
         PROPST(4) = GDOT0
         PROPST(5) = AM
         PROPST(6) = S0
         PROPST(7) = H0
         PROPST(8) = SS
         PROPST(9) = AHARD
         PROPST(10) = QL

         CALL INICRYSSLIP(CBSLIP,CMSLIP)

         CALL EULER_SLIP(EULER,TR)

         CALL ROTATE_CRYS_VECTOR(CBSLIP,TR,GBSLIP,NSLIP)
         CALL ROTATE_CRYS_VECTOR(CMSLIP,TR,GMSLIP,NSLIP)
         DO ISYS=1,NSLIP
            DO I=1,3
               DO J=1,3
                  S0ALPHASLIP(I,J,ISYS)=GBSLIP(I,ISYS)*
     &                 GMSLIP(J,ISYS)
               ENDDO          
            ENDDO                                 
         ENDDO   

         CALL MAKE_ELASTIC_STIFFNESS(DDA_2D,C11A,C12A,C44A)
         CALL MAKE_ELASTIC_STIFFNESS(DDM_2D,C11M,C12M,C44M)
         CALL TR2TO4(DDA_2D,DDA_4D)
         CALL TR2TO4(DDM_2D,DDM_4D)
         CALL ROTATE4D(DDA_4D,TR,DDA_GG)
         CALL ROTATE4D(DDM_4D,TR,DDM_GG)
         CALL TR4TO2(DDA_GG,DDA_GG_2D)
         CALL TR4TO2(DDM_GG,DDM_GG_2D)
         DO I=1,6
            AMATA(I)=ALPHAA
            AMATM(I)=ALPHAM
         ENDDO
      ENDIF
           
C      IF(IREAD.EQ.0)THEN
C         IREAD=1
C         OPEN(17,FILE=PWD//TEXINP)
CCC   
C         IF(ITRANS.EQ.1)THEN
            
C            C11A=PROPS(1)
C            C12A=PROPS(2)
C            C44A=PROPS(3)
C            C11M=PROPS(4)
C            C12M=PROPS(5)
C            C44M=PROPS(6)
C            ALPHAA=PROPS(7)
C            ALPHAM=PROPS(8)
C            TRANSRESIS=PROPS(9)
C            GAMMA0=PROPS(10)
C            THETA_TRANS=PROPS(11)
C            THETA_REF_LOW=PROPS(12)
C            HEAT_LATENT=PROPS(13)    
C            THETA_REF_HIGH=PROPS(24)
            
C            NPROPS_TRANS=13
C            NSTATEV_TRANS=2     


C            CALL ARRAYINITIALIZE3(ZETAOLD_STORE,NTRANS,NGAU,NELX)
C            CALL ARRAYINITIALIZE3(ZETANEW_STORE,NTRANS,NGAU,NELX)
C            CALL ARRAYINITIALIZE2(TOTALZETAOLD_STORE,NGAU,NELX)
C            CALL ARRAYINITIALIZE2(TOTALZETANEW_STORE,NGAU,NELX)
                 
C            DO NEL=1,NELX
C               DO IG=1,NGAU
C                  DO I=1,3
C                     DO J=1,3
C                        IF(I.EQ.J)THEN
C                           FPEFFOLD_STORE(I,J,IG,NEL)=1.0
C                           FPEFFNEW_STORE(I,J,IG,NEL)=1.0
C                           FTRANSFORMATIONOLD_STORE(I,J,IG,NEL)=1.0
C                           FTRANSFORMATIONNEW_STORE(I,J,IG,NEL)=1.0                    C       
C                        ELSE
C                           FTRANSFORMATIONOLD_STORE(I,J,IG,NEL)=0.0
C                           FTRANSFORMATIONNEW_STORE(I,J,IG,NEL)=0.0
C                        ENDIF
C                     ENDDO ! end J
C                  ENDDO ! end I
C               ENDDO ! end IG
C            ENDDO ! end NEL
C               CALL INICRYS(CB,CM)
                  
C               READ(17,*)PHI1,PHI2,PHI3,NELIN,NIPTIN 
C               EULER(1)=PHI1
C               EULER(2)=PHI2
C               EULER(3)=PHI3      
C               WRITE(*,*)EULER
C               CALL EULER_SLIP(EULER,TR)
C              Transformation Matrix for 210 Direction
C               TR(1,1) =  0.D0
C               TR(1,2) =  0.D0
C               TR(1,3) =  1.D0
C               TR(2,1) =  1.D0/SQRT(5.D0)
C               TR(2,2) = -2.D0/SQRT(5.D0)
C               TR(2,3) =  0.D0
C               TR(3,1) =  2.D0/SQRT(5.D0)
C               TR(3,2) =  1.D0/SQRT(5.D0)
C               TR(3,3) =  0.D0

C              Transformation Matrix for 111 Direction
C               TR(1,1) =  1.D0/SQRT(2.D0)
C               TR(1,2) = -1.D0/SQRT(2.D0)
C               TR(1,3) =  0.D0
C               TR(2,1) =  1.D0/SQRT(6.D0)
C               TR(2,2) =  1.D0/SQRT(6.D0)
C               TR(2,3) = -2.D0/SQRT(6.D0)
C               TR(3,1) =  1.D0/SQRT(3.D0)
C               TR(3,2) =  1.D0/SQRT(3.D0)
C               TR(3,3) =  1.D0/SQRT(3.D0)

C               WRITE(*,*) TR

c$$$               CALL ROTATE_CRYS_VECTOR(CB,TR,GB,NTRANS)
c$$$               CALL ROTATE_CRYS_VECTOR(CM,TR,GM,NTRANS)
c$$$            
c$$$               DO ISYS=1,NTRANS
c$$$                  DO I=1,3
c$$$                     DO J=1,3
c$$$                        S0ALPHA(I,J,ISYS)=GB(I,ISYS)*GM(J,ISYS)
c$$$                     ENDDO
c$$$                  ENDDO
c$$$               ENDDO
c$$$                    
c$$$        
c$$$               CALL MAKE_ELASTIC_STIFFNESS(DDA_2D,C11A,C12A,C44A)
c$$$               CALL MAKE_ELASTIC_STIFFNESS(DDM_2D,C11M,C12M,C44M)
c$$$               CALL TR2TO4(DDA_2D,DDA_4D)
c$$$               CALL TR2TO4(DDM_2D,DDM_4D)
c$$$               CALL ROTATE4D(DDA_4D,TR,DDA_GG)
c$$$               CALL ROTATE4D(DDM_4D,TR,DDM_GG)
c$$$               CALL TR4TO2(DDA_GG,DDA_GG_2D)
c$$$               CALL TR4TO2(DDM_GG,DDM_GG_2D)
c$$$C               WRITE(*,*) DDA_GG_2D
c$$$C               WRITE(*,*) DDM_GG_2D
c$$$               DO I=1,6
c$$$                  AMATA(I)=ALPHAA
c$$$                  AMATM(I)=ALPHAM
c$$$               ENDDO
C     STORE THE MATPROP
c$$$               DO IG=1,NGAU
c$$$                  DO I=1,6
c$$$                     DO J=1,6
c$$$                        DDA_GG_2D_STORE(I,J,IG,NEL)=DDA_GG_2D(I,J)
c$$$                        DDM_GG_2D_STORE(I,J,IG,NEL)=DDM_GG_2D(I,J)
c$$$                     ENDDO
c$$$                  ENDDO
c$$$                  DO ISYS=1,NTRANS
c$$$                     DO I=1,3
c$$$                        DO J=1,3
c$$$                           S0ALPHA_STORE(I,J,ISYS,IG,NEL)=
c$$$     &                          S0ALPHA(I,J,ISYS)
c$$$                        ENDDO
c$$$                     ENDDO
c$$$                  ENDDO
c$$$
c$$$C     MAKING ITERACTION MATRIX
c$$$               CALL MAKE_INT_HARD(HINT,PROPS(3),CB,CM)
c$$$C               DO ISYS=1,NTRANS
c$$$C                  DO JSYS=1,NTRANS
c$$$C                     HINT_STORE(ISYS,JSYS,NEL)=HINT(ISYS,JSYS)
c$$$C                  ENDDO
c$$$C               ENDDO
c$$$  
c$$$               ENDDO
c$$$            ENDDO 
c$$$         ELSE
c$$$            NPROPS_TRANS=13
c$$$            NSTATEV_TRANS=0
C         ENDIF ! end ?
c$$$C         CLOSE(17)
c$$$C         OPEN(17,FILE=PWD//TEXINP)
c$$$         
C         IF(ICP.EQ.1)THEN
c$$$            C11A  =PROPS(NPROPS_TRANS+1)
c$$$            C12A  = PROPS(NPROPS_TRANS+2)   
c$$$            C44A  = PROPS(NPROPS_TRANS+3)   
c$$$                GDOT0 = PROPS(NPROPS_TRANS+4)   
c$$$                    AM    = PROPS(NPROPS_TRANS+5)     
c$$$                        S0    = PROPS(NPROPS_TRANS+6)  
c$$$                            H0    = PROPS(NPROPS_TRANS+7)  
c$$$                                SS    = PROPS(NPROPS_TRANS+8)  
c$$$                                    AHARD = PROPS(NPROPS_TRANS+9)  
c$$$            QL    = PROPS(NPROPS_TRANS+10)
C            CALL ARRAYINITIALIZE3(DELTAGAMMAOLD_STORE,NSLIP,NGAU,NELX)
C            CALL ARRAYINITIALIZE3(DELTAGAMMANEW_STORE,NSLIP,NGAU,NELX)
C            CALL ARRAYINITIALIZE2(TOTALZETAOLD_STORE,NGAU,NELX)
C            CALL ARRAYINITIALIZE2(TOTALZETANEW_STORE,NGAU,NELX)            
C            DO NEL=1,NELX
C               DO IG=1,NGAU
C                  DO ISYS=1,NSLIP
C                     SHARDOLD_STORE(ISYS,IG,NEL)=S0
C                     SHARDNEW_STORE(ISYS,IG,NEL)=S0
C                     DELTAGAMMANEW_STORE(ISYS,IG,NEL)=0
C                     DELTAGAMMAOLD_STORE(ISYS,IG,NEL)=0
C                  ENDDO
C                  DO I=1,3
C                     DO J=1,3
C                        IF(I.EQ.J)THEN
C                           FPEFFOLD_STORE(I,J,IG,NEL)=1.0
C                           FPEFFNEW_STORE(I,J,IG,NEL)=1.0
C                           FPLASTICOLD_STORE(I,J,IG,NEL)=1.0
C                           FPLASTICNEW_STORE(I,J,IG,NEL)=1.0
C                        ELSE
C                           FPEFFOLD_STORE(I,J,IG,NEL)=0.0
C                           FPEFFNEW_STORE(I,J,IG,NEL)=0.0
C                           FPLASTICOLD_STORE(I,J,IG,NEL)=0.0
C                           FPLASTICNEW_STORE(I,J,IG,NEL)=0.0
C                        ENDIF
C                     ENDDO
C                  ENDDO
C               ENDDO
c$$$               CALL INICRYSSLIP(CBSLIP,CMSLIP)
C               READ(17,*)PHI1,PHI2,PHI3,NELIN,NIPTIN 
C               EULER(1)=PHI1
C               EULER(2)=PHI2
C               EULER(3)=PHI3    
C               CALL EULER_SLIP(EULER,TR)

C              Transformation Matrix for 210 Direction
C               TR(1,1) =  0.D0
C               TR(1,2) =  0.D0
C               TR(1,3) =  1.D0
C               TR(2,1) =  1.D0/SQRT(5.D0)
C               TR(2,2) = -2.D0/SQRT(5.D0)
C               TR(2,3) =  0.D0
C               TR(3,1) =  2.D0/SQRT(5.D0)
C               TR(3,2) =  1.D0/SQRT(5.D0)
C               TR(3,3) =  0.D0

C              Transformation Matrix for 111 Direction
C               TR(1,1) =  1.D0/SQRT(2.D0)
C               TR(1,2) = -1.D0/SQRT(2.D0)
C               TR(1,3) =  0.D0
C               TR(2,1) =  1.D0/SQRT(6.D0)
C               TR(2,2) =  1.D0/SQRT(6.D0)
C               TR(2,3) = -2.D0/SQRT(6.D0)
C               TR(3,1) =  1.D0/SQRT(3.D0)
C               TR(3,2) =  1.D0/SQRT(3.D0)
C               TR(3,3) =  1.D0/SQRT(3.D0)

C              WRITE(*,*)TR

C               CALL ROTATE_CRYS_VECTOR(CBSLIP,TR,GBSLIP,NSLIP)
C               CALL ROTATE_CRYS_VECTOR(CMSLIP,TR,GMSLIP,NSLIP)
C               DO ISYS=1,NSLIP
c$$$                  DO I=1,3
c$$$                     DO J=1,3
c$$$                        S0ALPHASLIP(I,J,ISYS)=GBSLIP(I,ISYS)*
c$$$     &                       GMSLIP(J,ISYS)
c$$$                     ENDDO          
c$$$                  ENDDO                                 
c$$$               ENDDO   
c$$$               CALL MAKE_ELASTIC_STIFFNESS(DDA_2D,C11A,C12A,C44A)
c$$$               CALL MAKE_ELASTIC_STIFFNESS(DDM_2D,C11M,C12M,C44M)
c$$$               CALL TR2TO4(DDA_2D,DDA_4D)
c$$$               CALL TR2TO4(DDM_2D,DDM_4D)
c$$$               CALL ROTATE4D(DDA_4D,TR,DDA_GG)
c$$$               CALL ROTATE4D(DDM_4D,TR,DDM_GG)
c$$$               CALL TR4TO2(DDA_GG,DDA_GG_2D)
c$$$               CALL TR4TO2(DDM_GG,DDM_GG_2D)
c$$$               DO I=1,6
c$$$                  AMATA(I)=ALPHAA
c$$$                  AMATM(I)=ALPHAM
c$$$               ENDDO
C     STORE THE MATPROP

C               DO IG=1,NGAU
C                  DO I=1,6
C                     DO J=1,6
C                        DDA_GG_2D_STORE(I,J,IG,NEL)=DDA_GG_2D(I,J)
C                        DDM_GG_2D_STORE(I,J,IG,NEL)=DDM_GG_2D(I,J)
C                     ENDDO
C                  ENDDO
C                  DO ISYS=1,NSLIP
C                     DO I=1,3
C                        DO J=1,3
C                           S0ALPHASLIP_STORE(I,J,ISYS,IG,NEL)=
C     &                          S0ALPHASLIP(I,J,ISYS)                          
C                        ENDDO
C                     ENDDO                                     
C                  ENDDO            
C               ENDDO
C            ENDDO     
C         ENDIF
C     CLOSE IF CP OR TR
C         CLOSE(17)
C      ENDIF
C     CLOSE IF IREAD
     
C      IF(KINC.NE.IKINC)THEN
C         IKINC=KINC
C         IF(ITRANS.EQ.1)THEN
C            DO NEL=1,NELX
C               DO IG=1,NGAU
C                  DO I=1,3
C                     DO J=1,3
C                        FPEFFOLD_STORE(I,J,IG,NEL)=
C     &                       FPEFFNEW_STORE(I,J,IG,NEL)
C                       FTRANSFORMATIONOLD_STORE(I,J,IG,NEL)=
C     &                       FTRANSFORMATIONNEW_STORE(I,J,IG,NEL)
C                     ENDDO
C                  ENDDO
                  
C                  DO ISYS=1,NTRANS
C                     ZETAOLD_STORE(ISYS,IG,NEL)=
C     &                    ZETANEW_STORE(ISYS,IG,NEL)
C                  ENDDO
C                  TOTALZETAOLD_STORE(IG,NEL)=TOTALZETANEW_STORE(IG,NEL)  
C               ENDDO
C            ENDDO
C         ENDIF
C         IF(ICP.EQ.1)THEN
C           DO NEL=1,NELX
C               DO IG=1,NGAU
C                  DO I=1,3
C                     DO J=1,3
C                        FPEFFOLD_STORE(I,J,IG,NEL)=
C     &                       FPEFFNEW_STORE(I,J,IG,NEL)
C                        FPLASTICOLD_STORE(I,J,IG,NEL)=
C     &                       FPLASTICNEW_STORE(I,J,IG,NEL)
C                     ENDDO
C                  ENDDO
C                  DO ISYS=1,NSLIP
C                     DELTAGAMMAOLD_STORE(ISYS,IG,NEL)=
C     &                    DELTAGAMMANEW_STORE(ISYS,IG,NEL)
C                     SHARDOLD_STORE(ISYS,IG,NEL)=
C     &                    SHARDNEW_STORE(ISYS,IG,NEL)
C                  ENDDO                   
C               ENDDO
C            ENDDO
C         ENDIF
C      ENDIF

    
C     GET VARIABLES AT TIME T
C      DO I=1,3
C         DO J=1,3
C            FPEFFOLD(I,J)=FPEFFOLD_STORE(I,J,NPT,NOEL)
C         ENDDO
C      ENDDO

C       DO I=1,6
C         DO J=1,6
C            DDA_GG_2D(I,J)=DDA_GG_2D_STORE(I,J,NPT,NOEL)
C            DDM_GG_2D(I,J)=DDM_GG_2D_STORE(I,J,NPT,NOEL)
C
C         ENDDO
C      ENDDO        
C      TOT_ZETA_T=TOTALZETAOLD_STORE(NPT,NOEL)
      TOT_ZETA_TAU=TOT_ZETA_T
C      WRITE(*,*)TOT_ZETA_TAU
      NSTATEVOFFSET=NSLIP+NTRANS+1
      EQPLASTICOLD=STATEV(NSTATEVOFFSET+3)

      IF(ICP.EQ.1)THEN
C     GET CP VARIABLES AT TIME T
         DO ISYS=1,NSLIP
C            DELTAGAMMA_T(ISYS)=DELTAGAMMAOLD_STORE(ISYS,NPT,NOEL)
            DELTAGAMMA_TRIAL(ISYS)=DELTAGAMMA_T(ISYS)
C            SHARD_T(ISYS)=SHARDOLD_STORE(ISYS,NPT,NOEL)       
         ENDDO
C         DO ISYS=1,NSLIP
C            DO I=1,3
C               DO J=1,3
C                  S0ALPHASLIP(I,J,ISYS)=
C     &                 S0ALPHASLIP_STORE(I,J,ISYS,NPT,NOEL)
C               ENDDO          
C            ENDDO
C         ENDDO
C         DO I=1,3
C            DO J=1,3
C               FPLASTICOLD(I,J)=FPLASTICOLD_STORE(I,J,NPT,NOEL)
C            ENDDO
C         ENDDO 
      
      ENDIF
    
         
C      IF(ITRANS.EQ.1)THEN
C      DO ISYS=1,NTRANS
C         ZETA_T(ISYS)=ZETAOLD_STORE(ISYS,NPT,NOEL)
C      ENDDO                 
C
C      DO ISYS=1,NTRANS
C         DO I=1,3
C            DO J=1,3
C               S0ALPHA(I,J,ISYS)=S0ALPHA_STORE(I,J,ISYS,NPT,NOEL)
C            ENDDO
C         ENDDO
C      ENDDO
C      DO I=1,3
C         DO J=1,3
C            FTRANSFORMATIONOLD(I,J)=
C     &           FTRANSFORMATIONOLD_STORE(I,J,NPT,NOEL)
C         ENDDO
C      ENDDO 
C
C      ENDIF
      
     
      THETA_REF=THETA_REF_HIGH
      TIME_HT=200
      TIME_RT=250
      ITIMETEMP1=TIME(2)/TIME_HT
      ITIMETEMP2=TIME(2)/TIME_RT

      IF((ITIMETEMP1.GE.1).AND.(ITIMETEMP2.LT.1))THEN
         THETA_REF=THETA_REF_HIGH-((THETA_REF_HIGH-THETA_REF_LOW)*
     &(TIME(2)-TIME_HT)/(TIME_RT-TIME_HT))
      ELSEIF(ITIMETEMP2.GE.1)THEN
         THETA_REF=THETA_REF_LOW
      ENDIF

C
C     ITERATE TILL GAMMA,ZETA AND HARDNESS CONVERGE
C      
      ITER=0
      ITERMAX=50
      ZETATOL=0.02
      GAMMATOL=0.002
      GAMMANORM=10
      ZETANORM=10

          
      DO WHILE(((GAMMANORM.GT.GAMMATOL).OR.
     &     (ZETANORM.GT.ZETATOL)).AND.(ITER.LE.ITERMAX))
         ITER=ITER+1
                   
         IF(ICP.EQ.1)THEN
            DO I=1,3
               DO J=1,3
                  SLIPLP(I,J)=0.0
                  DO ISYS=1,NSLIP      
                     SLIPLP(I,J)=SLIPLP(I,J)+((1-TOT_ZETA_TAU)*
     &                    DELTAGAMMA_TRIAL(ISYS)*DTIME*
     &                    S0ALPHASLIP(I,J,ISYS))
                  ENDDO
               ENDDO
            ENDDO
            CALL PROD(SLIPLP,FPEFFOLD,FP_LP) 
            DO I=1,3
               DO J=1,3
                  FTRANSOLD(I,J)=FPEFFOLD(I,J)+FP_LP(I,J)
               ENDDO
            ENDDO
         ELSE
            DO I=1,3
               DO J=1,3
                  FTRANSOLD(I,J)=FPEFFOLD(I,J)
               ENDDO
            ENDDO
         ENDIF   
        
         IF(ITRANS.EQ.1)THEN         
     
        
C            WRITE(*,*)'STOP1',KINC,NOEL,NPT
C            WRITE(*,*)DFGRD0
            CALL CALSTRESSPT(DFGRD0,DFGRD1,FTRANSOLD,S0ALPHA,DDA_GG_2D,
     &           DDM_GG_2D,ZETA_T,ZETA_TAU,TOT_ZETA_T,TOT_ZETA_TAU,
     &           CAUCHY2D,FTRANSNEW,DDEQ_T,DELTA_GAMMAPT,C_ALPHA_1D,
     &           DGAMMAPT_DTSTAR,TPK1D_TAU,
     &           NACTIVE,KINC,NOEL,FDRIVE_TRANS,PNEWDT,HINT,PTCON,
     &           AMATA,AMATM)
C                 WRITE(*,*)'STOP2',KINC,NOEL,NPT
C            WRITE(*,*)DFGRD1
            DO I=1,3
               DO J=1,3
                  TRLP(I,J)=0.0
                  DO ISYS=1,NTRANS      
                     TRLP(I,J)=TRLP(I,J)+
     &                    (DELTA_GAMMAPT(ISYS)*
     &                    S0ALPHA(I,J,ISYS))
                  ENDDO
               ENDDO
            ENDDO
            CALL PROD(TRLP,FPEFFOLD,FTR_LTR) 
            
            DO I=1,3
               DO J=1,3
                  FPOLD(I,J)=FPEFFOLD(I,J)+FTR_LTR(I,J)
               ENDDO
            ENDDO

            DO I=1,3
               DO J=1,3
                  FTRANSFORMATIONNEW(I,J)=
     &                 FTRANSFORMATIONOLD(I,J)+FTR_LTR(I,J)
               ENDDO
            ENDDO
         
          
         ELSE
            DO I=1,3
               DO J=1,3
                  FPOLD(I,J)=FPEFFOLD(I,J)
                  FTRANSFORMATIONNEW(I,J)=
     &                 FTRANSFORMATIONOLD(I,J)
               ENDDO
            ENDDO


            TOT_ZETA_TAU=0
            CALL ARRAYINITIALIZE2(FTR_LTR,3,3)
            DO I=1,6
               DO J=1,6
                  DDEQ_T(I,J)=DDA_GG_2D(I,J)
               ENDDO
            ENDDO

         ENDIF
            
         IF(ICP.EQ.1)THEN         

            CALL  CALSTRESSCP(DFGRD0,DFGRD1,FPOLD,TPK1D_TAU,DELTAGAMMA,
     &           SHARD_T,SHARD_TAU,S0ALPHASLIP,
     &           C_ALPHA_1DSLIP,DGAMMA_DTSTAR,
     &           DTIME,PNEWDT,
     &           TOT_ZETA_TAU,DDEQ_T,IERRORCP,KINC,TAU,PROPST)

            statev(300) = SHARD_TAU(1)


            IF(IERRORCP.EQ.1)THEN
               RETURN
            ENDIF

            DO I=1,3
               DO J=1,3
                  SLIPLP(I,J)=0.0
                  DO ISYS=1,NSLIP      
                     SLIPLP(I,J)=SLIPLP(I,J)+((1-TOT_ZETA_TAU)*
     &                    DELTAGAMMA(ISYS)*
     &                    S0ALPHASLIP(I,J,ISYS))
                  ENDDO
               ENDDO
            ENDDO
            CALL PROD(SLIPLP,FPEFFOLD,FP_LP) 
            CALL PROD(SLIPLP,FPLASTICOLD,FP_LP_POST) 
            DO I=1,3
               DO J=1,3
                  FPLASTICNEW(I,J)=
     &                 FPLASTICOLD(I,J)+FP_LP_POST(I,J)
               ENDDO
            ENDDO            
         ELSE
            CALL ARRAYINITIALIZE2(FP_LP,3,3)
            DO I=1,3
               DO J=1,3
                  FPLASTICNEW(I,J)=
     &                 FPLASTICOLD(I,J)
               ENDDO
            ENDDO
         ENDIF
         IF((ICP.EQ.1).AND.(ITRANS.EQ.1))THEN            
            IF(ITER.EQ.1)THEN
               DO ISYS=1,NSLIP
                  ERRORDGAMMA(ISYS)=(DELTAGAMMA(ISYS)/DTIME)-
     &                 DELTAGAMMA_TRIAL(ISYS)
                  DELTAGAMMA_TRIAL(ISYS)=DELTAGAMMA(ISYS)/DTIME
               ENDDO
               CALL VECMAG(ERRORDGAMMA,NSLIP,GAMMANORM)
               ZETANORM=0
               DO ISYS=1,NTRANS
                  ZETA_TRIAL(ISYS)=ZETA_TAU(ISYS)
               ENDDO
            ELSE        
               DO ISYS=1,NTRANS
                  ERRORZETA(ISYS)=ZETA_TAU(ISYS)-ZETA_TRIAL(ISYS)
                  ZETA_TRIAL(ISYS)=ZETA_TAU(ISYS)
               ENDDO
               CALL VECMAG(ERRORZETA,NTRANS,ZETANORM)
               DO ISYS=1,NSLIP
                  ERRORDGAMMA(ISYS)=(DELTAGAMMA(ISYS)/DTIME)-
     &                 DELTAGAMMA_TRIAL(ISYS)
                  DELTAGAMMA_TRIAL(ISYS)=DELTAGAMMA(ISYS)/DTIME
               ENDDO
               CALL VECMAG(ERRORDGAMMA,NSLIP,GAMMANORM)                                    
            ENDIF
            ELSE
               GAMMANORM=0
               ZETANORM=0
            ENDIF
    

      ENDDO


  

      IF(ITER.GE.ITERMAX)THEN
         WRITE(*,*)'COMBINED CP PT DIDNOT CONVERGE',KINC
         PNEWDT=0.5
      ENDIF
C            WRITE(*,*)'STOP6',KINC,NOEL,NPT

         DO I=1,3
            DO J=1,3
               FPEFFNEW(I,J)=FPEFFOLD(I,J)+FP_LP(I,J)+FTR_LTR(I,J)
            ENDDO
         ENDDO

           
      CALL MATINV3(FPEFFNEW,FPEFFNEWINV,DETFPEFFNEW)
      CALL PROD(DFGRD1,FPEFFNEWINV,FE_TAU)
      CALL TRANS(FE_TAU,FET_TAU)
      CALL PROD(FET_TAU,FE_TAU,CE)

      CALL TRANS(FPLASTICNEW,FPLASTICNEWT)
      CALL PROD(FPLASTICNEWT,FPLASTICNEW,CPLASTIC)

      
      DO I=1,3
         DO J=1,3
            IF(I.EQ.J)THEN
               DELTAIJ=1.0
            ELSE
               DELTAIJ=0.0
            ENDIF
            ESTRAIN2D(I,J)=0.50*(CE(I,J)-DELTAIJ)
            EPLASTIC2D(I,J)=0.50*(CPLASTIC(I,J)-DELTAIJ)
        ENDDO
      ENDDO
      CALL TR2TO1(ESTRAIN2D,ESTRAIN1D)

C     STEP 3
      CALL TENSORMUL42(TPK1D_TAU,DDEQ_T,ESTRAIN1D)
      CALL TR1TO2(TPK1D_TAU,TPK2D_TAU)
      CALL PROD(TPK2D_TAU,FET_TAU,TPK2D_FET_TAU)
      CALL MATINV3(FE_TAU,FE_INV_TAU,DET_FE_TAU)
      CALL PROD(FE_TAU,TPK2D_FET_TAU,CAUCHY2D)
      DO I=1,3
         DO J=1,3
            CAUCHY2D(I,J)=CAUCHY2D(I,J)/DET_FE_TAU
         ENDDO
      ENDDO
C     
      CALL TR2TO1(CAUCHY2D,STRESS)

c$$$      IF(KINC.eq.1) THEN
c$$$C      IF(CAUCHY2D(1,1).GE.500) THEN
c$$$         print*, "CAUCHY2D", CAUCHY2D 
c$$$         print*, "TPK2D_TAU", TPK2D_TAU
c$$$         print*, "TAU(1)", TAU(1)
c$$$         print*,"DELTAGAMMA(1)",DELTAGAMMA(1)
c$$$         print*,"FE_TAU",FE_TAU
c$$$         print*, "SLIPLP", SLIPLP
c$$$         print*, "FP_LP", FP_LP
c$$$         print*, "CPLASTIC", CPLASTIC
c$$$         print*, "EPLASTIC2D",EPLASTIC2D
c$$$         print*, "ESTRAIN1D", ESTRAIN1D
c$$$         print*,  "FPLASTICOLD", FPLASTICOLD
c$$$         print*,  "FPLASTICNEW", FPLASTICNEW
c$$$         stop
c$$$      ENDIF
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C      WRITE(*,*)STRESS
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C      DO I=1,3
C         DO J=1,3
C            FPEFFNEW_STORE(I,J,NPT,NOEL)=FPEFFNEW(I,J)
C         ENDDO
C      ENDDO

C      DO I=1,3
C         DO J=1,3
C            FPLASTICNEW_STORE(I,J,NPT,NOEL)=FPLASTICNEW(I,J)
C            FTRANSFORMATIONNEW_STORE(I,J,NPT,NOEL)=
C     &           FTRANSFORMATIONNEW(I,J)
C         ENDDO
C      ENDDO


C      IF(ICP.EQ.1)THEN
C         DO ISYS=1,NSLIP
C            DELTAGAMMANEW_STORE(ISYS,NPT,NOEL)=DELTAGAMMA(ISYS)/DTIME
C             SHARDNEW_STORE(ISYS,NPT,NOEL)=SHARD_TAU(ISYS)      
C         ENDDO
C      ENDIF

      
C      IF(ITRANS.EQ.1)THEN
C         DO ISYS=1,NTRANS
C            ZETANEW_STORE(ISYS,NPT,NOEL)=ZETA_TAU(ISYS)
C         ENDDO            
C         TOTALZETANEW_STORE(NPT,NOEL)= TOT_ZETA_TAU
C      ENDIF
C$$$      
C$$$      
      CALL TR2TO4(DDEQ_T,DDEQ_T4D)
      
      IF(KINC.LE.1)THEN

         DO I=1,6
            DO J=1,6
               DDSDDE(I,J)=DDEQ_T(I,J)
            ENDDO
         ENDDO

       ELSE

          DO ISYS=1,NSLIP
             DELTA_GAMMACP(ISYS)=(1-TOT_ZETA_TAU)*DELTAGAMMA(ISYS)
          ENDDO

       CALL  JACOBIAN(DFGRD0,DFGRD1,FPEFFOLD,FPEFFNEW,S0ALPHA,
     &     S0ALPHASLIP,C_ALPHA_1D, C_ALPHA_1DSLIP,
     &     DELTA_GAMMACP,DELTA_GAMMAPT,
     &     DGAMMA_DTSTAR,DGAMMAPT_DTSTAR,
     &     DDEQ_T4D,TPK1D_TAU,
     &     DDSDDE,NTENS,ICP,ITRANS)

      ENDIF

      statev(202) = EPLASTIC2D(1,1)

      STATEV(1)=TOT_ZETA_TAU
C      WRITE(*,*)TOT_ZETA_TAU
C     2-25 ZETA
      DO ISYS=1,NTRANS
         STATEV(1+ISYS)=ZETA_TAU(ISYS)
      ENDDO
C     26-37 SLIP
      TOTALGAMMA=0
      DO ISYS=1,NSLIP
         STATEV(25+ISYS)=STATEV(25+ISYS)+((1-TOT_ZETA_TAU)
     &        *ABS(DELTAGAMMA(ISYS)))
         TOTALGAMMA=TOTALGAMMA+(STATEV(25+ISYS))
      ENDDO
C TOTALGAMMA outputs NaN Usually, but some entries are 0.0000
C      WRITE(*,*)TOTALGAMMA

      NSTATEVOFFSET=NSLIP+NTRANS+1
C     38:SCHMID(3,3)      
      STATEV(NSTATEVOFFSET+1)=S0ALPHA(3,3,1)
C     39:TOTAL GAMMA
      STATEV(NSTATEVOFFSET+2)=TOTALGAMMA
C     40:EQPLASTIC
C      STATEV(NSTATEVOFFSET+3)=EPLASTIC2D(3,3)
      STATEV(NSTATEVOFFSET+3)=EPLASTIC2D(3,3)
C     41:ACTIVITY      
      IACTIVE=((DABS(TOT_ZETA_TAU-TOT_ZETA_T))/DTIME)/0.001
      IF(IACTIVE.GE.1)THEN
         STATEV(NSTATEVOFFSET+4)=1
      ELSE
         STATEV(NSTATEVOFFSET+4)=0
      ENDIF
C     42-65:FDRIVE FOR TRANS 
      DO ISYS=1,NTRANS
         STATEV(NSTATEVOFFSET+4+ISYS)=FDRIVE_TRANS(ISYS)
      ENDDO
      
C     66-77 TAU/G FOR SLIP
      DO ISYS=1,NSLIP
         STATEV(65+ISYS)=ABS(TAU(ISYS)/SHARD_TAU(ISYS))
      ENDDO
      
C     78 Temperature
      STATEV(78) = THETA_REF

C     79-81 euler angles
      STATEV(79) = EULER(1)
      STATEV(80) = EULER(2)
      STATEV(81) = EULER(3)

C      82-100 left blank incase nslip increases

C     101 - 109 effective plastic deformation gradient
      STATEV(101) = FPEFFNEW(1,1)
      STATEV(102) = FPEFFNEW(1,2)
      STATEV(103) = FPEFFNEW(1,3)
      STATEV(104) = FPEFFNEW(2,1)
      STATEV(105) = FPEFFNEW(2,2)
      STATEV(106) = FPEFFNEW(2,3)
      STATEV(107) = FPEFFNEW(3,1)
      STATEV(108) = FPEFFNEW(3,2)
      STATEV(109) = FPEFFNEW(3,3)

C     110 - 121 (assuming nslip = 12) JAM
      DO ISYS=1,NSLIP
         STATEV(109+ISYS) = DELTAGAMMA(ISYS)/DTIME
      ENDDO
C     122 - 133 (assuming nslip = 12) JAM
      DO ISYS=1,NSLIP
         STATEV(109+NSLIP+ISYS) = SHARD_TAU(ISYS)      
      ENDDO
      STATEV(299) = DELTAGAMMA(1)
      STATEV(298) = TAU(1)
C     134 - 142 (assuming nslip = 12) JAM
      STATEV(109+2*NSLIP+1) = FPLASTICNEW(1,1)
      STATEV(109+2*NSLIP+2) = FPLASTICNEW(1,2)
      STATEV(109+2*NSLIP+3) = FPLASTICNEW(1,3)
      STATEV(109+2*NSLIP+4) = FPLASTICNEW(2,1)
      STATEV(109+2*NSLIP+5) = FPLASTICNEW(2,2)
      STATEV(109+2*NSLIP+6) = FPLASTICNEW(2,3)
      STATEV(109+2*NSLIP+7) = FPLASTICNEW(3,1)
      STATEV(109+2*NSLIP+8) = FPLASTICNEW(3,2)
      STATEV(109+2*NSLIP+9) = FPLASTICNEW(3,3)

C     143 - 151 (assuming nslip = 12) JAM
      STATEV(109+2*NSLIP+9+1) = FTRANSFORMATIONNEW(1,1)
      STATEV(109+2*NSLIP+9+2) = FTRANSFORMATIONNEW(1,2)
      STATEV(109+2*NSLIP+9+3) = FTRANSFORMATIONNEW(1,3)
      STATEV(109+2*NSLIP+9+4) = FTRANSFORMATIONNEW(2,1)
      STATEV(109+2*NSLIP+9+5) = FTRANSFORMATIONNEW(2,2)
      STATEV(109+2*NSLIP+9+6) = FTRANSFORMATIONNEW(2,3)
      STATEV(109+2*NSLIP+9+7) = FTRANSFORMATIONNEW(3,1)
      STATEV(109+2*NSLIP+9+8) = FTRANSFORMATIONNEW(3,2)
      STATEV(109+2*NSLIP+9+9) = FTRANSFORMATIONNEW(3,3)

C     END UMAT JAM
      END
 
      SUBROUTINE CALSTRESSCP(DFGRD0,DFGRD1,FPOLD,TPK1D_TAU,DELTAGAMMA,
     &     SHARD_T,SHARD_TAU,S0ALPHASLIP,
     &     C_ALPHA_1DSLIP,DGAMMA_DTSTAR,
     &     DTIME,PNEWDT,TOT_ZETA_TAU,
     &     DDEQ_TAU,IERRORCP,KINC,TAU,PROPST)
C      IMPLICIT REAL *8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I, J, K, KINC, ISYS, ITER1MAX, ITER1, IOPT
      INTEGER ISYS_A, ISYS_B, NSLIP, ITERMAX, ITER
      REAL*8 DFGRD0,DFGRD1,FPOLD,TPK1D_TAU,DELTAGAMMA,
     &     SHARD_T,SHARD_TAU,S0ALPHASLIP,
     &     C_ALPHA_1DSLIP,DGAMMA_DTSTAR,
     &     DTIME,PNEWDT,TOT_ZETA_TAU,
     &     DDEQ_TAU,IERRORCP,TAU,PROPST
      REAL*8 STOL, SNORM, HARDNORM, ETA, RESI, XJAC
      REAL*8 TPK_COR_MAG, TPK_COR, XJACINV, GARG, HBETA
      REAL*8 HARDTOL, SHARDDOT, QLAT, HAB, DGAMMA_DTAU
      REAL*8 TAUOVERG, SHARD_ERROR, AMATEQ_TAU, SHARD_TRIAL
      REAL*8 C_ALPHA_2DSLIP, B_ALPHA_1DSLIP, B_ALPHA_2DSLIP
      REAL*8 TPK1D_TRIAL, STRAIN1D_TRIAL, STRAIN2D_TRIAL, DELTAIJ
      REAL*8 CE_TRIAL, FET_TRIAL, DETFTRANS, FPINVOLD, FE_TRIAL
      REAL*8 Ql, AHARD, SS, H0, S0, AM, GDOT0, C11A, C12A, C44A
      


c      INCLUDE 'globals2.inc'
      PARAMETER(NSLIP = 12)
      DIMENSION FPOLD(3,3),FPINVOLD(3,3),DFGRD1(3,3),DFGRD0(3,3),
     &     FE_TRIAL(3,3),FET_TRIAL(3,3),CE_TRIAL(3,3),
     &     STRAIN2D_TRIAL(3,3),
     &     STRAIN1D_TRIAL(6),TPK1D_TRIAL(6),DDEQ_TAU(6,6),AMATEQ_TAU(6),
     &     S0ALPHASLIP(3,3,NSLIP),
     &     B_ALPHA_2DSLIP(3,3,NSLIP),B_ALPHA_1DSLIP(6,NSLIP), 
     &     C_ALPHA_2DSLIP(3,3,NSLIP),C_ALPHA_1DSLIP(6,NSLIP),
     &     TPK1D_TAU(6),
     &     SHARD_TRIAL(NSLIP),SHARD_T(NSLIP),SHARD_TAU(NSLIP),
     &     RESI(6),XJAC(6,6),DELTAGAMMA(NSLIP),XJACINV(6,6),
     &     TPK_COR(6),
     &     HBETA(NSLIP),QLAT(NSLIP,NSLIP),HAB(NSLIP,NSLIP),
     &     SHARDDOT(NSLIP),SHARD_ERROR(NSLIP),
     &     TAU(NSLIP),DGAMMA_DTSTAR(3,3,NSLIP),DGAMMA_DTAU(NSLIP),
     &     PROPST(10)

C       COMMON/PROPST/C11A,C12A,C44A,GDOT0,AM,S0,H0,SS,AHARD,QL
      C11A = PROPST(1)
      C12A = PROPST(2)
      C44A = PROPST(3)
      GDOT0 = PROPST(4)
      AM = PROPST(5)
      S0 = PROPST(6)
      H0 = PROPST(7)
      SS = PROPST(8)
      AHARD = PROPST(9)
      QL = PROPST(10)
      
     
      CALL MATINV3(FPOLD,FPINVOLD,DETFTRANS)
      CALL PROD(DFGRD1,FPINVOLD,FE_TRIAL)
    
      CALL TRANS(FE_TRIAL,FET_TRIAL)
      CALL PROD(FET_TRIAL,FE_TRIAL,CE_TRIAL)
  
      DO I=1,3
         DO J=1,3
            IF(I.EQ.J)THEN
               DELTAIJ=1.0
            ELSE
               DELTAIJ=0.0
            ENDIF
            STRAIN2D_TRIAL(I,J)=0.50*(CE_TRIAL(I,J)-DELTAIJ)
         ENDDO
      ENDDO
      CALL TR2TO1(STRAIN2D_TRIAL,STRAIN1D_TRIAL)

      CALL TENSORMUL42(TPK1D_TRIAL,DDEQ_TAU,STRAIN1D_TRIAL)


      DO ISYS=1,NSLIP
         DO I=1,3
            DO J=1,3
               B_ALPHA_2DSLIP(I,J,ISYS)=0.D0
               DO K=1,3
                  B_ALPHA_2DSLIP(I,J,ISYS)=B_ALPHA_2DSLIP(I,J,ISYS)
     &                 +CE_TRIAL(I,K)*S0ALPHASLIP(K,J,ISYS)+
     &                 S0ALPHASLIP(K,I,ISYS)*CE_TRIAL(K,J)
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      CALL TR2TO1CRYS(B_ALPHA_2DSLIP,B_ALPHA_1DSLIP,NSLIP)

      CALL TENSORMUL42_CRYS(C_ALPHA_1DSLIP,DDEQ_TAU,B_ALPHA_1DSLIP,
     &     NSLIP)

      DO ISYS=1,NSLIP
         DO I=1,6
            C_ALPHA_1DSLIP(I,ISYS)=0.5*C_ALPHA_1DSLIP(I,ISYS)           
         ENDDO            
      ENDDO

      CALL TR1TO2CRYS(C_ALPHA_1DSLIP,C_ALPHA_2DSLIP,NSLIP)

      DO I=1,6
         TPK1D_TAU(I)=TPK1D_TRIAL(I)
      ENDDO
      DO ISYS=1,NSLIP
         SHARD_TAU(ISYS)=SHARD_T(ISYS)
      ENDDO

      IF(KINC.EQ.0)THEN
         RETURN
      ENDIF



      
      DO ISYS=1,NSLIP
         SHARD_TRIAL(ISYS)=SHARD_T(ISYS)
      ENDDO      
      ITER=0
      ITERMAX=100 
      HARDTOL=500
      HARDNORM=1000


      DO WHILE((HARDNORM.GT.HARDTOL).AND.(ITER.LT.ITERMAX))
         ITER=ITER+1
         SNORM=10
         STOL=1
         ITER1=0
         ITER1MAX=500         
         DO WHILE((SNORM.GT.STOL).AND.(ITER1.LT.ITER1MAX))        
            ITER1=ITER1+1
            ETA=1
            IOPT=1
C                  WRITE(*,*)'STOP4B',KINC,TPK1D_TAU
          
            CALL STRESS_RESIDUECP(TPK1D_TAU,TPK1D_TRIAL,S0ALPHASLIP,
     &           DTIME,SHARD_TRIAL,C_ALPHA_1DSLIP,C_ALPHA_2DSLIP,RESI,
     &           XJAC,DELTAGAMMA,TOT_ZETA_TAU,TAU,IOPT,PROPST)
C                 WRITE(*,*)'STOP4C',KINC,RESI


            DO ISYS=1,NSLIP
               IF(ABS(DELTAGAMMA(ISYS)).GE.0.02)THEN
                  WRITE(*,*)'LARGE DELTA_GAMMA',KINC
                  IERRORCP=1
                  PNEWDT=0.25
                  RETURN
               ENDIF
            ENDDO   
            
            DO I=1,6
               DO J=1,6 
                  XJACINV(I,J)=XJAC(I,J)
               ENDDO
            ENDDO
            
            CALL MATINV(XJACINV,6,6)
            DO I=1,6
               TPK_COR(I)=0
               DO J=1,6
                  TPK_COR(I)=TPK_COR(I)+(XJACINV(I,J)*RESI(J))
               ENDDO
            ENDDO

            CALL VECMAG(TPK_COR,6,TPK_COR_MAG)
               IF(TPK_COR_MAG.GE.50)THEN
                  ETA=0.1
               ENDIF
            
                  
            DO I=1,6
               TPK1D_TAU(I)=TPK1D_TAU(I)-(ETA*TPK_COR(I))
            ENDDO
            IOPT=0
C                WRITE(*,*)'2 SR IN'
            CALL STRESS_RESIDUECP(TPK1D_TAU,TPK1D_TRIAL,S0ALPHASLIP,
     &           DTIME,SHARD_TRIAL,C_ALPHA_1DSLIP,C_ALPHA_2DSLIP,RESI,
     &           XJAC,DELTAGAMMA,TOT_ZETA_TAU,TAU,IOPT, PROPST)
C                WRITE(*,*)'2 SR OUT'
             CALL VECMAG(RESI,6,SNORM)
          ENDDO
          IF(ITER1.EQ.ITER1MAX)THEN
             WRITE(*,*)'DELTA_GAMMA LEVEL ITER DID NOT CONVERGE',KINC
             IERRORCP=1
             PNEWDT=0.25
          ELSE
             IERRORCP=0
          ENDIF
           
C          WRITE(*,*)'OUT GAMMA LOOP'

C         Hardening law updated here
          DO ISYS=1,NSLIP
C             WRITE(*,*)ISYS
             GARG=DBLE(1.0-(SHARD_TRIAL(ISYS)/SS))
C             IF(GARG.GE.0)THEN
                HBETA(ISYS)=0.0*H0*(ABS(GARG)**AHARD)
C                HBETA(ISYS)=H0*SQRT(SQRT(SQRT(ABS(GARG))))
C             ELSE
C                HBETA(ISYS)=-100*SQRT(SQRT(SQRT(ABS(GARG)**0.1)))
C                HBETA(ISYS)=-H0*(ABS(GARG)**AHARD)
C             ENDIF             
          ENDDO  
C          WRITE(*,*)'DEFINED HAB1'
          
          DO ISYS_A=1,NSLIP
             DO ISYS_B=1,NSLIP
                IF(ISYS_A.EQ.ISYS_B)THEN
                   QLAT(ISYS_A,ISYS_B)=QL
                ELSE
                   QLAT(ISYS_A,ISYS_B)=1.0
                ENDIF
             ENDDO
          ENDDO       
          DO ISYS_A = 1,NSLIP
             DO ISYS_B= 1,NSLIP
                HAB(ISYS_A,ISYS_B)=QLAT(ISYS_A,ISYS_B)*
     &               HBETA(ISYS_B)
             ENDDO
          ENDDO

C          WRITE(*,*)'DEFINED HAB'
          DO ISYS_A=1,NSLIP
             SHARDDOT(ISYS_A)=0
             DO ISYS_B=1,NSLIP
                SHARDDOT(ISYS_A)=SHARDDOT(ISYS_A)+
     &               (HAB(ISYS_A,ISYS_B)*
     &               ABS(DELTAGAMMA(ISYS_B)))
             ENDDO            
             SHARD_TAU(ISYS_A)=SHARD_T(ISYS_A)+SHARDDOT(ISYS_A)
             SHARD_ERROR(ISYS_A)=SHARD_TRIAL(ISYS_A)-SHARD_TAU(ISYS_A)
                  SHARD_TRIAL(ISYS_A)=SHARD_TAU(ISYS_A)
C             WRITE(*,*)'HARD',SHARD_T(ISYS_A),SHARD_TAU(ISYS_A),
C     &            SHARD_TRIAL(ISYS_A)
          ENDDO
          CALL VECMAG(SHARD_ERROR,NSLIP,HARDNORM)            
          IOPT=0
C          WRITE(*,*)'LAST SR IN'
          CALL STRESS_RESIDUECP(TPK1D_TAU,TPK1D_TRIAL,S0ALPHASLIP,
     &         DTIME,SHARD_TAU,C_ALPHA_1DSLIP,C_ALPHA_2DSLIP,RESI,XJAC,
     &         DELTAGAMMA,TOT_ZETA_TAU,TAU,IOPT, PROPST)          
C          WRITE(*,*)'LAST SR OUT'
       ENDDO 
C
C     FOR JACOBIAN
C
C          WRITE(*,*)'OUT CONVERGED STRESS'
         DO ISYS=1,NSLIP
C            WRITE(*,*)'FOR JAC',ISYS,TAU(ISYS),SHARD_TAU(ISYS)
            TAUOVERG=TAU(ISYS)/SHARD_TAU(ISYS)
            DGAMMA_DTAU(ISYS)=(1-TOT_ZETA_TAU)*
     &           DTIME*GDOT0*(1/AM)*
     &           (ABS(TAUOVERG)**((1/AM)-1))*(1/SHARD_TAU(ISYS))
C     &           (ABS(TAUOVERG)**((49)))*(1/SHARD_TAU(ISYS))

            DO I=1,3
               DO J=1,3                
                  DGAMMA_DTSTAR(I,J,ISYS)=DGAMMA_DTAU(ISYS)*
     &                 0.5*(S0ALPHASLIP(I,J,ISYS)+S0ALPHASLIP(J,I,ISYS))
               ENDDO
            ENDDO
         ENDDO
C
C
       
C                WRITE(*,*)'END STRESS CAL CP'

       IF(ITER.GE.ITERMAX)THEN
          WRITE(*,*)'HARDNESS ITERATION DID NOT CONVERGE',HARDNORM
          PNEWDT=0.25
           IERRORCP=1
          RETURN
       ELSE
          IERRORCP=0                   
       ENDIF
      END
      
   


      SUBROUTINE STRESS_RESIDUECP(TPK1D_TAU,TPK1D_TRIAL,S0ALPHASLIP,
     &     DTIME,SHARD,C_ALPHA_1DSLIP,C_ALPHA_2DSLIP,RESI,XJAC,
     &     DELTAGAMMA,TOT_ZETA_TAU,TAU,IOPT, PROPST)
C      IMPLICIT REAL *8(A-H,O-Z)     
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I, J, K, IOPT, NSLIP, ISYS, L
      REAL*8 TPK1D_TAU,TPK1D_TRIAL,S0ALPHASLIP,
     &     DTIME,SHARD,C_ALPHA_1DSLIP,C_ALPHA_2DSLIP,RESI,XJAC,
     &     DELTAGAMMA,TOT_ZETA_TAU,TAU, PROPST
      REAL*8 XJAC4D, DGAMMA_DTPK, DGAMMA_DTAU, DGAMMA_C_ALPHA, TAUOVERG
      REAL*8 TPK2D, QL, AHARD, SS, H0, S0, AM, GDOT0, C44A, C12A, C11A
     


c      INCLUDE 'globals2.inc'
      PARAMETER(NSLIP = 12)
      DIMENSION TPK1D_TAU(6),TPK1D_TRIAL(6),S0ALPHASLIP(3,3,NSLIP),
     &     SHARD(NSLIP),C_ALPHA_1DSLIP(6,NSLIP),
     &     C_ALPHA_2DSLIP(3,3,NSLIP),RESI(6),XJAC(6,6),
     &     DELTAGAMMA(NSLIP),
     &     TPK2D(3,3),TAU(NSLIP),DGAMMA_C_ALPHA(6),
     &     DGAMMA_DTAU(NSLIP),DGAMMA_DTPK(3,3,NSLIP),
     &     XJAC4D(3,3,3,3), PROPST(10)
C       COMMON/PROPST/C11A,C12A,C44A,GDOT0,AM,S0,H0,SS,AHARD,QL
      C11A = PROPST(1)
      C12A = PROPST(2)
      C44A = PROPST(3)
      GDOT0 = PROPST(4)
      AM = PROPST(5)
      S0 = PROPST(6)
      H0 = PROPST(7)
      SS = PROPST(8)
      AHARD = PROPST(9)
      QL = PROPST(10)

      CALL TR1TO2(TPK1D_TAU,TPK2D)
      DO ISYS=1,NSLIP
         TAU(ISYS)=0
         DO I=1,3
            DO J=1,3
               TAU(ISYS)=TAU(ISYS)+
     &              (TPK2D(I,J)*S0ALPHASLIP(I,J,ISYS))
            ENDDO
         ENDDO

C         WRITE(*,*)
         TAUOVERG=DBLE(TAU(ISYS)/SHARD(ISYS)) 

         IF(TAUOVERG.GE.0)THEN
            DELTAGAMMA(ISYS)=
     &           GDOT0*DTIME*(ABS(TAUOVERG)**(1/AM))
C     &           GDOT0*DTIME*(ABS(TAUOVERG)**(50))

         ELSE
            DELTAGAMMA(ISYS)=-
     &           GDOT0*DTIME*(ABS(TAUOVERG)**(1/AM))
C     &           GDOT0*DTIME*(ABS(TAUOVERG)**(50))

         ENDIF 

c$$$         IF(tau(1).GE.203.23) THEN
c$$$            print*, "SHARD(1)", SHARD(1)
c$$$            print*, "TAU(1)", TAU(1)
c$$$            print*, "DELTAGAMMA(1)", DELTAGAMMA(1)
c$$$            stop
c$$$         ENDIF

C     NO 100/010 SLIP
C$$$         IF(ISYS.LE.6)THEN
C$$$            DELTAGAMMA(ISYS)=0
C$$$         ENDIF
        
      ENDDO
             


         DO I=1,6
            DGAMMA_C_ALPHA(I)=0
            DO ISYS=1,NSLIP
               DGAMMA_C_ALPHA(I)=DGAMMA_C_ALPHA(I)+
     &              (DELTAGAMMA(ISYS)*C_ALPHA_1DSLIP(I,ISYS))
            ENDDO
         ENDDO
         DO I=1,6
            RESI(I)=TPK1D_TAU(I)-TPK1D_TRIAL(I)+
     &              ((1-TOT_ZETA_TAU)*DGAMMA_C_ALPHA(I))
         ENDDO
       
C           WRITE(*,*)'IN SR'
         IF(IOPT.EQ.1)THEN
C     CALCULATE JACOBIAN FOR NR OF CP
         DO ISYS=1,NSLIP
            TAUOVERG=TAU(ISYS)/SHARD(ISYS)
            DGAMMA_DTAU(ISYS)=(1-TOT_ZETA_TAU)*
     &           DTIME*GDOT0*(1/AM)*
     &           (ABS(TAUOVERG)**((1/AM)-1))*(1/SHARD(ISYS))
C     &           (ABS(TAUOVERG)**(49))*(1/SHARD(ISYS))

            DO I=1,3
               DO J=1,3                
                  DGAMMA_DTPK(I,J,ISYS)=DGAMMA_DTAU(ISYS)*
     &                 0.5*(S0ALPHASLIP(I,J,ISYS)+S0ALPHASLIP(J,I,ISYS))
               ENDDO
            ENDDO
       
         ENDDO
         DO I=1,3
            DO J=1,3
               DO K=1,3
                  DO L=1,3
                     XJAC4D(I,J,K,L)=0
                     DO ISYS=1,NSLIP
                        XJAC4D(I,J,K,L)=XJAC4D(I,J,K,L)+
     &                      ( C_ALPHA_2DSLIP(I,J,ISYS)*
     &                       DGAMMA_DTPK(K,L,ISYS))
                     ENDDO
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
           

         CALL TR4TO2(XJAC4D,XJAC)
         DO I=1,6
            DO J=1,6
               IF(I.EQ.J)THEN
                  XJAC(I,J)=XJAC(I,J)+1
               ENDIF
            ENDDO
         ENDDO
           
      ENDIF
      

      END


      SUBROUTINE CALSTRESSPT(DFGRD0,DFGRD1,FTRANSOLD,S0ALPHA,DDA_GG_2D,
     &     DDM_GG_2D,ZETA_T,ZETA_TAU,TOT_ZETA_T,TOT_ZETA_TAU,
     &     CAUCHY2D,FTRANSNEW,DDEQ_T,DELTA_GAMMAPT,C_ALPHA_1D,
     &     DGAMMA_DTSTAR,TPK1D_TAU,
     &     NACTIVE,KINC,NOEL,FDRIVE_TRIAL,PNEWDT,HINT, PTCON,
     &     AMATA,AMATM)
C      IMPLICIT REAL *8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I,J, K, KINC, NOEL, NTRANS, JSYS, ISYS, ISETPA
      INTEGER ITER, NCOUNT, IDIFF, ISYS_A, ISYS_B, ISETPANEW
      INTEGER INDEXNEW, LWORK, ICOUNT, ICONSIS, ITERC, ITERCMAX
      INTEGER IZETA_LB, IZETA_UB, IFORWDCHK
      INTEGER ITOTZETA_UB, ITOTZETA_LB, IBACKCHK, INFO, ISSVDSIGN
      INTEGER IDELTAZETASIGN2, IZETASIGN, ITOTZETASIGN, ITOTZETAMAX
      INTEGER NACTIVE

      REAL*8 DFGRD0,DFGRD1,FTRANSOLD,S0ALPHA,DDA_GG_2D,
     &     DDM_GG_2D,ZETA_T,ZETA_TAU,TOT_ZETA_T,TOT_ZETA_TAU,
     &     CAUCHY2D,FTRANSNEW,DDEQ_T,DELTA_GAMMAPT,C_ALPHA_1D,
     &     DGAMMA_DTSTAR,TPK1D_TAU,
     &     FDRIVE_TRIAL,PNEWDT,HINT, PTCON,
     &     AMATA,AMATM
      REAL*8 STRAIN1D_TRIAL,  TPK2D, TPK1D_TRIAL
      REAL*8 DELTAIJ, CE_TRIAL, CE_TRIAL_JAC, ZEROTOL, DET_FE_TAU
      REAL*8 TAU_TRIAL, CETPK2D, FORCEINT, TOL, BSTORE
      REAL*8 ZETASIGN, TOTZETASIGN , RNORM
      REAL*8 ASOL, DETLA_ZETA, BSOL, DELTA_ZETA, CE_S0, SYMCE_S0
      REAL*8 DDEQ_CE_S01D, DDEQ_CE_S0, USVD, SSVD, CE_S0_1D
      REAL*8 WORK, VTSVD, SSVDSIGN, SSVDINV
      REAL*8 VSVD, SUTSVD, USVD_T,  ASOL_INV
      REAL*8 DELTAZETASIGN2,IDELTAZETASIGN1
      REAL*8 DELTAZETASIGN1,  RESID_BACK
      REAL*8 DELTAFTRANS, DFTRANS_FTRANSTOLD
      REAL*8 RESID_FORWD, FE_TAU, FTRANSNEWINV, FET_FE_TAU, FET_TAU
      REAL*8 ESTRAIN1D, ESTRAIN2D, RESI, TPK2D_TAU, B_ALPHA
      REAL*8 C_ALPHA_2D, B_ALPHA_1D, XJACINV
      REAL*8 DGAMMA_DTAU, XJAC, TPK1D_COR, FE_INV_TAU, TPK2D_FET_TAU
      REAL*8 AMATEQ_T, STRAIN2D_TRIAL, FET_TRIAL, FE_TRIAL
      REAL*8 DETFTRANS, FTRANSINVOLD, XTOL, ZETA_T0, THETA_REF_LOW
      REAL*8 THETA_REF_HIGH, HEAT_LATENT, THETA_REF, THETA_TRANS
      REAL*8 GAMMA0, TRANSRESIS
      

c      INCLUDE 'globals2.inc'
C     globals2.inc information
      PARAMETER(LWORK=1000)
      PARAMETER(NTRANS=24)
C      PARAMETER(NELX = 1)
      DIMENSION FTRANSOLD(3,3),FTRANSINVOLD(3,3),DFGRD1(3,3),
     &     DFGRD0(3,3),
     &     FE_TRIAL(3,3),FET_TRIAL(3,3),CE_TRIAL(3,3),
     &     STRAIN2D_TRIAL(3,3),STRAIN1D_TRIAL(6),DDEQ_T(6,6),
     &     AMATEQ_T(6),
     &     DDA_GG_2D(6,6),DDM_GG_2D(6,6),AMATA(6),AMATM(6),
     &     TPK1D_TRIAL(6),B_ALPHA(3,3,NTRANS),S0ALPHA(3,3,NTRANS),
     &     B_ALPHA_1D(6,NTRANS),C_ALPHA_1D(6,NTRANS),
     &     C_ALPHA_2D(3,3,NTRANS),TPK1D_TAU(6),
C     INPUT/OUTPUT FOR STRESS RESIDUE
     &     ZETA_T(NTRANS),RESI(NTRANS),XJAC(6,6),DELTA_ZETA(NTRANS),
     &     ZETA_TAU(NTRANS),DGAMMA_DTSTAR(3,3,NTRANS),
     &     DGAMMA_DTAU(NTRANS),CETPK2D(3,3),
C 
     &     XJACINV(6,6),TPK1D_COR(6),TPK2D(3,3),TPK2D_TAU(3,3),
     &     DELTA_GAMMAPT(NTRANS),DELTAFTRANS(3,3),
     &     DFTRANS_FTRANSTOLD(3,3),FTRANSNEW(3,3),
     &     FTRANSNEWINV(3,3),FE_TAU(3,3),FET_TAU(3,3),
     &     FET_FE_TAU(3,3),TPK2D_FET_TAU(3,3),FE_INV_TAU(3,3),
     &     CAUCHY2D(3,3),
     &     ESTRAIN2D(3,3),ESTRAIN1D(6),
     &     FDRIVE_TRIAL(NTRANS),TAU_TRIAL(NTRANS),
     &     RESID_FORWD(NTRANS),RESID_BACK(NTRANS),
     &     ISETPA(NTRANS),ISETPANEW(NTRANS),
     &     INDEXNEW(NTRANS),
     &     ASOL(NTRANS,NTRANS),BSOL(NTRANS),
     &     CE_S0(3,3),CE_S0_1D(6),DDEQ_CE_S01D(6),DDEQ_CE_S0(3,3),
     &     SSVD(NTRANS),USVD(NTRANS,NTRANS),VTSVD(NTRANS,NTRANS),
     &     WORK(LWORK),SSVDINV(NTRANS,NTRANS),USVD_T(NTRANS,NTRANS),
     &     SUTSVD(NTRANS,NTRANS),ASOL_INV(NTRANS,NTRANS),
     &     VSVD(NTRANS,NTRANS),
     &     BSTORE(NTRANS),SYMCE_S0(3,3),ZETA_T0(NTRANS)

      DIMENSION CE_TRIAL_JAC(3,3)
 
      DIMENSION PTCON(7)

C     INTERACTION
      DIMENSION HINT(NTRANS,NTRANS),FORCEINT(NTRANS)!,
C     &     HINT_STORE(NTRANS,NTRANS,NELX)
C      COMMON/HARDINT/HINT_STORE

C      COMMON/PTCON/
C     &     TRANSRESIS,GAMMA0,THETA_TRANS,THETA_REF,
C     &     HEAT_LATENT


C        unPack phase transformtion consitutive props
          TRANSRESIS      = PTCON(1)
          GAMMA0          = PTCON(2)
          THETA_TRANS     = PTCON(3)
          THETA_REF       = PTCON(4)
          HEAT_LATENT     = PTCON(5)
          THETA_REF_HIGH  = PTCON(6)
          THETA_REF_LOW   = PTCON(7)


C     STEP1
      DO ISYS=1,NTRANS
         ZETA_T0(ISYS)=ZETA_T(ISYS)
      ENDDO

      XTOL=0.5
      RNORM=2
      ITERCMAX=100
      ITERC=0
      ICONSIS=1



      CALL MATINV3(FTRANSOLD,FTRANSINVOLD,DETFTRANS)
      CALL PROD(DFGRD1,FTRANSINVOLD,FE_TRIAL)
    
      CALL TRANS(FE_TRIAL,FET_TRIAL)
      CALL PROD(FET_TRIAL,FE_TRIAL,CE_TRIAL_JAC)






      DO WHILE((ICONSIS.GT.0).AND.(ITERC.LE.ITERCMAX))
         ITERC=ITERC+1
  
      CALL MATINV3(FTRANSOLD,FTRANSINVOLD,DETFTRANS)
      CALL PROD(DFGRD1,FTRANSINVOLD,FE_TRIAL)
    
      CALL TRANS(FE_TRIAL,FET_TRIAL)
      CALL PROD(FET_TRIAL,FE_TRIAL,CE_TRIAL)
  
      DO I=1,3
         DO J=1,3
            IF(I.EQ.J)THEN
               DELTAIJ=1.0
            ELSE
               DELTAIJ=0.0
            ENDIF
            STRAIN2D_TRIAL(I,J)=0.50*(CE_TRIAL(I,J)-DELTAIJ)
         ENDDO
      ENDDO
      CALL TR2TO1(STRAIN2D_TRIAL,STRAIN1D_TRIAL)
C     STEP 2
      DO I=1,6
         DO J=1,6
            DDEQ_T(I,J)=((1-TOT_ZETA_T)*DDA_GG_2D(I,J))+
     &           ((TOT_ZETA_T)*DDM_GG_2D(I,J))
         ENDDO
      ENDDO

      DO I=1,6
         AMATEQ_T(I)=((1-TOT_ZETA_T)*AMATA(I))+
     &        ((TOT_ZETA_T)*AMATM(I))
      ENDDO

C     STEP 3
      CALL TENSORMUL42(TPK1D_TRIAL,DDEQ_T,STRAIN1D_TRIAL)






      IF(KINC.EQ.0)THEN
         DO I=1,6
            TPK1D_TAU(I)=TPK1D_TRIAL(I)
         ENDDO
         CALL TR1TO2(TPK1D_TAU,TPK2D)         
         CALL PROD(TPK2D,FET_TRIAL,TPK2D_FET_TAU)
         CALL MATINV3(FE_TRIAL,FE_INV_TAU,DET_FE_TAU)
         CALL PROD(FE_TRIAL,TPK2D_FET_TAU,CAUCHY2D)
         DO I=1,3
            DO J=1,3
               CAUCHY2D(I,J)=CAUCHY2D(I,J)/DET_FE_TAU
            ENDDO
         ENDDO
         DO I=1,3
            DO J=1,3
               IF(I.EQ.J)THEN
                  FTRANSNEW(I,J)=1.0
               ELSE
                  FTRANSNEW(I,J)=0.0
               ENDIF
            ENDDO
         ENDDO         
         RETURN
      ENDIF






      ZEROTOL=1.D-12

      CALL TR1TO2(TPK1D_TRIAL,TPK2D)
      CALL PROD(CE_TRIAL,TPK2D,CETPK2D)
      CALL ARRAYINITIALIZE1(TAU_TRIAL,NTRANS)
      DO ISYS=1,NTRANS
         TAU_TRIAL(ISYS)=0.0
         
         DO I=1,3
            DO J=1,3
               TAU_TRIAL(ISYS)=TAU_TRIAL(ISYS)+
     &              (TPK2D(I,J)*S0ALPHA(I,J,ISYS))
            ENDDO
         ENDDO 
      ENDDO


C     
C     MAKE INTERACTION MATRIX
C     
               DO ISYS=1,NTRANS                  
                  FORCEINT(ISYS)=0
                  DO JSYS=1,NTRANS
C                     FORCEINT(ISYS)=FORCEINT(ISYS)+
C     &                    (HINT_STORE(ISYS,JSYS,NOEL)*
C     &                    ZETA_T(JSYS))
                     FORCEINT(ISYS)=FORCEINT(ISYS)+
     &                    (HINT(ISYS,JSYS)*
     &                    ZETA_T(JSYS))
                  ENDDO
               ENDDO





C     STEP 5
      DO ISYS=1,NTRANS
         FDRIVE_TRIAL(ISYS)=GAMMA0*(TAU_TRIAL(ISYS))
     &        -((HEAT_LATENT/THETA_TRANS)*(THETA_REF-THETA_TRANS))
     &        - (FORCEINT(ISYS))
C     - INTERACTION TERM
      ENDDO
C     STEP 6
      ICOUNT=0
      CALL ARRAYINITIALIZE1(RESID_FORWD,NTRANS)            
      CALL ARRAYINITIALIZE1(RESID_BACK,NTRANS)
      
      CALL ARRAYINITIALIZE1(BSTORE,NTRANS)            



      TOL=1.0D-10
      DO ISYS=1,NTRANS
         ISETPA(ISYS)=0
C
C     ACTIVATE ONLY TRANS SYSTEM 4
C      
C     IF(ISYS.EQ.4)THEN 
         RESID_FORWD(ISYS)=FDRIVE_TRIAL(ISYS)-TRANSRESIS 
         IFORWDCHK=FDRIVE_TRIAL(ISYS)/TRANSRESIS
         ZETASIGN=SIGN(1.0,ZETA_T(ISYS)-TOL)
         IZETA_LB=ZETASIGN+0.5
         IZETA_UB=(ZETA_T(ISYS)+TOL)/1.0
         TOTZETASIGN=SIGN(1.0,TOT_ZETA_T-TOL)
         ITOTZETA_LB=TOTZETASIGN+0.5
         ITOTZETA_UB=(TOT_ZETA_T+TOL)/1.0

         IF((IFORWDCHK.GE.1).AND.
C     &                 (IZETA_LB.GT.0).AND.
     &        (IZETA_UB.LT.1).AND.
C     &                 (ITOTZETA_LB.GT.0).AND.
     &        (ITOTZETA_UB.LT.1))THEN
         ICOUNT=ICOUNT+1
         ISETPA(ISYS)=ISYS
         BSTORE(ISYS)=RESID_FORWD(ISYS)
      ENDIF
      RESID_BACK(ISYS)=FDRIVE_TRIAL(ISYS)+TRANSRESIS 
      IBACKCHK=FDRIVE_TRIAL(ISYS)/TRANSRESIS
      IF((IBACKCHK.LE.-1).AND.
     &     (IZETA_LB.GT.0).AND.
     &     (ITOTZETA_LB.GT.0).AND.
     &     (ITOTZETA_UB.LE.1).AND.
     &     (IZETA_UB.LE.1))THEN            
         ICOUNT=ICOUNT+1
         ISETPA(ISYS)=ISYS
         BSTORE(ISYS)=RESID_BACK(ISYS)
      ENDIF
C     ENDIF
      ENDDO

      

      NCOUNT=ICOUNT
    
      ITER=0
      IDIFF=1
      CALL ARRAYINITIALIZE1(DELTA_ZETA,NTRANS)

      DO WHILE(IDIFF.NE.0)
         ITER=ITER+1
         CALL ARRAYINITIALIZE2(ASOL,NTRANS,NTRANS)
         CALL ARRAYINITIALIZE1(BSOL,NTRANS)
         DO ISYS_A=1,NTRANS
            DO ISYS_B=1,NTRANS
               DO I=1,3
                  DO J=1,3
                     CE_S0(I,J)=0.0
                     DO K=1,3
                        IF(ISETPA(ISYS_B).NE.0)THEN
                           CE_S0(I,J)=CE_S0(I,J)+
     &                          (CE_TRIAL(I,K)*
     &                          S0ALPHA(K,J,ISETPA(ISYS_B)))
                        ENDIF
                     ENDDO
                  ENDDO
               ENDDO 
               DO I=1,3
                  DO J=1,3
                     SYMCE_S0(I,J)=0.5*(CE_S0(I,J)+CE_S0(J,I))
                  ENDDO
               ENDDO
               CALL TR2TO1(SYMCE_S0,CE_S0_1D)

               CALL TENSORMUL42(DDEQ_CE_S01D,DDEQ_T,CE_S0_1D)
            


             CALL TR1TO2(DDEQ_CE_S01D,DDEQ_CE_S0)
               ASOL(ISYS_A,ISYS_B)=0.0
               IF(ISETPA(ISYS_A).NE.0)THEN
                  DO I=1,3
                     DO J=1,3
                        ASOL(ISYS_A,ISYS_B)=ASOL(ISYS_A,ISYS_B)
C     NEED TO PUT HIJ THE INTERACTION MATRIX
     &            +(1.0*
C     &                             HINT_STORE(ISYS_A,ISYS_B,NOEL))
     &                             HINT(ISYS_A,ISYS_B))
     &                       +(GAMMA0*GAMMA0*
     &                       S0ALPHA(I,J,ISETPA(ISYS_A))*
     &                       DDEQ_CE_S0(I,J))
                     ENDDO
                  ENDDO
               ENDIF
C$$$               IF(KINC.EQ.21)THEN
C$$$                  WRITE(*,*)'ASOL',ISYS_A,ISYS_B,ASOL(ISYS_A,ISYS_B)
C$$$               ENDIF
            ENDDO
            BSOL(ISYS_A)=0.0
            IF(ISETPA(ISYS_A).NE.0)THEN                  
               BSOL(ISYS_A)=BSTORE(ISYS_A)
            ENDIF
         ENDDO
C     
C     SVD ASOL
C     
 

         CALL DGESVD( 'A', 'A', NTRANS, NTRANS, ASOL,NTRANS,
     &        SSVD,USVD,
     &        NTRANS, VTSVD, NTRANS,
     $        WORK, LWORK, INFO )              
C$$$
C$$$         
C$$$
C$$$
C$$$            
         CALL ARRAYINITIALIZE2(SSVDINV,NTRANS,NTRANS)
         DO I=1,NTRANS
            SSVDSIGN=SIGN(1.0,ABS(SSVD(I))-1.D-5)
            ISSVDSIGN=SSVDSIGN+0.5
            IF(ISSVDSIGN.GT.0)THEN
               SSVDINV(I,I)=1.0/SSVD(I)
            ELSE
               SSVDINV(I,I)=0.0
            ENDIF
         ENDDO
C$$$
C$$$         
C$$$
         CALL TRANSMN(USVD,USVD_T,NTRANS,NTRANS)
         CALL MATMUL
     &        (SUTSVD,SSVDINV,USVD_T,NTRANS,NTRANS,NTRANS)
         CALL TRANSMN(VTSVD,VSVD,NTRANS,NTRANS)      
         CALL MATMUL
     &        (ASOL_INV,VSVD,SUTSVD,NTRANS,NTRANS,NTRANS)
C$$$
         CALL MATMUL(DELTA_ZETA,ASOL_INV,BSOL,NTRANS,NTRANS,1)
C$$$
         DO ISYS=1,NTRANS
            DELTA_ZETA(ISYS)=0
            DO JSYS=1,NTRANS
               DELTA_ZETA(ISYS)=DELTA_ZETA(ISYS)+
     &              (ASOL_INV(ISYS,JSYS)*BSOL(JSYS))
            ENDDO
         ENDDO

C         IF(ABS(ASOL(1,1)).GE.0.01)THEN
C            ASOL_INV(1,1)=1/ASOL(1,1)
C            DELTA_ZETA(1)=BSOL(1)/ASOL(1,1)
C         ELSE
C            DELTA_ZETA(1)=0
C            ASOL_INV(1,1)=0
C         ENDIF


      
         ICOUNT=0
         TOL=1.0D-10
         DO ISYS=1,NTRANS
            ISETPANEW(ISYS)=1000+ISYS
         ENDDO

         DO ISYS=1,NTRANS
            IF(ISETPA(ISYS).NE.0)THEN                          
               IFORWDCHK=FDRIVE_TRIAL((ISYS))/TRANSRESIS
               IBACKCHK=FDRIVE_TRIAL((ISYS))/TRANSRESIS
               DELTAZETASIGN1=SIGN(1.0,DELTA_ZETA(ISYS)-TOL)
               IDELTAZETASIGN1=DELTAZETASIGN1+0.5
               DELTAZETASIGN2=SIGN(1.0,DELTA_ZETA(ISYS)+TOL)
               IDELTAZETASIGN2=DELTAZETASIGN2+0.5                     
               IF(IFORWDCHK.GE.1)THEN
                  IF(IDELTAZETASIGN1.GT.0)THEN
                     ICOUNT=ICOUNT+1
                     ISETPANEW(ISYS)=ISYS
                  ENDIF

               ENDIF
               IF(IBACKCHK.LE.-1)THEN
                  IF(IDELTAZETASIGN2.EQ.0)THEN
                     ICOUNT=ICOUNT+1
                     ISETPANEW(ISYS)=ISYS
                  ENDIF
               ENDIF
            ELSE
               DELTA_ZETA(ISYS)=0.0
            ENDIF
         ENDDO

         NACTIVE=ICOUNT
         CALL ISORTRX(NTRANS,ISETPANEW,INDEXNEW)

         ICOUNT=1
         DO ISYS=1,NTRANS
            ISETPA(ISYS)=0.0
            IF(ISYS.EQ.ISETPANEW(INDEXNEW(ICOUNT)))THEN
               ISETPA(ISYS)=ISYS
               ICOUNT=ICOUNT+1
            ENDIF                  
         ENDDO               
         IDIFF=NACTIVE-NCOUNT
         NCOUNT=NACTIVE
      ENDDO




C     STEP 8
      TOL=1.0D-10

C$$$      IF(NCOUNT.GE.1)THEN
C$$$         WRITE(*,*)'NCOUNT',NCOUNT
C$$$      ENDIF

      DO ISYS=1,NTRANS
         ZETA_TAU(ISYS)=ZETA_T(ISYS)
         IF(ISETPA(ISYS).NE.0)THEN
            ZETA_TAU(ISETPA(ISYS))=ZETA_T(ISETPA(ISYS))+
     &           DELTA_ZETA(ISETPA(ISYS))
         ENDIF
         IZETA_UB=(ZETA_TAU(ISYS)+TOL)/1.0
         IF(IZETA_UB.GE.1)THEN
            ZETA_TAU(ISYS)=1.0
            DELTA_ZETA(ISYS)=1.0-ZETA_T(ISYS)
         ENDIF
         ZETASIGN=SIGN(1.0,ZETA_TAU(ISYS)-TOL)
         IZETASIGN=ZETASIGN+0.5
         IF(IZETASIGN.EQ.0)THEN
            ZETA_TAU(ISYS)=ZEROTOL
            DELTA_ZETA(ISYS)=ZEROTOL-ZETA_T(ISYS)
         ENDIF
C     IF(ZETA_TAU(ISYS).LE.TOL)THEN
C     ZETA_TAU(ISYS)=0.0
C     ENDIF
      ENDDO
      TOT_ZETA_TAU=0
      DO ISYS=1,NTRANS
         TOT_ZETA_TAU=TOT_ZETA_TAU+ZETA_TAU(ISYS)
      ENDDO
      ITOTZETA_UB=(TOT_ZETA_TAU+TOL)/1.0
      IF(ITOTZETA_UB.GE.1)THEN
         TOT_ZETA_TAU=1.0
      ENDIF
      TOTZETASIGN=SIGN(1.0,TOT_ZETA_TAU-TOL)
      ITOTZETASIGN=TOTZETASIGN+0.5
      IF(ITOTZETASIGN.EQ.0)THEN
         TOT_ZETA_TAU=NTRANS*ZEROTOL
      ENDIF

   

      
C     STEP 8
      CALL ARRAYINITIALIZE2(DELTAFTRANS,3,3)
      DO I=1,3
         DO J=1,3
            DO ISYS=1,NTRANS
               DELTAFTRANS(I,J)=DELTAFTRANS(I,J)+
     &              (DELTA_ZETA(ISYS)*GAMMA0*
     &              S0ALPHA(I,J,ISYS))                    
            ENDDO
         ENDDO
      ENDDO

      CALL PROD(DELTAFTRANS,FTRANSOLD,DFTRANS_FTRANSTOLD)
      DO I=1,3
         DO J=1,3
            FTRANSNEW(I,J)=FTRANSOLD(I,J)+DFTRANS_FTRANSTOLD(I,J)
         ENDDO
      ENDDO

      
      
      CALL MATINV3(FTRANSNEW,FTRANSNEWINV,DETFTRANS)
      CALL PROD(DFGRD1,FTRANSNEWINV,FE_TAU)
      CALL TRANS(FE_TAU,FET_TAU)
      CALL PROD(FET_TAU,FE_TAU,FET_FE_TAU)

        DO I=1,3
            DO J=1,3
               IF(I.EQ.J)THEN
                  DELTAIJ=1.0
               ELSE
                  DELTAIJ=0.0
               ENDIF
               ESTRAIN2D(I,J)=0.50*(FET_FE_TAU(I,J)-DELTAIJ)
            ENDDO
         ENDDO
         CALL TR2TO1(ESTRAIN2D,ESTRAIN1D)

         DO I=1,6
            DO J=1,6
               DDEQ_T(I,J)=((1-TOT_ZETA_TAU)*DDA_GG_2D(I,J))+
     &              ((TOT_ZETA_TAU)*DDM_GG_2D(I,J))
            ENDDO
         ENDDO


C     STEP 3
         CALL TENSORMUL42(TPK1D_TAU,DDEQ_T,ESTRAIN1D)


C     CAL RESIDUAL 
         CALL TR1TO2(TPK1D_TAU,TPK2D_TAU)

         CALL ARRAYINITIALIZE1(TAU_TRIAL,NTRANS)
         DO ISYS=1,NTRANS
            TAU_TRIAL(ISYS)=0.0
            
            DO I=1,3
               DO J=1,3
                  TAU_TRIAL(ISYS)=TAU_TRIAL(ISYS)+
     &                 (TPK2D_TAU(I,J)*S0ALPHA(I,J,ISYS))
               ENDDO
            ENDDO 
         ENDDO

               DO ISYS=1,NTRANS
                  FORCEINT(ISYS)=0
                  DO JSYS=1,NTRANS
                     FORCEINT(ISYS)=FORCEINT(ISYS)+
C     &                    (HINT_STORE(ISYS,JSYS,NOEL)*
     &                    (HINT(ISYS,JSYS)*
     &                    ZETA_TAU(JSYS))
                  ENDDO
               ENDDO



                  DO ISYS=1,NTRANS
            FDRIVE_TRIAL(ISYS)=GAMMA0*(TAU_TRIAL(ISYS))
     &           -((HEAT_LATENT/THETA_TRANS)*(THETA_REF-THETA_TRANS))
     &           -FORCEINT(ISYS)
            IF(ISETPA(ISYS).NE.0)THEN
               RESI(ISYS)=ABS(FDRIVE_TRIAL(ISYS))-TRANSRESIS
            ELSE
               RESI(ISYS)=0
            ENDIF
         ENDDO

          CALL  VECMAG(RESI,NTRANS,RNORM) 

          DO ISYS=1,NTRANS
             ZETA_T(ISYS)=ZETA_TAU(ISYS)
          ENDDO
          TOT_ZETA_T=TOT_ZETA_TAU
          DO I=1,3
             DO J=1,3
                FTRANSOLD(I,J)=FTRANSNEW(I,J)
             ENDDO
          ENDDO
           ICONSIS=RNORM/XTOL
C           RNORM=0
C     END RESIDUE
           ENDDO
        


         CALL TR1TO2(TPK1D_TAU,TPK2D_TAU)



      CALL PROD(TPK2D_TAU,FET_TAU,TPK2D_FET_TAU)
      CALL MATINV3(FE_TAU,FE_INV_TAU,DET_FE_TAU)
      CALL PROD(FE_TAU,TPK2D_FET_TAU,CAUCHY2D)
      DO I=1,3
         DO J=1,3
            CAUCHY2D(I,J)=CAUCHY2D(I,J)/DET_FE_TAU
         ENDDO
      ENDDO
C
C     FOR JACOBIAN
C

      DO ISYS=1,NTRANS
         DO I=1,3
            DO J=1,3
               B_ALPHA(I,J,ISYS)=0D0
               DO K=1,3
                  B_ALPHA(I,J,ISYS)=B_ALPHA(I,J,ISYS)
     &                 +CE_TRIAL_JAC(I,K)*S0ALPHA(K,J,ISYS)+
     &                 S0ALPHA(K,I,ISYS)*CE_TRIAL_JAC(K,J)
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      CALL TR2TO1CRYS(B_ALPHA,B_ALPHA_1D,NTRANS)

      CALL TENSORMUL42_CRYS(C_ALPHA_1D,DDEQ_T,B_ALPHA_1D,NTRANS)
      DO ISYS=1,NTRANS
         DO I=1,6
            C_ALPHA_1D(I,ISYS)=0.5*C_ALPHA_1D(I,ISYS)
         ENDDO
      ENDDO            

      CALL TR1TO2CRYS(C_ALPHA_1D,C_ALPHA_2D,NTRANS)



        TOT_ZETA_T=0
        DO ISYS=1,NTRANS
         DELTA_GAMMAPT(ISYS)=GAMMA0*(ZETA_TAU(ISYS)-ZETA_T0(ISYS))
         DO I=1,3
            DO J=1,3
                DGAMMA_DTSTAR(I,J,ISYS)=0
                 DO JSYS=1,NTRANS
                     DGAMMA_DTSTAR(I,J,ISYS)=DGAMMA_DTSTAR(I,J,ISYS)+
     &           (GAMMA0*GAMMA0*ASOL_INV(ISYS,JSYS)*
     &           (0.5*(S0ALPHA(I,J,JSYS)+S0ALPHA(J,I,JSYS))))
C     &            S0ALPHA(I,J,JSYS))
                ENDDO
               
              ENDDO
             ENDDO
             ZETA_T(ISYS)=ZETA_T0(ISYS)
             TOT_ZETA_T=TOT_ZETA_T+(ZETA_T(ISYS))
       ENDDO
       ITOTZETAMAX=TOT_ZETA_T/1.0
       IF(ITOTZETAMAX.GE.1)THEN
          TOT_ZETA_T=1
          ENDIF
           IF(ITERC.GE.ITERCMAX)THEN
              WRITE(*,*)'PT UMAT DIDNOT CONVERGE'
              PNEWDT=0.5
              RETURN
           ENDIF
  
CC
C     END FOR JACOBIAN
C      

      END


      

      




C------------------------------------------------------------------------
C     C
C     CALCULATES THE JACOBIAN FOR UMAT
C-------------------------------------------------------------------------
C     REFERING TO THESIS PG 185
C     CL_4D - LIJKL
C     DE_G  - CIJKL
C     D_TANG_4D -DIJKL
C     G_TAN_4D -GMNKL
C     CJ_TAN_4D - JIJKL
C     B_TAN_2D  - B
C     CK_TAN_4D - KIJKL
C     Q_TAN     -QIJKL
C     R_TAN     -RIJ
C     SIJKL_TAN - SIJKL
C     W_TAN     - WIJKL 
C------------------------------------------------------------------------
      SUBROUTINE JACOBIAN(DFGRD0,DFGRD1,FP_T,FP_TAU,S0ALPHA,S0ALPHASLIP,
     &     C_ALPHA_1D, C_ALPHA_1DSLIP,
     &     DELTA_GAMMA,DELTA_GAMMAPT,
     &     DGAMMA_DTSTAR,DGAMMAPT_DTSTAR,
     &     DE_G,TPK_1D_TAU,
     &     DDSDDE,NTENS,ICP,ITRANS)
      
C      IMPLICIT REAL*8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I, J , K , L , NTENS,ICP,ITRANS, N, M, IP, ISYS, NSLIP
      INTEGER IQ, NTRANS
      REAL*8 DFGRD0,DFGRD1,FP_T,FP_TAU,S0ALPHA,S0ALPHASLIP
      REAL*8 C_ALPHA_1D, C_ALPHA_1DSLIP
      REAL*8 DGAMMA_DTSTAR,DGAMMAPT_DTSTAR
      REAL*8 DE_G,TPK_1D_TAU, DDSDDE, G_TAN_4D, B_TAN_2D
      REAL*8 CJ_TAN_4D, CJ_TAN_4DSLIP, G_TAN_4DSLIP
      REAL*8 C_ALPHASLIP, GAMMA_JSLIP, DELTA_GAMMA, CKRONE
      REAL*8 CB_TANSLIP, B_TAN_2DSLIP, CK_TAN_2D_INV, CK_TAN_2D
      REAL*8 CK_TAN_4D, Q2_TAN, Q1_TAN, CK_TAN_4D_INV, R_TAN
      REAL*8 Q_TAN, Q3_TAN, GAMMA_S_2DSLIP, SIJKL_TAN_1ST, R_TANSLIP
      REAL*8 GAMMA_S_2D, SIJKL_TAN_2ND, FE_GAMMA_S, R_SSLIP, R_S
      REAL*8 GAMMA_S_2DNET, SIJKL_TAN_3RD, RT_U_FE_T, R_SNET
      REAL*8 W_TAN_1ST, T_FE_T, SIJKL_TAN, FE_STRESS_2D, W_TAN_2ND, FE_Q
      REAL*8 SIJKL_TAN_FE_INV, FE_STRESS_2D_FE_T, W_TAN_3RD, W_TAN_4TH
      REAL*8 W_TAN_2D, TAU, G_ALPHA_TAU, DEFE_1, W_TAN, CJ1_TAN_4DSLIP
      REAL*8 GAMMA_J, CB_TAN, CJ1_TAN, DELTA_GAMMAPT, C_ALPHA
      REAL*8 CJ1_TAN_4D, D_TANG_4D, D_TANG1_4D, CL_4D, DET_FE_TAU
      REAL*8 FE_TAU_INV, FE_TAU_T, FE_TAU, FP_TAU_INV, TPK_2D_G, C2L
      REAL*8 CL2, FE_T_T, CL1, FE_T, FP_T_INV, UT_TAU, RT_TAU
      REAL*8 DFGRDT, DET, DFGRD0_INV, CKRONE_2D


C     INCLUDE 'aba_param.inc'
      
c      INCLUDE 'globals2.inc'
      PARAMETER(NTRANS=24)
      PARAMETER(NSLIP = 12)
      DIMENSION FE_T_T(3,3),FE_TAU_INV(3,3),
     &     CL1(3,3),CL2(3,3),FE_GAMMA_S(3,3),DE_G(3,3,3,3),
     &     CL_4D(3,3,3,3),D_TANG1_4D(3,3,3,3),DEFE_1(NTRANS),
     &     D_TANG_4D(3,3,3,3),G_TAN_4D(3,3,3,3,NTRANS),TPK_2D_G(3,3),
     &     CJ_TAN_4D(3,3,3,3,NTRANS),CJ1_TAN_4D(3,3,3,3,NTRANS), 
     &     B_TAN_2D(3,3,NTRANS),CK_TAN_4D(3,3,3,3),CK_TAN_2D(6,6),
     &     CKRONE(3,3,3,3),CB_TAN(3,3,3,3),C_ALPHA(3,3,NTRANS),
     &     GAMMA_J(3,3,3,3),Q_TAN(3,3,3,3),R_TAN(3,3,NTRANS),
     &     CK_TAN_2D_INV(6,6),CK_TAN_4D_INV(3,3,3,3),Q1_TAN(3,3,3,3),
     &     SIJKL_TAN_1ST(3,3,3,3),SIJKL_TAN_2ND(3,3,3,3),R_S(3,3,3,3),
     &     T_FE_T(3,3),W_TAN_1ST(3,3,3,3),FE_Q(3,3,3,3),DFGRD0_INV(3,3),
     &     W_TAN_2ND(3,3,3,3),W_TAN(3,3,3,3),W_TAN_3RD(3,3,3,3),
     &     W_TAN_4TH(3,3,3,3),FE_STRESS_2D(3,3),FE_STRESS_2D_FE_T(3,3),
     &     SIJKL_TAN_3RD(3,3,3,3),SIJKL_TAN(3,3,3,3),
     &     G_ALPHA_TAU(NTRANS),
     &     SIJKL_TAN_FE_INV(3,3),RT_U_FE_T(3,3),DFGRD0(3,3),
     &     W_TAN_2D(6,6),DDSDDE(NTENS,NTENS),TAU(NTRANS),DFGRD1(3,3)
      
      DIMENSION C_ALPHA_1D(6,NTRANS),FE_T(3,3),FE_TAU(3,3),
     &     FE_TAU_T(3,3),
     &     S0ALPHA(3,3,NTRANS),
     &     FP_T(3,3),FP_T_INV(3,3),
     &     DELTA_GAMMAPT(NTRANS),FP_TAU(3,3),FP_TAU_INV(3,3),
     &     GAMMA_S_2D(3,3),CKRONE_2D(3,3),
     &     TPK_1D_TAU(6),
     &     DFGRDT(3,3),RT_TAU(3,3),UT_TAU(3,3)    
      DIMENSION DGAMMAPT_DTSTAR(3,3,NTRANS),Q2_TAN(3,3,3,3),
     &     Q3_TAN(3,3,3,3)

C     PLASTICITY
      DIMENSION G_TAN_4DSLIP(3,3,3,3,NSLIP),S0ALPHASLIP(3,3,NSLIP),
     & CJ1_TAN_4DSLIP(3,3,3,3,NSLIP),CJ_TAN_4DSLIP(3,3,3,3,NSLIP),
     & B_TAN_2DSLIP(3,3,NSLIP),
     & C_ALPHA_1DSLIP(6,NSLIP),C_ALPHASLIP(3,3,NSLIP),
     & DGAMMA_DTSTAR(3,3,NSLIP),CB_TANSLIP(3,3,3,3),
     & GAMMA_JSLIP(3,3,3,3),R_TANSLIP(3,3,NSLIP),
     & GAMMA_S_2DSLIP(3,3),DELTA_GAMMA(NSLIP),
     & GAMMA_S_2DNET(3,3),R_SSLIP(3,3,3,3),
     & R_SNET(3,3,3,3)
   
      CALL CAL_CKRONE_2D(CKRONE_2D)

   
      CALL MATINV3(DFGRD0,DFGRD0_INV,DET)      


      CALL MAT33(DFGRDT,DFGRD1,DFGRD0_INV,3)
    
      CALL RUDCMP(DFGRDT,RT_TAU,UT_TAU)
    

      CALL MATINV3(FP_T,FP_T_INV,DET)



      CALL MAT33(FE_T,DFGRD0,FP_T_INV,3)
      CALL MAT33(CL1,UT_TAU,FE_T,3)
      CALL TRANS(FE_T,FE_T_T)
      CALL MAT33(CL2,FE_T_T,UT_TAU,3)
      CALL TR1TO2(TPK_1D_TAU,TPK_2D_G)
   
    
      CALL MATINV3(FP_TAU,FP_TAU_INV,DET)
   


      CALL MAT33(FE_TAU,DFGRD1,FP_TAU_INV,3)
      CALL TRANS(FE_TAU,FE_TAU_T)
  
      CALL MATINV3(FE_TAU,FE_TAU_INV,DET_FE_TAU)

    
C     STEP1


      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  CL_4D(I,J,K,L)=FE_T_T(I,K)*CL1(L,J)+CL2(I,K)*FE_T(L,J)      
               ENDDO
            ENDDO
         ENDDO
      ENDDO

C     STEP2 

      CALL MAT44(D_TANG1_4D,DE_G,CL_4D) 

   
C     STEP3

      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  D_TANG_4D(I,J,K,L)=0.5D0*D_TANG1_4D(I,J,K,L)
               ENDDO
            ENDDO
         ENDDO
      ENDDO
     
C     STEP 4A
      IF(ITRANS.EQ.1)THEN
         DO ISYS=1,NTRANS
            DO M=1,3
               DO N=1,3
                  DO K=1,3
                     DO L=1,3
                        G_TAN_4D(M,N,K,L,ISYS)=0D0
                        DO IP=1,3
                           G_TAN_4D(M,N,K,L,ISYS)=G_TAN_4D(M,N,K,L,ISYS)
     &                          +CL_4D(M,IP,K,L)*S0ALPHA(IP,N,ISYS)
     &                          +S0ALPHA(IP,M,ISYS)*CL_4D(IP,N,K,L)
                        ENDDO
                     ENDDO
                  ENDDO
               ENDDO
            ENDDO 
         ENDDO


C     STEP 4B    
      CALL MAT44CRYS(CJ1_TAN_4D,DE_G,G_TAN_4D,NTRANS)

      DO ISYS=1,NTRANS
         DO I=1,3
            DO J=1,3
               DO K=1,3
                  DO L=1,3
                     CJ_TAN_4D(I,J,K,L,ISYS)=0.5D0
     &                    *CJ1_TAN_4D(I,J,K,L,ISYS)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDDO

C     STEP 5
      DO ISYS=1,NTRANS
         DO I=1,3
            DO J=1,3
               B_TAN_2D(I,J,ISYS)=DGAMMAPT_DTSTAR(I,J,ISYS)
            ENDDO
         ENDDO
      ENDDO

      CALL TR1TO2CRYS(C_ALPHA_1D,C_ALPHA,NTRANS) 
      
      CALL MAT33CRYS(CB_TAN,C_ALPHA,B_TAN_2D,NTRANS)


      DO M=1,3
         DO N=1,3
            DO K=1,3
               DO L=1,3
                  GAMMA_J(M,N,K,L)=0D0
                  DO ISYS=1,NTRANS 
                     GAMMA_J(M,N,K,L)=GAMMA_J(M,N,K,L)
     &                    +DELTA_GAMMAPT(ISYS)*CJ_TAN_4D(M,N,K,L,ISYS)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDDO

    
      ELSE
         DO ISYS=1,NTRANS
            DO I=1,3
               DO J=1,3
                  DO K=1,3
                     DO L=1,3
                        CJ_TAN_4D(I,J,K,L,ISYS)=0
                     ENDDO
                  ENDDO
               ENDDO
            ENDDO
         ENDDO

         DO ISYS=1,NTRANS
            DO I=1,3
               DO J=1,3
                  B_TAN_2D(I,J,ISYS)=0
               ENDDO
            ENDDO
         ENDDO
         
         DO I=1,3
            DO J=1,3
               DO K=1,3
                  DO L=1,3
                     CB_TAN(I,J,K,L)=0
                      GAMMA_J(I,J,K,L)=0
                  ENDDO
               ENDDO
            ENDDO
         ENDDO

      ENDIF


C     STEP 4A
      IF(ICP.EQ.1)THEN
         DO ISYS=1,NSLIP
            DO M=1,3
               DO N=1,3
                  DO K=1,3
                     DO L=1,3
                        G_TAN_4DSLIP(M,N,K,L,ISYS)=0D0
                        DO IP=1,3
                           G_TAN_4DSLIP(M,N,K,L,ISYS)=
     &                          G_TAN_4DSLIP(M,N,K,L,ISYS)
     &                          +CL_4D(M,IP,K,L)*S0ALPHASLIP(IP,N,ISYS)
     &                          +S0ALPHASLIP(IP,M,ISYS)*CL_4D(IP,N,K,L)
                        ENDDO
                     ENDDO
                  ENDDO
               ENDDO
            ENDDO 
         ENDDO


C     STEP 4B    
      CALL MAT44CRYS(CJ1_TAN_4DSLIP,DE_G,G_TAN_4DSLIP,NSLIP)

      DO ISYS=1,NSLIP
         DO I=1,3
            DO J=1,3
               DO K=1,3
                  DO L=1,3
                     CJ_TAN_4DSLIP(I,J,K,L,ISYS)=0.5D0
     &                    *CJ1_TAN_4DSLIP(I,J,K,L,ISYS)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDDO

C     STEP 5
      DO ISYS=1,NSLIP
         DO I=1,3
            DO J=1,3
               B_TAN_2DSLIP(I,J,ISYS)=DGAMMA_DTSTAR(I,J,ISYS)
            ENDDO
         ENDDO
      ENDDO

      CALL TR1TO2CRYS(C_ALPHA_1DSLIP,C_ALPHASLIP,NSLIP) 
      
      CALL MAT33CRYS(CB_TANSLIP,C_ALPHASLIP,B_TAN_2DSLIP,NSLIP)

      DO M=1,3
         DO N=1,3
            DO K=1,3
               DO L=1,3
                  GAMMA_JSLIP(M,N,K,L)=0D0
                  DO ISYS=1,NSLIP
                     GAMMA_JSLIP(M,N,K,L)=GAMMA_JSLIP(M,N,K,L)
     &                    +DELTA_GAMMA(ISYS)*CJ_TAN_4DSLIP(M,N,K,L,ISYS)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDDO



      
      ELSE
         DO ISYS=1,NSLIP
            DO I=1,3
               DO J=1,3
                  DO K=1,3
                     DO L=1,3
                        CJ_TAN_4DSLIP(I,J,K,L,ISYS)=0
                     ENDDO
                  ENDDO
               ENDDO
            ENDDO
         ENDDO

         DO ISYS=1,NSLIP
            DO I=1,3
               DO J=1,3
                  B_TAN_2DSLIP(I,J,ISYS)=0
               ENDDO
            ENDDO
         ENDDO
         
         DO I=1,3
            DO J=1,3
               DO K=1,3
                  DO L=1,3
                     CB_TANSLIP(I,J,K,L)=0
                     GAMMA_JSLIP(I,J,K,L)=0
                  ENDDO
               ENDDO
            ENDDO
         ENDDO

      ENDIF





C     STEP 6 A

      CALL CAL_CKRONE(CKRONE)
C     
      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  CK_TAN_4D(I,J,K,L)=CKRONE(I,J,K,L)+
     &                 CB_TAN(I,J,K,L)+CB_TANSLIP(I,J,K,L)
               ENDDO
            ENDDO
         ENDDO
      ENDDO          

C    STEP 6  (B) QIJKL=
      CALL TR4TO2(CK_TAN_4D,CK_TAN_2D)

      DO I=1,6
         DO J=1,6 
            CK_TAN_2D_INV(I,J)=CK_TAN_2D(I,J)
         ENDDO
      ENDDO

   
      CALL MATINV(CK_TAN_2D_INV,6,6)
      CALL TR2TO4(CK_TAN_2D_INV,CK_TAN_4D_INV)

  



      CALL MAT44(Q1_TAN,CK_TAN_4D_INV,D_TANG_4D)

      CALL MAT44(Q2_TAN,CK_TAN_4D_INV,GAMMA_J)      

      CALL MAT44(Q3_TAN,CK_TAN_4D_INV,GAMMA_JSLIP)        
      
      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  Q_TAN(I,J,K,L)=Q1_TAN(I,J,K,L)-
     &                 (Q2_TAN(I,J,K,L)+Q3_TAN(I,J,K,L))
               ENDDO
            ENDDO
         ENDDO
      ENDDO

 

C     STEP 7
      IF(ITRANS.EQ.1)THEN
         DO ISYS=1,NTRANS
            DO I=1,3
               DO J=1,3
                  R_TAN(I,J,ISYS)=0D0
                  DO K=1,3 
                     DO L=1,3
                        R_TAN(I,J,ISYS)=R_TAN(I,J,ISYS)
     &                       +B_TAN_2D(K,L,ISYS)*Q_TAN(K,L,I,J)      
                     ENDDO
                  ENDDO
               ENDDO
            ENDDO
         ENDDO 
         DO I=1,3
            DO J=1,3
               GAMMA_S_2D(I,J)=0.D0
               DO ISYS=1,NTRANS
                  
                  GAMMA_S_2D(I,J)=GAMMA_S_2D(I,J)+DELTA_GAMMAPT(ISYS)
     &                 *S0ALPHA(I,J,ISYS)
               ENDDO
            ENDDO
         ENDDO
      ELSE
         DO I=1,3
            DO J=1,3
               DO ISYS=1,NTRANS
                  R_TAN(I,J,ISYS)=0
               ENDDO
               GAMMA_S_2D(I,J)=0
            ENDDO
         ENDDO
      ENDIF


C     STEP 7
      IF(ICP.EQ.1)THEN
         DO ISYS=1,NSLIP
            DO I=1,3
               DO J=1,3
                  R_TANSLIP(I,J,ISYS)=0D0
                  DO K=1,3 
                     DO L=1,3
                        R_TANSLIP(I,J,ISYS)=R_TANSLIP(I,J,ISYS)
     &                       +B_TAN_2DSLIP(K,L,ISYS)*Q_TAN(K,L,I,J)      
                     ENDDO
                  ENDDO
               ENDDO
            ENDDO
         ENDDO 

         DO I=1,3
            DO J=1,3
               GAMMA_S_2DSLIP(I,J)=0.D0
               DO ISYS=1,NSLIP                  
                  GAMMA_S_2DSLIP(I,J)=GAMMA_S_2DSLIP(I,J)
     &                 +DELTA_GAMMA(ISYS)
     &                 *S0ALPHASLIP(I,J,ISYS)
               ENDDO
            ENDDO
         ENDDO
      ELSE
         DO I=1,3
            DO J=1,3
               DO ISYS=1,NSLIP
                  R_TANSLIP(I,J,ISYS)=0
               ENDDO
               GAMMA_S_2DSLIP(I,J)=0
            ENDDO
         ENDDO
      ENDIF



      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  SIJKL_TAN_1ST(I,J,K,L)=RT_TAU(I,K)*FE_T(L,J)
               ENDDO
            ENDDO
         ENDDO
      ENDDO
      
      DO I=1,3
         DO J=1,3
            GAMMA_S_2DNET(I,J)= GAMMA_S_2D(I,J)+ GAMMA_S_2DSLIP(I,J)
         ENDDO
      ENDDO


      CALL MAT33(FE_GAMMA_S,FE_T,GAMMA_S_2DNET,3)


      DO I=1,3 
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  SIJKL_TAN_2ND(I,J,K,L)=RT_TAU(I,K)*FE_GAMMA_S(L,J)
               ENDDO
            ENDDO
         ENDDO
      ENDDO

    
      CALL MAT33CRYS(R_S,R_TAN,S0ALPHA,NTRANS)
      CALL MAT33CRYS(R_SSLIP,R_TANSLIP,S0ALPHASLIP,NSLIP)
      
      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  R_SNET(I,J,K,L)=R_S(I,J,K,L)+R_SSLIP(I,J,K,L)
               ENDDO
            ENDDO
         ENDDO
      ENDDO


      CALL MAT33(RT_U_FE_T,DFGRDT,FE_T,3)
   
      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  SIJKL_TAN_3RD(I,J,K,L)=0D0
                  DO IP=1,3
                     SIJKL_TAN_3RD(I,J,K,L)=SIJKL_TAN_3RD(I,J,K,L)
     &                    +RT_U_FE_T(I,IP)*R_SNET(K,L,IP,J) 
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  SIJKL_TAN(I,J,K,L)=SIJKL_TAN_1ST(I,J,K,L)
     &                 -SIJKL_TAN_2ND(I,J,K,L)-SIJKL_TAN_3RD(I,J,K,L)
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      CALL MAT33(T_FE_T,TPK_2D_G,FE_TAU_T,3) 
      CALL MAT42(W_TAN_1ST,SIJKL_TAN,T_FE_T)
 

      DO I=1,3
         DO N=1,3
            DO K=1,3
               DO L=1,3
                  FE_Q(I,N,K,L)=0D0
                  DO M=1,3
                     FE_Q(I,N,K,L)=FE_Q(I,N,K,L)+FE_TAU(I,M)
     &                    *Q_TAN(M,N,K,L)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      CALL MAT42(W_TAN_2ND,FE_Q,FE_TAU_T)


      CALL MAT33(FE_STRESS_2D,FE_TAU,TPK_2D_G,3)

      

      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  W_TAN_3RD(I,J,K,L)=0D0
                  DO N=1,3
                     W_TAN_3RD(I,J,K,L)=W_TAN_3RD(I,J,K,L)
     &                    +FE_STRESS_2D(I,N)*SIJKL_TAN(J,N,K,L)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDDO


      CALL MAT33(FE_STRESS_2D_FE_T,FE_STRESS_2D,FE_TAU_T,3)
      DO K=1,3
         DO L=1,3
            SIJKL_TAN_FE_INV(K,L)=0D0 
            DO IP=1,3
               DO IQ=1,3
                  SIJKL_TAN_FE_INV(K,L)=SIJKL_TAN_FE_INV(K,L)
     &                 +SIJKL_TAN(IP,IQ,K,L)*FE_TAU_INV(IQ,IP)   
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  W_TAN_4TH(I,J,K,L)=FE_STRESS_2D_FE_T(I,J)
     &                 *SIJKL_TAN_FE_INV(K,L)
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  W_TAN(I,J,K,L)=(1/DET_FE_TAU)*(W_TAN_1ST(I,J,K,L)
     &                 +W_TAN_2ND(I,J,K,L)
     &                 +W_TAN_3RD(I,J,K,L)-W_TAN_4TH(I,J,K,L))
               ENDDO
            ENDDO
         ENDDO
      ENDDO

      CALL TR4TO2(W_TAN,W_TAN_2D)

      DO I=1,6
          DO J=1,3
                 DDSDDE(I,J)=W_TAN_2D(I,J)
                  ENDDO
         DO J=4,6
            DDSDDE(I,J)=W_TAN_2D(I,J)*1.0
         ENDDO
      ENDDO

     
      
      RETURN
      END


C-------------------------------------------
C     RU DECOMPOSITON          

      SUBROUTINE RUDCMP(F,RD,UD) 
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I, J , K 
      REAL*8 F,RD,UD, ROOT, O3, ROOT3
      REAL*8 C11, C12, C13, C23, C22, C33
      REAL*8 C1212, C1313, C2323, C2313, C1223, C1213
      REAL*8 S11, UI1,  UI2, UI3, IU1S, Q, R, XMOD
      REAL*8 SCL1, SCL2, SCl0, XMODSCL1, SDETM
      REAL*8 UI1S, CT3, ST3, AA, BB, CC, XLAMDA1, XLAMDA2
      REAL*8 XLAMDA3, XLI1, XLI2, XLI3, S33, S12, S13, S23, S22
      REAL*8 SI11, SI12, SI13, SI22, SI33, SI23, UI11, UI22, UI33
      REAL*8 UI12, UI13,  UI23

C     IMPLICIT INTEGER*8 (I-N)                                          
C     REAL *8 LI1,LI2,LI3,LAMDA1,LAMDA2,LAMDA3
C     
      DIMENSION RD(3,3),F(3,3),UD(3,3) 

C     
C     WRITE(6,*)'ENTERING RU'
      O3=1.0D0/3.0
      ROOT3=DSQRT(3.D0)
      C11=F(1,1)*F(1,1)+F(2,1)*F(2,1)+F(3,1)*F(3,1)
      C12=F(1,1)*F(1,2)+F(2,1)*F(2,2)+F(3,1)*F(3,2)
      C13=F(1,1)*F(1,3)+F(2,1)*F(2,3)+F(3,1)*F(3,3)
      C23=F(1,2)*F(1,3)+F(2,2)*F(2,3)+F(3,2)*F(3,3)
      C22=F(1,2)*F(1,2)+F(2,2)*F(2,2)+F(3,2)*F(3,2)
      C33=F(1,3)*F(1,3)+F(2,3)*F(2,3)+F(3,3)*F(3,3)
      C1212=C12*C12
      C1313=C13*C13
      C2323=C23*C23
      C2313=C23*C13
      C1223=C12*C23
      C1213=C12*C13
      S11=C22*C33-C2323
      UI1=O3*(C11+C22+C33)
      UI2=S11+C11*C22+C33*C11-C1212-C1313
      UI3=C11*S11+C12*(C2313-C12*C33)
     1     +C13*(C1223-C22*C13)
      UI1S=UI1*UI1
      Q    =DSQRT(-DMIN1(O3*UI2-UI1S,0.D0))                     
      R    =0.5*(UI3-UI1*UI2)+UI1*UI1S
      XMOD =Q*Q*Q
      SCL1 =.5D0+DSIGN(.5D0,XMOD-1.D-30)                          
      SCL2 =.5D0+DSIGN(.5D0,XMOD-DABS(R))                   
      SCL0 =DMIN1(SCL1,SCL2)                               
      SCL1 =1.-SCL0

      IF(SCL1.EQ.0)THEN
         XMODSCL1=XMOD
      ELSE
         XMODSCL1=XMOD+SCL1
      ENDIF

      SDETM=DACOS(R/(XMODSCL1))*O3

      Q  =SCL0*Q
      CT3=Q*DCOS(SDETM)
      ST3=Q*ROOT3*DSIN(SDETM)
      SDETM=SCL1*DSQRT(DMAX1(0.0D0,R))
      AA=2.000*(CT3+SDETM)+UI1 
      BB=-CT3+ST3-SDETM+UI1
      CC=-CT3-ST3-SDETM+UI1                         
      XLAMDA1=DSQRT(DMAX1(AA,0.D0))
      XLAMDA2=DSQRT(DMAX1(BB,0.D0))
      XLAMDA3=DSQRT(DMAX1(CC,0.D0))
      SDETM=XLAMDA1*XLAMDA2
      XLI1=XLAMDA1+XLAMDA2+XLAMDA3
      XLI2= SDETM+XLAMDA2*XLAMDA3+XLAMDA3*XLAMDA1
      XLI3= SDETM*XLAMDA3/XLI1
      S11= C11+XLI3
      S22= C22+XLI3
      S33= C33+XLI3
      S12= C2313-C12*S33
      S13= C1223-S22*C13
      S23=-C2323+S22*S33
      SDETM=1./(XLI1*(S11*S23+C12*S12+C13*S13))
      C11=C11+XLI2
      C22=C22+XLI2
      C33=C33+XLI2
      SI11=SDETM*S23
      SI12=SDETM*S12
      SI13=SDETM*S13
      SI22=SDETM*( S11*S33-C1313)
      SI23=SDETM*(-S11*C23+C1213)
      SI33=SDETM*( S11*S22-C1212)
      S12=C12*SI12
      S13=C13*SI13
      S23=C23*SI23
      UI11=C11*SI11+S12+S13
      UI22=S12+C22*SI22+S23
      UI33=S13+S23+C33*SI33
      UI12=C11*SI12+C12*SI22+C13*SI23
      UI13=C11*SI13+C12*SI23+C13*SI33
      UI23=C12*SI13+C22*SI23+C23*SI33
      RD(1,1)=F(1,1)*UI11+F(1,2)*UI12+F(1,3)*UI13
      RD(1,2)=F(1,1)*UI12+F(1,2)*UI22+F(1,3)*UI23
      RD(1,3)=F(1,1)*UI13+F(1,2)*UI23+F(1,3)*UI33
      RD(2,1)=F(2,1)*UI11+F(2,2)*UI12+F(2,3)*UI13
      RD(2,2)=F(2,1)*UI12+F(2,2)*UI22+F(2,3)*UI23
      RD(2,3)=F(2,1)*UI13+F(2,2)*UI23+F(2,3)*UI33
      RD(3,1)=F(3,1)*UI11+F(3,2)*UI12+F(3,3)*UI13
      RD(3,2)=F(3,1)*UI12+F(3,2)*UI22+F(3,3)*UI23
      RD(3,3)=F(3,1)*UI13+F(3,2)*UI23+F(3,3)*UI33 



      DO I=1,3
         DO J=1,3
            UD(I,J)=0.D0
            DO K=1,3
               UD(I,J)=UD(I,J)+RD(K,I)*F(K,J)
            ENDDO
         ENDDO
      ENDDO
C     WRITE(6,*)'EXITING RU'
      RETURN
      END

      SUBROUTINE MAKE_INT_HARD(HINT,C44,CB,CM)
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER NTRANS, ISYS, I, J, JSYS
      REAL*8 HINT, C44, CB, CM, C, CIN , TRSTRAIN, DETTRSTRAINNET
      REAL*8 ITOL, TRSTRAINNET
      
c      INCLUDE 'globals2.inc'
      PARAMETER(NTRANS=24)
      DIMENSION HINT(NTRANS,NTRANS),CB(3,NTRANS),CM(3,NTRANS),
     &     TRSTRAIN(3,3,NTRANS),TRSTRAINNET(3,3)
      C=C44/16000.0
      CIN=C44/4000.0
C     C=0
C     CIN=0

      DO ISYS=1,NTRANS
         DO I=1,3
            DO J=1,3
               TRSTRAIN(I,J,ISYS)=0.5*((CB(I,ISYS)*CM(J,ISYS))+
     &              (CB(J,ISYS)*CM(I,ISYS)))
            ENDDO
         ENDDO
      ENDDO
      
      DO ISYS=1,NTRANS
         DO JSYS=1,NTRANS
            DO I=1,3
               DO J=1,3
                  TRSTRAINNET(I,J)=
     &                 TRSTRAIN(I,J,ISYS)-TRSTRAIN(I,J,JSYS)
               ENDDO
            ENDDO
            CALL CALDETMAT3(TRSTRAINNET,DETTRSTRAINNET)
            ITOL=DABS(DETTRSTRAINNET)/0.0001
            IF(ITOL.LE.0)THEN
               HINT(ISYS,JSYS)=C
            ELSE
               HINT(ISYS,JSYS)=CIN
            ENDIF            
         ENDDO
      ENDDO
      
      END
      
      SUBROUTINE CALDETMAT3(A,DET)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      DIMENSION A(3,3)     
      DET=(A(1,1)*A(2,2)*A(3,3)-A(1,1)*A(2,3)*A(3,2)-A(2,1)*A(1,2)
     &     *A(3,3)+A(2,1)*A(1,3)*A(3,2)+A(3,1)*A(1,2)*A(2,3)-A(3,1)
     &     *A(1,3)*A(2,2))
      END
      
 

      SUBROUTINE TR1TO2(X,XX)
C      IMPLICIT REAL*8 (A-H,O-Z)  
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      REAL*8 X,XX    
      DIMENSION X(6),XX(3,3)
      XX(1,1)=X(1)                                                    
      XX(2,2)=X(2)                                                   
      XX(3,3)=X(3)                                                  
      XX(2,3)=X(6)                                                 
      XX(3,2)=X(6)                                                
      XX(3,1)=X(5)                                               
      XX(1,3)=X(5)                                               
      XX(1,2)=X(4)                                              
      XX(2,1)=X(4)
      END

      SUBROUTINE MAKE_ELASTIC_STIFFNESS(DD_2D,C11,C12,C44)
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      REAL*8 DD_2D, C11, C12, C44
      DIMENSION DD_2D(6,6)
      CALL ARRAYINITIALIZE2(DD_2D,6,6)
      DD_2D(1,1)=C11
      DD_2D(2,2)=C11
      DD_2D(3,3)=C11
      DD_2D(4,4)=C44
      DD_2D(5,5)=C44
      DD_2D(6,6)=C44
      DD_2D(1,2)=C12
      DD_2D(2,1)=C12
      DD_2D(1,3)=C12
      DD_2D(3,1)=C12
      DD_2D(2,3)=C12
      DD_2D(3,2)=C12
      END


      SUBROUTINE ARRAYINITIALIZE3(A,NDIMX,NDIMY,NDIMZ)
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER NDIMX, NDIMY, NDIMZ, I, J, K
      REAL*8 A
      DIMENSION A(NDIMX,NDIMY,NDIMZ)
      DO I=1,NDIMX
         DO J=1,NDIMY
            DO K=1,NDIMZ
               A(I,J,K)=0.0
            ENDDO
         ENDDO
      ENDDO
      END   

      SUBROUTINE ARRAYINITIALIZE2(A,NDIMX,NDIMY)
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I,J, NDIMX, NDIMY
      REAL*8 A
      DIMENSION A(NDIMX,NDIMY)
      DO I=1,NDIMX
         DO J=1,NDIMY
            A(I,J)=0.0
         ENDDO
      ENDDO
      END 

      SUBROUTINE ARRAYINITIALIZE1(A,NDIMX)
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I
      REAL*8 A, NDIMX
      DIMENSION A(NDIMX)
      DO I=1,NDIMX
         A(I)=0.0
      ENDDO
      END 


      SUBROUTINE INICRYSSLIP(CBSLIP,CMSLIP)   
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER NSLIP, I, ISYS
      REAL*8 CBSLIP, CMSLIP, CBMAG, CMMAG
c      INCLUDE 'globals2.inc'
      PARAMETER(NSLIP = 12)
      DIMENSION CBSLIP(3,12),CMSLIP(3,12)

C       WRITE(*,*)'IN INICRYS'
C       WRITE(*,*)'IN INICRYS'
C      OPEN(35,FILE=PWD//SLIPIN)
C      Superslipsys.inc file information here
C      DO I=1,NSLIP
C         READ(35,*)(CMSLIP(J,I),J=1,3),(CBSLIP(J,I),J=1,3)
C      ENDDO
C
C     CMSLIP Data
      CMSLIP(1,1)  =  0.D0
      CMSLIP(2,1)  =  1.D0
      CMSLIP(3,1)  =  0.D0
      CMSLIP(1,2)  =  0.D0
      CMSLIP(2,2)  =  0.D0
      CMSLIP(3,2)  =  1.D0
      CMSLIP(1,3)  =  1.D0
      CMSLIP(2,3)  =  0.D0
      CMSLIP(3,3)  =  0.D0
      CMSLIP(1,4)  =  0.D0
      CMSLIP(2,4)  =  0.D0
      CMSLIP(3,4)  =  1.D0
      CMSLIP(1,5)  =  1.D0
      CMSLIP(2,5)  =  0.D0
      CMSLIP(3,5)  =  0.D0
      CMSLIP(1,6)  =  0.D0
      CMSLIP(2,6)  =  1.D0
      CMSLIP(3,6)  =  0.D0
      CMSLIP(1,7)  =  0.D0
      CMSLIP(2,7)  =  1.D0
      CMSLIP(3,7)  =  1.D0
      CMSLIP(1,8)  =  0.D0
      CMSLIP(2,8)  =  1.D0
      CMSLIP(3,8)  = -1.D0
      CMSLIP(1,9)  =  1.D0
      CMSLIP(2,9)  =  0.D0
      CMSLIP(3,9)  =  1.D0
      CMSLIP(1,10) =  1.D0
      CMSLIP(2,10) =  0.D0
      CMSLIP(3,10) = -1.D0
      CMSLIP(1,11) =  1.D0
      CMSLIP(2,11) =  1.D0
      CMSLIP(3,11) =  0.D0
      CMSLIP(1,12) = -1.D0
      CMSLIP(2,12) =  1.D0
      CMSLIP(3,12) =  0.D0
C     CMSLIP Data
      CBSLIP(1,1)  =  1.D0
      CBSLIP(2,1)  =  0.D0
      CBSLIP(3,1)  =  0.D0
      CBSLIP(1,2)  =  1.D0
      CBSLIP(2,2)  =  0.D0
      CBSLIP(3,2)  =  0.D0
      CBSLIP(1,3)  =  0.D0
      CBSLIP(2,3)  =  1.D0
      CBSLIP(3,3)  =  0.D0
      CBSLIP(1,4)  =  0.D0
      CBSLIP(2,4)  =  1.D0
      CBSLIP(3,4)  =  0.D0
      CBSLIP(1,5)  =  0.D0
      CBSLIP(2,5)  =  0.D0
      CBSLIP(3,5)  =  1.D0
      CBSLIP(1,6)  =  0.D0
      CBSLIP(2,6)  =  0.D0
      CBSLIP(3,6)  =  1.D0
      CBSLIP(1,7)  =  1.D0
      CBSLIP(2,7)  =  0.D0
      CBSLIP(3,7)  =  0.D0
      CBSLIP(1,8)  =  1.D0
      CBSLIP(2,8)  =  0.D0
      CBSLIP(3,8)  =  0.D0
      CBSLIP(1,9)  =  0.D0
      CBSLIP(2,9)  =  1.D0
      CBSLIP(3,9)  =  0.D0
      CBSLIP(1,10) =  0.D0
      CBSLIP(2,10) =  1.D0
      CBSLIP(3,10) =  0.D0
      CBSLIP(1,11) =  0.D0
      CBSLIP(2,11) =  0.D0
      CBSLIP(3,11) =  1.D0
      CBSLIP(1,12) =  0.D0
      CBSLIP(2,12) =  0.D0
      CBSLIP(3,12) =  1.D0

C      WRITE(*,*)CMSLIP
C      WRITE(*,*)CBSLIP
      DO ISYS=1,NSLIP
         CMMAG=SQRT(CMSLIP(1,ISYS)**2+CMSLIP(2,ISYS)**2+
     &        CMSLIP(3,ISYS)**2)
         CBMAG=SQRT(CBSLIP(1,ISYS)**2+CBSLIP(2,ISYS)**2+
     &        CBSLIP(3,ISYS)**2) 
         DO I=1,3
            CMSLIP(I,ISYS)=CMSLIP(I,ISYS)/CMMAG
            CBSLIP(I,ISYS)=CBSLIP(I,ISYS)/CBMAG
         ENDDO
      ENDDO
      
C       CLOSE(35)
   
C       WRITE(*,*)'OUT INICRYS'
      END

      SUBROUTINE INICRYS(CB,CM)
      
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I, NTRANS
      REAL*8 CB, CM, A1, A2, A3, A4, A5, A6

c      INCLUDE 'globals2.inc'
      PARAMETER(NTRANS=24)
      DIMENSION CM(3,NTRANS),CB(3,NTRANS)

      A1=0.8888
      A2=0.4045
      A3=0.2153
      A4=0.4343
      A5=0.4878
      A6=0.7576
      CM(1,1)=-A1
      CM(2,1)=-A2
      CM(3,1)=A3
      CB(1,1)=A4
      CB(2,1)=-A5
      CB(3,1)=A6
      
C$$$  M(1)
      DO I=1,8
         CM(1,I)=-A1
      ENDDO
      CM(1,9)=A2
      CM(1,10)=-A2
      CM(1,11)=A3
      CM(1,12)=A3
      
      CM(1,13)=-A3
      CM(1,14)=-A3
      CM(1,15)=A2
      CM(1,16)=-A2
      CM(1,17)=A3
      CM(1,18)=A3
      CM(1,19)=A2
      CM(1,20)=-A2
      CM(1,21)=A2
      CM(1,22)=-A2
      CM(1,23)=-A3
      CM(1,24)=-A3

C     %M(2)
      CM(2,1)=-A2
      CM(2,2)=A2
      CM(2,3)=A3
      CM(2,4)=A3
      CM(2,5)=-A3
      CM(2,6)=-A3
      CM(2,7)=A2
      CM(2,8)=-A2
      CM(2,9)=-A1
      CM(2,10)=-A1
      CM(2,11)=-A1
      CM(2,12)=-A1

      CM(2,13)=-A1
      CM(2,14)=-A1
      CM(2,15)=-A1
      CM(2,16)=-A1
      CM(2,17)=A2
      CM(2,18)=-A2
      CM(2,19)=A3
      CM(2,20)=A3
      CM(2,21)=-A3
      CM(2,22)=-A3
      CM(2,23)=A2
      CM(2,24)=-A2


C     %M(3)
      CM(3,1)=A3
      CM(3,2)=A3
      CM(3,3)=-A2
      CM(3,4)=A2
      CM(3,5)=A2
      CM(3,6)=-A2
      CM(3,7)=-A3
      CM(3,8)=-A3
      CM(3,9)=A3
      CM(3,10)=A3
      CM(3,11)=-A2
      CM(3,12)=A2


      CM(3,13)=A2
      CM(3,14)=-A2
      CM(3,15)=-A3
      CM(3,16)=-A3
      DO I=17,24
         CM(3,I)=-A1
      ENDDO
      
C     %B(1)
      DO I=1,8
         CB(1,I)=A4
      ENDDO
      CB(1,9)=A5
      CB(1,10)=-A5
      CB(1,11)=A6
      CB(1,12)=A6

      CB(1,13)=-A6
      CB(1,14)=-A6
      CB(1,15)=A5
      CB(1,16)=-A5
      CB(1,17)=A6
      CB(1,18)=A6
      CB(1,19)=A5
      CB(1,20)=-A5
      CB(1,21)=A5
      CB(1,22)=-A5
      CB(1,23)=-A6
      CB(1,24)=-A6
      


C     %B(2)
      CB(2,1)=-A5
      CB(2,2)=A5
      CB(2,3)=A6
      CB(2,4)=A6
      CB(2,5)=-A6
      CB(2,6)=-A6
      CB(2,7)=A5
      CB(2,8)=-A5
      DO I=9,16
         CB(2,I)=A4
      ENDDO

      CB(2,17)=A5
      CB(2,18)=-A5
      CB(2,19)=A6
      CB(2,20)=A6
      CB(2,21)=-A6
      CB(2,22)=-A6
      CB(2,23)=A5
      CB(2,24)=-A5
C     %B(3)
      CB(3,1)=A6
      CB(3,2)=A6
      CB(3,3)=-A5
      CB(3,4)=A5
      CB(3,5)=A5
      CB(3,6)=-A5
      CB(3,7)=-A6
      CB(3,8)=-A6
      CB(3,9)=A6
      CB(3,10)=A6
      CB(3,11)=-A5
      CB(3,12)=A5

      CB(3,13)=A5
      CB(3,14)=-A5
      CB(3,15)=-A6
      CB(3,16)=-A6
      DO I=17,24
         CB(3,I)=A4
      ENDDO      

      END



      SUBROUTINE  ABQARRAY2MAT(ABQARRAY,KM,NBLOCK,NR,XMATRIX)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION ABQARRAY(NBLOCK,NR),XMATRIX(3,3)
      IF(NR.EQ.9)THEN
         XMATRIX(1,1)=ABQARRAY(KM,1)
         XMATRIX(2,2)=ABQARRAY(KM,2)
         XMATRIX(3,3)=ABQARRAY(KM,3)
         XMATRIX(1,2)=ABQARRAY(KM,4)
         XMATRIX(2,3)=ABQARRAY(KM,5)
         XMATRIX(3,1)=ABQARRAY(KM,6)
         XMATRIX(2,1)=ABQARRAY(KM,7)
         XMATRIX(3,2)=ABQARRAY(KM,8)
         XMATRIX(1,3)=ABQARRAY(KM,9)
      ELSEIF(NR.EQ.6)THEN
         XMATRIX(1,1)=ABQARRAY(KM,1)
         XMATRIX(2,2)=ABQARRAY(KM,2)
         XMATRIX(3,3)=ABQARRAY(KM,3)
         XMATRIX(1,2)=ABQARRAY(KM,4)
         XMATRIX(2,3)=ABQARRAY(KM,5)
         XMATRIX(3,1)=ABQARRAY(KM,6)            
         XMATRIX(2,1)=ABQARRAY(KM,4)
         XMATRIX(3,2)=ABQARRAY(KM,6)
         XMATRIX(1,3)=ABQARRAY(KM,6)
      ENDIF
      END

      SUBROUTINE  ABQMAT2ARRAY(ABQARRAY,KM,NBLOCK,NR,XMATRIX)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
      DIMENSION ABQARRAY(NBLOCK,NR),XMATRIX(3,3)
      IF(NR.EQ.9)THEN
         ABQARRAY(KM,1)= XMATRIX(1,1)
         ABQARRAY(KM,2)= XMATRIX(2,2)
         ABQARRAY(KM,3)= XMATRIX(3,3)
         ABQARRAY(KM,4)= XMATRIX(1,2)
         ABQARRAY(KM,5)= XMATRIX(2,3)
         ABQARRAY(KM,6)= XMATRIX(3,1)
         ABQARRAY(KM,7)= XMATRIX(2,1)
         ABQARRAY(KM,8)= XMATRIX(3,2)
         ABQARRAY(KM,9)= XMATRIX(1,3)
      ELSEIF(NR.EQ.6)THEN
         ABQARRAY(KM,1)= XMATRIX(1,1)
         ABQARRAY(KM,2)= XMATRIX(2,2)
         ABQARRAY(KM,3)= XMATRIX(3,3)
         ABQARRAY(KM,4)= 0.50*(XMATRIX(1,2)+XMATRIX(2,1))
         ABQARRAY(KM,5)= 0.50*(XMATRIX(2,3)+XMATRIX(3,2))
         ABQARRAY(KM,6)= 0.50*(XMATRIX(3,1)+XMATRIX(1,3))
      ENDIF
      END




C     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C     FUNCTION STATEMENT TO CHANGE A NUMBER INTO A CHARACTER ******
C     
      FUNCTION STRGET(ITEMP)
C     
      INTEGER ITEMP
      CHARACTER*4 STRGET
C     
      STRGET=CHAR(48+ITEMP/1000)//CHAR(48+MOD(ITEMP/100,10))
     *     //CHAR(48+MOD(ITEMP/10,10))//
     *     CHAR(48+MOD(ITEMP,10))
C     
      RETURN
      END


C     CALCULATES THE INVERSE OF A 3*3 MATRIX
      SUBROUTINE MATINV3(A,AI,DET)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

C     INCLUDE 'aba_param.inc'

      DIMENSION A(3,3), AI(3,3)
C     
      DET=(A(1,1)*A(2,2)*A(3,3)-A(1,1)*A(2,3)*A(3,2)-A(2,1)*A(1,2)
     &     *A(3,3)+A(2,1)*A(1,3)*A(3,2)+A(3,1)*A(1,2)*A(2,3)-A(3,1)
     &     *A(1,3)*A(2,2))
      AI(1,1) =  ( A(2,2)*A(3,3)-A(2,3)*A(3,2))/DET
      AI(1,2) = -( A(1,2)*A(3,3)-A(1,3)*A(3,2))/DET
      AI(1,3) = -(-A(1,2)*A(2,3)+A(1,3)*A(2,2))/DET
      AI(2,1) = -( A(2,1)*A(3,3)-A(2,3)*A(3,1))/DET
      AI(2,2) =  ( A(1,1)*A(3,3)-A(1,3)*A(3,1))/DET
      AI(2,3) = -( A(1,1)*A(2,3)-A(1,3)*A(2,1))/DET
      AI(3,1) =  ( A(2,1)*A(3,2)-A(2,2)*A(3,1))/DET
      AI(3,2) = -( A(1,1)*A(3,2)-A(1,2)*A(3,1))/DET
      AI(3,3) =  ( A(1,1)*A(2,2)-A(1,2)*A(2,1))/DET
      RETURN
      END





C-----------------------------------------------------

C     THIS SUBROUTINE CALCULATES THE TRANSFORMATION MATRIX
C----------------------------------------------------------
C     PHI   - EULER(1)
C     THETA - EULER(2)
C     OMEGA - EULER(3)
C---------------------------------------------------


      SUBROUTINE EULER_SLIP(EULER,TLG)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)  

C     INCLUDE 'aba_param.inc'

      DIMENSION EULER(3),TLG(3,3),TLGT(3,3)

      PI=4.0*ATAN(1.0)

      PHI=EULER(1)*PI/180.0 
      THETA =EULER(2)*PI/180.0
      OMEGA  =EULER(3)*PI/180.0 

C      TLG(1,1)=1.0/SQRT(2.0)
C      TLG(1,2)=-1.0/SQRT(2.0)
C      TLG(1,3)=0.0
C      TLG(2,1)=1.0/SQRT(6.0)
C      TLG(2,2)=1.0/SQRT(6.0)
C      TLG(2,3)=-2.0/SQRT(6.0)
C      TLG(3,1)=1.0/SQRT(3.0)
C      TLG(3,2)=1.0/SQRT(3.0)
C      TLG(3,3)=1.0/SQRT(3.0)

C$$$
C$$$      TLG(1,1)=0.0
C$$$      TLG(1,2)=0.0
C$$$      TLG(1,3)=1.0
C$$$      TLG(2,1)=1.0/SQRT(2.0)
C$$$      TLG(2,2)=1.0/SQRT(2.0)
C$$$      TLG(2,3)=0.0
C$$$      TLG(3,1)=-1.0/SQRT(2.0)
C$$$      TLG(3,2)=1.0/SQRT(2.0)
C$$$      TLG(3,3)=0.0

C$$$
C$$$      
      SP=DSIN(PHI)                      
      CP=DCOS(PHI)                     
      ST=DSIN(THETA)                     
      CT=DCOS(THETA)                    
      SO=DSIN(OMEGA)                    
      CO=DCOS(OMEGA)   
      TLG(1,1)=CO*CP-SO*SP*CT
      TLG(1,2)=CO*SP+SO*CT*CP   
      TLG(1,3)=SO*ST   
      TLG(2,1)=-SO*CP-SP*CO*CT 
      TLG(2,2)=-SO*SP+CT*CO*CP
      TLG(2,3)=CO*ST
      TLG(3,1)=SP*ST       
      TLG(3,2)=-ST*CP       
      TLG(3,3)=CT
C$$$C$$$C     TLG IS GLOBAL TO LOCAL IF IT IS BUNGE DEF. OF EULER ANGLE
C$$$C$$$      CALL TRANS(TLG,TLGT)

      RETURN
      END   





      SUBROUTINE TENSORMUL42(SIGMA,CIJKL,EPSILON)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      DIMENSION CIJKL(6,6),EPSILON(6),SIGMA(6)     
      DO I=1,6
         SIGMA(I)=0.0
         DO J=1,6
            IF(J.GE.4)THEN
               SIGMA(I)=SIGMA(I)+(CIJKL(I,J)*(2.0*EPSILON(J)))
               ELSE
               SIGMA(I)=SIGMA(I)+(CIJKL(I,J)*(EPSILON(J)))
            ENDIF
         ENDDO
      ENDDO
      END


      SUBROUTINE TENSORMUL42_CRYS(SIGMA,CIJKL,EPSILON,NDIM)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      DIMENSION CIJKL(6,6),EPSILON(6,NDIM),SIGMA(6,NDIM)
      DO ISYS=1,NDIM
         DO I=1,6
            SIGMA(I,ISYS)=0.0
            DO J=1,6
               IF(J.GE.4)THEN
                  SIGMA(I,ISYS)=SIGMA(I,ISYS)+
     &                 (CIJKL(I,J)*(2.0*EPSILON(J,ISYS)))
               ELSE
                  SIGMA(I,ISYS)=SIGMA(I,ISYS)+
     &                 (CIJKL(I,J)*(EPSILON(J,ISYS)))
               ENDIF
            ENDDO
         ENDDO
      ENDDO

      END


C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
      SUBROUTINE SIGMAT(X,XX)    
C     
C     ARRANGES INTO A SYMMETRIC ARRAY XX, THE SIX COMMPONENTS OF A VECTOR X
C     
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     INCLUDE 'aba_param.inc'
      DIMENSION X(6),XX(3,3)
C     
      XX(1,1)=X(1)                                                    
      XX(2,2)=X(2)                                                    
      XX(3,3)=X(3)                                                    
      XX(2,3)=X(6)                                                    
      XX(3,2)=X(6)                                                    
      XX(3,1)=X(5)                                                    
      XX(1,3)=X(5)                                                    
      XX(1,2)=X(4)                                                    
      XX(2,1)=X(4)                                                    
      RETURN
      END




C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
      SUBROUTINE TR1TO2CRYS(X,XX,NDIM)    
C     
C     ARRANGES INTO A SYMMETRIC ARRAY XX, THE SIX COMMPONENTS OF A VECTOR X
C     
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     INCLUDE 'aba_param.inc'
      DIMENSION X(6,NDIM),XX(3,3,NDIM)
C     
      DO ISYS=1,NDIM
         XX(1,1,ISYS)=X(1,ISYS)
         XX(2,2,ISYS)=X(2,ISYS)
         XX(3,3,ISYS)=X(3,ISYS)
         XX(2,3,ISYS)=X(6,ISYS)
         XX(3,2,ISYS)=X(6,ISYS)
         XX(3,1,ISYS)=X(5,ISYS)
         XX(1,3,ISYS)=X(5,ISYS)
         XX(1,2,ISYS)=X(4,ISYS)
         XX(2,1,ISYS)=X(4,ISYS)
      ENDDO

      RETURN
      END



C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     MATRIX MULTIPLICATION SUBROUTINE 

      SUBROUTINE MATMUL(CC,AA,BB,N,M,L)
      
C     INCLUDE 'aba_param.inc'
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)

C     THIS SUBROUTINE RETURNS MARTIX [CC] = MATRIX [AA] * MATRIX [BB]
C     N=NO. OF ROWS OF [AA]      =NO. OF ROWS OF [CC]
C     M=NO. OF COLUMNS OF [AA]   =NO. OF ROWS OF [BB]
C     L=NO. OF COLUMNS OF [BB]   =NO. OF COLUMNS OF [CC]

      DIMENSION AA(N,M),BB(M,L),CC(N,L)

      DO 10 I=1,N
         DO 10 J=1,L
            CC(I,J)=0.0
            DO 10 K=1,M
 10                        CC(I,J)=CC(I,J)+AA(I,K)*BB(K,J)
               RETURN 
               END  
C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
      SUBROUTINE RCLEAR (A,NMAX)                                       
C     
C     FILLS A REAL ARRAY A WITH ZEROS
C     
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                             
C     INCLUDE 'aba_param.inc'
      DIMENSION A(NMAX)
C     
      DO I=1,NMAX                                                 
         A(I) = 0.0                                                     
      ENDDO
      RETURN
      END         
C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
      SUBROUTINE PROD(A,B,C)                                          
C     
C     COMPUTES THE MATRIX PRODUCT C=A*B  ALL MATRICIES ARE 3X3
C     
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                             
C     INCLUDE 'aba_param.inc'
      DIMENSION A(3,3),B(3,3),C(3,3)                                  
C     
      DO 200 J=1,3                                                    
         DO 200 I=1,3                                                    
            S=0.0                                                           
            DO 100 K=1,3                                                    
               S=S+A(I,K)*B(K,J)                                             
 100                   CONTINUE
            C(I,J)=S                                                        
 200             CONTINUE
         RETURN
         END   




C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     MULTIPLICATION OF TWO 3*3 MATRICES

      SUBROUTINE MAT33(X,Y,Z,L)
C     
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C     INCLUDE 'aba_param.inc'
      DIMENSION X(3,L),Y(3,3),Z(3,L)
C     
      DO I=1,3
         DO J=1,L
            X(I,J)=0
            DO  K=1,3
               X(I,J)=X(I,J)+Y(I,K)*Z(K,J)
            ENDDO
         ENDDO
      ENDDO
      RETURN
      END
C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--               
      SUBROUTINE ROTATE(TCH,R,T)                                      
C     
C     COMPUTES THE MATRIX T=R*TCH*(RTRANS)  (ALL MATRICES ARE 3X3)
C     
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     INCLUDE 'aba_param.inc'
      DIMENSION T(3,3),R(3,3),TCH(3,3)                                
C     
      DO 200 J=1,3                                                    
         DO 200 I=1,3                                                    
            S=0.0                                                           
            DO 100 L=1,3                                                    
               RJL=R(J,L)                                                    
               DO 100 K=1,3                                                  
                  S=S+R(I,K)*RJL*TCH(K,L)                                       
 100                         CONTINUE                                                      
               T(I,J)=S                                                        
 200                   CONTINUE                                                        
            RETURN
            END 


C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     CONVERTS FOURTH ORDER TENSOR TO EQUIVALENT SECOND ORDER TENSOR

      SUBROUTINE TR4TO2(ELMAT,ELMATR)
      IMPLICIT REAL*8(A-H,O-Z)
C     COURTESY L.ANAND
      DIMENSION ELMAT(3,3,3,3), ELMATR(6,6)
      
      DO 10 I = 1,6
         DO 10 J = 1,6
                ELMATR(I,J) = 0.0
 10                   CONTINUE
         
         
         ELMATR(1,1)  = ELMAT(1,1,1,1)
         ELMATR(1,2)  = ELMAT(1,1,2,2)
         ELMATR(1,3)  = ELMAT(1,1,3,3) 
         ELMATR(1,4)  = 0.5 * ( ELMAT(1,1,1,2) + ELMAT(1,1,2,1) )
         ELMATR(1,5)  = 0.5 * ( ELMAT(1,1,1,3) + ELMAT(1,1,3,1) )
         ELMATR(1,6)  = 0.5 * ( ELMAT(1,1,2,3) + ELMAT(1,1,3,2) )


         ELMATR(2,1)  = ELMAT(2,2,1,1)
         ELMATR(2,2)  = ELMAT(2,2,2,2)
         ELMATR(2,3)  = ELMAT(2,2,3,3) 
         ELMATR(2,4)  = 0.5 * ( ELMAT(2,2,1,2) + ELMAT(2,2,2,1) )
         ELMATR(2,5)  = 0.5 * ( ELMAT(2,2,1,3) + ELMAT(2,2,3,1) )
         ELMATR(2,6)  = 0.5 * ( ELMAT(2,2,2,3) + ELMAT(2,2,3,2) )
         
         ELMATR(3,1)  = ELMAT(3,3,1,1)
         ELMATR(3,2)  = ELMAT(3,3,2,2)
         ELMATR(3,3)  = ELMAT(3,3,3,3) 
         ELMATR(3,4)  = 0.5 * ( ELMAT(3,3,1,2) + ELMAT(3,3,2,1) )
         ELMATR(3,5)  = 0.5 * ( ELMAT(3,3,1,3) + ELMAT(3,3,3,1) )
         ELMATR(3,6)  = 0.5 * ( ELMAT(3,3,2,3) + ELMAT(3,3,3,2) )   
         
         ELMATR(4,1)  = ELMAT(1,2,1,1)
         ELMATR(4,2)  = ELMAT(1,2,2,2)
         ELMATR(4,3)  = ELMAT(1,2,3,3) 
         ELMATR(4,4)  = 0.5 * ( ELMAT(1,2,1,2) + ELMAT(1,2,2,1) )
         ELMATR(4,5)  = 0.5 * ( ELMAT(1,2,1,3) + ELMAT(1,2,3,1) )
         ELMATR(4,6)  = 0.5 * ( ELMAT(1,2,2,3) + ELMAT(1,2,3,2) ) 
         
         ELMATR(5,1)  = ELMAT(1,3,1,1)
         ELMATR(5,2)  = ELMAT(1,3,2,2)
         ELMATR(5,3)  = ELMAT(1,3,3,3) 
         ELMATR(5,4)  = 0.5 * ( ELMAT(1,3,1,2) + ELMAT(1,3,2,1) )
         ELMATR(5,5)  = 0.5 * ( ELMAT(1,3,1,3) + ELMAT(1,3,3,1) )
         ELMATR(5,6)  = 0.5 * ( ELMAT(1,3,2,3) + ELMAT(1,3,3,2) ) 

         ELMATR(6,1)  = ELMAT(2,3,1,1)
         ELMATR(6,2)  = ELMAT(2,3,2,2)
         ELMATR(6,3)  = ELMAT(2,3,3,3) 
         ELMATR(6,4)  = 0.5 * ( ELMAT(2,3,1,2) + ELMAT(2,3,2,1) )
         ELMATR(6,5)  = 0.5 * ( ELMAT(2,3,1,3) + ELMAT(2,3,3,1) )
         ELMATR(6,6)  = 0.5 * ( ELMAT(2,3,2,3) + ELMAT(2,3,3,2) ) 
         
         
         RETURN

         END


C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     CONVERTS A SECOND ORDER TENSOR TO A EQUIVALENT FOURTH ORDER TENSOR

      SUBROUTINE TR2TO4(ELMATR,ELMAT)
C      IMPLICIT REAL*8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I,J,K,L
      REAL*8 ELMATR, ELMAT

C     THIS SUBROUTINE RECONSTRUCTS A STIFFNESS MATRIX ELMATREC
C     AS A 3 X 3 X 3 X 3 MATRIX FROM ITS 
C     REDUCED 6 X 6 FORM ELMATR
C     
C     IMPLICIT REAL*8(A-H,O-Z) 
      DIMENSION ELMATR(6,6),ELMAT(3,3,3,3)
      
      DO 10 I = 1,3
         DO 10 J = 1,3
            DO 10 K = 1,3
               DO 10 L = 1,3
                  ELMAT(I,J,K,L) = 0.0
 10                           CONTINUE
               

C     
C     RECONSTRUCT THE MATRIX ELMAT FROM ITS REDUCED FORM ELMATR
C     
               
               ELMAT(1,1,1,1)  = ELMATR(1,1)
               ELMAT(1,1,2,2)  = ELMATR(1,2)
               ELMAT(1,1,3,3)  = ELMATR(1,3) 
               ELMAT(1,1,1,2)  = ELMATR(1,4)
               ELMAT(1,1,2,1)  = ELMATR(1,4)
               ELMAT(1,1,1,3)  = ELMATR(1,5)
               ELMAT(1,1,3,1)  = ELMATR(1,5)
               ELMAT(1,1,2,3)  = ELMATR(1,6)
               ELMAT(1,1,3,2)  = ELMATR(1,6)
               

               ELMAT(2,2,1,1)  = ELMATR(2,1)
               ELMAT(2,2,2,2)  = ELMATR(2,2)
               ELMAT(2,2,3,3)  = ELMATR(2,3) 
               ELMAT(2,2,1,2)  = ELMATR(2,4)
               ELMAT(2,2,2,1)  = ELMATR(2,4)
               ELMAT(2,2,1,3)  = ELMATR(2,5)
               ELMAT(2,2,3,1)  = ELMATR(2,5)
               ELMAT(2,2,2,3)  = ELMATR(2,6)
               ELMAT(2,2,3,2)  = ELMATR(2,6)
               
               ELMAT(3,3,1,1)  = ELMATR(3,1)
               ELMAT(3,3,2,2)  = ELMATR(3,2)
               ELMAT(3,3,3,3)  = ELMATR(3,3) 
               ELMAT(3,3,1,2)  = ELMATR(3,4)
               ELMAT(3,3,2,1)  = ELMATR(3,4)
               ELMAT(3,3,1,3)  = ELMATR(3,5)
               ELMAT(3,3,3,1)  = ELMATR(3,5)
               ELMAT(3,3,2,3)  = ELMATR(3,6)
               ELMAT(3,3,3,2)  = ELMATR(3,6)
               
               ELMAT(1,2,1,1)  =  ELMATR(4,1)
               ELMAT(2,1,1,1)  =  ELMATR(4,1)
               ELMAT(1,2,2,2)  =  ELMATR(4,2)
               ELMAT(2,1,2,2)  =  ELMATR(4,2)
               ELMAT(1,2,3,3)  =  ELMATR(4,3)
               ELMAT(2,1,3,3)  =  ELMATR(4,3)
               ELMAT(1,2,1,2)  =  ELMATR(4,4) 
               ELMAT(2,1,1,2)  =  ELMATR(4,4) 
               ELMAT(1,2,2,1)  =  ELMATR(4,4)
               ELMAT(2,1,2,1)  =  ELMATR(4,4)
               ELMAT(1,2,1,3)  =  ELMATR(4,5) 
               ELMAT(2,1,1,3)  =  ELMATR(4,5) 
               ELMAT(1,2,3,1)  =  ELMATR(4,5)
               ELMAT(2,1,3,1)  =  ELMATR(4,5)
               ELMAT(1,2,2,3)  =  ELMATR(4,6) 
               ELMAT(2,1,2,3)  =  ELMATR(4,6) 
               ELMAT(1,2,3,2)  =  ELMATR(4,6)
               ELMAT(2,1,3,2)  =  ELMATR(4,6)
               
               
               ELMAT(1,3,1,1)  =  ELMATR(5,1)
               ELMAT(3,1,1,1)  =  ELMATR(5,1)
               ELMAT(1,3,2,2)  =  ELMATR(5,2)
               ELMAT(3,1,2,2)  =  ELMATR(5,2)
               ELMAT(1,3,3,3)  =  ELMATR(5,3)
               ELMAT(3,1,3,3)  =  ELMATR(5,3)
               ELMAT(1,3,1,2)  =  ELMATR(5,4) 
               ELMAT(3,1,1,2)  =  ELMATR(5,4) 
               ELMAT(1,3,2,1)  =  ELMATR(5,4)
               ELMAT(3,1,2,1)  =  ELMATR(5,4)
               ELMAT(1,3,1,3)  =  ELMATR(5,5) 
               ELMAT(3,1,1,3)  =  ELMATR(5,5) 
               ELMAT(1,3,3,1)  =  ELMATR(5,5)
               ELMAT(3,1,3,1)  =  ELMATR(5,5)
               ELMAT(1,3,2,3)  =  ELMATR(5,6) 
               ELMAT(3,1,2,3)  =  ELMATR(5,6) 
               ELMAT(1,3,3,2)  =  ELMATR(5,6)
               ELMAT(3,1,3,2)  =  ELMATR(5,6)
               
               ELMAT(2,3,1,1)  =  ELMATR(6,1)
               ELMAT(3,2,1,1)  =  ELMATR(6,1)
               ELMAT(2,3,2,2)  =  ELMATR(6,2)
               ELMAT(3,2,2,2)  =  ELMATR(6,2)
               ELMAT(2,3,3,3)  =  ELMATR(6,3)
               ELMAT(3,2,3,3)  =  ELMATR(6,3)
               ELMAT(2,3,1,2)  =  ELMATR(6,4) 
               ELMAT(3,2,1,2)  =  ELMATR(6,4) 
               ELMAT(2,3,2,1)  =  ELMATR(6,4)
               ELMAT(3,2,2,1)  =  ELMATR(6,4)
               ELMAT(2,3,1,3)  =  ELMATR(6,5) 
               ELMAT(3,2,1,3)  =  ELMATR(6,5) 
               ELMAT(2,3,3,1)  =  ELMATR(6,5)
               ELMAT(3,2,3,1)  =  ELMATR(6,5)
               ELMAT(2,3,2,3)  =  ELMATR(6,6) 
               ELMAT(3,2,2,3)  =  ELMATR(6,6) 
               ELMAT(2,3,3,2)  =  ELMATR(6,6)
               ELMAT(3,2,3,2)  =  ELMATR(6,6)

               RETURN 
               END





      SUBROUTINE MATINV (A,M,ISIZE)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C----------------------------------------------------------
C     INVERSE AND DETERMINANT OF A BY THE GAUSS-JORDAN METHOD.
C     M IS THE ORDER OF THE SQUARE MATRIX, A.
C     A-INVERSE REPLACES A.
C     DETERMINANT OF A IS PLACED IN DET.
C     COOLEY AND LOHNES (1971:63)
C-----------------------------------------------------------
      PARAMETER (IDIM=50)
      DIMENSION A(ISIZE,ISIZE),IPVT(IDIM+2),PVT(IDIM+2),IND(IDIM+2,2)
      DET=.1D1
      DO 10 J=1,M
 10            IPVT(J)=0
         DO 100 I=1,M
C     SEARCH FOR THE PIVOT ELEMENT
            AMAX=.0D0
            DO 40 J=1,M
               IF(IPVT(J).EQ.1)GO TO 40
               DO 30 K=1,M
                  IF(IPVT(K)-1)20,30,130
 20                              IF(DABS(AMAX).GE.DABS(A(J,K)))GO TO 30
                  IROW=J
                  ICOL=K
                  AMAX=A(J,K)
 30                           CONTINUE
 40                                    CONTINUE
            IPVT(ICOL)=IPVT(ICOL)+1
C     INTERCHANGE THE ROWS TO PUT THE PIVOT ELEMENT ON THE DIAGONAL
            IF(IROW.EQ.ICOL)GO TO 60
            DET=-DET
            DO 50 L=1,M
               SWAP=A(IROW,L)
               A(IROW,L)=A(ICOL,L)
 50                        A(ICOL,L)=SWAP
 60                                    IND(I,1)=IROW
               IND(I,2)=ICOL
               PVT(I)=A(ICOL,ICOL)
C     DIVIDE THE PIVOT ROW BY THE PIVOT ELEMENT
               A(ICOL,ICOL)=.1D1
               DO 70 L=1,M
                  A(ICOL,L)=A(ICOL,L)/PVT(I)
 70                           CONTINUE
C     REDUCE THE NON-PIVOT ROWS
               DO 100 L1=1,M
                  IF(L1.EQ.ICOL)GO TO 90
                  SWAP=A(L1,ICOL)
                  A(L1,ICOL)=.0D0
                  IF(SWAP.LT..1D-30.AND.SWAP.GT.-.1D-30)SWAP=.0D0
                  DO 80 L=1,M
                     A(L1,L)=A(L1,L)-A(ICOL,L)*SWAP
 80                                 CONTINUE
 90                                                CONTINUE
 100                                                          CONTINUE
C     INTERCHANGE THE COLUMNS
               DO 120 I=1,M
                  L=M+1-I
                  IF(IND(L,1).EQ.IND(L,2))GO TO 120
                  IROW=IND(L,1)
                  ICOL=IND(L,2)
                  DO 110 K=1,M
                     SWAP=A(K,IROW)
                     A(K,IROW)=A(K,ICOL)
                     A(K,ICOL)=SWAP
 110                               CONTINUE
 120                                          CONTINUE
 130                                                     CONTINUE
               RETURN
               END


C     
C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
      SUBROUTINE MAT44(X,Y,Z)
C     
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C     INCLUDE 'aba_param.inc'
      DIMENSION X(3,3,3,3),Y(3,3,3,3),Z(3,3,3,3) 
C     
      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  X(I,J,K,L)=00
                  DO M=1,3
                     DO N=1,3
                        X(I,J,K,L)=X(I,J,K,L)+Y(I,J,M,N)*Z(M,N,K,L)
                     ENDDO
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDDO
C     
      RETURN
      END           
C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     CALCULATES THE TRANSPOSE OF A MATRIX

      SUBROUTINE TRANS(X,X_T)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C     INCLUDE 'aba_param.inc'
      DIMENSION X(3,3),X_T(3,3) 
C     
      DO I=1,3
         DO J=1,3
            X_T(J,I)=X(I,J)
         ENDDO
      ENDDO
      RETURN
      END

C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     CALCULATES THE TRANSPOSE OF A MATRIX

      SUBROUTINE TRANSMN(X,X_T,M,N)
      IMPLICIT DOUBLE PRECISION(A-H,O-Z)
C     INCLUDE 'aba_param.inc'
      DIMENSION X(M,N),X_T(N,M) 
C     
      DO I=1,M
         DO J=1,N
            X_T(J,I)=X(I,J)
         ENDDO
      ENDDO
      RETURN
      END
C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--






      SUBROUTINE MAT42(X,Y,Z)
C      IMPLICIT REAL*8 (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I,J,K, L,N
      REAL*8 X,Y,Z

C     INCLUDE 'aba_param.inc'
      DIMENSION X(3,3,3,3),Y(3,3,3,3),Z(3,3)
C     
      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  X(I,J,K,L)=00
                  DO N=1,3
                     X(I,J,K,L)=X(I,J,K,L)
     &                    +Y(I,N,K,L)*Z(N,J)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDDO
C     
      RETURN
      END

C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
      SUBROUTINE MAT33CRYS(X,Y,Z,NDIM)

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      
      DIMENSION X(3,3,3,3),Y(3,3,NDIM),Z(3,3,NDIM)
C     
      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  X(I,J,K,L)=0D0
                  DO ISYS=1,NDIM
                     X(I,J,K,L)=X(I,J,K,L)
     &                    +Y(I,J,ISYS)*Z(K,L,ISYS)
                  ENDDO
               ENDDO
            ENDDO
         ENDDO
      ENDDO
C     
      RETURN
      END
C---+----1----+----2----+----3----+----4----+----5----+----6----+----7--
      SUBROUTINE MAT44CRYS(X,Y,Z,NDIM)
C
       IMPLICIT DOUBLE PRECISION(A-H,O-Z)
       
      DIMENSION X(3,3,3,3,NDIM),Y(3,3,3,3),Z(3,3,3,3,NDIM) 
C
      DO ISYS=1,NDIM
        DO I=1,3
          DO J=1,3
            DO K=1,3
              DO L=1,3
                X(I,J,K,L,ISYS)=0D0
                DO M=1,3
                  DO N=1,3
                    X(I,J,K,L,ISYS)=X(I,J,K,L,ISYS)
     &                +Y(I,J,M,N)*Z(M,N,K,L,ISYS)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
C
      RETURN
      END
C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7-
C     FORMS A FOURTH ORDER IDENTITY TENSOR

      SUBROUTINE CAL_CKRONE(CKRONE)

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     INCLUDE 'aba_param.inc'
      DIMENSION CKRONE(3,3,3,3)

      DO I=1,3
         DO J=1,3
            DO K=1,3
               DO L=1,3
                  CKRONE(I,J,K,L)=00
               ENDDO
            ENDDO
         ENDDO
      ENDDO
C     
      CKRONE(1,1,1,1)=1
      CKRONE(2,2,2,2)=1
      CKRONE(3,3,3,3)=1
      CKRONE(1,2,1,2)=1
      CKRONE(1,3,1,3)=1
      CKRONE(2,3,2,3)=1
      CKRONE(2,1,2,1)=1.0
      CKRONE(3,1,3,1)=1.0
      CKRONE(3,2,3,2)=1.0
C     
C     
      RETURN
      END
C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     FORMS A 1-D IDENTITY VECTOR

      SUBROUTINE CAL_CKRONE_1D(CKRONE_1D)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     INCLUDE 'aba_param.inc'    
      DIMENSION CKRONE_1D(6)
C     
      DO I=1,6
         IF(I.EQ.1.OR.I.EQ.2.OR.I.EQ.3) THEN
            CKRONE_1D(I)=1
         ELSE
            CKRONE_1D(I)=0
         ENDIF
      ENDDO

      RETURN
      END
C     
C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     FORMS A SECOND ORDER IDENTITY TENSOR

      SUBROUTINE CAL_CKRONE_2D(CKRONE_2D)

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C     INCLUDE 'aba_param.inc'
      DIMENSION CKRONE_2D(3,3)
C     
      DO I=1,3
         DO J=1,3
            IF(I.EQ.J) THEN
               CKRONE_2D(I,J)=1.0
            ELSE
               CKRONE_2D(I,J)=0.0
            ENDIF
         ENDDO
      ENDDO
C     
      RETURN
      END
C---  +----1----+----2----+----3----+----4----+----5----+----6----+----7--
C     SETS ALL THE COMPONENTS OF A 6*6 MATRIX TO ZERO

      SUBROUTINE RCLEAR66(X)

C      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER I,J
      REAL*8 X

C     INCLUDE 'aba_param.inc'
      DIMENSION X(6,6)
C     
      DO I=1,6
         DO J=1,6
            X(I,J)=0
         ENDDO
      ENDDO
C     
      RETURN
      END


C******************************************************


      
C----------------------------------------------------
C     CALCULATES THE SIGN 
      
      FUNCTION DSIGNF(X)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)      
      IF(X.GE.0)THEN
         DSIGNF=+1.0
      ELSE
         DSIGNF=-1.0
      ENDIF
      RETURN
      END
      
C-----------------------------------------------------
      SUBROUTINE ROTATE_CRYS_VECTOR(A,Q,A_PRIME,NDIM)
C      IMPLICIT REAL*8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER NDIM, ISYS, I, J
      REAL*8 A, Q, A_PRIME

c      INCLUDE 'globals2.inc'
      DIMENSION A(3,NDIM),A_PRIME(3,NDIM),Q(3,3)
      DO ISYS=1,NDIM
         DO I=1,3
            A_PRIME(I,ISYS)=0.0
            DO J=1,3
               A_PRIME(I,ISYS)=A_PRIME(I,ISYS)+Q(I,J)*A(J,ISYS)
            ENDDO
         ENDDO
      ENDDO
      RETURN
      END



C------------------------------------------------------------
C     TRANSFORMS THE FOURTH ORDER ELASTICITY TENSOR FROM 
C     CRYSTAL SYSTEM TO GLOBAL 
C     COURTESY L.ANAND
      SUBROUTINE ROTATE4D(ELMATC,Q,ELMAT)
      IMPLICIT REAL*8(A-H,O-Z)
C     TRANSFORMS A FOURTH ORDER TENSOR 
      DIMENSION ELMATC(3,3,3,3),ELMAT(3,3,3,3),Q(3,3)
      DIMENSION AUXMAT1(3,3,3,3),AUXMAT2(3,3,3,3)

      DO 20 I=   1,3
         DO 20 N =  1,3
            DO 20 IO = 1,3
               DO 20 IP = 1,3
                  AUXMAT1(I,N,IO,IP) = 0.0
                  DO 15  M = 1,3
                     AUXMAT1(I,N,IO,IP) = AUXMAT1(I,N,IO,IP) +
     &                    ELMATC(M,N,IO,IP)*Q(I,M)
 15                                 CONTINUE
 20                                             CONTINUE

               DO 30 I=   1,3
                  DO 30 J =  1,3
                     DO 30 IO = 1,3
                        DO 30 IP = 1,3
                           AUXMAT2(I,J,IO,IP) = 0.0
                           DO 25  N = 1,3
                              AUXMAT2(I,J,IO,IP) = AUXMAT2(I,J,IO,IP) +
     &                             AUXMAT1(I,N,IO,IP)*Q(J,N)
 25                                                   CONTINUE
 30                                                   CONTINUE

                        DO 40 I  = 1,3
                           DO 40 J  = 1,3
                              DO 40 K  = 1,3
                                 DO 40 IP = 1,3
                                    AUXMAT1(I,J,K,IP) = 0.0
                                    DO 35  IO = 1,3
                                       AUXMAT1(I,J,K,IP) = 
     &                                      AUXMAT1(I,J,K,IP) +
     &                                      AUXMAT2(I,J,IO,IP)*Q(K,IO)
 35                                                             CONTINUE
 40                                                             CONTINUE

                                 DO 50 I  = 1,3
                                    DO 50 J  = 1,3
                                       DO 50 K  = 1,3
                                          DO 50 L  = 1,3
                                             ELMAT(I,J,K,L) = 0.0
                                             DO 45  IP = 1,3
                                                ELMAT(I,J,K,L) = 
     &                                               ELMAT(I,J,K,L) +
     &                                               AUXMAT1(I,J,K,IP)
     &                                               *Q(L,IP)
 45                                                             CONTINUE
 50                                                             CONTINUE

                                          
                                          RETURN
                                          END




      SUBROUTINE TR2TO1(A_2D,A_1D)
C     Does this subroutine work for kinetic and kinematic tensors???? JAM
C     This may need fixed
C      IMPLICIT REAL*8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      REAL*8 A_2D, A_1D

      DIMENSION A_2D(3,3),A_1D(6)
      A_1D(1)=A_2D(1,1)
      A_1D(2)=A_2D(2,2)
      A_1D(3)=A_2D(3,3)
      A_1D(4)=0.5*(A_2D(1,2)+A_2D(2,1))
      A_1D(5)=0.5*(A_2D(1,3)+A_2D(3,1))
      A_1D(6)=0.5*(A_2D(2,3)+A_2D(3,2))
      RETURN
      END

      SUBROUTINE TR2TO1CRYS(A_2D,A_1D,NDIM)
C     Does this subroutine work for kinetic and kinematic tensors???? JAM
C     This may need fixed
C      IMPLICIT REAL*8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER NDIM, ISYS
      REAL*8 A_2D, A_1D

      DIMENSION A_2D(3,3,NDIM),A_1D(6,NDIM)
      DO ISYS=1,NDIM
         A_1D(1,ISYS)=A_2D(1,1,ISYS)
         A_1D(2,ISYS)=A_2D(2,2,ISYS)
         A_1D(3,ISYS)=A_2D(3,3,ISYS)
         A_1D(4,ISYS)=0.50*(A_2D(1,2,ISYS)+A_2D(2,1,ISYS))
         A_1D(5,ISYS)=0.5D0*(A_2D(1,3,ISYS)+A_2D(3,1,ISYS))
         A_1D(6,ISYS)=0.5D0*(A_2D(2,3,ISYS)+A_2D(3,2,ISYS))
      ENDDO
      RETURN
      END


C------------------------------------------
C     CALCULATES THE NORM OF THE RESIDUAL
      SUBROUTINE VECMAG(A,N,R)
C     IMPLICIT REAL*8(A-H,O-Z)
      IMPLICIT NONE
C     Becuase I added implicit none JAM
      INTEGER N, I
      REAL*8 A, R

      DIMENSION A(N)
      R=0.0
      DO I=1,N
         R=R+A(I)**2
      ENDDO
      R=SQRT(R)
      RETURN
      END
C-------------------------------------------

      


C     
C     QUICK SORT
C     
C     FROM LEONARD J. MOSS OF SLAC:

C     HERE'S A HYBRID QUICKSORT I WROTE A NUMBER OF YEARS AGO.  IT'S
C     BASED ON SUGGESTIONS IN KNUTH, VOLUME 3, AND PERFORMS MUCH BETTER
C     THAN A PURE QUICKSORT ON SHORT OR PARTIALLY ORDERED INPUT ARRAYS.  

      SUBROUTINE SORTRX(N,SORTDATA,INDEX)
C===================================================================
C     
C     SORTRX -- SORT, REAL INPUT, INDEX OUTPUT
C     
C     
C     INPUT:  N     INTEGER
C     SORTDATA  REAL
C     
C     OUTPUT: INDEX INTEGER (DIMENSION N)
C     
C     THIS ROUTINE PERFORMS AN IN-MEMORY SORT OF THE FIRST N ELEMENTS OF
C     ARRAY SORTDATA, RETURNING INTO ARRAY INDEX THE INDICES OF ELEMENTS OF
C     SORTDATA ARRANGED IN ASCENDING ORDER.  THUS,
C     
C     SORTDATA(INDEX(1)) WILL BE THE SMALLEST NUMBER IN ARRAY SORTDATA;
C     SORTDATA(INDEX(N)) WILL BE THE LARGEST NUMBER IN SORTDATA.
C     
C     THE ORIGINAL DATA IS NOT PHYSICALLY REARRANGED.  THE ORIGINAL ORDER
C     OF EQUAL INPUT VALUES IS NOT NECESSARILY PRESERVED.
C     
C===================================================================
C     
C     SORTRX USES A HYBRID QUICKSORT ALGORITHM, BASED ON SEVERAL
C     SUGGESTIONS IN KNUTH, VOLUME 3, SECTION 5.2.2.  IN PARTICULAR, THE
C     "PIVOT KEY" [MY TERM] FOR DIVIDING EACH SUBSEQUENCE IS CHOSEN TO BE
C     THE MEDIAN OF THE FIRST, LAST, AND MIDDLE VALUES OF THE SUBSEQUENCE;
C     AND THE QUICKSORT IS CUT OFF WHEN A SUBSEQUENCE HAS 9 OR FEWER
C     ELEMENTS, AND A STRAIGHT INSERTION SORT OF THE ENTIRE ARRAY IS DONE
C     AT THE END.  THE RESULT IS COMPARABLE TO A PURE INSERTION SORT FOR
C     VERY SHORT ARRAYS, AND VERY FAST FOR VERY LARGE ARRAYS (OF ORDER 12
C     MICRO-SEC/ELEMENT ON THE 3081K FOR ARRAYS OF 10K ELEMENTS).  IT IS
C     ALSO NOT SUBJECT TO THE POOR PERFORMANCE OF THE PURE QUICKSORT ON
C     PARTIALLY ORDERED DATA.
C     
C     CREATED:  15 JUL 1986  LEN MOSS
C     
C===================================================================
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      INTEGER   N,INDEX(N)
      REAL*8      SORTDATA(N)
      
      INTEGER   LSTK(31),RSTK(31),ISTK
      INTEGER   L,R,I,J,P,INDEXP,INDEXT
      REAL*8      SORTDATAP
      
C     QUICKSORT CUTOFF
C     
C     QUIT QUICKSORT-ING WHEN A SUBSEQUENCE CONTAINS M OR FEWER
C     ELEMENTS AND FINISH OFF AT END WITH STRAIGHT INSERTION SORT.
C     ACCORDING TO KNUTH, V.3, THE OPTIMUM VALUE OF M IS AROUND 9.
      
      INTEGER   M
      PARAMETER (M=9)
      
C===================================================================
C     
C     MAKE INITIAL GUESS FOR INDEX
      
      DO 50 I=1,N
         INDEX(I)=I
 50         CONTINUE
      
C     IF ARRAY IS SHORT, SKIP QUICKSORT AND GO DIRECTLY TO
C     THE STRAIGHT INSERTION SORT.
      
      IF (N.LE.M) GOTO 900
      
C===================================================================
C     
C     QUICKSORT
C     
C     THE "QN:"S CORRESPOND ROUGHLY TO STEPS IN ALGORITHM Q,
C     KNUTH, V.3, PP.116-117, MODIFIED TO SELECT THE MEDIAN
C     OF THE FIRST, LAST, AND MIDDLE ELEMENTS AS THE "PIVOT
C     KEY" (IN KNUTH'S NOTATION, "K").  ALSO MODIFIED TO LEAVE
C     DATA IN PLACE AND PRODUCE AN INDEX ARRAY.  TO SIMPLIFY
C     COMMENTS, LET SORTDATA[I]=SORTDATA(INDEX(I)).
      
C     Q1: INITIALIZE
      ISTK=0
      L=1
      R=N
      
 200    CONTINUE
      
C     Q2: SORT THE SUBSEQUENCE SORTDATA[L]..SORTDATA[R].
C     
C     AT THIS POINT, SORTDATA[L] <= SORTDATA[M] <= SORTDATA[R] FOR ALL L < L,
C     R > R, AND L <= M <= R.  (FIRST TIME THROUGH, THERE IS NO
C     SORTDATA FOR L < L OR R > R.)
      
      I=L
      J=R
      
C     Q2.5: SELECT PIVOT KEY
C     
C     LET THE PIVOT, P, BE THE MIDPOINT OF THIS SUBSEQUENCE,
C     P=(L+R)/2; THEN REARRANGE INDEX(L), INDEX(P), AND INDEX(R)
C     SO THE CORRESPONDING SORTDATA VALUES ARE IN INCREASING ORDER.
C     THE PIVOT KEY, SORTDATAP, IS THEN SORTDATA[P].
      
      P=(L+R)/2
      INDEXP=INDEX(P)
      SORTDATAP=SORTDATA(INDEXP)
      
      IF (SORTDATA(INDEX(L)) .GT. SORTDATAP) THEN
         INDEX(P)=INDEX(L)
         INDEX(L)=INDEXP
         INDEXP=INDEX(P)
         SORTDATAP=SORTDATA(INDEXP)
      ENDIF
      
      IF (SORTDATAP .GT. SORTDATA(INDEX(R))) THEN
         IF (SORTDATA(INDEX(L)) .GT. SORTDATA(INDEX(R))) THEN
            INDEX(P)=INDEX(L)
            INDEX(L)=INDEX(R)
         ELSE
            INDEX(P)=INDEX(R)
         ENDIF
         INDEX(R)=INDEXP
         INDEXP=INDEX(P)
         SORTDATAP=SORTDATA(INDEXP)
      ENDIF
      
C     NOW WE SWAP VALUES BETWEEN THE RIGHT AND LEFT SIDES AND/OR
C     MOVE SORTDATAP UNTIL ALL SMALLER VALUES ARE ON THE LEFT AND ALL
C     LARGER VALUES ARE ON THE RIGHT.  NEITHER THE LEFT OR RIGHT
C     SIDE WILL BE INTERNALLY ORDERED YET; HOWEVER, SORTDATAP WILL BE
C     IN ITS FINAL POSITION.
      
 300    CONTINUE
      
C     Q3: SEARCH FOR DATUM ON LEFT >= SORTDATAP
C     
C     AT THIS POINT, SORTDATA[L] <= SORTDATAP.  WE CAN THEREFORE START SCANNING
C     UP FROM L, LOOKING FOR A VALUE >= SORTDATAP (THIS SCAN IS GUARANTEED
C     TO TERMINATE SINCE WE INITIALLY PLACED SORTDATAP NEAR THE MIDDLE OF
C     THE SUBSEQUENCE).
      
      I=I+1
      IF (SORTDATA(INDEX(I)).LT.SORTDATAP) GOTO 300
      
 400    CONTINUE
      
C     Q4: SEARCH FOR DATUM ON RIGHT <= SORTDATAP
C     
C     AT THIS POINT, SORTDATA[R] >= SORTDATAP.  WE CAN THEREFORE START SCANNING
C     DOWN FROM R, LOOKING FOR A VALUE <= SORTDATAP (THIS SCAN IS GUARANTEED
C     TO TERMINATE SINCE WE INITIALLY PLACED SORTDATAP NEAR THE MIDDLE OF
C     THE SUBSEQUENCE).
      
      J=J-1
      IF (SORTDATA(INDEX(J)).GT.SORTDATAP) GOTO 400
      
C     Q5: HAVE THE TWO SCANS COLLIDED?
      
      IF (I.LT.J) THEN
         
C     Q6: NO, INTERCHANGE SORTDATA[I] <--> SORTDATA[J] AND CONTINUE
         
         INDEXT=INDEX(I)
         INDEX(I)=INDEX(J)
         INDEX(J)=INDEXT
         GOTO 300
      ELSE
         
C     Q7: YES, SELECT NEXT SUBSEQUENCE TO SORT
C     
C     AT THIS POINT, I >= J AND SORTDATA[L] <= SORTDATA[I] == SORTDATAP <= SORTDATA[R],
C     FOR ALL L <= L < I AND J < R <= R.  IF BOTH SUBSEQUENCES ARE
C     MORE THAN M ELEMENTS LONG, PUSH THE LONGER ONE ON THE STACK AND
C     GO BACK TO QUICKSORT THE SHORTER; IF ONLY ONE IS MORE THAN M
C     ELEMENTS LONG, GO BACK AND QUICKSORT IT; OTHERWISE, POP A
C     SUBSEQUENCE OFF THE STACK AND QUICKSORT IT.
         
         IF (R-J .GE. I-L .AND. I-L .GT. M) THEN
            ISTK=ISTK+1
            LSTK(ISTK)=J+1
            RSTK(ISTK)=R
            R=I-1
         ELSE IF (I-L .GT. R-J .AND. R-J .GT. M) THEN
            ISTK=ISTK+1
            LSTK(ISTK)=L
            RSTK(ISTK)=I-1
            L=J+1
         ELSE IF (R-J .GT. M) THEN
            L=J+1
         ELSE IF (I-L .GT. M) THEN
            R=I-1
         ELSE
C     Q8: POP THE STACK, OR TERMINATE QUICKSORT IF EMPTY
            IF (ISTK.LT.1) GOTO 900
            L=LSTK(ISTK)
            R=RSTK(ISTK)
            ISTK=ISTK-1
         ENDIF
         GOTO 200
      ENDIF
      
 900    CONTINUE
      
C===================================================================
C     
C     Q9: STRAIGHT INSERTION SORT
      
      DO 950 I=2,N
         IF (SORTDATA(INDEX(I-1)) .GT. SORTDATA(INDEX(I))) THEN
            INDEXP=INDEX(I)
            SORTDATAP=SORTDATA(INDEXP)
            P=I-1
 920                CONTINUE
            INDEX(P+1) = INDEX(P)
            P=P-1
            IF (P.GT.0) THEN
               IF (SORTDATA(INDEX(P)).GT.SORTDATAP) GOTO 920
            ENDIF
            INDEX(P+1) = INDEXP
         ENDIF
 950       CONTINUE
      
C===================================================================
C     
C     ALL DONE
      
      END

      
C     
C     QUICK SORT
C     
C     FROM LEONARD J. MOSS OF SLAC:

C     HERE'S A HYBRID QUICKSORT I WROTE A NUMBER OF YEARS AGO.  IT'S
C     BASED ON SUGGESTIONS IN KNUTH, VOLUME 3, AND PERFORMS MUCH BETTER
C     THAN A PURE QUICKSORT ON SHORT OR PARTIALLY ORDERED INPUT ARRAYS.  

      SUBROUTINE ISORTRX(N,SORTDATA,INDEX)
C===================================================================
C     
C     SORTRX -- SORT, REAL INPUT, INDEX OUTPUT
C     
C     
C     INPUT:  N     INTEGER
C     SORTDATA  INTEGER
C     
C     OUTPUT: INDEX INTEGER (DIMENSION N)
C     
C     THIS ROUTINE PERFORMS AN IN-MEMORY SORT OF THE FIRST N ELEMENTS OF
C     ARRAY SORTDATA, RETURNING INTO ARRAY INDEX THE INDICES OF ELEMENTS OF
C     SORTDATA ARRANGED IN ASCENDING ORDER.  THUS,
C     
C     SORTDATA(INDEX(1)) WILL BE THE SMALLEST NUMBER IN ARRAY SORTDATA;
C     SORTDATA(INDEX(N)) WILL BE THE LARGEST NUMBER IN SORTDATA.
C     
C     THE ORIGINAL DATA IS NOT PHYSICALLY REARRANGED.  THE ORIGINAL ORDER
C     OF EQUAL INPUT VALUES IS NOT NECESSARILY PRESERVED.
C     
C===================================================================
C     
C     SORTRX USES A HYBRID QUICKSORT ALGORITHM, BASED ON SEVERAL
C     SUGGESTIONS IN KNUTH, VOLUME 3, SECTION 5.2.2.  IN PARTICULAR, THE
C     "PIVOT KEY" [MY TERM] FOR DIVIDING EACH SUBSEQUENCE IS CHOSEN TO BE
C     THE MEDIAN OF THE FIRST, LAST, AND MIDDLE VALUES OF THE SUBSEQUENCE;
C     AND THE QUICKSORT IS CUT OFF WHEN A SUBSEQUENCE HAS 9 OR FEWER
C     ELEMENTS, AND A STRAIGHT INSERTION SORT OF THE ENTIRE ARRAY IS DONE
C     AT THE END.  THE RESULT IS COMPARABLE TO A PURE INSERTION SORT FOR
C     VERY SHORT ARRAYS, AND VERY FAST FOR VERY LARGE ARRAYS (OF ORDER 12
C     MICRO-SEC/ELEMENT ON THE 3081K FOR ARRAYS OF 10K ELEMENTS).  IT IS
C     ALSO NOT SUBJECT TO THE POOR PERFORMANCE OF THE PURE QUICKSORT ON
C     PARTIALLY ORDERED DATA.
C     
C     CREATED:  15 JUL 1986  LEN MOSS
C     
C===================================================================
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER   N,INDEX(N)
      INTEGER      SORTDATA(N)
      
      INTEGER   LSTK(31),RSTK(31),ISTK
      INTEGER   L,R,I,J,P,INDEXP,INDEXT
      INTEGER      SORTDATAP
      
C     QUICKSORT CUTOFF
C     
C     QUIT QUICKSORT-ING WHEN A SUBSEQUENCE CONTAINS M OR FEWER
C     ELEMENTS AND FINISH OFF AT END WITH STRAIGHT INSERTION SORT.
C     ACCORDING TO KNUTH, V.3, THE OPTIMUM VALUE OF M IS AROUND 9.
      
      INTEGER   M
      PARAMETER (M=9)
      
C===================================================================
C     
C     MAKE INITIAL GUESS FOR INDEX
      
      DO 50 I=1,N
         INDEX(I)=I
 50         CONTINUE
      
C     IF ARRAY IS SHORT, SKIP QUICKSORT AND GO DIRECTLY TO
C     THE STRAIGHT INSERTION SORT.
      
      IF (N.LE.M) GOTO 900
      
C===================================================================
C     
C     QUICKSORT
C     
C     THE "QN:"S CORRESPOND ROUGHLY TO STEPS IN ALGORITHM Q,
C     KNUTH, V.3, PP.116-117, MODIFIED TO SELECT THE MEDIAN
C     OF THE FIRST, LAST, AND MIDDLE ELEMENTS AS THE "PIVOT
C     KEY" (IN KNUTH'S NOTATION, "K").  ALSO MODIFIED TO LEAVE
C     DATA IN PLACE AND PRODUCE AN INDEX ARRAY.  TO SIMPLIFY
C     COMMENTS, LET SORTDATA[I]=SORTDATA(INDEX(I)).
      
C     Q1: INITIALIZE
      ISTK=0
      L=1
      R=N
      
 200    CONTINUE
      
C     Q2: SORT THE SUBSEQUENCE SORTDATA[L]..SORTDATA[R].
C     
C     AT THIS POINT, SORTDATA[L] <= SORTDATA[M] <= SORTDATA[R] FOR ALL L < L,
C     R > R, AND L <= M <= R.  (FIRST TIME THROUGH, THERE IS NO
C     SORTDATA FOR L < L OR R > R.)
      
      I=L
      J=R
      
C     Q2.5: SELECT PIVOT KEY
C     
C     LET THE PIVOT, P, BE THE MIDPOINT OF THIS SUBSEQUENCE,
C     P=(L+R)/2; THEN REARRANGE INDEX(L), INDEX(P), AND INDEX(R)
C     SO THE CORRESPONDING SORTDATA VALUES ARE IN INCREASING ORDER.
C     THE PIVOT KEY, SORTDATAP, IS THEN SORTDATA[P].
      
      P=(L+R)/2
      INDEXP=INDEX(P)
      SORTDATAP=SORTDATA(INDEXP)
      
      IF (SORTDATA(INDEX(L)) .GT. SORTDATAP) THEN
         INDEX(P)=INDEX(L)
         INDEX(L)=INDEXP
         INDEXP=INDEX(P)
         SORTDATAP=SORTDATA(INDEXP)
      ENDIF
      
      IF (SORTDATAP .GT. SORTDATA(INDEX(R))) THEN
         IF (SORTDATA(INDEX(L)) .GT. SORTDATA(INDEX(R))) THEN
            INDEX(P)=INDEX(L)
            INDEX(L)=INDEX(R)
         ELSE
            INDEX(P)=INDEX(R)
         ENDIF
         INDEX(R)=INDEXP
         INDEXP=INDEX(P)
         SORTDATAP=SORTDATA(INDEXP)
      ENDIF
      
C     NOW WE SWAP VALUES BETWEEN THE RIGHT AND LEFT SIDES AND/OR
C     MOVE SORTDATAP UNTIL ALL SMALLER VALUES ARE ON THE LEFT AND ALL
C     LARGER VALUES ARE ON THE RIGHT.  NEITHER THE LEFT OR RIGHT
C     SIDE WILL BE INTERNALLY ORDERED YET; HOWEVER, SORTDATAP WILL BE
C     IN ITS FINAL POSITION.
      
 300    CONTINUE
      
C     Q3: SEARCH FOR DATUM ON LEFT >= SORTDATAP
C     
C     AT THIS POINT, SORTDATA[L] <= SORTDATAP.  WE CAN THEREFORE START SCANNING
C     UP FROM L, LOOKING FOR A VALUE >= SORTDATAP (THIS SCAN IS GUARANTEED
C     TO TERMINATE SINCE WE INITIALLY PLACED SORTDATAP NEAR THE MIDDLE OF
C     THE SUBSEQUENCE).
      
      I=I+1
      IF (SORTDATA(INDEX(I)).LT.SORTDATAP) GOTO 300
      
 400    CONTINUE
      
C     Q4: SEARCH FOR DATUM ON RIGHT <= SORTDATAP
C     
C     AT THIS POINT, SORTDATA[R] >= SORTDATAP.  WE CAN THEREFORE START SCANNING
C     DOWN FROM R, LOOKING FOR A VALUE <= SORTDATAP (THIS SCAN IS GUARANTEED
C     TO TERMINATE SINCE WE INITIALLY PLACED SORTDATAP NEAR THE MIDDLE OF
C     THE SUBSEQUENCE).
      
      J=J-1
      IF (SORTDATA(INDEX(J)).GT.SORTDATAP) GOTO 400
      
C     Q5: HAVE THE TWO SCANS COLLIDED?
      
      IF (I.LT.J) THEN
         
C     Q6: NO, INTERCHANGE SORTDATA[I] <--> SORTDATA[J] AND CONTINUE
         
         INDEXT=INDEX(I)
         INDEX(I)=INDEX(J)
         INDEX(J)=INDEXT
         GOTO 300
      ELSE
         
C     Q7: YES, SELECT NEXT SUBSEQUENCE TO SORT
C     
C     AT THIS POINT, I >= J AND SORTDATA[L] <= SORTDATA[I] == SORTDATAP <= SORTDATA[R],
C     FOR ALL L <= L < I AND J < R <= R.  IF BOTH SUBSEQUENCES ARE
C     MORE THAN M ELEMENTS LONG, PUSH THE LONGER ONE ON THE STACK AND
C     GO BACK TO QUICKSORT THE SHORTER; IF ONLY ONE IS MORE THAN M
C     ELEMENTS LONG, GO BACK AND QUICKSORT IT; OTHERWISE, POP A
C     SUBSEQUENCE OFF THE STACK AND QUICKSORT IT.
         
         IF (R-J .GE. I-L .AND. I-L .GT. M) THEN
            ISTK=ISTK+1
            LSTK(ISTK)=J+1
            RSTK(ISTK)=R
            R=I-1
         ELSE IF (I-L .GT. R-J .AND. R-J .GT. M) THEN
            ISTK=ISTK+1
            LSTK(ISTK)=L
            RSTK(ISTK)=I-1
            L=J+1
         ELSE IF (R-J .GT. M) THEN
            L=J+1
         ELSE IF (I-L .GT. M) THEN
            R=I-1
         ELSE
C     Q8: POP THE STACK, OR TERMINATE QUICKSORT IF EMPTY
            IF (ISTK.LT.1) GOTO 900
            L=LSTK(ISTK)
            R=RSTK(ISTK)
            ISTK=ISTK-1
         ENDIF
         GOTO 200
      ENDIF
      
 900    CONTINUE
      
C===================================================================
C     
C     Q9: STRAIGHT INSERTION SORT
      
      DO 950 I=2,N
         IF (SORTDATA(INDEX(I-1)) .GT. SORTDATA(INDEX(I))) THEN
            INDEXP=INDEX(I)
            SORTDATAP=SORTDATA(INDEXP)
            P=I-1
 920                CONTINUE
            INDEX(P+1) = INDEX(P)
            P=P-1
            IF (P.GT.0) THEN
               IF (SORTDATA(INDEX(P)).GT.SORTDATAP) GOTO 920
            ENDIF
            INDEX(P+1) = INDEXP
         ENDIF
 950       CONTINUE
      
C===================================================================
C     
C     ALL DONE
      
      END


C
C       ONE D LINEAR INTERPOLATION
C
        SUBROUTINE ONEDINTERP(X,Y,NDATA,XHAT,YHAT)
C        IMPLICIT DOUBLE PRECISION (A-H,O-Z)
         IMPLICIT NONE
C        Becuase I added implicit none JAM
         INTEGER NDATA, I
         REAL*8 X, Y, XHAT, YHAT, XL1, XL2

         DIMENSION X(NDATA),Y(NDATA)
         IF(DABS(XHAT).LE.1.D-8)THEN
                 YHAT=0
         ELSE

         CALL BINSEARCH(X,NDATA,XHAT,I)
         XL1 = (X(I+1) - XHAT)/(X(I+1) - X(I))
         XL2 = (XHAT - X(I))/(X(I+1) - X(I))
         YHAT = Y(I)*XL1 + Y(I+1)*XL2
         ENDIF

         END

         SUBROUTINE BINSEARCH(X,N,XHAT,IXFIND)
C         IMPLICIT DOUBLE PRECISION (A-H,O-Z) JAM
         IMPLICIT NONE
C        Becuase I added implicit none JAM
         INTEGER N, IXFIND, IA, IB, IM, IXHAT
         REAL*8 X, XHAT

         DIMENSION X(N)
C     BINSEARCH  BINARY SEARCH TO FIND INDEX I SUCH THAT X(I)<= XHAT <= X(I+1)
C
C SYNOPSIS:  I = BINSEARCH(X,XHAT)
C INPUT:     X    = VECTOR OF MONOTONIC DATA
C            XHAT = TEST VALUE
C
C OUTPUT:    I = INDEX IN X VECTOR SUCH THAT X(I)<= XHAT <= X(I+1)



      IF ((XHAT.LT.X(1)).OR.(XHAT.GT.X(N)))THEN
         WRITE(*,*)'VALUE OUT OF RANGE'
         STOP
      ENDIF

      IA = 1 
      IB = N
C       INITIALIZE LOWER AND UPPER LIMITS 
      DO WHILE (IB-IA.GT.1)
        IM = ((IA+IB)/2)
C  INTEGER VALUE OF MIDPOINT
        IXHAT=X(IM)/XHAT
        IF(IXHAT.LE.0)THEN
                IA=IM
        ELSE
                IB=IM
        ENDIF
      ENDDO
        IXFIND=IA
      END        
      




