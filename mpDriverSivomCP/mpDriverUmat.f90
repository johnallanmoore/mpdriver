       program mpDriver

       implicit none

       integer i, Ninc, fDrive, j

       integer ntens,nstatev, nprops, ndir, nshr
       integer kinc, kspt, kstep, layer, noel, npt
       
       real*8 coords(3), dfgrd0(3,3), dfgrd1(3,3)
       real*8 dpred(1), drot(3,3), predef(1), time(2)
       real*8 celent, dtime, pnewdt, dtemp, rpl
       real*8 scd, spd, sse, temp
       ! dummy values that may have different dimensions
       ! but that probably wont get used
       real*8 drpldt

       real*8 startTemp
       real*8 strainIncScalarX, strainIncScalarY, strainIncScalarZ
       real*8 totalStrainX, totalStrainY, totalStrainZ

       real*8 fMax(3,3), Emat(3,3), df(3,3)

       character*80 cmname

       real*8, dimension (:,:), allocatable :: ddsdde 
       real*8, dimension (:), allocatable :: ddsddt 
       real*8, dimension (:), allocatable :: drplde
       real*8, dimension (:), allocatable :: dstrain 
       real*8, dimension (:), allocatable :: props 
       real*8, dimension (:), allocatable :: statev 
       real*8, dimension (:), allocatable :: strain
       real*8, dimension (:), allocatable :: stress
 
       ! variables used in code but not in umat
       real*8, dimension (:), allocatable:: strainState

       ! coords,        !  coordinates of Gauss pt. being evaluated
       ! ddsdde,        ! Tangent Stiffness Matrix
       ! ddsddt,	! Change in stress per change in temperature
       ! dfgrd0,	! Deformation gradient at beginning of step
       ! dfgrd1,	! Deformation gradient at end of step
       ! dpred,	        ! Change in predefined state variables
       ! drplde,	! Change in heat generation per change in strain
       ! drot,	        ! Rotation matrix
       ! dstrain,	! Strain increment tensor stored in vector form
       ! predef,	! Predefined state vars dependent on field variables
       ! props,	        ! Material properties passed in
       ! statev,	! State Variables
       ! strain,	! Strain tensor stored in vector form
       ! stress,	! Cauchy stress tensor stored in vector form
       ! time		! Step Time and Total Time
       ! celent         ! Characteristic element length
       ! drpldt         ! Variation of RPL w.r.t temp.
       ! dtemp          ! Increment of temperature
       ! dtime          ! Increment of time
       ! kinc           !Increment number
       ! kspt           ! Section point # in current layer
       ! kstep          ! Step number
       ! layer          ! layer number
       ! noel           ! element number
       ! npt            ! Integration point number
       ! pnewdt         ! Ratio of suggested new time increment/time increment
       ! rpl            ! Volumetric heat generation
       ! scd            ! “creep” dissipation
       ! spd            ! plastic dissipation
       ! sse            ! elastic strain energy
       ! temp           ! temperature

!      Integer Inputs
       ndir = 3
       nshr = 3
       ntens = ndir + nshr
       nstatev = 300
       nprops = 27

       !drive load using F rather than strain
       fDrive = 0


! !     Dimension Reals
       allocate (ddsdde(ntens,ntens) )
       allocate (ddsddt(ntens) )
       allocate (drplde(ntens) )
       allocate (dstrain(ntens) )
       allocate (props(nprops) )
       allocate (statev(nstatev) )
       allocate (strain(ntens) )
       allocate (stress(ntens) )

       ! variables used in code but not in umat
       allocate (strainState(ntens))

       !User Inputs
       open(unit=2,file="results.dat",recl=204)
       !totalStrainX = 0.1D0
       !totalStrainY = -0.0D0
       !totalStrainZ = -0.0D0

       ! strain similar to element 2d
       !totalStrainX = 0.0385D0
       !totalStrainY = -0.0069D0
       !totalStrainZ = -0.0181D0

       ! strain to test stress drivin MP driver
       totalStrainX = 0.006*0.75
       totalStrainY = -0.3*totalStrainX*0.75
       totalStrainZ = -0.3*totalStrainX*.75

       if (fDrive.eq.1) then
          fMax(1,1) = 1.0374D0
          fMax(1,2) = 0.0029D0
          fMax(1,3) = -0.0067D0
          fMax(2,1) = 0.0184D0
          fMax(2,2) = 0.9931D0
          fMax(2,3) = 0.0061D0
          fMax(3,1) = 0.0220D0
          fMax(3,2) = -0.0008D0
          fMax(3,3) = 0.9817D0
          fMax = transpose(fMax)
       endif

       dtime = 0.001D0
       ! props(1:nprops) = (/12000.,  12000.,  12000.,  12000.,  12000.,  12000., 1.1e-05, 7.6e-06, &
       !     9.0,  0.1308,    230.,    275.,   130., 12000.,  12000.,  12000., &
       !     0.002,    0.02,    450.,   1000.,    900.,     0.125,    1.4,    275., &
       !     -136.520000000000, -106.340000000000, -90.3640000000000     /)

        !props(1:nprops) = (/12000.,  12000.,  12000.,  12000.,  12000.,  12000., 1.1e-05, 7.6e-06, &
        !    9.0,  0.1308,    230.,    275.,   130., 12000.,  12000.,  12000., &
        !    0.002,    0.02,    450.,   1000.,    900.,     0.125,    1.4,    275., &
        !    -159.16,-61.799,-136.0     /)

       props(1:nprops) = (/143000.0,107800.0,37400.0,71500.0,53900.0,18700.0, 1.1e-05, 6.6e-06, &
8.5,  0.1,    257.,    277.,    130., 143000.0,107800.0, 37400.0, &
0.002,    0.02,    210.,   500.,   900.,     0.125,     1.4,    277., &
            45.,0.,0.     /)

       cmname = "Material-1"
       Ninc = 10000
       coords(1:ndir) = (/0.0D0,0.0D0,0.0D0  /)
       !strainState(1:ntens) = (/1.D0, -0.0D0, -0.0D0, 0.D0, 0.D0, 0.D0/)
       strainIncScalarX = totalStrainX/Ninc
       strainIncScalarY = totalStrainY/Ninc
       strainIncScalarZ = totalStrainZ/Ninc
       dstrain(1:3) = (/strainIncScalarX, strainIncScalarY,strainIncScalarZ /)
       
       if (fDrive.eq.1) then
          do i = 1,3
             do j = 1,3
                if (i.eq.j) then
                   df(i,j) = (fMax(i,j)-1.D0)/Ninc 
                else
                   df(i,j) = fMax(i,j)/Ninc 
                endif
             enddo
          enddo
          
          
       endif


       ! initalize varibles
       kinc = 1
       kspt = 1
       kstep = 1
       layer = 1
       noel = 1
       npt = 1

       !intialize variables
       startTemp = 300.D0
       temp = startTemp
       stress(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       do i = 1, nstatev
          statev(i) =  0.D0
       end do
       ddsdde(:,:) = 0.D0
       sse = 0.D0
       spd = 0.D0
       scd = 0.D0
       rpl = 0.D0
       ddsddt(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       drplde(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       drpldt = 0.D0
       strain(1:ntens) = (/0.D0, 0.D0, 0.D0, 0.D0, 0.D0, 0.D0/)
       time = 0.D0
       dtemp = 0.D0
       dpred = 0.D0
       drot(:,:) = 0.D0
       pnewdt = 1.D0
       celent = 1.D0

       ! no stretch no shear
       dfgrd0(1,1:3) = (/1.D0, 0.D0, 0.D0/)
       dfgrd0(2,1:3) = (/0.D0, 1.D0, 0.D0/)
       dfgrd0(3,1:3) = (/0.D0, 0.D0, 1.D0/)

       dfgrd1 = dfgrd0
       
       do i = 1, Ninc
          kinc = i
          kstep = kinc

          !no shear deformtion gradient (approx)
          !dfgrd0(1,1:3) = (/strain(1) + 1.D0, 0.002D0, 0.D0/)
          !dfgrd0(2,1:3) = (/0.D0, strain(2) + 1.D0, 0.D0/)
          !dfgrd0(3,1:3) = (/0.D0, 0.D0, strain(3) + 1.D0/)

          if (fDrive.eq.1) then
             dfgrd0 = dfgrd1
             dfgrd1 = dfgrd0 + df
             eMat = matmul(transpose(dfgrd1),dfgrd1)
             eMat(1,1) = eMat(1,1) - 1.D0
             eMat(2,2) = eMat(2,2) - 1.D0
             eMat(3,3) = eMat(3,3) - 1.D0
             eMat = 0.5D0 * eMat
             strain(1) = eMat(1,1)
             strain(2) = eMat(2,2)
             strain(3) = emat(3,3)
             strain(4) = 2.D0*eMat(1,2)
             strain(5) = 2.D0*eMat(1,3)
             strain(6) = 2.D0*eMat(2,3)
             !print *, "emat ", emat
             !print *, "strain ", strain
             !print *,  "dfgrd1 ", dfgrd1 
          else
             dfgrd0(1,1:3) = (/strain(1) + 1.D0, 0.0D0, 0.D0/)
             dfgrd0(2,1:3) = (/0.D0, strain(2) + 1.D0, 0.D0/)
             dfgrd0(3,1:3) = (/0.D0, 0.D0, strain(3) + 1.D0/)
             
             strain(1:ntens) = strain(1:ntens) + dstrain(1:ntens)
          
             !dfgrd1(1,1:3) = (/strain(1) + 1.D0, 0.002D0, 0.D0/)
             !dfgrd1(2,1:3) = (/0.D0, strain(2) + 1.D0, 0.D0/)
             !dfgrd1(3,1:3) = (/0.D0, 0.D0, strain(3) + 1.D0/)

             dfgrd1(1,1:3) = (/strain(1) + 1.D0, 0.0D0, 0.D0/)
             dfgrd1(2,1:3) = (/0.D0, strain(2) + 1.D0, 0.D0/)
             dfgrd1(3,1:3) = (/0.D0, 0.D0, strain(3) + 1.D0/)
          endif

          !print *, statev(1)
          call umat( &
               stress,  statev,  ddsdde,  sse,     spd, &
               scd,     rpl,     ddsddt,  drplde,  drpldt, &
               strain,  dstrain, time,    dtime,   temp, &
               dtemp,   predef,  dpred,   cmname,  ndir, &
               nshr,    ntens,   nstatev,  props,   nprops, &
               coords,  drot,    pnewdt,  celent,  dfgrd0, &
               dfgrd1,  noel,    npt,     layer,   kspt, &
               kstep,   kinc )

          time  = time + dtime
          write(2,"(E,$)") time(1)
          write(2,"(E,$)") strain(1), strain(2), strain(3)
          write(2,"(E,$)") strain(4), strain(5), strain(6)
          write(2,"(E,$)") stress(1), stress(2), stress(3)
          write(2,"(E,$)") stress(4), stress(5), stress(6)
          write(2,"(E,$)") statev(202), statev(300), statev(134)
          write(2,"(E,$)") statev(299), statev(298)
          write(2,"(E)"  )  
          !print *, time
          !print *, strain
          !print *, stress
          !print *, "statev(79)", statev(79)
       end do ! end strain increment loop  

       end program mpDriver



