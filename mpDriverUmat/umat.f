C                                          _______
C                 |    |   |\  /|    /\       |
C                 |    |   | \/ |   /__\      |
C                 |____|   |    |  /    \     |
C
C  Updates stresses, Jacobin, state, and energy for 3D solid elements ONLY
C
C  
C     SIMPLE LINEAR ELASTIC ISOTROPIC MATERIAL, SMALL STRAIN
C  
C  ============================================================================
      subroutine umat (stress,  statev,  ddsdde,  sse,     spd,
     &			scd,     rpl,     ddsddt,  drplde,  drpldt,
     &			strain,  dstrain, time,    dtime,   temp,
     &			dtemp,   predef,  dpred,   cmname,  ndi,
     &			nshr,    ntens,   nstatv,  props,   nprops,
     &			coords,  drot,    pnewdt,  celent,  dfgrd0,
     &			dfgrd1,  noel,    npt,     layer,   kspt,
     &			kstep,   kinc )
C
C      include 'vaba_param.inc'
C

      implicit none

      CHARACTER*80 CMNAME

      integer ntens,nstatv, nprops, ndi, nshr

      real*8 STRESS,STATEV,DDSDDE,SSE,SPD,SCD
      real*8 RPL,DDSDDT,DRPLDE,DRPLDT
      real*8 STRAIN,DSTRAIN,TIME,DTIME,TEMP,DTEMP,PREDEF,DPRED
      real*8 PROPS,COORDS,DROT,PNEWDT
      real*8 CELENT,DFGRD0,DFGRD1,NOEL,NPT,LAYER,KSPT,KSTEP,KINC
      real*8 dStress

      DIMENSION STRESS(NTENS),STATEV(NSTATV),
     1 DDSDDE(NTENS,NTENS),DDSDDT(NTENS),DRPLDE(NTENS),
     2 STRAIN(NTENS),DSTRAIN(NTENS),TIME(2),PREDEF(1),DPRED(1),
     3 PROPS(NPROPS),COORDS(3),DROT(3,3),DFGRD0(3,3),DFGRD1(3,3),
     4 dstress(NTENS)
      real*8 E,nu, lame1, G, C11, C12, C44

      E = props(1)
      nu = props(2)

      lame1 = E*nu/((1.D0+nu)*(1.D0-2.D0*nu)) ! lame1
      G     = E/(2.D0*(1.D0+nu)) ! lame2

      C11 = lame1 + 2.D0*G
      C12 = lame1
      C44 = G

      ddsdde(1,1) = C11 
      ddsdde(1,2) = C12  
      ddsdde(1,3) = C12
      ddsdde(1,4) = 0.D0  
      ddsdde(1,5) = 0.D0 
      ddsdde(1,6) = 0.D0

      ddsdde(2,2) = C11 
      ddsdde(2,3) = C12
      ddsdde(2,4) = 0.D0 
      ddsdde(2,5) = 0.D0
      ddsdde(2,6) = 0.D0
   
      ddsdde(3,3) = C11
      ddsdde(3,4) = 0.D0  
      ddsdde(3,5) = 0.D0
      ddsdde(3,6) = 0.D0
 
      ddsdde(4,4) = C44  
      ddsdde(4,5) = 0.D0
      ddsdde(4,6) = 0.D0
   
      ddsdde(5,5) = C44
      ddsdde(5,6) = 0.D0

      ddsdde(6,6) = C44

!     symmetry
      ddsdde(2,1) = ddsdde(1,2)  
      ddsdde(3,1) = ddsdde(1,3)
      ddsdde(4,1) = ddsdde(1,4)
      ddsdde(5,1) = ddsdde(1,5)
      ddsdde(6,1) = ddsdde(1,6)

      ddsdde(3,2) = ddsdde(2,3)
      ddsdde(4,2) = ddsdde(2,4)
      ddsdde(5,2) = ddsdde(2,5)
      ddsdde(6,2) = ddsdde(2,6)
   
      ddsdde(4,3) = ddsdde(3,4)  
      ddsdde(5,3) = ddsdde(3,5)
      ddsdde(6,3) = ddsdde(3,6)

      ddsdde(5,4) = ddsdde(4,5)
      ddsdde(6,4) = ddsdde(4,6)
   
      ddsdde(5,6) = ddsdde(6,5)
      
      dStress = matmul(ddsdde,dstrain) 

      stress(1:ntens) = stress(1:ntens) + dStress(1:ntens)

      return
      end
